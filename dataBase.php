<?php


// MODEL

class MainModel /*extends mysqli*/ {
    private $pdo;

    function __construct(){
        include('__databaseLogin.php');
    }

    function getRoom($startDate,$endDate){
      $prep = $this->pdo->prepare("
      SELECT ro.num, ro.nb_pers from Room ro
      WHERE NOT EXISTS (
          select ro_re.room_id from Room_Reservation ro_re
          JOIN Reservation Re ON Re.id = ro_re.reservation_id AND :startDate < Re.d_checkout
              AND :endDate >= Re.d_checkin AND confirmed != 0 AND (Re.paid = true OR Re.d_create > :maxDateToPaid)
          where ro_re.room_id = ro.num
          limit 1
      )");

      $nbWeekBeforeDelete = 1;
      $prep->execute([
        'endDate' => $endDate,
        'startDate' => $startDate,
        'maxDateToPaid' => (new \DateTime)->modify("- $nbWeekBeforeDelete week")->format('Y-m-d')
      ]);
      $rooms = $prep->fetchAll();
      return $rooms;
    }

    function book($idsChambre,$startDate,$endDate,$email,$tel,$color,$name,$price,$nbPers,$message,$isConnect){
      $prep = $this->pdo->prepare("
        INSERT INTO Reservation(client_name,d_checkin,d_checkout,paid,mail,phone,confirmed,color_code,price,d_create,nbPers,message)
        VALUES (:name,:startDate,:endDate,:paid,:email,:tel,:confirmed,:color,:price,:d_create,:nbPers,:message)
      ");
      $prep->execute([
        'name' => $name,
        'startDate' => $startDate,
        'endDate' => $endDate,
        'email' => $email,
        'd_create' =>(new \DateTime)->format('Y-m-d'),
        'tel' => $tel,
        'color' => $color,
        'price' => $price,
        'nbPers' => $nbPers,
        'confirmed' => $isConnect ? 1 : -1,
        "message" => $message,
        'paid' => $isConnect ? 1 : 0
      ]);

      $reservation_id = $this->pdo->lastInsertId();
      $this->addRoomsToReservation($reservation_id,$idsChambre);
    }


    function addRoomsToReservation($reservation_id,$idsChambre){
        forEach($idsChambre as $idChambre){
              $prep2 = $this->pdo->prepare("
                INSERT INTO Room_Reservation(reservation_id,room_id)
                VALUES (:reservation_id,:idChambre)
              ");
              $prep2->execute([
                'reservation_id' => $reservation_id,
                'idChambre' => $idChambre
              ]);
        }
    }

    public function arrayToString($type,$array){
        $in = '(';
        if($type == 'INT'){
            for($i = count($array);$i--;){
                $in .= +$array[$i];//+ is against sql injection to convert to int
                if($i){
                    $in.= ',';
                }
            }
        }
        $in .= ')';
        return $in;
    }
      /**
      * Si pas de paid après une semaine on ne tient plus compte de la réservation
      */
    function getBooking($mode,$params = []){
        $cond = [];
        if(in_array($mode,['notConfirmed','week'])){
            switch ($mode) {
                case 'notConfirmed':
                    $cond[] = 're.confirmed = -1';
                    break;
                case 'week':
                    $nbWeekBeforeDelete = 1;
                    $cond[] = 're.confirmed = 1';
                    $cond[] = 're.d_checkin < :endDate AND re.d_checkout > :startDate';
                    $cond[] = 'paid = true OR re.d_create > :maxDateToPaid';
                    $params['maxDateToPaid'] = (new \DateTime)
                            ->modify("- $nbWeekBeforeDelete week")->format('Y-m-d');
            }
        }
        if($params['room_ids']??0){
            $cond[] = 'ro_re.room_id in '.$this->arrayToString('INT',$params['room_ids']);
            unset($params['room_ids']);
        }
        if($params['noThisRes']??0){
            $cond[] = 're.id != :noThisRes';
        }

        if(count($cond)){
            $condStr = 'where ('.join(') AND (',$cond).')';
        }else{
            $condStr = '';
        }
        $sql = "
            SELECT
                re.id as resId,re.price, re.client_name, re.d_checkin, re.d_checkout, re.mail, re.color_code,re.paid,re.nbPers,re.confirmed,re.message,re.note,
                GROUP_CONCAT(ro_re.room_id SEPARATOR ',') AS room_ids
            FROM
                Reservation re
                INNER JOIN Room_Reservation ro_re ON ro_re.reservation_id = re.id
            $condStr
            GROUP BY re.id
            ORDER BY re.d_checkin,re.price,re.d_checkout
        ";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        // `room_ids` column will contain a string like "1,2,3,9"
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    function updateReservation($o){
        //mettre à jour la réservation
        if($o['confirmed'] == 1){//verifions que il y=reste de la place
            $res = $this->getBooking('week',[
                'startDate' => $o['d_checkin'],
                'endDate'   => $o['d_checkout'],
                'room_ids'  => $o['room_ids'],
                'noThisRes' => $o['resId']
            ]);
            if(count($res) > 0){
                $sDate = $res[0]['d_checkin'];
                $eDate = $res[0]['d_checkout'];
                $name = $res[0]['client_name'];
                $this->lastError = "la réservation numéro ".$res[0]['resId']." du $sDate au $eDate au nom de $name empêche de valider la réservation.";
                return false;
            }
        }

        $stmt = $this->pdo->prepare("UPDATE Reservation Re
            SET
                client_name =  :client_name,
                mail        =  :mail,
                d_checkin   =  :d_checkin,
                d_checkout  =  :d_checkout,
                price       =  :price,
                confirmed   =  :confirmed,
                paid        = :paid,
                nbPers     = :nbPers,
                color_code  = :color_code,
                note  = :note
            WHERE id  =  :resId
        ");
        $paid = $o['paid'] == 1 ? 1 : 0;
        $stmt->execute([
            'client_name' =>  $o['client_name'],
            'mail'        =>  $o['mail'],
            'paid'        =>  $paid,
            'confirmed'   =>  $o['confirmed'],/* -1 undefined, 1 confirmed, 0 refused*/
            'd_checkin'   =>  $o['d_checkin'],
            'd_checkout'  =>  $o['d_checkout'],
            'price'       =>  $o['price'],
            'resId'       =>  $o['resId'],
            'color_code'  =>  $o['color_code'],
            'nbPers'      =>  $o['nbPers'],
            'note'      =>  $o['note']
        ]);
        // supprime les chambres associées
        $this->removeRoomReservation($o);
        //ajouter les nouvelles chambres associées
        $this->addRoomsToReservation($o['resId'],$o['room_ids']);
        return true;
    }

    function removeRoomReservation($o){
        $stmt = $this->pdo->prepare("DELETE FROM Room_Reservation WHERE reservation_id =  :resId");
        $stmt->execute(['resId' => $o['resId']]);
    }

/*    function removeReservation($o){
        $stmt = $this->pdo->prepare("DELETE FROM Reservation WHERE id =  :resId");
        $stmt->execute(['resId' => $o['resId']]);
        $this->removeRoomReservation($o);
    }*/
}
