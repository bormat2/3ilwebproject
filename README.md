Ce site a pour but de faire la promotion du motel a vigna de Corte.
il doit aussi permettre de: 
	- faire une réservation, 
	- de localiser le motel sur une carte 
	- prendre en compte la visite de touriste étrangé en étant multilangue.

Contraite graphique:
	- couleur de la vigne
	- stay simple, pas d'animations épileptiques.

Contraite de navigations:
	- la navigation doit être fluide et en ajax, la page n'est pas affiché tant que le DOM n'est pas totalement chargé pour éviter les effets visuels de sauts (on affiche quand même avant de recevoir toutes les images pour ne pas retarder le chargement).
	- Le zoom ne doit pas être bloqué sur mobile comme le font la plupart des sites
	- quand une page est chargé en ajax, l'url doit correspondre au nouveau contenu
	- la langue doit être dans l'url
	- le site doit pouvoir être référencé:
		- le contenu ne doit pas être inséré en js (pas que) mais être en clair dans le html, la traduction se fait donc en php la première fois et dynamiquement en cliquant sur une langue car une insertion seulement en js serait mal référencé.
		- les liens doivent être des vrais lien même si chargé en ajax balise <a href></a>
		- on doit pouvoir faire un clic molete sur ces liens pour ouvrir dans un nouvel onglet.
		- l'url doit être propre pas de paramètre get '?' et '&' elle ne doit pas non plus afficher l'extension php des pages.

	- l'apparition de la scrollbar ne doit pas décaler le menu ou le footer
	- la fenêtre doit être remimensionalble sans faire apparaitre de scroll horizontal jusqu') 320px
	- la version PC doit être accessible par un simple clic sur mobile
	- le scroll ne doit pas être intercepté par un élément en particulier la map de google map.
	- utiliser le plus possible les protocoles pour application externe, "mailto" (mail) et "tel:" pour lancer un appel sur mobile

Non contraite:
	- le site n'a pas besoin d'être accessible sans js car trop contraignant à faire
	- le site n'a pas besoins de passer le test w3c Validator mais le tester quand même sur w3c validator reste interessant pour éviter de futur incompatibilités.
		on ignorera les contraintes w3c suivantes car sans conséquences et bien supporté :
			- l'attribut alt pour une image est obligatoire
			- mettre une balise style dans le body est mal

Contrainte de programmation:
	- les attributs personalisés doivent contenir un tiret dans leur nom pour éviter de futurs incompatibilités, si besoin d'acceder à la valeur data-{theName} permet d'y accédé via la propriété dataset
	- éviter les unité vw (car la version PC doit être accessible par un simple clic sur mobile)
	- no jQuery (car on peut s'en passer et ça pèse lourd)
	- pas d'animation en js seulement en css3
	- aucun setTimeout ne doit tourner en permanence en tache de fond, se baser sur les évenements à la place
	- les scripts doivent avoir un type spécial type="text/whenDocumentReady" qui empêche le script de s'executer directement, car on gère cela manuellement l'execution des script à la fin du chargement de la page, cela permet d'être sûr que l'on a le même comportement en ajax et en non ajax et de rajouter un comportement spécial qui est:
		Les balises scripts ont leurs variables indépendantes (IIFE protection) sauf si on utilise explicitement window.nomvariable = valeur pour éviter la polution invonlolontaire des variables globales.

		attribut de script possible:
			data-name: 
				si un nom est donné à un script alors au prochain chargement en ajax d'une page ayant un script de même nom, ce script ne sera pas executé (permet d'empecher l'ajout multiple de listener sur un même element dom)
			data-src:
				url d'un script à charger, à éviter si possible car on ferais une requête ajax dans une requete ajax et cela est mauvais pour les perfs.
				il existe par contre des solutions de préchargement des script voir "prefetch" qui peuvent du coup justifier l'utilisation de data-src.

	- Les listener ne doivent pas être ajouté via onclick ou addEventListener sur l'élement mais grace à une fonction interne $.on('eventName','selector',callback) car cela permet la délégation d'évenement qui fait qu'on peut cibler plusieurs élements avec un seul listener.
	Deplus l'insertion de nouveaux elements DOM ne nécesite pas l'ajout d'un nouveau listener.
	Meilleurs perfs + plus simple à utiliser.
	De plus, de base l'évenement n'est pas propagé au parent si ce n'est pas explicitement écrit avec un return true.

Choix de conception:
	- Toutes pages du dossier public est chargé par une page appelante "index.php" ou "ajax.php"
		cela permet de traiter différemment une requete ajax ou un chargement direct de la page
		en effet on a pas besoin du header en ajax par exemple car il est déjà chargé.
	- la connection se fait sans BDD avec un json car on a qu'un seul utilisateur et l'accès à un fichier est plus rapide qu'à une base
	- la connection doit être permanente par cookie.
	- éviter les CDN pour être indépendant

Choix technique:
	ajax,
	scss (simplifie,factorise beaucoup l'écriture du css tout en restant du css)
	flexbox pour le css
	Privilégier pour les images: Les fonts, les svg, png, jpeg progressif.

Navigateurs supportés:
	chrome(PC/android):	oui
	firefox: oui
	safaris(max/ipad/iphone): oui
	edge: oui

	IE 11: oui en cours malgrès des différences grahique
	IE 10: necessite quelque modification de propriétés flex

	IE9: Peut être jamais car ne supporte pas FormData et les flexbox et plein d'autre chose
		il faut des polyfills

Contrainte 3IL:
	carousel(avec légende)
	html5,css,interaction dom, redimensionnement, codage,fonctionnement.



---------------------

centos 7

wget https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm
yum localinstall mysql57-community-release-el7-11.noarch.rpm
yum repolist enabled | grep "mysql.*-community.*"
yum install mysql-community-server
service mysqld start
obtenir le mdp
 grep 'temporary password' /var/log/mysqld.log

/fdW2bzHgndw


-------
ubuntu

apt-get remove --purge mysql-client
apt-get --reinstall install bsdutils

wget https://downloads.mysql.com/archives/get/file/mysql-community-server_5.7.21-1ubuntu14.04_amd64.deb
wget https://downloads.mysql.com/archives/get/file/mysql-client_5.7.21-1ubuntu14.04_amd64.deb
wget https://downloads.mysql.com/archives/get/file/mysql-server_5.7.21-1ubuntu14.04_amd64.deb 



dpkg -i mysql-client_5.7.21-1ubuntu14.04_amd64.deb 
dpkg -i mysql-community-server_5.7.21-1ubuntu14.04_amd64.deb 
dpkg -i mysql-server_5.7.21-1ubuntu14.04_amd64.deb 

-----------------

mysql -p -u root
Attention pour la connexion à mettre le port et 127.0.0.1 à la place de localhost

SET PASSWORD FOR 'root'@'localhost' = PASSWORD('toto');
I4wfxm@vh

SET PASSWORD FOR 'root'@'localhost' = PASSWORD('z}z{;u.eW^ujZ8A}');
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('I4wfxm@vh');

I4wfxm@vh

SHOW GLOBAL VARIABLES LIKE 'PORT';


CREATE DATABASE motelavigna;
use motelavigna
CREATE TABLE Reservation
(
	client_name char(50),
	id int NOT NULL AUTO_INCREMENT,
	d_create char(11),
	d_checkin char(11),
	d_checkout char(11),
	paid boolean,
	comment TEXT ,
	mail char(50),
	phone char(50),
	confirmed boolean,
  	color_code char(7),
  	price int,
	PRIMARY KEY(id),
	nbPers int,
	alreadyPaid char(11),
	message varchar(10000)
);

CREATE TABLE Room
(
	num int,
	surface int,
	nb_pers int,
	PRIMARY KEY (num)
);

Create Table Room_Reservation(
	reservation_id int,
	room_id int,
	PRIMARY KEY(reservation_id,room_id)
);

INSERT INTO Room (num, surface,nb_pers) VALUES (16, 0, 4);
INSERT INTO Room (num, surface,nb_pers) VALUES (5, 0, 3);
INSERT INTO Room (num, surface,nb_pers) VALUES (6, 0, 3);
INSERT INTO Room (num, surface,nb_pers) VALUES (7, 0, 3);
INSERT INTO Room (num, surface,nb_pers) VALUES (8, 0, 3);
INSERT INTO Room (num, surface,nb_pers) VALUES (13, 0, 3);
INSERT INTO Room (num, surface,nb_pers) VALUES (14, 0, 2);
INSERT INTO Room (num, surface,nb_pers) VALUES (15, 0, 2);
INSERT INTO Room (num, surface,nb_pers) VALUES (18, 0, 2);


alter Table Reservation
add note TEXT(10000);


-------

restorer une base 

mysql -u root -p motelavigna < motelavigna_20190410.sql

sauvegarder une base

mysqldump -u root -p motelavigna > motelavigna_20190410.sql

mysqldump -u root -p motelavigna > motelavigna_20201230.sql

----


------
yum install epel-release yum-utils
yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
yum-config-manager --enable remi-php73
yum install php php-common php-opcache php-mcrypt php-cli php-gd php-curl php-mysqlnd
php -v

cd /var/www/html

yum install git
git clone https://gitlab.com/bormat2/3ilwebproject.git



apache

httpd.conf :

ajouter
AllowOverride All 
Require all granted
pour le dossier contenant le site

