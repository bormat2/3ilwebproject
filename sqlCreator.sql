DROP TABLE Room_Reservation;
DROP TABLE Room;
DROP TABLE Reservation;

CREATE TABLE Reservation
(
	client_name char(50),
	id int NOT NULL AUTO_INCREMENT,
	d_create char(11),
	d_checkin char(11),
	d_checkout char(11),
	paid boolean,
	comment TEXT ,
	mail char(50),
	phone char(50),
	confirmed boolean,
  	color_code char(7),
  	price int,
	PRIMARY KEY(id),
	nbPers int,
	alreadyPaid char(11),
	message varchar(10000)
);

CREATE TABLE Room
(
	num int,
	surface int,
	nb_pers int,
	PRIMARY KEY (num)
);

Create Table Room_Reservation(
	reservation_id int,
	room_id int,
	PRIMARY KEY(reservation_id,room_id)
);

INSERT INTO Room (num, surface,nb_pers) VALUES (16, 0, 4);
INSERT INTO Room (num, surface,nb_pers) VALUES (5, 0, 3);
INSERT INTO Room (num, surface,nb_pers) VALUES (6, 0, 3);
INSERT INTO Room (num, surface,nb_pers) VALUES (7, 0, 3);
INSERT INTO Room (num, surface,nb_pers) VALUES (8, 0, 3);
INSERT INTO Room (num, surface,nb_pers) VALUES (13, 0, 3);
INSERT INTO Room (num, surface,nb_pers) VALUES (14, 0, 2);
INSERT INTO Room (num, surface,nb_pers) VALUES (15, 0, 2);
INSERT INTO Room (num, surface,nb_pers) VALUES (18, 0, 2);

alter Table Reservation
add note varchar(10000)