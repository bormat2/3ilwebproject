<header class="flex-start f-container column fill">
<div class="widthContent f-container wrap fill" id="top-bar">
	<div class="f-container contact-infos">
		<div class="li">
			<address class="f-container not-grow">
				<a target="_blank" class="noLink" href="https://www.google.fr/maps/dir//Motel+a+Vigna,+20250+Corte/@42.3173289,9.0788402,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x12d0ad097a63f531:0xe11168c028a55cb4!2m2!1d9.1488804!2d42.3172233">
					<i class="fa fa-map-marker" aria-hidden="true"></i>
					<span class="no-mobile">Chemin de Saint-Pancrace,&nbsp;</span>
					<span class="inlineB no-mobile">20250 CORTE</span>
				</a>
			</address>
		</div>
		<div class="li notFlex">
			<a target="_blank" href="tel:+33677876874" class="notFlex noLink f-container not-grow">
				<i class="fa fa-phone notFlex" aria-hidden="true"></i>
				<span class="no-mobile notFlex">+33 (0)6 77 87 68 74</span>
			</a>
		</div>
	</div>

	<div class="rightPart f-container">

		<?php if($GLOBALS['user']->isConnect()){?>
			<li class="f-container">
				<a href="admin">
					<i class="fa fa-user-circle-o" aria-hidden="true"></i>
					<span class="no-mobile"><?= $GLOBALS['trad']('menu-admin'); ?></span>
				</a>
			</li class="f-container">
			<li class="f-container">
				<a href="validBooking">
					<i class="fa fa-user-circle-o" aria-hidden="true"></i>
					<span class="no-mobile"><?= $GLOBALS['trad']('menu-validBooking'); ?></span>
				</a>
			</li>
			<li class="f-container">
				<a href="allBooking">
					<i class="fa fa-user-circle-o" aria-hidden="true"></i>
					<span class="no-mobile"><?= $GLOBALS['trad']('menu-allBooking'); ?></span>
				</a>
			</li>
			<li class="f-container">
				<a href="weekBooking">
					<i class="fa fa-user-circle-o" aria-hidden="true"></i>
					<span class="no-mobile"><?= $GLOBALS['trad']('menu-weekBooking'); ?></span>
				</a>
			</li>
		<?php } ?>

		<li class="f-container"><?php
			if($GLOBALS['user']->isConnect()){?>
				<a href="setPassword">
					<i class="fa fa-key" aria-hidden="true"></i>
					<span class="no-mobile"><?= $GLOBALS['trad']('setPassword'); ?></span>
				</a>
		</li>
		<li class="f-container">
			<a href="deconnect">
				<i class="fa fa-sign-out" aria-hidden="true"></i>
				<span class="no-mobile"><?= $GLOBALS['trad']('deconnect'); ?></span>
			</a>
			<?php }else{?>
				<a href="login">
					<i class="fa fa-sign-in" aria-hidden="true"></i>
					<span class="no-mobile"><?= $GLOBALS['trad']('log-in') ?></span>
				</a>
			<?php } ?>
		</li>
		<span class="pcToPhoneParent f-item notFlex">
			<style>
				#pcToPhone .fa.fa-mobile{
							font-size: 30px;
							margin-right: -5px;
				}
				body.pc .fa.fa-desktop{
					display: none;
				}
				body.phone .fa.fa-mobile,body.ipad .fa.fa-mobile{
					display: none;
				}
			</style>
				<span id="pcToPhone" class="pointer f-container">
					<i class="fa fa-desktop" aria-hidden="true"></i>
					<i class="fa fa-mobile" aria-hidden="true"></i>
				</span>
		</span>
			<span id="langChoice" class="f-container f-item flex-start">
				<a class="f-container f-item" lang="fr"  href="<?= $_GET['path'].'/fr/'.$_GET['page'] ?>" >
					FR
				</a>
				<a class="f-container f-item" lang="en" href="<?= $_GET['path'].'/en/'.$_GET['page'] ?>" >
					EN
				</a>
				<a class="f-container f-item" lang="co" href="<?= $_GET['path'].'/en/'.$_GET['page'] ?>">
					CO
				</a>
			</span>

			<span class="hide f-container pointer">
				<i class="fa fa-refresh reload" aria-hidden="true"></i>
			</span>
	</div>

</div>
<div class=" fill" id="page-header">
<div id="page-header-intern" class="f-container wrap widthContent">
	<div class="f-item logoContainer">
		<img class="f-item" src="../img/logo.svg"/>
		<div lineTitle class="f-item" id="page-title">
			<div class="f-container wrap"><span class ="lineTitle">motel&nbsp;&nbsp;&nbsp;</span><span class ="lineTitle">a&nbsp;&nbsp;&nbsp;vigna</span></div>
		</div>
	</div>
	<nav class="f-container">
		<ul class="f-container wrap">
			<li><a href="home">
				<i class="fa fa-home" aria-hidden="true"></i>
				<?= $GLOBALS['trad']('menu-home'); ?>
			</a> </li>
			<li>
				<a href="book"><i class="fa fa-bed" aria-hidden="true"></i>
				<?= $GLOBALS['trad']('menu-book'); ?>
			</a></li>
			<li>
				<a href="activity"><i class="fa fa-cc-discover" aria-hidden="true"></i>
				<?= $GLOBALS['trad']('menu-discover'); ?>
			</a></li>
		</ul>
	</nav>
</div>
</div>
</header>
