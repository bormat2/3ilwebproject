<?php include('phpFunc.php');?>
<!DOCTYPE html>
<html lang="<?=$lang?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="<?= $_GET['path'].'/'.$lang ?>/" >
 	<link rel="stylesheet" type="text/css" href="../css/index.css">
  	<link rel="stylesheet" type="text/css" href="../fonts/fonts.css">
	<link rel="icon" type="image/png" href="../img/favicon.png" />
	<script>var $_GET = <?= json_encode($_GET) ?> </script>
	<title><?= $pageCode ?></title>
	<script src="../js/index.js"></script>
	<script src="../noIndex/dynamicJS.php"></script>
	<script src="../js/utils.js"></script>
	<script src="../js/navigationUrlHistory.js"></script>
	<script src="../js/domManipulation.js"></script>
	<script src="../js/domEvent.js"></script>
	<script src="../js/library/moment.min.js"></script>
	<script type="text/javascript">
		// si il y a une erreur php il faudra 2seconde avant de l'afficher
		setTimeout(function(){
			$id('fakeBody').classList.remove('hide')
		},2000)
	</script>
</head>

<body class=""  data-current-page="<?= $pageCode ?>">
	<div id="tripPlug" class="hide">
		<!-- trip advisor plugin caché jusqu'a déplacement dans la page d'acceuil-->
			<div id="TA_selfserveprop111" class="TA_selfserveprop">
				<ul id="zJc7Tx1Z1" class="TA_links DASjhGW">
				<li id="i18gUfkEo" class="xxCo9dkGOL2">
				<a target="_blank" href="https://www.tripadvisor.fr/"><img src="https://www.tripadvisor.fr/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
				</li>
				</ul>
			</div>

		<div id="TA_percentRecommended10" class="TA_percentRecommended">
			<ul id="ZiVHTRt" class="TA_links OquSoOH">
			<li id="UkGCSKSFIId9" class="7pbiFndy4PU">
			<a target="_blank" href="https://www.tripadvisor.fr/"><img src="https://www.tripadvisor.fr/img2/widget/logo_shadow_109x26.png" alt="TripAdvisor" class="widPERIMG" id="CDSWIDPERLOGO"/></a>
			</li>
			</ul>
		</div>
	</div>
	<script src="https://www.jscache.com/wejs?wtype=percentRecommended&amp;uniq=10&amp;locationId=1903943&amp;border=true&amp;display_version=2"></script>
	<script src="https://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=111&amp;locationId=1903943&amp;rating=true&amp;nreviews=5&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=true&amp;display_version=2"></script>
	<script>

	</script>

<div id="fakeBody" class="hide fakeBody">
	<div id="header-and-content" class="f-container column">
	<?php include('pageHeader.php') ?>

	<div id="bodyContent" class="justify-start f-container fill" >
		<div id="ajaxContent" class="f-container">
			<div class="page active loadByPHP f-container fill column" id="<?= $pageCode ?>">
				<?php
				//$user->setPasswordConnection('tt');
				if(file_exists('public/'.$page)){
					require('public/'.$page);
				}else{
					require('public/'.'404.php');
				}?>
			</div>
		</div>
		<div id="scriptExecutor">
<!-- 			<script src="https://use.fontawesome.com/698aa4e2c2.js"></script>
 -->		</div>

		<script>
			$.initScript()
		</script>
	</div>
	</div>
	<footer auto-height class="flex-start f-container column">
		<div class="footerDiv f-container wrap ul containerPaddingV2">
			<div class="li">
				<address>
					<a
						target="_blank"
						class="noLink"
						href="https://www.google.fr/maps/dir//Motel+a+Vigna,+20250+Corte/@42.3173289,9.0788402,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x12d0ad097a63f531:0xe11168c028a55cb4!2m2!1d9.1488804!2d42.3172233"
					>
						<span auto-height class="f-container wrap maxWidthContentV2">
							<span><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;Motel A Vigna,&nbsp;</span>
							<span>Chemin de Saint-Pancrace,&nbsp;</span>
							<span>20250 CORTE</span>
						</span>
					</a>
				</address>
			</div>
			<div class="li">
				<a target="_blank" href="tel:+33495460219" class="noLink">
					<i class="fa fa-phone" aria-hidden="true"></i>&nbsp;
					 +33 (0)4 95 46 02 19
				</a>
			</div>
			<div class="li">
				<a target="_blank" href="mailto:tcampana@wanadoo.fr" class="noLink">
					<i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
					 tcampana@wanadoo.fr
				 </a>
			 </div>
 			<div class="li f-container wrap maxWidthContent">
				<i class="fa fa-copyright" aria-hidden="true"></i>
				&nbsp;Designed By
				<a target="_blank" href="mailto:rafael.horvat@icloud.com" class="noLink">
					&nbsp;Horvat Rafael
				</a>
				<a target="_blank" href="mailto:bormat2@gmail.com" class="noLink">
					&nbsp;&amp;&nbsp;Bortolaso Mathieu
				</a>
			</div>
		</div>
	</footer>
</div>
<script>
	$.endPage()
</script>

</body>
</html>
