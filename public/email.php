<?php
	$param =  [
		'checkin' 		=> '2017-01-01',
		'checkout' 		=> '2017-01-01',
		'pricePart1' 	=> '35',
		'totalPrice'    => '50',
		'diffPrice'     => '15',
		'nbPers' 		=> 4,
		'nbStudio'		=> 5
	];
	
	$param['url'] = $_GET['path'].'/emailViewer?token='.base64_encode(json_encode($param));
	
	sendMail([
		'title' => 'test',
		'to' => 'bormat2@gmail.com',
		'hmtlMode' => true,
		'templateUrl' => '/noIndex/emailConf1/',
		'vars' => $param
	]);
	
?>
