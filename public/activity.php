<div class="f-container column page-content left fill dscvr">
	<h1><?= $trad('activityH1')?></h1>
	<br>
	<h2><?= $trad('actiPlage')?></h2>
	<p class="fill"><?= $trad('actiPlagePres')?></p>
</div>

<div class="f-container column page-content left fill dscvr">
	<h2><?= $trad('actiRiver');?></h2>
	<p class="fill">
		<?= $trad('actiRiverPres');?>
	</p>
	<div class="phone100vw figureParent wrap">
	<figure class="f-container   imgP inlineB notFlex">
		<div class="contentfig">
			<img class="half" src="../img/ancienFiltre1.jpg" />
		</div>
		<div class="">
			<figcaption>Ancien filtre, Restaunica</figcaption>
		</div>
	</figure>
	<figure class="f-container imgP inlineB notFlex">
		<div class="contentfig">
			<img class="half" src="../img/lac_noir-compressor.jpg" />
		</div>
		<div class="">
			<figcaption>Lac noir, Tavignano</figcaption>
		</div>
	</figure>
</div>

</div>

<div class="f-container column page-content left fill dscvr">
	<h2><?= $trad('actiHorse');?></h2>
	<p class="fill"><?= $trad('actiHorsePres');?></p>
	<div class="f-container fill">
		<a target="_blank" href="http://www.fae-equiloisirs.com/club-corte/"><?= $trad('moreInfo');?></a>
	</div>
</div>

<div class="f-container column page-content left fill dscvr">
	<h2><?= $trad('threeToThree');?></h2>
	<p class="fill"><?= $trad('threeToThreePres');?></p>
	<div class="f-container fill">
		<a target="_blank" href="http://www.corsicanatura-activites.fr/accrobranche-corse-vizzavona-11.html"><?= $trad('moreInfo');?></a>
	</div>
</div>

<div class="f-container column page-content left fill dscvr">
	<h2><?= $trad('actiRando');?></h2>
	<p class="fill">
		<?= $trad('actiRandoPres');?>
	</p>
	<p class="fill" style="color:#9a2f26; opacity:.6">
		<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
		<?= $trad('actiRandoWarn'); ?>
	</p>
	<figure class="f-container imgP inlineB notFlex">
			<img class="nino" src="../img/nino.jpg" />
			<figcaption style="text-align:center"><span class="inlineB">Lac de Nino</span></figcaption>
	</figure>
	<br>
	<p class="fill">
		<?= $trad('actiRandoAlerternative'); ?>
	</p>

	<div class="phone100vw figureParent wrap">
	<figure class="f-container imgP inlineB notFlex">
		<img class="nino" src="../img/arche.jpg" />
		<figcaption><span class="inlineB">L'</span><span class="inlineB">Arche</span></figcaption>
	</figure>
	<br>
	<p class="fill">
	</p>
	</div>
</div>

<div class="f-container column page-content left fill dscvr">
	<h2><?= $trad('aerodrome'); ?></h2>
	<p class="fill"><?=  $trad('aerodromePres') ?></p>
</div>

<div class="safari_bug f-container column page-content left fill dscvr">
	<h2 class="fill"><?= $trad('terraCorsa'); ?></h2>
	<div class="fill">
		<div class="f-container phone100vw imgP block">
			<img src="../img/terraCorsa.jpg" class="nino" />
		</div>
		<div class="fill"><br><?= $trad('terraCorsaPres'); ?></div>
		<br>
		<div class="f-container fill moreInfo">
			<a target="_blank" href="http://www.interracorsa.com/"><?= $trad('moreInfo');?></a>
		</div>
	</div>
</div>

<div class="f-container column page-content left fill dscvr">
	<h2 class="fill"><?= $trad('altipiani'); ?></h2>
	<p class="fill">
		<?= $trad('altipianiPres'); ?>
		<div class="f-container fill">
			<a target="_blank" href="http://www.altipiani-corse.com/fr/canyoning.php"><?= $trad('moreInfo');?></a>
		</div>
	</p>
</div>

<div class="f-container column page-content left fill dscvr">
	<h2 class="fill"><?= $trad('stradaDiISensi'); ?></h2>
	<p class="fill">
		<?= $trad('stradaDiISensiPres'); ?>
		<div class="f-container fill">
			<a target="_blank" href="http://www.corsicatours.com/La-route-des-Sens-Authentiques.html"><?= $trad('moreInfo');?></a>
		</div>
	</p>
</div>

<div class="safari_bug f-container column page-content left fill dscvr">
	<h2 class="fill"><?= $trad('museum'); ?></h2>
	<div class="fill">
		<div class="f-container phone100vw imgP">
			<img src="../img/citadelle.jpg" class="nino" />
		</div>
		<div class="fill"><br><?= $trad('museumPres'); ?></div>
		<br>
		<div class="f-container fill moreInfo">
			<a target="_blank" href="http://www.musee-corse.com/index.php/"><?= $trad('moreInfo');?></a>
		</div>
	</div>
</div>
<style>
.safari_bug.f-container .fill{
	display: block;
}
.safari_bug.f-container .fill.moreInfo{
	display: flex;
}
</style>

<?php include('advisors_v2.php') ?>
<script type="text/whenDocumentReady">
	//document.body.style.backgroundImage = "url('"+ $_GET['img'] + "')"
</script>
