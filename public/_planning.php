<?php 
	$endDate = (new \DateTime($startDate))->modify("+$nbSemaine week - 1 day")->format('Y-m-d');
	$reservations = $db->getBooking('week',['startDate' => $startDate,'endDate' => $endDate]);
	
	$resByRooms = [];
	$rooms = [5,6,7,8,13,14,15,16,18];
	foreach ($rooms as $room) {
		$resByRooms[$room] = [];
		for($i= $nbSemaine;$i--;){
			array_push($resByRooms[$room],0,0,0,0,0,0,0);
		}
	}
	foreach ($reservations as $res) {
		$rooms = explode(',', $res['room_ids']);
		$date = new DateTime($startDate);
		$date2 = (clone $date)->format('Y-m-d');
		for($i=0;$i < 7 * $nbSemaine;++$i){
			foreach ($rooms as $room) {
				$res2 = &$resByRooms[$room][$i];
				if($date2 >= $res['d_checkin'] && $date2 < $res['d_checkout']){
					$res2 = $res;
				}
			}				
			$date2 = $date->modify('+1 day')->format('Y-m-d');
		}
	}
	$rooms = [5,6,7,8,13,14,15,16,18];
	$date = new DateTime($startDate);
	$days = ['Di','Lu','Ma','Me','Je','Ve','Sa'];
	$months = ['Janv','Févr','Mars','Avr','Mai','Juin','Juil','Aout','Sept','Oct','Nov','Dec']
?>
	<!--S00-->
	<div class="borto-table-parent"><?php
	for($k=0;$k<$nbSemaine;++$k){ ?><!--noWhiteSpace
****--><!--S10--><!--noWhiteSpace
****--><div class="borto-table js-container-img containerImg clientW" style="display:inline-block" >
			<!--S20-->
			<div class="borto-line borto-first-line">
				<!--S30-->
				<div class="borto-column">
					N de chambre
				</div>
				<!--E30--><?php 
		for($i=0;$i<7;++$i){ ?>
				<!--S31-->
				<div class="borto-column">
					<?= $days[$date->format('N')%7].' '.$date->format('d').' '
						.$months[$date->format('n') -1] ?>
				</div>
				<!--E31--><?php 
			$date->modify('+1 day');
		} ?>
			</div>
			<!--E20--><?php
		foreach($rooms as $room){ ?>
			<!--S21-->
			<div class="borto-line">
				<!--S32-->
				<div class="borto-column borto-pres" >
					<?= $room ?>
				</div>
				<!--E32--><?php 
			for($i=$k*7;$i<$k*7 + 7;++$i){
				$res = $resByRooms[$room][$i]; ?>
				<!--S33-->
				<div class="borto-column" 
					is_paid="<?= $res['paid'] ?>" 
					style="background-color:<?= $res['color_code'] ?>" 
					data-res-number="<?= $res['resId'] ?>" 
				><?php 
				if($res && isset($isAdmin)){
					echo $res['client_name'] ;
				} ?>
				</div>
				<!--E33--><?php
			} ?>
			</div>
			<!--E21--><?php
		} ?>
		</div><!--noWhiteSpace
*****--><!--E10--><?php 
	} ?>
		<style>
			body{
				margin: 0;
				padding: 0;
			}
			.borto-table{
				height:15cm;
				width: 100%;
/*				min-width: 20cm;
*/				background:yellow;
				border: solid 1px black; 
				display: block;
				margin-top: 10px;
			}
			.carousel .borto-table{
				margin-top: 0px;
				
			}
			.carousel .borto-table-parent{
				width: auto;
			}
			@media print {
				.borto-table{
					height: calc(100% ) !important;
					margin: 10px 0 0 0;
				}
				.borto-column{
	    			min-width: calc(100% / 8 - 2px);
	    		}
			}
			.borto-table *{
				color:black;
			}
			.borto-line{
				display: flex;
				width: 100%;
				background: blue;
				height:	calc(100% / 10);
			}
			.borto-column{
				display: flex;
				flex: 0 0;
    			min-width: calc(100% / 8);
				width: 1px;
				background: whitesmoke;
				border: solid 1px black;
				text-align: center;
				justify-content: center; /* center items vertically, in this case */
			    align-items: center;     /* center items horizontally, in this case */
			}
			.borto-pres{
				background: #c0d8c4
			}

			.borto-line.borto-first-line .borto-column{
				background: #778a52
			}
			div[is_paid="0"]{
				background-image: repeating-linear-gradient(45deg, transparent, transparent 35px, rgba(255,255,255,.5) 35px, rgba(255,255,255,.5) 70px);
			}
		</style>
	</div>
	<!--E00-->
	<div class="hide js-editReservation f-container parentPopup">
		<div class="contentPopup js-contentPopup"><?php 
			$temp = file_get_contents($baseDirPath.'/noIndex/formResTemplate.txt');
			echo $temp;	?>
		</div>
	</div>
	<script type="text/whenDocumentReady">
		$.cache['reservations'] = <?= json_encode($reservations) ?>;
	</script>
	<?php if(!isset($modePlanning)){ ?>
		<script  type="text/whenDocumentReady">
		$('.page.active .borto-table').each(function(el){
			el.classList.remove('clientW')
			el.style.display = ""
		})
		</script>
	<?php } ?>
	<script data-name="listenerWeekBooking" type="text/whenDocumentReady">
		var templateFormString = $0('.js-editReservation .js-contentPopup').innerHTML

		window.printPlan = function(){
			var w = window.open('about:blank')
			w.document.body.innerHTML = $0('.borto-table-parent').outerHTML;
			//w.document.body.innerHTML = 'toto'
			w.print();
	       // w.close();
		}

		$.on('click','div[data-res-number]',function(){
			var id = this.dataset.resNumber;
			var res = $.cache['reservations'].filter(function(res2){
				return res2.resId == id
			})[0];

			res.is_checked_paid = +res.paid ? "checked" : ""
			// on considère que le template ne contient qu'une variable associative $res utilisé avec les balise < ?= et ? > (sans les espaces)
			var newHtml = templateFormString.replace(/{{(.*?)}}/g,function(match, $1){
				return res[$1] || '';
			})


			var $newHtml = $.create(newHtml)
			$newHtml.querySelectorAll('select[value]').each(function(option){
				option.value = option.getAttribute('value')
			})
			$newHtml.$0('input[name="startDate2"]').value = $0('input[name="startDate2"]').value
			$newHtml.$0('input[name="nbSemaine"]').value = $0('input[name="nbSemaine"]').value

			$0('.js-contentPopup').innerHTML = ''
			$0('.js-contentPopup').append($newHtml)
			$0('.js-editReservation').classList.remove('hide');
		})
		$.on('click', '.js-editReservation',function(){
	        $0('.js-editReservation').classList.add('hide')
	    })
	    $.on('click','.js-editReservation .contentPopup',function(){
	        //si on return pas true le click est pas propagé
	    })
	</script>