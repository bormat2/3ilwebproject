<div class="f-container page-content column fill">
	<h1 style="text-align: center">Connexion au panneau de configuration</h1>
</div>
<div class="f-container fill page-content column last">
	<form id="sendPasswordForm">

		<style>
			#sendPasswordForm > div.f-container {
				padding-top: 25px;
			}
		</style>

		<?php
			if(isset($post['isPost'])){
				if($user->connect($post['password'])){
					echo formSuccess('success');
					reloadMenu();
					?><script type="text/whenDocumentReady">
						 $.loadAndAddToHistory('weekBooking')
					</script><?php
				}else{
					echo formError('error');
				};
			}?>
		<div class="f-container fill column">
			<div class="input-group">
				<input name="password" type="password" required>
				<span class="highlight"></span>
				<span class="bar"></span>
				<label><?= $trad('password') ?></label>
			</div>
			<button type="submit" id="sendPasswordButton" class="bigButton"><?= $trad('log-in') ?></button>
		</div>
	</form>
</div>
