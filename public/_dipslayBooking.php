<?php 
	include('redirectIfNotAdmin.php');
	include('public/_updateRes.php');
	$reservations = $db->getBooking($pageCode == 'validBooking' ? 'notConfirmed' : 'all');
?>
<div id="listReserv" style="flex-wrap:wrap"></div>
<script id="formResTemplate" type="text/borto-template" >
	<?= file_get_contents($baseDirPath.'/noIndex/formResTemplate.txt') ?>
</script>
<script type="text/whenDocumentReady">
	var listReservHTML = '' ;
	var templateFormString = $id('formResTemplate').innerHTML;
	var reservations = <?= json_encode($reservations) ?>;
	$.eachA(reservations,function(res){
		listReservHTML += templateFormString.replace(/{{(.*?)}}/g,function(match, $1){
			if($1 == 'is_checked_paid' && +res.paid){
				return ' checked ';
			}
			return res[$1] || '';
		})
	});

	var $listReserv = $.creates(listReservHTML)
	$.eachA($listReserv,function($res){
		$.eachA($res.$('select[value]'),function(option){
			option.value = option.getAttribute('value')
		})
	})
	$.appendChilds($id('listReserv'),$listReserv)
</script>