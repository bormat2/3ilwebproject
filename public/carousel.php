<?php
    $dirNameEnd = '/noIndex/files/GallerieAccueil/';
    $images = glob($baseDirPath.$dirNameEnd."*.{jpg,gif,png}",GLOB_BRACE);
?>
<div class="f-container fill page-content carouselParent column backgroundCenter">
    <div class="carousel widthContent">
        <div class="realImgContainer clientW" >
                <div class="largeContainerParent">
                <div class="largeContainer anim flex">
                    <?php
                    foreach($images as $key => $image) {
                        $ext = pathinfo($image, PATHINFO_EXTENSION);
                        $codeTrad = 'gallery1::'.basename($image,".$ext"); ?>
                        <div class="js-container-img containerImg clientW" style="display:inline-block" >
                            <img
                                id="realImg"
                                src="..<?= $dirNameEnd.basename("$image"); ?>"
                                data-key="<?=$key ?>"
                                data-key-trad="<?= basename($image,".$ext"); ?>"
                            />
                            <span><?= $tradOrEmptyString($codeTrad) ? $trad($codeTrad) : ''; ?></span>
                        </div><?php
                   }
                ?>
                </div>
        </div>
    </div>

    </div>

        <div class="buttonCarousel f-container fill page-content column transparent">
                <div class="prev js-prev pointer" >
                    <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                </div>
                <div class="next js-next pointer">
                    <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                </div>
            </div>
        <div id="bottomMini" class="phone100vw ipad100vw">
            <div class="miniatures miniaturesBottom space-between"><?php
                foreach($images as $key => $image) {
                    $ext = pathinfo($image, PATHINFO_EXTENSION);
                    ?>
                    <div class="js-container-img containerImg" >
                    <img
                        class="miniature js-miniature clientW"
                        width="200"
                        src="..<?= $dirNameEnd.basename("$image"); ?>"
                        data-key="<?=$key ?>"
                    />
                    <span></span>
                    </div><?php
                }
            ?>
            </div>
        </div>
        <script type="text/whenDocumentReady">
            var currentScript = this.currentScript
            /*fin récup des param de this*/

            $.carousel(currentScript.closest('.carouselParent'))
        </script>
</div>
<script data-name="addListenerButtonImgCaroussel" type="text/whenDocumentReady">
  
</script>
<script type="text/whenDocumentReady">
    /*$0('.js-miniature').click()*/
</script>
