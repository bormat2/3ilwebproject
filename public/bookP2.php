<!-- TODO :
	 TODO : make scroll to error when showing one
 -->
<script type="text/whenDocumentReadyAndOnPopState">
    var currentScript = this.currentScript
    if($.cache['checkAvabilityForm']){
        $checkAvabilityForm = $id('checkAvabilityForm')
        $checkAvabilityForm && $checkAvabilityForm.remove()
        currentScript.$insert($.cache['checkAvabilityForm'],'afterend')
    }
  /* TODO réparer click bordure et date réinitialisé */
</script>
<div class="f-container column page-content fill dscvr book-validation">

	<?php
	 	$nbPers = $post['nbAdults'] + $post['nbKids'];
	 	$rooms = $db->getRoom($post['startDate'],$post['endDate']);
	?>
	<?php if(!isset($rooms[0])){?>
		<div class="bigBlockError">
			<span class="error">
				<?= $trad('tryAnOtherDate') ?>
			</span>
		</div>
	<?php
		} else {
	?>
		<div id="room-list"><?= count($rooms)."&nbsp;".$GLOBALS['trad']('found-rooms'); ?>.
	<?php } ?>
	</div>
 	<?php

 		foreach ($rooms as $room) {
 			?><div class="js-roomPres roomPres f-container wrap fill" data-room="<?=  $room['num'] ?>">
				<div class="f-container column">
					<div class="f-container" >
						<span class="room-num"><?= $room['num'] ?></span>
					</div>
					<div class="f-container">
						<i class="fa fa-bed" aria-hidden="true"></i>
						<?= $room['nb_pers'] ?>
					</div>
				</div>

        <?php
        $dirNameEnd = '/noIndex/files/Rooms/'.$room['num'].'/';
        $folder_room = $baseDirPath.$dirNameEnd;
        $images = glob($folder_room."*.{jpg,gif,png}",GLOB_BRACE);
        $class_carrouss = isset($images[0]) ? "carroussel_v3" : '';
        ?>
 				<div class="room-img-container <?= $class_carrouss ?>">
          <?php
          if( isset($images[0]) ){
            $include_carroussel = ['room_number' => $room['num'] ];
            include('carroussel_room.php');
          ?>
          <div style="display:none" class="pic-unavailable f-container"><?= $trad('currently-unavailable'); ?>
          <?php
            }else{
           ?>
					<!-- <img src="../img/notAvailable.png" /> -->
					<div  class="pic-unavailable f-container"><?= $trad('currently-unavailable'); ?>
          <?php
         }
          ?>
          </div>
				</div>

 			</div><?php

 		}
 	?>

	<script data-src="../js/pages/book.js" data-name="bookController" type="text/whenDocumentReady"></script>


	<?php if(isset($rooms[0])){ ?>
		<h1>
			<?= $trad('reservation-title'); ?>
		</h1>
		<form novalidate id="bookValidation" action="book" class="f-container column fill">

			<!-- HIDDEN INPUTS -->
			<input id="nbPers" type="hidden" name="nbPers" 	value="<?= $nbPers ?>">
			<input type="hidden" name="startDate" 	value="<?= $post['startDate'] ?>">
			<input type="hidden" name="endDate" 	value="<?= $post['endDate'] ?>" >
			<input type="hidden" name="idChambre">
			<!-- ///////////// -->
		<div class="input-group">
			<input name="name" type="text" required>
			<span class="highlight"></span>
			<span class="bar"></span>
			<label><?= $GLOBALS['trad']('name'); ?></label>
		</div>
		<div class="input-group">
			<input name="tel" type="text" required>
			<span class="highlight"></span>
			<span class="bar"></span>
			<label><?= $GLOBALS['trad']('tel'); ?></label>
		</div>
		<div class="input-group">
			<input name="email" type="text" required>
			<span class="highlight"></span>
			<span class="bar"></span>
			<label><?= $GLOBALS['trad']('email'); ?></label>
		</div>
		<div class="input-group">
			<input name="message" type="text" add-empty-selector >
			<span class="highlight"></span>
			<span class="bar"></span>
			<label><?= $GLOBALS['trad']('optionnal-message'); ?></label>
		</div>
		<button type="submit" class="bigButton" id="send-reserv">
			<?= $GLOBALS['trad']('make-reservation'); ?>
		</button>
	</form>
		<div class="reservation-state f-container">
			<?php if ($nbPers > 0) {?>
			<div class="f-container wrap">
				<i class="fa fa-bed" aria-hidden="true"></i>
				<span>Personnes sans chambre :</span>
				<span id="nbPersRes" class="big-value"><?= $nbPers ?></span>
			</div>
			<?php } ?>
			<div class="f-container">
				<i class="fa fa-shopping-cart" aria-hidden="true"></i>
				<span>TOTAL : </span>
				<span>
					<span class="js-priceBookEstimation big-value">0</span>
					<span class="big-value">€</span>
				</span>
			</div>
		</div>

		<script type="text/whenDocumentReady">
			$0('.page.active').$insert($.cache['checkAvabilityForm'],'afterbegin')
	 		var url = window.location.href
	 		$.disablePopState = true;
			//
			//window.location.href += "#room-list"
			$.scrollTo({
				time: 100,
				top: $id('room-list').offsetTop,
				parent: $id('fakeBody')
			});
			//window.history.replaceState($.getState(), 'title',url)
	 		$.disablePopState = false;
		</script>
    <style>
      .room-img-container.carroussel_v3{
        width:100%;
        max-width: 100% !important;
      }
    </style>
	<?php } //end if room available ?>
</div>
