<?php include('redirectIfNotAdmin.php') ?>
<?php include('notYetFinish.php') ?>

<div class="f-container fill page-content column last">
	<p>
		S'il y a des réservations à valider, elles seront ci-dessous.
	</p>
	<?php 
		$reservations = $db->getBooking($pageCode == 'validBooking' ? 'notConfirmed' : 'all');
	?>
	<?php include('_dipslayBooking.php'); ?>

</div>
