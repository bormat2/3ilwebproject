<?php
	if(strpos($post['email'], '@') === false){
		?><script type="text/whenDocumentReady">
			alert('email faux')
			document.body.scrollTop = document.documentElement.scrollTop = 0
		</script><?php
	}else{
		include('BookClass.php');
		$rooms = explode(" ", $post['idChambre']);
		$book = new Book();
	 	$endDate = new DateTime($post['endDate']);
		$startDate =  new DateTime($post['startDate']);
		$interval = + date_diff($startDate, $endDate)->format('%R%a');
/*	   	$price = $book->getPriceSeason($post['nbPers'],count($rooms),$interval,'mediumSeason');*/
		$price = $book->getPrice($post);
		$isConnect = $user->isConnect();

	    $db->book($rooms,$post['startDate'],$post['endDate'],$post['email'],
	    	$post['tel'],'#800ddd',$post['name'],$price,$post['nbPers'],$post['message'],$isConnect);


		$mess = 'Une demande de reservation au nom de '.$post['name'].' a été faite <br>
			les chambres prises sont '.$post['idChambre'].'<br>
			En cliquant sur répondre vous répondrez automatiquement à '.$post['email'].'<br>
			son numéro est le '.$post['tel'].'<br>'
		;
		$param =  [
			'checkin' 		=> formatDate($startDate,['mode' => 'short']),
			'checkout' 		=> formatDate($endDate,['mode' => 'short']),
			'totalPrice'    => $price,
			'pricePart1' 	=> $price * 0.3,
			'diffPrice'     => $price * 0.7,
			'interval' 		=> $interval,
			'nbStudio' => count($rooms),
			'nbPers' => $post['nbPers'],
			'templateUrl' => '/noIndex/priseEnCompte/'
		];
		//echo "00089".json_encode($param)."00098";
		$param['url'] = $_GET['path'].'/emailViewer?token='.base64_encode(json_encode($param));

		//if(substr($post['email'],0,1) !== '@'){
		if(!$isConnect){
			sendMail([
				'title' => 'Prise en compte de votre réservation',
				'to' => $post['email'],
				'hmtlMode' => true,
				'templateUrl' => $param['templateUrl'],
				'vars' => $param
			]);
			// var_dump($param);
			sendMail([
				'title' => 'Nouvelle réservation: '.$startDate->format('d/m/Y'),
				'to' => 'tcampana@wanadoo.fr',
				'hmtlMode' => true,
				'templateUrl' => '/noIndex/priseEnCompteAdmin/',
				'vars' => $param
			]);
		}

		$param['message'] = $post['message']
?>
<div class="f-container fill page-content column last">
	<span>
		Votre réservation pour <?= $interval ?> nuits a été prise en compte,
		la gérante du motel en a été informé par mail, le prix total
		est de <?= $price ?> €. Vous recevrez une réponse par mail
		dans les plus brefs delais.
	</span>
</div>
<?php } ?>
