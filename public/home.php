<?php include('carousel.php') ?>

<div class="js-accueil f-container fill page-content column">
	<section class="fill f-container">
		<div s class="fill f-container column left">
			<h1>PRESENTATION</h1>
			<p class="fill" auto-width >
				<?= $trad('motelPres'); ?>
			</p>
		</div>
	</section>
	<section class="fill f-container">
		<div class="fill f-container column left">
			<h1><?= $trad('studioTitle'); ?></h1>
			<p class="fill">
				<?= $trad('studiosPres'); ?>
			</p>
		</div>
	</section>

	<section class="fill f-container fill">
		<div class="fill f-container column left fill">
			<h1><?= $trad('priceTitle'); ?></h1>
			<p class="fill">
					<?= $trad('pricePres'); ?>
 				<!-- 	<img src="../img/price.jpg" />	 -->		</p>
		<div class="borto-table price js-container-img containerImg" style="display:inline-block">
			<div class="borto-line borto-first-line">
				<div class="borto-column price"><?= $trad('Period') ?></div>
				<div class="borto-column price"><?= $trad('nightFor2') ?></div>
				<div class="borto-column price"><?= $trad('theWeek') ?></div>
				<div class="borto-column price"><?= $trad('extraBed') ?></div>
			</div>
			<div class="borto-line">
				<div class="borto-column price borto-pres"><?= $trad('per1Date') ?></div>
				<div class="borto-column price">60 €</div>
				<div class="borto-column price">400 €</div>
				<div class="borto-column price">10 € / nuit</div>
			</div>
			<div class="borto-line">
				<div class="borto-column price borto-pres"><?= $trad('per2Date') ?></div>
				<div class="borto-column price">65 €</div>
				<div class="borto-column price">430 €</div>
				<div class="borto-column price">10 € / nuit</div>
			</div>
			<div class="borto-line">
				<div class="borto-column price borto-pres"><?= $trad('per3Date') ?></div>
				<div class="borto-column price">68 €</div>
				<div class="borto-column price">446 €</div>
				<div class="borto-column price">10 € / nuit</div>
			</div>
			<div class="borto-line">
				<div class="borto-column price borto-pres"><?= $trad('per4Date') ?></div>
				<div class="borto-column price">65 €</div>
				<div class="borto-column price">430 €</div>
				<div class="borto-column price">10 € / nuit</div>
			</div>
		</div>
		<div style="position:relative" id="bugIeimg" class="space-between start f-container wrap fill acceptedPayment">
			<div style="margin-top: 13%;background: rgb(255, 0, 0);height: 1px; width: 1px;"></div>
			<div><img id="ancv" src="../img/ancv.png" /></div>
			<div><img id="virement"  src="../img/virement.png" /></div>
			<div><img id="cheque"  src="../img/cheque.png" /></div>
		</div>
	</section>
	<style>

	#bugIeimg{
		min-height: 1px;
	    margin-top: 5px;
	    margin-bottom: -59px;
	    z-index: 1;
	}
	#bugIeimg,.acceptedPayment,#bugIeimg.acceptedPayment div{
		/*flex: 0 0;*/
		align-items: flex-start;
		flex-wrap: nowrap;
	}
	#bugIeimg.acceptedPayment div{
		width: 25%;
	}
	#bugIeimg.acceptedPayment img{
		max-width: 100%;
		max-height: 147px;
		height: auto;
	}
	</style>
</div>
<?php
	include('bookP1.php');
	$startDate = (new \DateTime())->format('Y-m-d');
?>
<div class="doNotInsertNextIe11Lte"></div>
<?php
	 include('carouselPlanning.php');
	include('mapPosition.php');
	 include('advisors_v2.php');//2
?>
