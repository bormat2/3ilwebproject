<script type="text/whenDocumentReadyAndOnPopState">
    var currentScript = this.currentScript 
    if($.cache['checkAvabilityForm']){
        $0('.checkAvabilityForm').remove()
        currentScript.$insert($.cache['checkAvabilityForm'],'afterend')
    }
  /* TODO réparer click bordure et date réinitialisé */
</script>

<script data-name="cuppaCalendar" type="text/whenDocumentReady" data-src="../js/cuppa-calendarFORK.js"></script>
<form action="book" class="checkAvabilityForm f-container fill page-content last column noselect">
<div class="f-container wrap">
  <input name="checkAvabilityForm" value="1" type="hidden">
  <input name="startDate" type="hidden">
  <input name="endDate" type="hidden">
  <div class="f-container">
    <div id="startDate" class="f-container column"><label><?= $GLOBALS['trad']('check-in'); ?></label></div>
    <div id="endDate" class="f-container column"><label><?= $GLOBALS['trad']('check-out'); ?></label></div>
  </div>
  <div class="f-container wrap">
    <div class="f-container input">

      <label class="f-container" auto-height><?= $GLOBALS['trad']('adults'); ?></label>

    <button type="button" class="f-container plus-minus">
      <i class="fa fa-minus-circle" aria-hidden="true"></i>
    </button>

    <input name="nbAdults" type="text"  value="1" size="1">

    <button type="button" class="f-container plus-minus">
      <i class="fa fa-plus-circle" aria-hidden="true"></i>
    </button>

    </div>
    <div class="f-container input">

      <label class="f-container" auto-height><?= $GLOBALS['trad']('kids'); ?></label>

    <button type="button" class="f-container plus-minus">
      <i class="fa fa-minus-circle" aria-hidden="true"></i>
    </button>

    <input name="nbKids"  value="0" size="1">

    <button type="button" class="f-container plus-minus">
      <i class="fa fa-plus-circle" aria-hidden="true"></i>
    </button>

    </div>
  </div>
  <button class="bigButton" type="submit" id="checkAvabilityButton"><?= $GLOBALS['trad']('find-offer'); ?></button>
 </div>
 <div class="f-container fill horaires">
   <div class="f-container column">
    <h4><?= $trad('arrivals') ?></h4>
    <div><?= $trad('arrivalsHour')?>&nbsp;15h</div>
  </div>
     <div class="f-container column">
    <h4><?= $trad('departure') ?></h4>
    <div><?= $trad('departureHour')?><span>&nbsp;11h</span></div>
    </div>
 </div>
</form>
<script data-name="addListenerButton" type="text/whenDocumentReady">
  console.log('book.js')
  var a = 0;
    $.on('submit','.checkAvabilityForm',function(event){
        event.preventDefault();
        var error = false;
        console.log('here')
        $.resetError(this)
        var endDate = this.elements['endDate'].value
        var startDate = this.elements['startDate'].value
        if(endDate <= startDate){
            error = true
            $.appendErrorToForm(this,'errorDate')
        }
        if(!error){return true;}//continue
    })

	$.on('click','.plus-minus',function(){
		var parent = this.closest('.input')
		var input = parent.querySelector('input')
		input.value -= ~(parent.querySelector('.plus-minus') == this && ~!!+input.value)
	})

	$.on('click','#startDate, #endDate, .wc-date-container',function(){
		var pickerButton = this.querySelector('.wc-date-container')
		pickerButton && pickerButton.click()
	})

  /*
  * on retoune le format à partir de toLocaleDateString()
  */
  var getLocalFormat = function(){
    if($.getLang() == 'fr'){
        return 'DD/MM/YYYY'
    }
    var d = new Date('2017-11-16')
    var s = d.toLocaleDateString()
    s = s.replace('17','YY').replace('20','YY')
            .replace('11','MM')
            .replace('16','DD')

    return s;
  }

  var format = getLocalFormat();

  var setDateToInput = function(inputName,date){
    var f = moment(date).format('YYYY-MM-DD')
    $0('input[name="'+inputName+'"]').value = moment(date).format('YYYY-MM-DD');
    console.log(f)
  }

  var defaultDate = new Date();
  $.initCalendar = function(){
    ['startDate','endDate'].each(function(key){
        new WinkelCalendar({
          container: key,
          bigBanner: true,
          defaultDate: defaultDate, // TODO : do this in an intelligent way
          format : format,
          onSelect : function(date) {
            console.log(date)
            setDateToInput(key,date);
          }
        })
        setDateToInput(key,defaultDate)
    })
  }
  $.initCalendar()

  $.cache['checkAvabilityForm'] = $0('.checkAvabilityForm')

</script>


