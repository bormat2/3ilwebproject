<?php
include('redirectIfNotAdmin.php');
 $dirNameEnd = '/noIndex/files/GallerieAccueil/';
 $exts = ['jpg','gif','png','jpeg'] ;
 $upload = function($name) use ($baseDirPath,$dirNameEnd,$exts) {
    $f = &$_FILES['fichier'];
    $iImage = count($f['name']);
    if(!$iImage){
        return 'Veuillez remplir le formulaire svp !';
    }
    $randomName = !$name;
    while($iImage--){
        $extension  = strtolower(pathinfo($f['name'][$iImage],PATHINFO_EXTENSION));

        if(!in_array($extension,['jpg','gif','png','jpeg'])){
            return 'L\'extension du fichier est incorrecte !';
        }

        $infosImg = getimagesize($f['tmp_name'][$iImage]);
        if($infosImg[2] < 1 || $infosImg[2] > 14){
            return 'Le fichier à uploader n\'est pas une image !';
        }
        $src = $f['tmp_name'][$iImage];
        // on supprime l'image qui pourrait exister avec le même nom
        // on connait pas son extension donc
        // on utilise glob avec le joker *
        if($randomName){
            $name = md5(uniqid());
        }else{
            foreach(glob($baseDirPath.$dirNameEnd.$name.'.*') as $f) {
                unlink($f);
            }
        }
        $ouput = $baseDirPath.$dirNameEnd.$name.'.'. $extension;
        if(!move_uploaded_file($src,$ouput)){
            return 'Problème lors de l\'upload !';
        }
    }
    return 'Upload réussi !' ;
};

if(isset($post['isPost'])){
    if(isset($post['mode'])){
        $key = $post['imageKey'];
        switch($post['mode']){
            case 'updateDescr':
                $trads[$_GET['lang']]['gallery1::'.$key] = $post['newMess'];
                break;
            case 'removeImg':
                //on supprimes toutes les traductions affectés à l'image
                foreach ($trads as &$trad) {
                    unset($trad[$key]);
                }
                // on supprime l'image on connait pas son extension donc
                // on utilise glob avec le joker *
                foreach(glob($baseDirPath.$dirNameEnd.$key.'.*') as $f) {
                    unlink($f);
                }
                break;
        }
        // on met à jours les traductions avec la description et on retourne
        // le nouveau fichier des traductions, avec exit pour ne pas afficher le html
        $tradFile = json_encode($trads,JSON_PRETTY_PRINT);
        file_put_contents('noIndex/Translation.json',$tradFile);
        echo $tradFile;exit;
    }else{
        $name = isset($post['nameFile']) ? $post['nameFile'] : null ;
        $upload($name);
    }
}

?>

<!-- Debut du formulaire -->
<div class="adminGestion f-container fill page-content column admin">
	<h1>Gestion de la galerie d'images</h1>
    <p>
		Vous pouvez envoyer plusieurs images à la fois.
	</p>
</div>
<div class="adminGestion f-container fill page-content column admin">
    <form action="admin.php" method="post" class="f-container wrap">
            <input name="fichier[]" type="file" multiple />
            <input type="submit" name="submit" value="Uploader" class="bigButton"/>
    </form>
</div>
<div class="adminGestion f-container fill page-content column admin">
    <?php
        $images = glob($baseDirPath.$dirNameEnd."*.{jpg,gif,png}",GLOB_BRACE);
    ?><div class="f-container wrap space-around"><?php
        foreach($images as $image) {
            $ext = pathinfo($image, PATHINFO_EXTENSION);
            ?>
            <div class="js-container-img containerImg" style="display:inline-block" >
            <img
                data-key="<?= basename($image,".$ext"); ?>"
                class="miniature js-miniature"
                width="200"
                src="..<?= $dirNameEnd.basename("$image"); ?>"
            />
			<i class="fa fa-times js-remove remove" aria-hidden="true"></i>
            </div><?php
        }
    ?>
    </div>
    <div class="hide js-editImg editImg f-container parentPopup">
        <div class="js-editDescr f-container column contentPopup">
			<h2>Description de l'image :</h2>
            <textarea id="tradDescr">description</textarea>
            <button id="updateDescrAction" class="bigButton"><?= $trad('Mettre à jour') ?></button>
            <form action="admin.php" method="post" class="f-container wrap">
                <input id="simpleUpload" name="fichier[]" type="file"  />
                <input type="hidden" name="nameFile">
                <input type="submit" name="submit" value="Uploader" class="bigButton"/>
            </form>
        </div>
    </div>
</div>
<script data-name="addListenerButtonImg" type="text/whenDocumentReady">
    var lastKey;
    $.on('click','.adminGestion .js-miniature',function(){
        lastKey =  this.dataset.key;
        var key = 'gallery1::' + lastKey
        $0('.js-editImg').classList.remove('hide')
        var trans = $.translate(key);
        $id('tradDescr').value = (trans != key) ? trans : ''
        $0('input[name="nameFile"]').value = lastKey;
    })

    
    $.on('click', '.adminGestion .js-editImg',function(){
        $0('.js-editImg').classList.add('hide')
    })
    $.on('click','.js-editDescr',function(){
        //si on return pas true le click est pas propagé
    })


    $.on('click','.adminGestion #updateDescrAction',function(){
        $.ajax({
            url: 'admin.php',
            data: {
                mode: 'updateDescr',
                isPost: true,
                notAPage: true,
                imageKey: lastKey,
                newMess: $0('.js-editDescr textarea').value
            },
            success:function(trans){
                $.trans = JSON.parse(trans);
                $0('.js-editImg').classList.add('hide')
            }
        })
        //si on return pas true le click est pas propagé
    })

    /*$.on('click','.adminGestion .js-editDescr',function(){
        //si on return pas true le click est pas propagé
    })*/
    $.on('click','.adminGestion .js-remove',function(){
        var containerImg =  this.closest('.js-container-img')
        var key = containerImg.querySelector('.js-miniature').dataset.key;
        $.ajax({
            url: 'admin.php',
            data: {
                mode: 'removeImg',
                isPost: true,
                notAPage: true,
                imageKey: key,
            },
            success:function(trans){
               containerImg.remove()
            }
        })
    })

</script>
