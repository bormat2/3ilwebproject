<?php include('redirectIfNotAdmin.php'); ?>
<div class="f-container page-content column fill">
	<h1 style="text-align: center">Modification du mot de passe</h1>
</div>
<div class="f-container page-content column fill">
	<form id="sendNewPasswordForm" >
		<?php
			if(isset($post['isPost'])){
			    if($user->connect($post['oldPassword'] )){
			      $user->setPasswordConnection($post['password']);
			      echo formSuccess('success');
				  // TODO :
				  	// - go to home page, or last page visited
					// - not echo trasaction success
			    }else{
			      echo formError('error');
			    }
			}
		?>
		<div class="f-container fill column">
			  <div class="input-group">
				  <input name="oldPassword" type="password" required>
				  <span class="highlight"></span>
				  <span class="bar"></span>
				  <label><?= $trad('oldPassword') ?></label>
			  </div>
			  <div class="input-group">
				  <input name="password" type="password" required>
				  <span class="highlight"></span>
				  <span class="bar"></span>
				  <label><?= $trad('password') ?></label>
			  </div>
		</div>
		<button id="sendNewPasswordButton" type="submit" class="bigButton">
			<?= $trad('setPassword') ?>
		</button>
	</form>
</div>
