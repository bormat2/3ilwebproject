<?php
	error_reporting(~0);
	class Book{
		public function getPrice($post){
			$book = new Book();
			$rooms = explode(" ", $post['idChambre']);
		 	$endDate = new DateTime($post['endDate']);
			$startDate =  new DateTime($post['startDate']);
			$year = $startDate->format('Y');
			$configP = [[
				'startDate' => '04-01',
				'endDate' => '05-15',
				'priceD' => 60,
				'priceW' => 400
			],[
				'startDate' => '05-15',
				'endDate' => '07-21',
				'priceD' => 65,
				'priceW' => 430
			],[
				'startDate' => '07-21',
				'endDate' => '09-01',
				'priceD' => 68,
				'priceW' => 446
			],[
				'startDate' => '09-01',
				'endDate' => '09-30',
				'priceD' => 65,
				'priceW' => 430
			]];

			$price = 0;
			foreach ($configP as $key => $conf) {
				$max = new DateTime($startDate->format('Y').'-'.$conf['endDate']);
				// si une semaine commence en juin elle est au prix de juillet
				if($startDate < $max && $endDate > $max){
					$nbDayTilMax = date_diff($startDate,$max)->format('%R%a');
					$nbWeek = floor(($nbDayTilMax - 1) / 7) + 1;
					$max2 = (clone $startDate)->modify("+ $nbWeek week");
					if($endDate	>= $max2){// si la semaine n'est pas entière elle ne profite pas de l'alongement du max2
						$max = $max2;
						$configP[$key + 1]['startDate'] = $max->format('m-d');
					}
				}
				//var_dump($startDate);
				$nbNight = + date_diff($startDate,min($max,$endDate))->format('%R%a');
				$price += $this->getPriceSeason($post['nbPers'],count($rooms),$nbNight,$conf);
/*				var_dump($price);
				var_dump($post['nbPers'],count($rooms),$nbNight);
				echo '<br>';
				echo '<br>';*/
				if($startDate < $max){
					$startDate = $max;
				}
			}
			return $price;
		}

		function getPriceSeason($nbPers,$nbRoom,$nbNight,$conf) {
			$bedSupPrice = 10;

			if($nbNight < 1) return 0;
			$nbDayAlone = $nbNight%7;
			$nbWeek = ($nbNight - $nbDayAlone) / 7;
			$suppByNight = max(0,$nbPers - $nbRoom * 2) * $bedSupPrice;
			return $nbRoom * ($conf['priceD'] * $nbDayAlone + $conf['priceW'] * $nbWeek) + $suppByNight * $nbNight;
		}		
	}
?>