<div class="f-container fill page-content carouselParent column backgroundCenter planningCarousel">
    <div class="carousel widthContent">
        <div class="realImgContainer clientW" >
            <div class="largeContainerParent">
                <div class="largeContainer anim flex">
                    <?php
                        $modePlanning = 'carousel';
                        $date = (new \DateTime());
                        $startDate = $date->format('Y').'-05-01';
                        $nbSemaine = 23;
                        include('_planning.php');?>
                </div>
            </div>
        </div>

    </div>

    <div class="buttonCarousel f-container fill page-content column transparent">
        <div class="prev js-prev pointer" >
            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
        </div>
        <div class="next js-next pointer">
            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
        </div>
    </div>
    <script type="text/whenDocumentReady">
        var currentScript = this.currentScript
        /*fin récup des param de this*/
        $carr = currentScript.closest('.carouselParent')
        $.carousel($carr)

    </script>
</div>
<div class="ieMess f-container fill page-content column">
    <?= $trad('ieCarrousel'); ?>
    <a href="carouselPlanning">cliquez ici pour le charger malgré tout</a>
</div>
