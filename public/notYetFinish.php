<div class="f-container fill page-content column last">
<script type="text/whenDocumentReady">
	
	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor); 
	if(!isChrome){
		alert("cette page n'est compatible qu'avec google chrome pour le moment");
	}
</script>
<p class="fill" style="color:#9a2f26; opacity:.8
">
		<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
		<span data-trad="actiRandoWarn">&nbsp;Cette page est encore en cours de développement autant pour les fonctions que pour le style et n'est pas encore responsive et ne peux pas non plus être traduite en anglais.

		Les pages d'administration ne sont dans un premier temps que testé sous google chrome.
</span>	</p>
</div>