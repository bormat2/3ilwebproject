<?php 
	include('redirectIfNotAdmin.php');
	include('notYetFinish.php');
	$startDate = $_COOKIE['startDate2'] ?? (new DateTime())->format('Y-m-d');
	$nbSemaine = $_COOKIE['nbSemaine'] ?? 2;
	include('_updateRes.php');
 ?>
<!-- SI -->
<div class="f-container fill page-content column last">
	<button onclick="printPlan()">print</button>
	<form id="planningPrefChoice">
		Date de début: <input type="text" id="startDate2" value="<?=$startDate?>">
		nombre de semaine: <input type="text" id="nbSemaine" value="<?=$nbSemaine?>">
		<input type="submit">
		<script type="text/whenDocumentReady">
			$.on('submit','#planningPrefChoice',function(){
				$.setCookie('startDate2',$id('startDate2').value)
				$.setCookie('nbSemaine',$id('nbSemaine').value)
				return true//continue submit
			},1)
		</script>
	</form>
	<style>
		body[data-current-page="weekBooking"] .borto-table {
			min-width: 100%;
		}
		body[data-current-page="weekBooking"] .borto-table-parent {
			flex-wrap: wrap;
		}
	</style>
	<?php
		$isAdmin = true;
		include('_planning.php'); 
	?>
</div>


<!-- EI -->