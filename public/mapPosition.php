<div id="mapAbsolute" data-fake-parent="#mapFakeContainer" class="hide f-container column fill page-content last outOfPage">
	<div id="map"  class="interact f-container fill  phone100vw">
		<iframe  id="mapFram" frameBorder="0" scrolling="no" src="about:blank"></iframe>
	</div>
</div>
<div id="mapFakeContainer" class="f-container column fill page-content last">
	<div id="map" style="position:relative" class="f-container fill  phone100vw">
		<div class="butt1" style="position:absolute;z-index: 11;top:50%;left:50%;transform:translate(-50%,-50%)">
			<?= $trad('click_to_interact'); ?></div>
	</div><div class="js-externalLink" style="z-index:1; margin-top: -52px;;margin-bottom: 20px">
		<a target="_blank" href="https://www.google.com/maps/@42.3164769,9.1481937,3a,75y,36.49h,67.12t/data=!3m6!1e1!3m4!1s3C_B7apByK0QkP4nyj2C-Q!2e0!7i13312!8i6656!6m1!1e1?hl=en-US">Street view</a> <p>lat: 42.126031,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lng: 9.1488804,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		 Chemin De Saint-Pancrace, 20250 CORTE
</p>
	</div>
	<div class="f-container column">
		<?= $trad('whereIsMotel') ?>
		<a target="_blank"
		href="http://www.restonicavoyages.fr/wp-content/uploads/2016/04/aper%C3%A7u-carte-6.pdf">
			<?= $trad('lookUpSchedules') ?>
		</a><br>
			<div class="fill">
					EUROPCAR 04.95.46.06.02<br>
					GARE DE CORTE 04.95.46.00.97<br>
					AUTOCAR CORTENAIS 04.95.46.22.89 / 06.19.83.26.18
			</div>
	</div>
</div>

<script  type="text/whenDocumentReady">
	if(!$.cache.mapF){
		var $map = $.last($('#mapAbsolute'))
		var loadMap = $.loadMap = function(){

			var debug = [];
			['header','.carousel.widthContent','#bugIeimg','.checkAvabilityForm',
			'.f-container.fill.page-content.carouselParent.column.backgroundCenter','.planningCarousel'].forEach(function(selector){
				/* permet de vérifier pourquoi la map est mal placé*/
				debug.push($.top($0(selector)))
			})
			console.log(debug);
			var map2 =  $id('mapFakeContainer')
			var top = $.top(map2),left = $.left(map2);
			$map.style.position = 'absolute'
			var diff = Math.abs((parseInt($map.style.top)||0) - top)
				+ Math.abs((parseInt($map.style.left)||0) - left);
			if(diff > 5){
				$map.style.top = top + 'px'
				$map.style.left = $.left(map2) + 'px'
				console.log('mapPlaced' + t++);
			}
		}
		var t = 0;
		loadMap.name = 'loadMap'
		$.push($.resizeFuncs,$.getDivBlock(),loadMap,'loadMap')
		console.log('aaaaaaaaaaaaaa')
		$.onLeftThisPage('hideMap',function(){
			$map.style.top = '-15000px'
		})
		loadMap()
		// c'est génant quand on scroll si google map intercepte le scroll
		// donc on ne l'active qu'au clic sur la map
		$.on('click','#map',function(){
			this.classList.add('interact');
		});
		$id('ajaxContent').appendChild($map)
		setTimeout(function(){
			var iframe = $id('mapFram')
			iframe.src="../noIndex/map.html"
		})
		$map.classList.remove('hide')
		$.cache.mapF = 'true'
	}else{
		$.loadMap()
	}
</script>
<script type="text/whenDocumentReady" data-name="mapScrolrotection">
</script>
