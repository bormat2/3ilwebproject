<?php 
	if(isset($post['resId'])){
		$post['room_ids'] = explode(',', preg_replace('/\s+/', '', $post['room_ids']));
		if(count($post['room_ids']) > 0 ){
			$interval = + date_diff(new DateTime($post['d_checkin']), new DateTime($post['d_checkout']))->format('%R%a');
			$param =  [
				'checkin' 		=> 
						formatDate(new \DateTime($post['d_checkin']),['mode' => 'short']),
				'checkout' 		=> 
						formatDate(new \DateTime($post['d_checkout']),['mode' => 'short']),
				'totalPrice'    => $post['price'],
				'interval' 		=> $interval,
				'pricePart1' 	=> $post['price'] * 0.3,
				'diffPrice'     => $post['price'] * 0.7,
				'nbStudio' => count($post['room_ids']),
				'nbPers' => $post['nbPers']
			];
			$successModif = $db->updateReservation($post);
			if($successModif){
				switch ($post['confirmed']) {
					case -1:
						break;
					case 0:
						$title = 'Refus de votre réservation';
						$param['templateUrl'] = '/noIndex/emailRefused/';
						break;
					case 1:
						list($title,$param['templateUrl']) =  $post['paid']
						? ['Confirmation de votre réservation', '/noIndex/emailConf2/']
						: ['Veuillez confirmer votre réservation', '/noIndex/emailConf1/'];
						break;
				}
			}else{?>
				<script type="text/whenDocumentReady">
					document.body.scrollTop = document.documentElement.scrollTop = 0
				</script>
				<p style="color:#9a2f26; opacity:.8;background: white"><?= $db->lastError ?></p><?php
			}		
		}
		if(isset($param['templateUrl'])){
			$param['url'] = $_GET['path'].'/emailViewer?token='.base64_encode(
					json_encode($param));
			!$post['noEmail'] and sendMail([
				'title' => $title,
				'to' => $post['mail'],
				'hmtlMode' => true,
				'templateUrl' => $param['templateUrl'],
				'vars' => $param
			]);
		}

	}
?>