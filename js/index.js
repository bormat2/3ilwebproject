(function(){
	// return un tableau d'élément Dom
	var $ = function(selector){
		return document.querySelectorAll(selector)
	}

	$.getIEVersion = function(){
	  var sAgent = window.navigator.userAgent;
	  var idx = sAgent.indexOf("MSIE");
	  // If IE, return version number else 0.
	  if(idx < 0) return navigator.userAgent.match(/Trident\/7\./) ? '11' : 0
	  return +(sAgent.substring(idx+ 5, sAgent.indexOf(".", idx)));
	}

	$.appendErrorToForm = function($form,errorCode){
		var errorBlock = $form.querySelector('.bigBlockError')
		if(!errorBlock){
			$errorBlock = $.create('<div class="bigBlockError"></div>')
			$form.$insert($errorBlock,'afterbegin')
		}else{
			$errorBlock.classList.add('multipleError');
		}
		$errorBlock.$insert('<span class="error">' + $.transDomString(errorCode) + '</span>')
	}

	/**
	*	retourne une traduction dans un élement DOM en string qui garde le code de la traduction
	*	dans data-trad pour pouvoir mettre la jour la valeur de la traduction dynamiquement
	*/
	$.transDomString = function(codeTranslation){
		return '<span data-trad="' +codeTranslation+'" >'+ $.translate(codeTranslation) + '</span>'
	}

	$.resetError = function($form){
		var errorBlock = $form.querySelector('.bigBlockError')
		errorBlock && errorBlock.remove()
	}

	$.resizeFunc = function(){
		$.diffTime('resize')
		var isOs = navigator.userAgent.match.bind(navigator.userAgent)
		if(!$.body.matches('.forcePhonePc')){
			var cList = $.body.classList
			var availableClasses = ['phone','ipad','pc','cinema']
			availableClasses.each(function(clasN){
				cList.remove(clasN)
			})
			if(window.innerWidth < 500 || isOs(/iPhone/i) || isOs(/iPod/i) /*|| isOs(/Android/i)*/){
				cList.add('phone')
			}else if(window.innerWidth < 800){
				cList.add('ipad')
			}else if(window.innerWidth < 2000){
				cList.add('pc')
			}else{
				cList.add('cinema')
			}
		}

		var $s = $id('widthContentStyle')
			|| $.create('<style id="widthContentStyle"></style>')
		$id('fakeBody').classList.remove('opacity0')
		var sbWidth = getScrollbarWidth()
		var $fBody = $0('#fakeBody')
		// var $body = document.body
		// $body.style.transform = "translateZ(0)";
		// var clientW = $fBody.clientWidth//you can get here the value you want
		// $body.style.transform = "";//otherwise it will work only once

		var get_safe_value = function(elm,callback){
			var sty = elm.style
			sty.transform = "translateZ(1px)";
			var ret = callback(elm)//you can get here the value you want
			// var temp = elm.offsetHeight;
			sty.transform = "";
			return ret
		}
		// for safari to have the good clientWidth
		var clientW = $.repaint(function(elm){return $fBody.clientWidth},$fBody)
		// var clientW = $fBody.clientWidth

		console.error($fBody.clientWidth,document.body.clientWidth,$fBody.offsetWidth,document.body.offsetWidth)
		if($fBody.scrollHeight != $fBody.clientHeight){
			clientW += sbWidth
		}

		var w = clientW - sbWidth
		if($.body.matches('.forcePhonePc')){
			w = 1080;
		}
		var padding = Math.max(17,sbWidth);
		$s.innerHTML = '.widthContent{width:' + w + 'px !important}'
			+ '.maxWidthContent{max-width:' + w + 'px}'
			+ '.containerPaddingV2{width:' + (clientW - 2*padding)+ 'px;padding-left:' + padding + 'px}'
			+ '.maxWidthContentV2{max-width:' + (clientW - 2*padding - sbWidth) + 'px}'
			+ '.clientW{width:' + clientW + 'px}'
		$.body.$insert($s)

		$.diffTime('beforeautoheight')
		if($.body.classList.contains('ie11Lte')){
		// certaines propriétés css ne marche que quand le parent à une taille définit sous ie
		// le code ci-dessous va affecté la taille de l'élément à son style css
		// il suffit pour cela de mettre l'atribut auto-heigth à l'élément
			/*var autoHeight = $('*[auto-height]');
			$.eachA(autoHeight,function($el){
				$el.style.height = ""
			})
			$.eachA(autoHeight,function($el){
				$el.style.height = $el.offsetHeight + 'px'
			})*/
		}
		$.diffTime('afterautoheight')
		$.diffTime('afterFakePare')
		$.repaint()
		var fPfunc = function(){
			return;
			var $fakeParent = $('*[data-fake-parent]')
			$fakeParent.each(function($el){
				var $fp  = $0('.page.active ' + $el.dataset.fakeParent)
				if($fp){
					$el.style.top  =  $fp.offsetTop  + 'px'
					$el.style.left =  $fp.offsetLeft + 'px'
				}else{
					$el.style.top  = '-55000px'
				}
			})
		}
/*		fPfunc()
		setTimeout(fPfunc,2000)
		setTimeout(fPfunc,10000)*/
		$.diffTime('afterRepaint')

		$.recalculateCssRules()
		$.eachO($.resizeFuncs[$.getDivBlock()],function(callback){
			callback()
		})
		$.diffTime('recalculateCssRules')
		document.body.classList.remove('opacity0')
		$.diffTime('opacity0')
	}

	$.updateScrollbar = function(){
		var container = $id('fakeBody');
		Ps.update(container);
	}

	function getScrollbarWidth() {
	    var outer = document.createElement("div");
	    outer.style.visibility = "hidden";
	    outer.style.width = "100px";
	    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

	    document.body.appendChild(outer);

	    var widthNoScroll = outer.offsetWidth;
	    // force scrollbars
	    outer.style.overflow = "scroll";

	    // add innerdiv
	    var inner = document.createElement("div");
	    inner.style.width = "100%";
	    outer.appendChild(inner);

	    var widthWithScroll = inner.offsetWidth;

	    // remove divs
	    outer.parentNode.removeChild(outer);

	    return widthNoScroll - widthWithScroll;
	}

	$.initScript = function(){
		// on stocke les element Dom les plus courrant pour les perfs
		$.cache = {};
		$.body = document.body
		$.fakeBody = $id('fakeBody')
		$.menuActiveStyle = $id('menuActiveStyle')
		$.toggleClass = $.body.classList.toggle.bind($.body.classList)
		if($.getIEVersion()){
			$.toggleClass('ie11Lte',!0)
		}
		$.addPrototypes()
		$.addFunctionToDom()
		$.initEventOnPage()
		$.initNavigation()
		$.updateState()
		$.executeScripts($('script[type="text/whenDocumentReady"]'))
		$.displayPage($.body.dataset.currentPage)
		$.setLang($.getLang())
		var resizeTimer;
		window.addEventListener('resize',function(){
			if(!resizeTimer){
				$.body.classList.add('resize')
			}
		    clearTimeout(resizeTimer);
		    resizeTimer = setTimeout(function(){
		    	$.body.classList.remove('resize')
		    	resizeTimer = null
		    	$.resizeFunc()
		    }, 100);
		});
	}

	$.endPage = function(){
		$.resizeFunc()
	}
	window.$ = $

	$.passW3C = function(){
		$('img,area').each(function(img){
			img.alt = "image";
		})
		$('span[value]').each(function(span){
			span.removeAttribute('value');
		})
		//$0('a[target="_new"]').remove()
		$('script[charset="UTF-8"]').each(function($el){
			$el.remove()
		})
		$('script,link').each(function($el){
			$el.remove()
		})

		/*$('div[controlwidth],div[controlheight]').each(function(div){
			div.removeAttribute('controlwidth')
			div.removeAttribute('controlheight')
		})*/
		$id('map') && $id('map').remove()

		return '<!DOCTYPE html>\n' + $0('html').innerHTML
	}
})()
