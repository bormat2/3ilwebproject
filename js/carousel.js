(function(){
    var widthCarousel;
    var o = {
        big_carroussel: 2 ,
        instances: {},
        initListener: function(){
            $.css('.largeContainer .js-container-img',function(){
                widthCarousel = $0('.backgroundCenter').clientWidth
                return {
                    width: widthCarousel + 'px',
                    then: function(){
                        $('.carouselParent').each(function($el){
                            o.goTo($el,0);
                        })
                    }
                }
            })
            $.on('click','.carouselParent .js-prev',function(e){
                o.goTo(e.target.closest('.carouselParent'),-1)
            },1)
            $.on('click','.carouselParent .js-next',function(e){
                o.goTo(e.target.closest('.carouselParent'),1)
            },1)
            $.on('click','.carouselParent .js-miniature',function(e){
                var parentCarous = e.target.closest('.carouselParent')
                dispImg(parentCarous,this.dataset.key)
            },1)
        },
        getCreateInstanceOfCarousel : function(el){
            var uniqId  = el.dataset.uniqId = el.dataset.uniqId || $.uniqId()
            o.instances[uniqId] = o.instances[uniqId] || {
                trns1 : 0,
                trns2 : 0,
                lastKey : 0,
                $parent : el
            }
            return o.instances[uniqId]
        },
        goTo : function(parentCarous,plusMoins1){
            // really move in dom images, otherwise we can't go from the last image
            // to the first image with translation that move image to left
            var move_image_to_allow_circular_translation = function(){
              if(pictures.length > o.big_carroussel){
                  var i = -1
                  var to = position_visible_img + plusMoins1
                  if(plusMoins1 > 0 && (position_visible_img + plusMoins1) == pictures.length || to <= 0 && plusMoins1 < 0 ){
                    pictures[0].parentElement.$insert(pictures[plusMoins1 > 0 ? 0 : pictures.length - 1],plusMoins1 > 0 ? 'beforeend' : 'afterbegin')
                    inst.trns1 += plusMoins1
                    //translation without animation to have the same image visible that before move_image_to_allow_circular_translation
                    change_translation('trns1', '.largeContainerParent')
                  }
                  // while(inst.trns2 + inst.trns1 !== to){
                  //     pictures[0].parentElement.$insert($.at(pictures,neg ? ++i : i--),neg ? 'beforeend' : 'afterbegin')
                  //     inst.trns1 += neg ? 1: -1
                  //     //translation without animation to have the same image visible that before move_image_to_allow_circular_translation
                  //     change_translation('trns1', '.largeContainerParent')
                  // }
              }
            }

            var change_translation = function(trns_n,class_n){
              // if the translation have change update the css
              if(old_val[trns_n] != inst[trns_n]){
                var trns = parentCarous.$0(class_n)
                trns.classList.add('carrouss-'+trns_n)//debug only
                trns.style.transform
                        = 'translate(' + inst[trns_n] * widthCarousel + 'px' + ', 0)'
              }
            }

            var pictures = parentCarous.$('.largeContainer .js-container-img')
            var pictures_order = []
            pictures.each(function(elm, i){
              var data_key = elm.$0('*[data-key]')
              if(!data_key){
                data_key = elm.$0('*')
                data_key.dataset.key = i
              }
              pictures_order.push(+data_key.dataset.key)
            })
            var inst = o.getCreateInstanceOfCarousel(parentCarous)
            var position_visible_img = pictures_order.indexOf(inst.lastKey)

            var old_val = {trns1:inst.trns1, trns2:inst.trns2}
            if(pictures.length <= o.big_carroussel){
                if(inst.lastKey==pictures.length-1){
                    plusMoins1 = - Math.abs(plusMoins1);
                }else if(inst.lastKey == 0){
                    plusMoins1 = Math.abs(plusMoins1);
                }
            }

            var old_to = inst.trns2 + inst.trns1
            inst.trns2 -= plusMoins1;
            move_image_to_allow_circular_translation()

            //translation with animation
            change_translation('trns2', '.largeContainer')
            inst.lastKey = $.mod(inst.lastKey + plusMoins1,pictures.length)
        }
    }

    $.carousel = function($carouselParent){
        var inst = o.getCreateInstanceOfCarousel($carouselParent)
        o.initListener ? o.initListener() : o.initListener = 0
    }

    var dispImg = function(parentCarous,lastK){
        var inst = o.getCreateInstanceOfCarousel(parentCarous)
        var miniatures = parentCarous.querySelector('.js-miniature')
        var mod = miniatures.length
        var dec = lastK - inst.lastKey;
        var sign = dec < 0 ? -1 : 1

        //distance entre l'ancienne et la nouvelle miniature
        var abs = Math.abs(dec)

        // faire le tour
        if(mod - abs < abs && abs == 1 || miniatures.length == abs + 1){
            abs = mod - abs - 1;
            sign *= -1
        }
        // se déplacer à la bonne image
        while(abs--){
            o.goTo(parentCarous,sign)
        }
    }
})()
