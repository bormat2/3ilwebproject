(function() {
	/*-------------------- event listener  --------------*/

	$._events = {
		/*click: [{selector,callback,priotity}]*/
	}
	/*
	* ajoute un evenement via délegation sur un enfant du document.body
	* le callback est exécuté si le selecteur correspond à l'element
	* on peut ordonner les callback grace à priorité plus c'est grand plus c'est priotitaire
	*/
	var idEvent = 0
	$.on = function(event, selector, callback, priotity_) {
		if (!$._events[event]) {
			// si le tableau pour cet event existe pas le crée
			// et initialiser le listener sur le body
			$._events[event] = []
			document.body
				.addEventListener(event, function(event) {
					var $target = event.target
					//on boucle des events les plus prioritaires au moins prioritaires
					$
						._events[event.type]
						.each(function(listener) {
							// si return false on s'arrête
							return executeCallbackIfMatchSelector($target, listener.selector, listener.callback, event)
						})
				}, false)
		}
		$
			._events[event]
			.push({
				selector: selector,
				callback: callback,
				priotity: priotity_ || 0,
				id: ++idEvent
			})
		$
			._events[event]
			.sort(function(a, b) { //greater at first
				return b.priotity - a.priotity || b.id - a.id
			})
	}

	$.whenTransitionEnd = function($el, callback) {
		if (!$._transitionEventName) {
			var transitions = {
				'WebkitTransition': 'webkitTransitionEnd',
				'transition': 'transitionend',
				'OTransition': 'oTransitionEnd',
				'MozTransition': 'transitionend'
			}

			var undefined;
			transitions.each(function(transitionEventName, t) {
				if ($el.style[t] !== undefined) {
					$._transitionEventName = transitionEventName;
					return false
				}
			})
		}
		/*		if($._transitionEventName){
*/
		var eventCallback = function() {
			callback.apply(this, arguments)
			$el.removeEventListener($._transitionEventName, eventCallback);
		}
		$el.addEventListener($._transitionEventName, eventCallback);
		/*		}else{
			callback.call($el)
		}*/
	}

	var executeCallbackIfMatchSelector = function($el, selector, callback, event) {
		if (!$el) return
		var $closestMatch = $el.closest(selector)
		if ($closestMatch) {
			// si la fonction a return true alors on passe au parents
			// la plupart du temps ce n'est pas le comportement souhaité
			if (callback.call($closestMatch, event) == true) {
				executeCallbackIfMatchSelector($closestMatch.parentNode, selector, event)
			} else {
				return false
				// on empêche le reverseEach de tester les autres selectors
			}
		}
	}

	$.initEventOnPage = function() {
		var bodyClick = function(event) {
			$.toggleClass('open', false)
			$id('langChoice')
				.classList
				.remove('active2')

		}
		$.on('click', 'body', function(event) {
			bodyClick(event)
		})

		$.on('click', '#menuBlock', function() {
			$.toggleClass('open')
		})
		$.on('click', '.reload', function() {
			$.reloadPage()
			bodyClick(event)
		})

		$.on('click', '#langChoice', function() {
			this
				.classList
				.toggle('active')
		})

		/**
		*	On bloque le comportement normal des liens pour faire de l'ajax à la place
		*/
		$.on('click', 'a[href]:not([target="_blank"])', function(event) {
			if(event.which==2) return;//middle click for ie
			if(this.closest('.js-externalLink')) return true;
			$.loadAndAddToHistory(this.getAttribute('href'))
			event.preventDefault()
			bodyClick(event)
		})

		$.on('click', '#langChoice a', function(event) {
			event.preventDefault()
			var parent = this.closest('#langChoice')
			if (parent.matches('.active2')) {
				$.reloadTrads(this.lang)
				$.addState()
			}
			parent
				.classList
				.toggle('active2')
		})

		$.on('click', '#pcToPhone', function() {
			var cList = $.body.classList
			if(!cList.contains('forcePhonePc') && (cList.contains('phone') 
				|| cList.contains('ipad') || cList.contains('cinema'))){
				cList.remove('phone');
				cList.remove('ipad')
				cList.remove('cinema')
			} 
			cList.toggle('forcePhonePc')
			cList.toggle('pc')
			$.resizeFunc()
			$.body.scrollLeft = 5000;
		})

		//////////////

		var isInit = false;
		$.on('touchstart','#fakeBody',function(e){
			if(isInit)return true;
			isInit = true
			$.toggleClass('touchScreen',true)
			return true;
		})

		/* formulaire envoie par ajax
		*
		*/
		$.on('submit', 'form', function(event) {
			event.preventDefault()
			$.sendForm(this)
		}, -1)

		$.on('change','input[required],input[add-empty-selector]',function(){
			this.classList.toggle('isNotEmpty',this.value.length > 0)
			return true
		})

	}
})()
