/*--------------------ajax and navigation ------------------------*/
(function(){
	var lastAjaxRequest = []
	var iAjax= -1;
	$.ajax = function(param){
		$.bar = $.bar || new ProgressBar.Line($id('progressbar'), {
		  strokeWidth: 4,
		  easing: 'easeInOut',
		  duration: 300,
		  color: '#9a2f26',
		  trailColor: 'transparent',
		  trailWidth: 0,
		  svgStyle: {width: '100%', height: '100%'}
		});
		lastAjaxRequest[(++iAjax)%5] = param
		var req = new XMLHttpRequest()
		req.open("POST", param.url)
/*		req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
*/		var success,error;
		req.onreadystatechange = function (aEvt) {
			if(req.readyState == 4){
				if(req.status == 200){
					param.success(req.responseText)
					success = true
				}else{
					error = true
					console.log("Erreur pendant le chargement de la page.\n");
				}
			}
		};

		var currentWidth = 0;
		var $progressBar =  $.create('<div class="progressBar"></div>')
		// var $spinClass = $0('.reload').classList
		$.body.$insert($progressBar,'beforeend')

		var i = 2;
		var clignote = function(color){
			$.bar.animate(1);
			setTimeout(function(){
				$.bar.set(0);
			},500)
		}
		var requestAnimFrame = (function(){
	      return  window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function( callback ){ window.setTimeout(callback, 1000 / 60); };
	    })();
		// on met à jour la progressBar jusqu'à ce que la requête soit finie
		// on la supprime alors après 500ms
		var updateWidth = function(){
			// $spinClass.add('spin')
			if(success || error){
				// $spinClass.remove('spin')
				if(success){
					clignote('#9a2f26')
				}else{
					clignote('red')
				}
				return;
			}
			currentWidth += getInc(currentWidth)
			//$progressBar.style.transform ='translate3d(' + -(100 - currentWidth)+ '%,0,0)'
			$.bar.animate((100 - currentWidth)/100);
			/*$.repaint()*/
			/*$progressBar.offsetWidth */// repaint
			setTimeout(function(){
				updateWidth()
			},20)
		}

		var getInc = function(currentWidth){
			currentWidth /= 100
			if (currentWidth < 0.1) {
			  amount = 0.1 ;
			} else if (currentWidth < 0.15) {
			  amount =  5 ;
			} else if (currentWidth < 0.65) {
			  amount =  3 ;
			} else if (currentWidth < 0.9) {
			  amount = 1 ;
			} else if (currentWidth < 0.95) {
			  amount = 0.05;
			} else if (currentWidth < 0.99) {
				amount = 0.001;
			} else {
			  amount = 0;
			}
			return amount
		}
		updateWidth()
		var data = param.data || {toto:"yes"}
		data.lang = $.getLang()
		var dataForm = new FormData();
		if(param.files){
			for(var i = param.files.length;i--;){
				var files = param.files[i].files
				for(var j = files.length;j--;){
					dataForm.append(param.files[i].name,files[j])
				}
			}
		}
		dataForm.append('data',JSON.stringify(data))
		if(data.isPost){
			dataForm.append('isPost',true)
		}
		req.send(dataForm)
/*		req.send('data='+JSON.stringify(data))
*/	}

	$.ajaxGet = function(url,successScripts){
		var req = new XMLHttpRequest()
		req.open("GET",url,true)
		req.onreadystatechange = function (aEvt) {
			if(req.readyState == 4){
				if(req.status !== 200){
					error = true
					console.log("Erreur pendant le chargement de la page.\n");
				}
				successScripts(req.responseText)
			}
		};
		req.send(null);
	}

	$._onLeftThisPage = {}
	$.onLeftThisPage = function(name,callback){
		$.push($._onLeftThisPage,$.getDivBlock(),callback,name)
	}


	$.loadAndAddToHistory = function(page,opt){
		//$.updateState()
		$.loadPage(page,opt).then($.addState)
	}

	$.setPage = function(page){ $.body.dataset.currentPage = page }
	$.getPage = function(){
		var page = $.body.dataset.currentPage.split('.php')[0]
		return page
	}
	$.setDivBlock =  function(page){ $.body.dataset.divBlock = page }
	$.getDivBlock = function(){
		return $.body.dataset.divBlock || $.body.dataset.currentPage
	}

	$.setLang = function(lang){
		$.body.parentNode.lang = lang;
		var oldLang = $0('#langChoice a.active')
		oldLang && oldLang.classList.remove('active')
		$0('#langChoice a[lang='+ lang +']').classList.add('active')
	}
	$.getLang = function(){ return $.body.parentNode.lang }
	$.getUrl = function(){
		var getsParams = window.location.href.replace(/.*?([?#]|$)/,'$1')
		return $_GET['path'] + '/'+ $.getLang() + '/'  + $.getPage() + getsParams
	}
	$.addState = function(){
		var s = $.getState()
		window.history.pushState(s, 'title',s.url)
	}
	$.updateState = function(){
		var s = $.getState()
		window.history.replaceState(s, 'title',s.url)
	}
	$.getState = function(){
		return {
			url: $.getUrl(),
			page: $.getPage(),
			lang: $.getLang(),
			divBlock: $.getDivBlock()
		}
	}

	$.sendForm = function($form,params){
		params = params || {}
		var json = {
			isPost: true
		}
		$form.querySelectorAll('input,select,textarea').each(function(elm){
			if(elm.type == "radio" && !elm.checked){
				return;
			}
			if(elm.type == 'checkbox'){
				json[elm.name] = elm.checked;
				return;
			}
			json[elm.name] = elm.value;
		})
		var files = $form.querySelectorAll('input[type="file"]')
		if(params && params.keepPage){
			params.data = json
			$.ajax(params)
		}else{
			$.loadAndAddToHistory(null,{
				url: $form.action || window.location.href,
				data: json,
				files : files
			})
		}
	}

	//recharge les textes dans des span[data-trad]
	$.reloadTrads = function(lang){
		$.setLang(lang)
		$('span[data-trad]').each(function($p){
			$p.innerHTML = $.translate($p.dataset.trad, lang)
		})
	}

	$.loadPage = function(page,opt){//opt{data,url}
		var t = $.time()
		opt = opt || {}
		var changePage = function(page){
			$.eachO($._onLeftThisPage[$.getDivBlock()],function(callback){
				callback();
			})
			$.setPage(page);
		}
		if(page){
			/*$.setDivBlock('');*/
			changePage(page);
			opt.url = $.getUrl()
		}
		var prom = $.promise()
		opt.success = function(data){
			page = data.match(/<!--page=(.*?)-->/);
			var divBlock = data.match(/<!--divBlock=(.*?)-->/);
			if(!page){
				console.error(data);
				return;
			}
			page = page[1];
			changePage(page);
			divBlock = divBlock && divBlock[1] || page;
			$.setDivBlock(divBlock);
			var $page = $id(divBlock)
			if($page){
				$page.querySelectorAll('iframe,frame').each(function(iframe){
					iframe.bortoOnload && iframe.bortoOnload()
					iframe.contentWindow.document.body.outerHTML = ""
				})
				$page.remove()
				//$page.innerHTML = 'data' //dont use bug with ie, loosing reference
				$0('#ajaxContent')
				.$insert(
					$page,
					'afterbegin'//au debut car permet de ne pas casser les sélecteur par id qui stop au premier
				)
			}
			$newPage = $.create('<div class="page f-container fill column" id="'+ divBlock +'">'+data+'</div>')
			if($.body.classList.contains('ie11Lte')){
				$newPage.$('.doNotInsertNextIe11Lte').each(function($el){
					$el.nextElementSibling.remove();
				})
			}
			$0('#ajaxContent').$insert($newPage,'afterbegin')
			$.displayPage(divBlock || page)
			$.executeScripts($('script[type="text/whenDocumentReady"],'
				+'.page.active script[type="text/whenDocumentReadyAndOnPopState"]'),function(){
				$.resizeFunc()
				var t4 = $.time()
			})
			prom.setOk()
		}
		$.ajax(opt)
		return prom;
	}

	$.reloadPage = function(){
		$.loadPage($.getPage())
	}
/*	$.addHistory = function(url_){//
		window.history.pushState('', '', url_ || window.location.href);
	}*/
	$.displayPage = function(page){
		var oldPage = $0('.page.active')
		oldPage.classList.remove('active')
		$id(page).classList.add('active')
		var last = $0('header li.active')
		last && last.classList.remove('active')
		var $a = $0('header a[href="'+ page +'"]')
		$a && $a.closest('li').classList.add('active')
		document.title = page
		try{
			var $sponsor = $0('#sponsors-and-reviews')
			if($sponsor){
				$sponsor.classList.toggle('sponsHidden',['activity','home'].indexOf(page) < 0)
			}
		}catch(e){
			console.error(e)
		}

	/*	if(oldPage.id != page){
			$id('fakeBody').scrollTop = 0;
		}*/
	}
	$.getAllFromUrl = function(){
		var res = window.location.href.split(/[?&=]/)
		var retu = {}
		//le premier et la clef et le second la valeur
		for(var i = 1;i<res.length;i+=2){
			retu[res[i]] = res[i+1]
		}
		return retu
	}

	/*
	* non optimisé
	* set les clefs et valeurs d'un objet dans l'url
	*/
	$.setAllToUrl = function(obj){
		obj.each(function(value,i){
			$.setToUrl(i,value)
		})
	}

	/*
	* normalise une url(suppression des doublons pour la valeur) et change le paramètre souhaité
	* pour supprimer un param mettre une value à 0,false,'' ou null
	* si vous voulez mettre un 0 comme valeur le mettre entre quotes
	*/
	$.setToUrl = function (param, value) {
		if(!param || param === '' || /[&=?§]/.test(param + value)){
			throw " param invalid can't contains [&=?§]"
		}
		var rep = function(a,b){newURL = newURL.replace(a,b)}
		var newURL = window.location.href.replace(/[?&]+/g,'&') +'&§'  //on met &§ à la fin et remplace le ? par des &
		rep(new RegExp('&'+param+'.*?=[^&]*', 'g'),'&§') //on remplace si il existe le vieux param et sa valeur par &§ (plusieurs fois si nécessaire)
		value && rep('&§','&'+param+'='+value) //on met la nouvelle valeur au premier &§ si valeur il y a
		rep(/&+§/g,'') // on enleve les &§ restant
		rep('&','?') // met un ? à la place du premier &
		window.history.replaceState('', '', newURL );
	}



	$.initNavigation = function(){
		window.onpopstate = function(event) {
			if($.disablePopState) return;
			var state = window.history.state || $.getState()
			var page = state.page
			var oldLang = $.getLang()
			$.setLang(state.lang)
			$.setPage(page)
			var divBlock = state.divBlock || state.page;
			$.setDivBlock(divBlock)
			if($id(divBlock)){
				$.displayPage(divBlock)
				$.executeScripts($('.page.active script[type="text/whenDocumentReadyAndOnPopState"]'),null,{})
				$.resizeFunc()
			}else{
				$.loadPage(page)
			}
			var newLang = $.getLang()
			if(newLang != oldLang){
				$.reloadTrads(newLang)
			}
		    console.log(event)
		}
	}
})()
