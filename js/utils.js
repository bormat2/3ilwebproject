(function(){
	var __obj = Object.prototype
	var __arr = Array.prototype
	var __str = String.prototype

	$.promise = function(){
		var callB;
		var load = false
		var obj = {
			then: function(callback){
				if(load){
					callback()
				}else{
					callB = callback
				}
			},
			setOk: function(){
				callB && callB() || (load = true)
			}
		}
		return obj
	}

	$._initTime;
	$.diffTime = function(name){
		console.log(name, - $._initTime + ($._initTime = $.time()))
	}

	$.push = function(obj,arrName,value,_name){
		console.log(arrName)
		var arr = obj[arrName]
		if(!_name){
			(obj[arrName] = arr || []).push(value)
		}else{
			(obj[arrName] = arr || {})[_name] = value
		}
	}

	$.addPrototypes = function(){
		/*--------------------utilitaire-------------------------*/
		/*
		* ajoute au objets et array la fonction each
		* pour boucler sur les objets __obj.each et Array __arr.each
		* si le callback func return false alors la boucle s'arrête
		*/

		Object.defineProperty(__obj, "each", {
			enumerable: false,
			writable: true,
			value:function(func){
				var arr = this;
				var keys = Object.keys(arr)
				for(var i = 0,lg=keys.length;i < lg;++i){
					var key = keys[i]
					if(func(arr[key],key,i) === false){
						break;
					}
				}
				return {
					elseach: function(callback){ i == 0 && callback && callback() }
				}
			}
		});

		Object.defineProperty(__arr, "each", {
			enumerable: false,
			writable: true,
			value:function(func){
				var arr = this;
				for(var i = 0,lg=arr.length;i < lg;++i){
					if(func(arr[i],i)==false){
						break;
					}
				}
				return {
					elseach: function(callback){ i == 0 && callback && callback() }
				}
			}
		});

		Object.defineProperty(__arr, "last", {
			enumerable: false,
			writable: true,
			value:function(){
				return this[this.length-1]
			}
		});

		__arr.reverseEach = function(){
			for(var i = arr.length;i--;){
				if(func(arr[i],i)==false){
					return;
				}
			}
		}


		//ajoute aux string et array la fonction has qui retourne si une chaine ou un tableau possède une valeur
		__str.has = __arr.has = function(val){
			return ~this.indexOf(val)
		}
	}



	$.time = function(){
		return (new Date).getTime()
	}

	$.has = function(arr,val){
		return arr.indexOf(val) > -1 
	}
	/*
	* return la même function mais qui ne peut
	* s'executer plus d'une fois dans un intervalle donné
	* attention: NE MODIFIE PAS LA FONCTION D ORIGINE
	*/
	$.setMinInterval = function(callback,interval){
		var time = $.time();
		return function(){
			var newTime = $.time();
			var diff = newTime - time;
			if(diff < interval){
				return
			}
			newTime = time;
			return callback.apply(this,arguments)
		}
	}

	/* return un id unique pour toute l'application */
	var uniqId = 1;
	$.uniqId = function(){
		++uniqId
		return 'uniqId' + uniqId
	}

	$.getCookie = function(name){
		var reg = new RegExp(name+'=([^;]*)','g') //on prend du '$name=' exclus jusqu' au ';'
		var res = reg.exec(document.cookie);
		if(!res)return false;
		return res[1]
	}

	$.setCookie = function(name,value){
		document.cookie = name+'='+value
	}

	/* stocke en local des chaine de caractère */
	$.store = function(name,value){
		localStorage.setItem(name,value)
	}
	$.getStored = function(name){
		localStorage.getItem(name)
	}

	$.last = function(arr){
		return arr[arr.length-1]
	}
	$.at = function(arr,i){
		if(i < 0){
			i += arr.length
		}
		return arr[i]
	}
	$.mod = function(a,n){
        return a - n * Math[n > 0 ? 'floor' : 'ceil'](a/n);
    }
    //each for array
    $.eachA = function(arr,func){
		for(var i = 0,lg = arr ? arr.length : 0;i < lg;++i){
			if(func(arr[i],i)==false){
				break;
			}
		}
		return {
			elseach: function(callback){ i == 0 && callback && callback() }
		}
    }
    //each for object
    $.eachO = function(arr,func){
		var keys = arr ? Object.keys(arr) : []
		for(var i = 0,lg=keys.length;i < lg;++i){
			var key = keys[i]
			if(func(arr[key],key,i) === false){
				break;
			}
		}
		return {
			elseach: function(callback){ i == 0 && callback && callback() }
		}
    }
})()
