var idChambres = {}
var nbPlaces = {
	1:3,
	4:2,
	/* real*/
	5:3,
	6:3,
	7:3,
	8:3,
	13:3,
	14:2,
	15:2,
	16:4,
	18:2
}
console.log('book2.js')
$.on('click','.js-roomPres',function(){
	this.classList.toggle('selected')
	var newPlaces = +$id('nbPers').value
	$('.page.active .js-roomPres.selected').each(function($el){
		newPlaces -= nbPlaces[$el.dataset.room]
	})
	if(newPlaces < 0) newPlaces = 0
	$id('nbPersRes').innerHTML = newPlaces;

	prepareForsubmit()
	var $f = $id('bookValidation')
	$.sendForm($f,{
		keepPage: true,
		url: 'getPrice.php',
		success: function(price){
			$0('.js-priceBookEstimation').innerHTML = price;
		}
	})

})

var prepareForsubmit = function() {
	var form = $id('bookValidation')
	var $idChambre = form.elements['idChambre']
	var val = []
	$('.page.active .js-roomPres.selected').each(function($el){
		val.push($el.dataset.room)
	})
	$idChambre.value = val.join(' ')
	console.log($idChambre.value)
}
$.on('submit','#bookValidation',function(event){
	// event.preventDefault()
	try{
		var $form = this
		prepareForsubmit()
		$.resetError($form)
		var error = 0;
		$form.querySelectorAll('input[required]').each(function($inpt){
			if(!$inpt.value){
				$.appendErrorToForm($form,'requiredNotFilled')
				++error
				return false;//stop loop
			}
		})
		if($id('nbPersRes').innerHTML > 0){
            $.appendErrorToForm($form,'errorPersonNotAffected')
            ++error;
		}
        if(error){
        	event.preventDefault();//stop la validation
		}else{
			this.querySelector('button').disabled = true;
			return true;//propager l'event
		}
	}catch(e){
		console.error(e)
	}
})
