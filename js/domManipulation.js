/**
* finit par un _ = faculatif
* commence par un $ = élément ou fonction dom
* commence par un __ = prototype d'un objet
* propriété avec un _ = à considéré comme private
*/
(function(){//voir IIFE javascript si vous ne comprenez pas la syntaxe

	var __ell = Element.prototype;
	var __ellList = NodeList.prototype;

	/*--------------------Dom manipulation-------------------------*/



	/**
	*   exemple :
	*	$.css('body',function(){
	*       return {
				width: '100px',
				then: function(){
					"instruction à faire une foix le css à jour"
				}
			}
	*   })
	*/
	(function(){
		var cssRules = []
		$.css = function(selector,callback){
			var $style = $.create('<style />');
			(cssRules[cssRules.length] = function(){
				var css = selector + '{'
				callback().each(function(prop,propName){
					if(propName == 'then'){
						then = prop;
					}else{
						css += propName + ':' + prop + ';'
					}
				})
				$style.innerHTML = css + '}'
				then && then()
			})()
			$.body.appendChild($style)
		}
		$.recalculateCssRules = function(){
			cssRules.each(function(callback){
				callback()
			})
		}
	})()
	//Return un element au lieu d'un tableau à priviligier pour les perfs
	var $0 = function(selector){
		return document.querySelector(selector)
	}
	var $id = function(selector){
		return document.getElementById(selector)
	}
	__ell.$ = __ell.querySelectorAll
	__ell.$0 = __ell.querySelector

	var aElm;
	$.getAbsoluteUrl = function(url){(aElm = aElm || document.createElement('a')).href = url;return aElm.href}

	$._scripts = {}
	$.executeScripts = function($scripts,then,opt){
		opt = opt || {}
		var maxScripts = $scripts.length - 1;

		// function code OR 1 if don't need to be executed because script has a name and script with name are executed once
		var scripts2 = [];
		var lastExecuted = -1;

		// les scripts sont executés dès que possible mais dans l'ordre dans lequel ils sont rajouté.
		var successScripts = function(i,scriptCode){
			scripts2[i] = scriptCode || 1
			while(scripts2[lastExecuted + 1]){
				++lastExecuted
				if(scripts2[i] !== 1){
					try{
						var dataset = $scripts[lastExecuted].dataset
						var url = $.getAbsoluteUrl(dataset.src || dataset.name || lastExecuted + '.js')
						var f = new Function(scripts2[lastExecuted] + '\n//# sourceURL=' + url + $.uniqId());
						f.call({
							currentScript: $scripts[lastExecuted]
						})
					}catch(e){
						console.log(lastExecuted)
						console.error(e)
					}
				}
				// le script est supprimé pour ne pas être réexecuté à moins que dontRemove = true
				if(!$.has($scripts[i].type,"PopState")){
					$scripts[lastExecuted].remove()
				}
			}
			lastExecuted == maxScripts && then && then()
		}
		$scripts.each(function($script,i){
			if($script.dataset.name){//si il y a un nom on evalue que une fois le script
				if($._scripts[$script.dataset.name]){
					successScripts(i)//on ne passe pas le script il n'est pas executé
					return;
				}
				$._scripts[$script.dataset.name] = true
			}
			if($script.dataset.src){
				var req = new XMLHttpRequest()
				req.open("GET",$script.dataset.src,true)
				req.onreadystatechange = function (aEvt) {
					if(req.readyState == 4){
						if(req.status !== 200){
							error = true
							console.log("Erreur pendant le chargement de la page.\n");
						}
						successScripts(i,req.responseText)
					}
				};
				req.send(null);
			}else{
				successScripts(i,$script.innerHTML)
			}
		}).elseach(then)
	}
	$.executeUrlScript = function(url){
		var $s = document.createElement('script')
		$s.src = url
		$id('scriptExecutor').$insert($s)
	}

	var tmp = document.createElement('a')
	$.create = function(outerHTML){
		tmp.innerHTML = outerHTML
		return tmp.children[0]
	}

	$.creates = function(outerHTML){
		tmp.innerHTML = outerHTML
		return tmp.children
	}
	$.appendChilds = function($parentNode,$children){
		for(var i = $children.length;i--;){
			if($children[i]){
				$parentNode.appendChild($children[i])
			}
		}
		return $parentNode
	}

/*	$.toggleClass= function($coll,classes,_bool){
		classes = classes.split(' ')
		var toggle = Element.classList[
			typeof _bool == "undefined" ? 'toggle' : _bool ? 'add' : 'remove']
		$.eachA($.getArr($coll),function($dom){
			classes.eachA(function(clas){
				toggle.call($dom,clas.classList)
			})
		})
	}*/

	$.getArr = function($coll){
		if($coll.hasOwnProperty('length')){
			return $coll
		}
		return [$coll]
	}

	$.repaint = //function(callback){
        // $.body.style.display = 'none';
        // var temp = $.body.offsetHeight;
        // $.body.style.display = ""
        // temp = $.body.offsetHeight;
        // callback && callback()

			function(callback,elm){
					 elm = elm || document.body
					 var sty = elm.style
					 sty.transform = "translateZ(1px)";
					 var ret = callback && callback(elm)//you can get here the value you want
					 var temp = document.body.offsetHeight;
					 sty.transform = "";
					 return ret
	    }

    $.offset = function(topLeft,$el){
    	var x = 0
    	while($el){
			x += $el[topLeft];
			$el = $el.offsetParent;
    	}
    	return x;
    }

    $.left = $.offset.bind($,'offsetLeft')
    $.top = $.offset.bind($,'offsetTop')



	$.addFunctionToDom = function(){
			//tous les polyfills sont activés c'est pour tester dans les pires conditions l'application
		__ell.matchesSelector = __ell.mozMatchesSelector = __ell.msMatchesSelector = __ell.replaceWith
		= __ell.oMatchesSelector = __ell.webkitMatchesSelector = __ell.matches = __ell.closest = __ell.remove = null
		/**
		* polyfill pour matches
		* return si un element match un selector
		*/
		if (!__ell.matches) {
		    __ell.matches =
		        __ell.matchesSelector ||
		        __ell.mozMatchesSelector ||
		        __ell.msMatchesSelector ||
		        __ell.oMatchesSelector ||
		        __ell.webkitMatchesSelector ||
		        function(s) {
		            var matches = (this.document || this.ownerDocument).querySelectorAll(s),
		                i = matches.length;
		            while (--i >= 0 && matches.item(i) !== this) {}
		            return i > -1;
		        };
		}

		/*
		* return le premier element en partant de el compris vers le body qui match le selector
	    * @see https://dom.spec.whatwg.org/#dom-element-closest
	    */
	    if (!__ell.closest) {
		    __ell.closest = function(selectors) {
		        var element = this;
		        while (element) {
		        	if (element.nodeType === 1 && element.matches(selectors)) {
		        		return element;
		      		}
		      		element = element.parentNode;
		        }
		    	return null;
		    };
		}

	    if ( !__ell.remove) {
		    Element.prototype.remove = function() {
		        this.parentNode.removeChild(this);
		    };
		}

		if(!__ell.replaceWith){
			(function (arr) {
			    arr.forEach(function (item) {
			        item.replaceWith = item.replaceWith || function () {
			            var argArr = Array.prototype.slice.call(arguments),
			                docFrag = document.createDocumentFragment();

			            argArr.forEach(function (argItem) {
			                var isNode = argItem instanceof Node;
			                docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
			            });

			            this.parentNode.replaceChild(docFrag, this);
			        };
			    });
			})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);
		}
		/*
		* insert un outerHTML ou bien un element dom par rapport à $el
		*	<!-- beforebegin -->
		*	<p> <!-$el-->
		*	  <!-- afterbegin -->
		*	  foo
		*	  <!-- beforeend -->
		*	</p>
		*	<!-- afterend -->
		*
		* and replace to replace the element
		*/
		__ell.$insert = function(outerHTML,pos){
			$el = this
			if(pos == 'replace'){
				$el.$insert(outerHTML,'beforebegin')
				$el.remove()
				return
			}
			pos = pos || 'beforeend'
			if(typeof outerHTML == 'string'){
				$el.insertAdjacentHTML(pos,outerHTML)
			}else{
				switch(pos){
					case 'beforebegin' :  $el.parentNode.insertBefore(outerHTML, $el );break
					case 'afterbegin'  :  $el.insertBefore(outerHTML,$el.firstChild);break
					case 'beforeend'   :  $el.appendChild(outerHTML);break
					case 'afterend'    :  $el.parentNode.insertBefore(outerHTML,$el.nextSibling);break
					default: console.log('error '+ pos)
				}
			}
		}
	}


	//array of function to execute on resize
	$.resizeFuncs = []

	window.$0 = $0
	window.$id = $id
})()
