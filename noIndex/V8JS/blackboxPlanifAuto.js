{
	// addGetterStartHour: function(){
	// 	// Object.defineProperty(PeriodNeed.prototype, 'startHour', {
	// 	//     // get : function(){
	// 	//     // 	return this.startHour2;
	// 	//     // }
	// 	// });
	// },
	// addGetterStartHourInter: function(){
	// 	// Object.defineProperty(Intervention.prototype, 'startHour', {
	// 	//     get : function(){
	// 	//     	return this.startHour2;
	// 	//     },
	// 	//     set : function(val){
	// 	//     	this.startHour2 = val;
	// 	//     }
	// 	// });
	// },
	// anti boucle infinie
	add_iDebug_getter: (console,isV8js) => {
		if(!isV8js){
			Object.defineProperty(console, 'iDebug', {
			    get : function(){
			    	// if(this.iDebug2 > 100000000){
			    	// 	throw 'antiInfinityLoop'	
			    	// }
			    	return this.iDebug2;
			    },
			    set : function(val){
			    	if(this.iDebug2 < 0){
			    		throw 'erreur'
			    	} // pratique pour tuer le script dans la console de chrome sans avoir à attendre la fin
			    	return this.iDebug2 = val;
			    }
			});	
		}
	}
}
//# sourceURL=blackboxPlanifAuto.js