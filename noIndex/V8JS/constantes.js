(function(){
	try{
		var console = window.console
		var v8js = !epit.isNotV8Js;
	}catch(e){
		var v8js = true;
	}
	var _const = {
		WORKED_AMP_PERS: ['WORKED'],//dans activity controller
		CONFIG_shortAndLongMode: true,//affiche un text plus court quand pas d'hover sur la case
		ABS_STATES: ['AT-ABSENCE','AT-COMPENSATORY_REST_TEMP','AT-COMPENSATORY_REST'],
		STATE_AUTOMATICALY_VALIDATE: ['PROPOSED_SYSTEM'],//Il n'y a pas moyen de valider la popup sans qu'un proposed système se valide
		STATE_FOR_MISSDAY: ['VALID'],
		AT_CODE_INTER_COMPATIBLE: ['AT-MISSDAY'],//epit.sharedCst.AT_CODE_INTER_COMPATIBLE
		STATES_COMPATIBLE_ABS: ['CANCELED','REFUSED_MANAGER','REFUSED_CONSULTANT','WITHDRAW','UNAVAILABLE'],//epit.sharedCst.STATES_COMPATIBLE_ABS
		CPT_PERSON_ENABLED : ["volunteering","personCptWeek","absence","openClose"],
		CANT_WISHED_SUNDAY: ['CDI_WE','INT WE',"CDD WE"],
		VOLUNTEER_STATES: ['WISHED','WISHED2','PROPOSED'],
		CPT_PERSON: {
		  "volunteering": {
		    "S": 0,
		    "RM": 1,
		    "PM": 2,
		    "RC": 3,
		    "P": 4,
		    "R": 5,
		    "I": 6,
		    "T": 7,
		  //  "not_used": 8,
		    "REH": 8, 
		    "RR": 9,//faculatif donc doit être dernier sinon il faut recalculer tous les indices
		    "function": "personCpt",
		    // "isUpdateCompatible": false,
		  },
		  "personCptWeek": {
		    "PR": 0,
		    "PL": 1,
		    "ABS": 2,
		    "RAF": 3,
		    "function": "personCptWeek"
		  },
		  "absence": {
		    "FOR": 0,
		    "DEL": 1,
		    "AUT": 2,
		    "function": "absence2"
		  },
		  'openClose':{
		  	'OUV': 0,
		  	'FER': 1,
			"function": "openClose"
		  }
		},
		"ColorToInt" : {
		    "W": 1,
		    "G": 2,
		    "O": 3,
		    "R": 4,
		    "N": 5
		},
		presence: {
			width_td: 25 //epit.sharedCst.presence.width_td
		}
	}
	if(v8js){
		return JSON.stringify(_const);	
	}
	epit.sharedCst = _const
})()
