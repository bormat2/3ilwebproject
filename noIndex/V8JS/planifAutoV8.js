//window.iDebug = 1;
/*
@todo: il faut gérer le minimum de besoin

Il faut changer l'entité de la personne tant que ça améliore le besoins

Il faut gérer les rotations, si plus de rotation que les autres alors ne pas affecter

Il faut une fonction pour faire un backup de la journée pour une personne et la restaurer si cela n'est pas bon.

Lors des changements éviter de bouger les pauses.

Pour éviter que les personnes ne travaille en caisse trop longtemps à cause des temps minimums,
il faudrait affecter d'abord les personnes dont la pause est éloigné
    Sinon il faudrait décaler leur pause au lieu de changer l'entité dans laquel ils sont


plus tard:
Les pauses de demis journée ne doivent plus créer des listes séparés


*/
(function() {
    let Debug
    let controls_enabled_call
    const H = 60
    const d_f = ()=>{return null}// juste pour mettre des pointes d'arrêts
    const today = (new Date())
    const important_errors = new Set
    const assert_finite = (val)=>{controls_enabled(()=>{ if(!isFinite(val)){my_throw("val n'est pas un nombre")};return val } )}
    const assert_exists = (...args)=>{
        controls_enabled(()=>{
            args.forEach((arg,i)=>{
                if(arg === void 8){
                    my_throw(`la ${i} ème valeur est undefined`)
                }
            })
        })
    }
    const assert_key_exist = (obj,...args)=>{
        var res = true
        controls_enabled(()=>{
            res = !args.some((arg)=>{
                return arg in obj
            })
        })
        return res
    }


    const my_throw2 = function(level,msg_bis,...iidebugs){
        let msg = msg_bis
        if(level < conf.lever_throw){
            important_errors.add(msg)
        }else{
            if(!do_not_my_throw_error) {
                iidebugs.forEach(function(iidebug){
                    let val = console.iidebug[iidebug]
                    if(val instanceof Object){
                        val = JSON.stringify(val)
                    }
                    msg += `console.iidebug['${iidebug}'] = ${val} )\n`
                })
                let stop = true
                debugger//le temps de mettre stop à false
                if(stop){
                    throw msg
                }
            }else{
                if(!important_errors.has(msg)){
                    important_errors.add(msg)
                }
            }
        }
    }
    const my_throw = function(msg){
        my_throw2(100,msg)
    }

    const today_str ='' + today.getFullYear() + ('0' + (today.getMonth() - -1)).slice(-2) + ('0' + today.getDate()).slice(-2)
    const conf = {
        disabled_controls_until: 0  ,//343856
        lever_throw: 101,//pour voir toutes les erreur mettre 0
        do_not_my_throw_error: false,//si true les erreurs ne sont pas bloquante et sont afficher à la fin
        max_pause_time: 1 * H,
        is_controls_enabled: true,
        dont_care_about_amplitude: true,
        border_rotation_is_in_rotation: false,// est ce que les limite des rotation de fermeture et ouverture inclus ou non la fermeture et l'ouverture
        opening_minute: 10 * H, // si on commence avant cette heure-ci on fait une ouverture
        closing_minute: 20 * H, // si on termine après cette heure-ci on fait une fermeture
        opening_minute_supplement_time: 10 * H, // si on commence à cette heure-ci il faut ajouter un quart d'heure ou 10 minute avant une intervention
        closing_minute_supplement_time: 20 * H, // si on termine à cette heure-ci il faut ajouter un quart d'heure ou 10 minute après une intervention
        consecutive_days_on_2weeks: false,
        min_time_between_days: 11 * H,
        //si true alors le temps minimum entre deux jour ignore les jours
        //d'absence ou non travaillé @warning cette contrainte pose problème
        //car l'ajout peut raté ou réussir en fonction de l'ordre dans lequel
        //on ajoute les jours
        min_time_between_days_ignore_absence: false,
        // force_keep_same_days_saturday_rotation: false,
        even_if_no_replacement_found : false,
        force_to_have_2_consecutive_absences: true,
        tolerance_min_time: 0.5,
        fillMinProtectedFirst: false, //remplir les trou autour des intervention déjà présente pour respecter le temps minimum
        minTime: 0, // temps minimum d'affectation si le secteur n'en a pas
        displayMinTime: 30, // @depreciated : le temps d'une plage horaire Period
        //ratio pour privilégier la répartition des personnes par secteurs
        personSectorMinuteWeight: 1,
        //ratio pour privilégier la répartition des personnes pour remplir les besoins
        fillNeedWeight: 10000000,
        planifStartDate: '2017-09-04',
        planifEndDate: '2017-09-08',
        //pour résoudre le temps minimal ne pas essayé de changer le secteur
        //car si c'est un secteur strict il doit être ajouter
        //via la fonction strict_need
        do_not_try_to_change_sector_around_pause: true,
        do_not_try_to_change_sector_for_mintime: true,

        dont_catch_error_when_simulating: false,
        simulate_server: false
    }

    const do_not_my_throw_error = conf.do_not_my_throw_error


    // multipl
    const stacktrace = [];
    const addToStackTrace = function(message) {
        stacktrace.push(message, console.iDebug, console.iDebug2, console.iDebug3)
        controls_enabled(()=>{
                console.warn(message, console.iDebug, console.iDebug2, console.iDebug3) // ça aide à débugguer mais ce n'est pas bloquand donc ne pas l'afficher pour v8js sionon ça casse la màj
            })
    }

    // let last = (arr) => {
    //     return arr[arr.length - 1]
    // }

    // let at = (arr,pos) => {
    //     if(pos < 0){return arr[pos + arr.length]}
    //         return arr[pos]
    // }
    var is_really_v8js = false;
    try {
        var console = window.console;
        var glob = window.glob = window
    } catch (e) {
        var glob = {}
        is_really_v8js = true;
    }
    glob.conf = conf

    // faire exactement la même chose que coté serveur si possible
    // cela désactive les controles avec controls_enabled
    var simulate_server = true, dont_catch_error_when_simulating;
    if(!is_really_v8js){
        simulate_server = conf.simulate_server//à mettre à false une aide pour débugger
        dont_catch_error_when_simulating = conf.dont_catch_error_when_simulating// même en mode simulate serveur va éviter de catcher les erreurs
    }

    const Report = {obj:{}}
    if(!is_really_v8js){
        window.Report = Report
    }
    const v8js = is_really_v8js || simulate_server

    console_aff: {
        let green = function(msg) {
            console.log('%c ' + msg, 'background: #222; color: green');
        }
        if(v8js) {
            if(is_really_v8js){
                // on met une date car en cas d'oublie de l'enlever
                // ça ne restera qu'un jour
                if(today_str > '20190110'){//@doc_ref1
                    // print('<style> .debug_log{position: absolute; left: -5000px} </style>')
                }
                var console = {
                    log: function(mess) {
                        //print('<p style="color:green">'+ mess+'</p>')
                    },
                    //@console.error
                    error: function(msg) {
                        if(today_str <= '20190114'){//@doc_ref1
                            print('<p class="debug_log" style="color:red">' + msg + '</p>')
                        }
                    },
                    watch() {},
                    trace() {},
                    warn: function() {},
                    green: function(msg) {
                        // print('<p class="debug_log" style="color:green">' + msg + '</p>')
                    },
                    table: function(){}
                }
            }else{
                console.green = green
            }
        } else {
            window.conf = conf
            console.green = green
        }
    }

    Report.add = function(obj) {
        (this.obj[obj.name] = this.obj[obj.name] || []).push(obj)
    }
    Report.get = function(){
        var messages = []
        forOf(this.obj, function(list,name){
            messages.push({
                code: name,
                list: list.map((list)=>{
                    return list.args
                })
            })
        })
        return messages
    }
    Report.log = function(){
        if(Object.keys(this.obj).length){
            console.error("Le rapport d'erreur a détécté plusieurs choses")
            console.log(this.obj)
        }
    }
    var is_controls_enabled = conf.is_controls_enabled && !v8js
    //active des contrôles qui aident à débugguer, il ne doivent surtout pas être activté avec v8js car ils arrêteraient le script
    console.error(`LE MODE is_really_v8js est ${is_really_v8js ? 'T' : 'F'} et v8js est ${v8js ? 'T' : 'F'} <br>\n is_controls_enabled est ${is_controls_enabled ? 'T': 'F'}`)
    if(is_controls_enabled){
        if(conf.lever_throw){
            console.error("ATTENTION LE NIVEAU D'ALERTE DES MESSAGES N'EST PAS DE 0 METTRE conf.lever_throw À 0")
        }

        console.error('LES CONTRÔLES SONT ACTIFS ET GÉNÈRE UN LÉGER RALENTISSEMENT')
        controls_enabled_call = 0;
        var controls_enabled = (callback,notMain)=>{
            ++controls_enabled_call
            if(controls_enabled_call >= conf.disabled_controls_until){//[1701127,1701128]
                var check = !notMain && !Debug.controls_enabled_running
                if(check){
                    Debug.controls_enabled_running = true
                    var last_i_debug = console.iDebug
                    var last_id =  Main.uniqId
                }
                var ret = callback()
                if(check){
                    if(console.iDebug != last_i_debug || Main.uniqId != last_id){
                        console.error('Attention vous avez effectué des actions qui font faire que les ids dans les commentaires des interventions en base ne correspondront pas à ceux coté javascript')
                    }
                    Debug.controls_enabled_running = false
                }
            }
            return ret
        }
        var old_sort = Array.prototype.sort
        Array.prototype.sort = function(){
            let {controls_enabled_running} = Debug
            controls_enabled(()=>{
                if(controls_enabled_running && this.allowed_to_be_sorted !== 1){
                    my_throw( "Veuiller spécifier si le tableau à le droit d\'être trié (ce qui est possible s'il n'est pas utilisé en dehors de la fonction) lors des controls allowed_to_be_sorted = 1 ce qui pourrait changer le cour des choses entre la version coté client et la version coté serveur car on ne traitera pas les éléments dans le même ordre, si vous ne comprenez pas cette phrase faire un slice() pour copier le tableau, var copy = my_array.slice();copy.allowed_to_be_sorted = 1;copy.sort((a,b)=>{return a- b})")
                }
            })
            return old_sort.apply(this,arguments)
        }
    }else{
        var controls_enabled = ()=>{}
        if(!is_really_v8js || conf.lever_throw !== 0){
            console.error('POUR UNE AIDE POUR CORRIGER DES BUGS IL EST PLUS QUE CONSEILLÉ DE METTRE is_controls_enabled à true et conf.lever_throw à 0 ce qui n\'est actuellement pas le cas')
        }else{
            console.error('CHAQUE CHANGEMENT DANS LE CODE DEVRA ÊTRE TESTÉ DANS CHROME AVEC indexplanif.HTML ET is_controls_enabled À TRUE EN REGARDANT LA CONSOLE')
        }
    }
    if(!is_really_v8js){
        window.is_controls_enabled = is_controls_enabled
    }


    // C'est une classe pour limiter les propriété autorisé sur un objet et controller
    // tous les getter et setter de façon centralisé
    const isProxy = Symbol('isProxy')
    const proxy_creator = (function(){
        var common_proxy = (target,property,value,receiver) => {
            if(value && should_be_proxy.has(value) && !value[isProxy]){
                my_throw( "pk cet objet n'est pas un proxy")
            }
        }

        var allow_keys_opt_gen = new Set([
            // Symbol.toPrimitive,//pour appeler isFinite dessus
            isProxy
            ])
        var checker = (allow_keys_opt,property,target,opt)=>{
            if(allow_keys_opt_gen.has(property) || (opt.is_allowed && opt.is_allowed(property) ) ){
                return true
            }
            if(target === allow_keys_opt){
                throw "les clefs autorisé ne peuvent pas être l'objet"
            }
            if(!allow_keys_opt.has(property) && !(property in target)){
                // keys7[property] = 1
                console.log(property)
                my_throw( "cette propriété '"+property+"' n'est pas autorisée")
            }
        }

        //Un proxy sert à controler les propriété accédées et settées
        var should_be_proxy = new WeakSet()
        var proxy_creator = function(elm,allow_keys_opt, opt = {}){
            if(elm === allow_keys_opt){
                throw "les clefs autorisé ne peuvent pas être l'objet"
            }
            var opt2 = {
                set: function(target,property,value,receiver){
                    if(property === isProxy){
                        target[property] = value
                        return true
                    }
                    common_proxy(target,property,value,receiver)
                    checker(allow_keys_opt,property,target,opt)
                    if(opt.setter_check && !opt.setter_check(property,value,target) ){
                        my_throw( 'une erreur est survenue')
                    }
                    target[property] = value
                    return true;
                },
                get: function(target, property) {
                    if(property === isProxy){
                        return true
                    }
                    checker(allow_keys_opt,property,target,opt)
                    return target[property]
                }
            }
            if(opt.deleteProperty) opt2.deleteProperty = opt.deleteProperty
            if(elm.set_revoke){
                var {proxy, revoke} = Proxy.revocable(elm, opt2)
                elm.set_revoke(revoke);
            }else{
                var proxy = new Proxy(elm,opt2)
            }


            should_be_proxy.add(elm)
            should_be_proxy.add(proxy)
            return proxy
        }
        return proxy_creator
    })()


    // var require = require;
    if(!is_really_v8js) { //quand il y a pas v8js on va charger les données dans fakeData depuis planifAuto.json
        var require = function(url) {
            var res;
            var x = new XMLHttpRequest();
            url += '?date=' + (new Date).getTime()
            x.open('GET', url, false); //synchronous juste pour le développement bien sur
            x.onreadystatechange = function() {
                if(x.readyState === 4) {
                    switch (x.status) {
                        case 200:
                        res = x.responseText;
                        break;
                        default:
                        return '';
                        break;
                    }
                }
            }
            x.send();
            window.require_has_worked = false
            try{
                let s = document.createElement('script')
                s.innerHTML = `window.res= ${res};window.require_has_worked = true`
                s.onerror = function(){
                    throw `le fichier ${url} a raté son chargement`
                }
                document.body.append(s)
            }catch(e){
                console.error(e)
                throw `le fichier ${url} à une erreur`
            }

            return window.res
        }
        try {
            var fakeData = require('planifAuto.json')
        } catch (e) {
            console.log(res)
            my_throw( 'il faut utiliser l\'adresse localhost ou 127.0.0.1 ex: http://127.0.0.1/OSF/Symfony/web/public/js/V8JS/indexplanif.html')
        }



        console.watch = function(oObj, sProp, callback) {
            var sPrivateProp = "$_" + sProp + "_$"; // to minimize the name clash risk
            oObj[sPrivateProp] = oObj[sProp];

            // overwrite with accessor
            Object.defineProperty(oObj, sProp, {
                get: function() {
                    return oObj[sPrivateProp];
                },

                set: function(value) {
                    //console.log("setting " + sProp + " to " + value);
                    ////////////// ////debugger; // sets breakpoint
                    oObj[sPrivateProp] = value;
                    if(callback()) {
                        ////////////////debugger
                    }
                }
            });
        }
    } else {
        //if v8js
        var require = function(fileName) {
            var res;
            return eval('res=' + PHP.require('public/js/V8JS/' + fileName));
        }
    }

    if(today_str <= '20190106'){
        var fakeData = require('planifAuto.json')
    }

    if(is_really_v8js){
        conf.lever_throw = Number.POSITIVE_INFINITY
    }

    var get_function_at_url = function(url){
        console.green(`require ${url}`)
        var str1 = require(url)
        var func1;
        eval(`func1 = ${str1}
        //# sourceURL=${url}`)
        return func1
    }


    class Rotation {
        constructor(main) {
            var m = this;
            // m.by_key = new MyMap
            m.main = main
        }
        get_key(o) {
            // return '1'
            //
            return o.period.date
            // return o.period.id
        }
        is_rotation() { /*abstract*/
            abstractError()
        }

        reduce_rotation(wf) {
            abstractError()
        }

        get name() {
            return this.constructor.name
        }

        // get_mean(){
        //     return m.sum_all_wf / m.main.all_wfs.length
        // }

        /* le nombre de rotation pour un worksfor pour être équitable avec les autres */
        // rotation_to_do(wf){
        //     var to_do = (wf.rotation_ratio[m.name] - this.get_mean()) * Fake.rotationPossible(wf)
        //     return to_do
        // }



        //ajout ou supprime une periode d'une rotation dans la map associé à la clef
        inc_r(wf, o, plusMoins1) {
            console.iidebug[80] = idnext('80')
            var m = this;
            if(!m.is_rotation(o)) {
                return
            }
            Debug.check_rotation(wf)
            if(wf.wfId == 10324 && o.period.startMinute == 10) {
                ////////////// ////debugger
            }
            var key = m.get_key(o) // pour les samedi la clef est la date car on ne doit pas compter plusieurs fois un même samedi
            // on crée un map pour chaque personne avec les ouverture
            var rotationMaps = wf.rotationMaps[m.name] //m.name peut être Opening/Closing/Saturday
            //rotationMaps est ici un objet contenant des maps dont la clef est key
            // if(!rotationMaps) rotationMaps =
            if(!rotationMaps[key]) {
                rotationMaps[key] = new MyMap
            }
            var old_length = rotationMaps[key].length
            //si on a réussi à ajouter ou enlever
            if(rotationMaps[key][plusMoins1 > 0 ? 'add' : 'remove'](o.period)) { // c'est le add ou remove de la class MyMap
                var new_length = rotationMaps[key].length
            if(!new_length != !old_length) {
                console.iDebug17 = (console.iDebug17 | 0) + 1
                    // si on est passé de 0 à 1 ou de 1 à 0 pour la longueur
                    // car passé de 2 période travaille le même samedi à 1 on s'en fiche car 1 samedi n'est compté qu'une fois même si on y travaille plusieurs heures
                    wf.rotation[m.name] = (wf.rotation[m.name] || 0) + plusMoins1
                    controls_enabled(()=>{
                        // if(wf.rotation[m.name] > wf.rotation_possible[m.name]){
                        //     my_throw( "le nombre de rotation possible n'est pas cohérent")
                        // }
                    })
                    if(wf.rotation[m.name] < 0) {
                        controls_enabled(()=>{
                            my_throw( 'ne peut être négatif')
                        })
                    }

                    //combien il a fait d'Opening Closing et Saturday relativement à ce qu'il aurait pu faire
                    // wf.rotation_ratio[m.name] = wf.rotation[m.name] / wf.rotation_possible[m.name]
                    wf.update_rotation_ratio(m.name);
                    if(wf.wfId == 10606 && m.name == 'Saturday') {
                        ////////////////debugger;
                    }
                    // m.sum_all_wf +=  wf.rotation_ratio[m.name]rotationMaps
                    // Rotation.wf_to_update.add(wf)
                    // wf.rotation_zscore = wf.rotation_ratio.Opening + wf.rotation_ratio.Closing + wf.rotation_ratio.Saturday
                    return true
                }
            }
            Debug.check_rotation()

        }
    }

    const get_result_of_func_at_url = function(url){
        const definer = get_function_at_url(url)
        return definer()
    }

    const {clone,forOf,someOf,inc,is_between,immutable,not_null,isFinite,special_some,modul,
        lower,upper,array_create,filter_workable,STOP_LOOP,fl,equal_fl,def,first_def,abstractError,
        copy_properties,combine, get_between,min_u,max_u,at,last} = get_result_of_func_at_url('planifAuto/utils.js')

    const ListInfo = get_result_of_func_at_url('planifAuto/ListInfo.js')//pour une personne et un jour donnée
    const Person = get_result_of_func_at_url('planifAuto/Person.js')
    if(!Person.is_class_person){
        throw "!!! ceci devrait être la classe Person, il doit y avoir d'autres erreur dans la console"
    }
    const InterProxy = get_result_of_func_at_url('planifAuto/InterProxy.js')

    const Intervention = get_result_of_func_at_url('planifAuto/Intervention.js')
    const Sector = get_result_of_func_at_url('planifAuto/Sector.js')
    const Saturday = get_result_of_func_at_url('planifAuto/Saturday.js')
    const OpeningClosingCommon = get_result_of_func_at_url('planifAuto/OpeningClosingCommon.js')
    const {Fake,Cell,SortedDoubleCircularLinkedList} = get_result_of_func_at_url('planifAuto/not_used.js')
    const Pause = get_result_of_func_at_url('planifAuto/Pause.js')
    const PeriodNeed = get_result_of_func_at_url('planifAuto/PeriodNeed.js')
    const v2_total_need = get_result_of_func_at_url('planifAuto/v2_total_need.js')
    const Main = get_result_of_func_at_url('planifAuto/Main.js')
    const Block = require('planifAuto/Block.js')
    const launchTest = get_function_at_url('planifAuto/launchTest.js')

    define_Debug = get_function_at_url('planifAuto/Debug.js')
    glob.simulate_server = simulate_server
    Debug = glob.Debug = define_Debug()
    if(conf.disabled_controls_until){
        console.error("ATTENTION LES CONTROLES NE SONT ACTIFS QU'À PARTIR D'UN CERTAIN console.iDebug POUR UN CONTROLE SUR LA TOTALITÉ METTRE disabled_controls_until À 0")
    }

    glob.Intervention = Intervention
    glob.Sector = Sector
    glob.OpeningClosingCommon = OpeningClosingCommon
    glob.ListInfo = ListInfo
    glob.Person = Person
    glob.InterProxy = InterProxy
    glob.Intervention = Intervention
    glob.Sector = Sector
    glob.Saturday = Saturday
    glob.OpeningClosingCommon = OpeningClosingCommon
    glob.Pause = Pause
    glob.PeriodNeed = PeriodNeed
    glob.v2_total_need = v2_total_need
    glob.Main = Main
    glob.Block = Block
    glob.launchTest = launchTest


    if(!Debug.slow_debug){
        console.error("POUR CONNAITRE LE MOMENT OU UNE PERSONNE EST PASSÉ D'UN SECTEUR À UN AUTRE IL FAUT ACTIVER Debug.slow_debug dans Debug.js, l'info sera dans change_to_sector")
    }else{
        console.error("ATTENTION Debug.slow_debug est activé si vous n'avez pas besoin de l'historique de modification, désactivez le pour accélérer le traitement")
    }
    //@idnext
    var idnext = function(j) {
        if(Debug.forbid_to_increase_idnext){
            my_throw2(100,`il ne faut pas appeler de idnext quand il y a Debug.forbid_to_increase_idnext`)
        }
        // Debug.perfStart('idnext')
        /*if(main && main.all_wfs && !Debug.undo){
            var inter = Debug.getWf(10108).get_first_interv_at_date('2018-03-27')
            var nb_pause = 0;
            while(inter){
                if(inter.is_pause()){
                    ++nb_pause
                }
                inter = inter.next
            }
            if(nb_pause> 1){
                my_throw( 'error too much pause')
            }

        }*/
        if(Debug.controls_enabled_running){
            return --console.iDebug_neg
        }
        // if(main && main.all_amp_open && main.all_amp_open[18].periods[0].have > 1){
        //     debugger
        // }
        var ret = ++console.iDebug
        // if( [52785].includes(ret ) ){
        //     debugger
        // }
        if(main && main.wfMap && main.wfMap[10634] && main.wfMap[10634].nb_minu_worked == 2210){
            // debugger
            console.useless = 1
        }
        if(!j){
            my_throw( 'j est obligatoire')
        }

        // if(ret > 24072 && !Debug.addInter_running){
        //     var wf = main.wfMap[10324]
        //     var inter = wf.listInfo_by_date['2018-03-26'].get_inter_with_startMinute(840)
        //     if(inter.is_pause()){
        //         my_throw( 'ça devrait être une absence')
        //     }
        // }
        // if(137782 < ret){
        //     if(Debug.getWf(10179).nb_minu_worked == 2130){
        //         d_f()
        //     }
        // }

        // utile pour comparer l'historique avec le mode débug et simulate_serveur il faut que ce soit le
        // même
        if(Debug.histo_function){
            // if(is_between(ret,399718,400150) && ret%1 == 0){
                Debug.iidebug_history.push({j,ret})
            // }
        }

        // if([162731,162735,162742,162743,162744,162745].has(ret)){
        //     Debug.checker()
        //     Debug.logDayWf(10575,'2018-03-26');
        ////     ////debugger
        // }
        // Debug.perfEnd('idnext')
        return ret
    }
    // @Debug






    console.iidebug = [
        // 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        // 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        // 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        // 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        // 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        // 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        // 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        // 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        // 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        ]


        idnext('no_name9')


    //ajouter ce fichier comme un js blackboxé pour qu'il ne nous gène plus
    var blackboxFunc = require('blackboxPlanifAuto.js');
    var RandomGenerator = require('RandomGenerator.js');
    RandomGenerator.seed(4);
    blackboxFunc.add_iDebug_getter(console, v8js)
    console.iDebug = console.iDebug || 200;//@idnext
    console.iDebug_neg = console.iDebug_neg || -1;
    console.iDebug2 = console.iDebug2 || 1;
    console.iDebug3 = console.iDebug3 || 1;

    // les données qui permettent de se passer de la bdd
    var simulator = {
        amplitudeArr: [
        // '00h00','00h30','01h00','01h30','02h00','02h30','03h00','03h30','04h00','04h30','05h00','05h30',
        // '06h00','06h30','07h00','07h30','08h00','08h30','09h00','09h30','10h00','10h30','11h00','11h30','12h00',
        // /*'12h30',*/'13h00','13h30','14h00','14h30','15h00','15h30','16h00','16h30','17h00','17h30','18h00','18h30','19h00','19h30','20h00','20h30','21h00','22h00',
        // '22h30','23h00','23h30','24h00'
        //
        //'09h00','09h30','10h00','10h30','11h00','11h30','12h00',
        //'13h00','13h30',
        '14h00', '14h30', '15h00', '15h30', '16h00', '16h30', '17h00'
        ]
    }


    const Backup_week = class{
        constructor({wfs}){
            this.args = arguments[0]
            this.backups = {}
            this.wfs = wfs
            this.already_done = false
            this.wfs.forEach((wf)=>{
                wf.listInfo_sorted.forEach((li)=>{
                    const date = li.date
                    if(!li.first_interv) return;
                    const undo_day = wf.main.v2_try_a_change({
                        // get_array_of_backup: true,
                        date: date,
                        person: wf
                    })
                    if(li.first_minu_worked){
                        const full_obj = {
                            wfId: wf.wfId,
                            date,
                            is_opening: wf.opening_info({start_minute: li.first_minu_worked,date: li.date}).before_or_equal('rotation') ,
                            is_closing: wf.closing_info({end_minute: li.last_minu_worked,date: li.date}).after_or_equal('rotation'),
                            last_minu_worked: li.last_minu_worked,
                            first_minu_worked: li.first_minu_worked,
                            worked: li.worked
                        }
                        this.backups[`${li.person.wfId}_${li.date}`] = {
                            undo_day,
                            full_obj : full_obj,
                            stringify_obj: JSON.stringify(full_obj)
                        }
                    }
                })
            })
        }

        restore(){
            forOf(this.backups,(obj)=>{
                obj.undo_day()
            })
        }

        /* Quelles sont les différences avec le backup */
        log_diff(){
            var new_backup = new this.constructor(this.args)
            var new_lg = Object.keys(new_backup.backups).length
            var old_lg = Object.keys(this.backups).length
            if(new_lg != old_lg){
                console.error("le nombre de jour a changé")
            }
            Object.keys(new_backup.backups).forEach((key)=>{
                if(new_backup.backups[key].stringify_obj != this.backups[key].stringify_obj){
                    console.error(`le date ${new_backup.backups[key].date}, la person ${new_backup.backups[key].wfId} a eu un changement dans la journée`)
                }
            })
        }
    }



    /*forOf
     * voir les tests test1BinarySearsh
     */
     var sortedArrFunc = {
        binarySearch: (array, compFunc) => {
            let lo = -1 /*lower*/ ,
            hi = array.length /*higther*/ ;
            while (1 + lo !== hi) {
                const mi = lo + (hi - lo >> 1); // >> is for divide by 2 and floor the result
                if(compFunc(array[mi])) {
                    hi = mi;
                } else {
                    lo = mi;
                }
            }
            return hi;
        },

        /**
         * if we want to insert an item at good position in sorted array
         * get the lower position
         * Return i such that array[i - 1] < item <= array[i].
         */
         lowerBound: (array, item, getPropToCompare) => {
            return sortedArrFunc.binarySearch(array, j => getPropToCompare(item) <= getPropToCompare(j));
        },

        /**
         * if we want to insert an item at good position in sorted array
         * get the higher position
         * Return i such that array[i - 1] <= item < array[i].
         */
         upperBound: (array, item, getPropToCompare) => {
            return sortedArrFunc.binarySearch(array, j => getPropToCompare(item) < getPropToCompare(j));
        }
    }

    // window.keys7 = []
    var allow_keys_opt_v2_schedule_planning = new Set([
        'allow_to_move_time_between_days',
        'dates',//on ne veut planifier que ces dates
        'do_not_planif_on_planified_days',
        'end_minute',
        'exact_amplitude_or_not_planified',
        'forbidden_dates',
        'forced_ampli',
        'get_backup_functions',
        'id',
        'from',
        'do_not_call_optimum_schedule_on_extremity',
        'initial_planning',
        'minutes_to_place',
        'only_these_pers',
        'start_minute',
        'allow_to_go_more_that_nb_minutes_wanted_at_final',
        'do_not_put_pause_on',
        'get_dates_added',
        'before_placing_pause',// callback avant la pause en général pour remettre les protected
        'include_this_amplitude',//de nombreuse options ne sont pas développé avec ce mode comme start_not_before
        'forced_date',//dates dont les amplitudes ne sont pas discutable
        'opt_min_max',//les min et max inclus ou exclus de la journée
        // 'debug_forbid_saturday_wf',//personne interdite de travailler le samedi 2018-03-31 et 2018-04-07
    ])
    class opt_v2_schedule_planning{
        constructor(o){
            var m = this;
            controls_enabled(()=>{
                m = proxy_creator(this,allow_keys_opt_v2_schedule_planning)
            })
            clone(o,m)
            controls_enabled(()=>{
                if(m.id){
                    my_throw( 'error')
                }
            })
            m.id = idnext('no_name10')
            return m
        }
    }

    /*  @myMapClass juste une map qui associe un id de l'objet à son object et compte le nombre d'objet */
    class MyMap {
        //ATTENTION la function forOf est dépendante de la structure de myMap
        constructor(o) {
            var m = this
            m.id = idnext('idMyMap');
            // tout ce qui n'est pas une clef de map est dans '_' comme length et onAddRemove
            Object.defineProperty(m, '_', {
                value: {
                    length: 0,
                },
                enumerable: false,
                writable: false,
                configurable: false,
            })
            m.objs = {}
            if(o) {
                if(o.onAddRemove) {
                    m._.onAddRemove = o.onAddRemove;
                } else {
                    m.add(o)
                }
            }
        }

        add_multiple(list) {
            list.forEach((obj) => this.add(obj))
        }

        add(obj) {
            console.aaa47s = (console.aaa47s || 0) + 1;
            if(console.aaa47s == 18071) {
                ////debugger
            }
            var m = this;
            if(!obj.id) {
                obj.id = Main.getUniqId()
            }
            var id = obj.id;
            var objs = m.objs
            /*controls_enabled(()=>{
                if(obj.id == 'uniq748' && m.id == 430){
                    debugger
                }
            })*/
            if(objs[id] !== obj) {
                controls_enabled(()=>{
                    if(objs[id]) {
                        my_throw( 'un id est associé à un unique objet comment 2 objets différents ont eu le même id !!!')
                    }
                })
                objs[id] = obj;
                //une pause n'est pas compté car elle ne participe pas au besoin
                if(!obj.participate_to_need || obj.participate_to_need()) {
                    ++m._.length
                }
                if(m._.onAddRemove) m._.onAddRemove(+1, obj)
            } else {
                //je ne sais pas pk mais sans ça bug
                if(m._.onAddRemove) m._.onAddRemove(0, obj)
            }
            return true // n'était pas présent et à bien été ajouté
        }
        add_remove(obj, plusMoins1) {
            if(plusMoins1) {
                this.add(obj)
            } else {
                this.remove(obj)
            }
        }

        remove(obj) {
            var m = this;
            var id = obj.id;
            var objs = m.objs
            if(objs[id]) {
                delete objs[id]
                if(!obj.participate_to_need || obj.participate_to_need()) {
                    --m._.length;
                }
                if(m._.onAddRemove) m._.onAddRemove(-1, obj)
                return true; //était présent et a été supprimé
        }
    }

    reset() {
        var m = this;
        m._.length = 0
        m.objs = {}
    }

    get(id) {
        return this.objs[id]
    }

    has(obj) {
        return this.objs[obj.id] === obj
    }

    get_one() {
        var objs = this.objs
        var keys = Object.keys(objs)
        controls_enabled(()=>{
            if(!keys.length) {
                my_throw( 'error')
            }
        })
        return objs[keys[0]]
    }
    /*forEach qui s'arrête si true */
    some(callback) {
        var objs = this.objs
        Object.keys(objs).some((key) => {
            return callback(objs[key], key)
        })
    }

    getValues(){
        return Object.values(this.objs)
    }

    get length() {
        return this._.length
    }
}



    class Opening extends OpeningClosingCommon {

        constructor(...args){
            super(...args)
            this.names = {
                not_opening_rota: 'not_opening_rota',
                not_closing_rota: 'not_closing_rota',
                force_opening_rota: 'force_opening_rota',
                opening_info: 'opening_info',
                closing_rot_name: Closing.name,//'Closing',
                start_minute: 'start_minute',
                startMinute: 'startMinute',
                before_or_equal: 'before_or_equal',
                end_not_before: 'force_end_after_or_equal',
                // start_not_before: 'start_not_before' + (conf.border_rotation_is_in_rotation ? '_or_equal':''),
                // force_start_before: 'force_start_before' + (!conf.border_rotation_is_in_rotation ? '_or_equal':''),
                first_minu_worked: 'first_minu_worked',
                last_minu_worked: 'last_minu_worked',
                get_first_minu_worked: 'get_first_minu_worked',
                is_rotation_opening: 'is_rotation_opening',
                reduce_start: 'unplanif_start',
                reduce_end: 'unplanif_end',
            }
            controls_enabled(()=>{
                this.names = new Proxy(this.names,{
                    set: function(){
                        my_throw( "pas touche aux constantes")
                    },
                    get: function(target,property){
                        if(property in target){
                            return target[property]
                        }
                        my_throw( "Cette prop n'existe pas ")
                    }
                })
            })
        }
        get_limit_minute({wf,date}){
            return conf.opening_minute
        }

        is_rotation(o) {
            controls_enabled(()=>{
                if(!o.person){
                    my_throw( 'la personnes est obligatoire')
                }
            })
            var open_info = o.person.opening_info({
                start_minute: o.period.startMinute,
                date: o.period.date
            })
            return open_info.before_or_equal('rotation')
        }
        get_names(){
            return this.names
        }

        init() {
            this.nb_minute = 1 * H; //on estime qu'en faisant commencé une intervention d'ouverture une heure plus tard,
            //on est plus en ouverture
        }
        next() {
            return 'next'
        }
        prev() {
            return 'prev'
        }
        patchPause(inter) {
            if(inter.prev && inter.prev.is_pause()) {
                return inter.prev
            }
            return inter
        }

        //retourne la dernière intervention de la journée
        get_the_far_interv_of_same_day(theInter) {
            var val = this.main.get_first_last_inter_same_day(theInter, 'next')
            return val
        }

        sort_and_put_nearest_interv_first(first_interv_and_wfId) {
            first_interv_and_wfId.sort(function(o1, o2) {
                return o1.firstORlast_interv.startMinute - o2.firstORlast_interv.startMinute
            })
        }
        /*
         * Créer un objet pour ajouter de l'autre coté de la journée ce que l'on a enlever de l'autre
         * other_side_of_day
         */
         update_seria_to_add_what_we_delete_at_the_other_side_of_the_same_day(seria, last_inter_info, minute_realy_removed) {
            seria.startMinute = last_inter_info.end_minute
            seria.endMinute = seria.startMinute + minute_realy_removed
        }

        //en augmentant l'heure de fin on va ainsi enlever la rotation Opening
        update_seria_to_delete_the_rotation_on_first_person(seria) {
            var nb_minutes = this.nb_minute
            seria.startMinute += nb_minutes
            return nb_minutes
        }
        /*
         * Pour le remplaçant(substitute) comme on a ajouter au debut il faut enlever à la fin
         */
         get_start_and_end_minute_to_unplanif_for_substitute(last_inter_of_day, nb_minutes) {
            return {
                start_minute: last_inter_of_day.endMinute - nb_minutes,
                end_minute: last_inter_of_day.endMinute,
            }
        }

        get_start_and_end_minute_to_planif_for_substitute(
            first_interv, // première intervention de la journée
            old_start // l'heure de début de l'intervention que l'on a enlever à l'autre personne
            ) {
            return {
                start_minute: old_start,
                end_minute: first_interv.startMinute
            }
        }

        /**
         * dans le cas de l'opening c'est le first
         */
         get_firstORlast_interv_with_sector_at_date(wf, date) {
            return wf.get_first_interv_with_sector_at_date(date,wf)
        }

        // place_is_existing_to_place_inter(first_interv,old_sector,old_start){
        //     var place_is_existing_to_place_inter = first_interv.sector == old_sector
        //         || (first_interv.endMinute - first_interv.sector.minTimeSector >= old_start + old_sector.minTimeSector )
        //     return place_is_existing_to_place_inter;
        // }

        /*
         * Dans le cas de l'ouverture on retourne la old_start
         */
         get_old_startORend(seria) {
            return seria.startMinute
        }
    }

    class Closing extends OpeningClosingCommon {
        constructor(...args){
            super(...args)
            let names = {
                not_opening_rota: 'not_closing_rota',
                not_closing_rota: 'not_opening_rota',
                force_opening_rota: 'force_closing_rota',
                opening_info: 'closing_info',
                closing_rot_name: Opening.name,//'Opening',
                start_minute: 'end_minute',
                startMinute: 'endMinute',
                end_not_before: 'force_start_before_or_equal',
                before_or_equal: 'after_or_equal',
                // start_not_before: 'end_not_after' + (conf.border_rotation_is_in_rotation ? '_or_equal':''),
                // force_start_before: 'force_end_after' + (conf.border_rotation_is_in_rotation ? '_or_equal':''),
                first_minu_worked: 'last_minu_worked',
                last_minu_worked: 'first_minu_worked',
                get_first_minu_worked: 'get_last_minu_worked',
                is_rotation_opening: 'is_rotation_closing',
                reduce_start: 'unplanif_end',
                reduce_end: 'unplanif_start',
            }
            controls_enabled(()=>{
                names = new Proxy(names,{
                    set: function(target,property){
                        my_throw( "pas touche aux constantes")
                    },
                    get: function(target,property){
                        if(property in target){
                            return target[property]
                        }
                        my_throw( "Cette prop n'existe pas ")
                    }
                })
            })
            this.names = names
            return this
        }
        get_limit_minute({wf,date}){
            return conf.closing_minute
        }
        get_names(){
            return this.names
        }
        is_rotation(o) {
            var closing_info = o.person.closing_info({
                end_minute: o.period.endMinute,
                date: o.period.date
            })
            return closing_info.after_or_equal('rotation')
        }

        init() {
            this.nb_minute = 1 * H; //on estime qu'en faisant commencé une intervention d'ouverture une heure plus tôt,
            //on est plus en fermmeture
        }
        next() {
            return 'prev'
        }
        prev() {
            return 'next'
        }
        patchPause(inter) {
            if(inter.next && inter.next.is_pause()) {
                return inter.next
            }
            return inter
        }
        /*
         * Mettre en premier les interventions qui ont une date de fin proche de la fin de journée
         * donc plus succeptible de devenir facilement de Closing
         */
        sort_and_put_nearest_interv_first(last_interv_and_wfId) {
            last_interv_and_wfId.sort(function(o1, o2) {
                return o2.firstORlast_interv.endMinute - o1.firstORlast_interv.endMinute
            })
        }

        /*
         * Créer un objet pour ajouter de l'autre coté de la journée ce que l'on a enlever de l'autre
         * other_side_of_day
         */
         update_seria_to_add_what_we_delete_at_the_other_side_of_the_same_day(seria, first_inter_info, minute_realy_removed) {
            seria.endMinute = first_inter_info.start_minute
            seria.startMinute = first_inter_info.start_minute - minute_realy_removed
        }
        //retourne la première intervention de la journée
        get_the_far_interv_of_same_day(theInter) {
            return this.main.get_first_last_inter_same_day(theInter, 'prev')
        }

        // reduce_rotation_for_wf(wf){

        // }

        //en réduisant l'heure de fin on va ainsi enlever la rotation Closing
        update_seria_to_delete_the_rotation_on_first_person(seria) {
            var nb_minutes = this.nb_minute
            seria.endMinute -= nb_minutes
            return nb_minutes
        }

        /*
         * Pour le remplaçant(substitute) comme on a ajouter à la fin il faut enlever au début
         */
         get_start_and_end_minute_to_unplanif_for_substitute(first_inter_of_the_day, nb_minutes) {
            return {
                start_minute: first_inter_of_the_day.startMinute,
                end_minute: first_inter_of_the_day.startMinute + nb_minutes
            }
        }

        get_start_and_end_minute_to_planif_for_substitute(
            last_interv // dernière intervention de cette personne dans la journée
            , old_end // l'heure de fin à laquelle était la fermeture qu'on a enlever à l'autre personne
            ) {
            return {
                start_minute: last_interv.endMinute,
                end_minute: old_end
            }
        }

        /*
         * dans le cas du Closing c'est le last
         */
        get_firstORlast_interv_with_sector_at_date(wf, date) {
            return wf.get_last_interv_with_sector_at_date(date,wf)
        }

        // place_is_existing_to_place_inter(last_interv,old_sector,old_end){
        //     var place_is_existing_to_place_inter = last_interv.sector == old_sector
        //         || (last_interv.endMinute - last_interv.sector.minTimeSector >= old_end + old_sector.minTimeSector )
        //     return place_is_existing_to_place_inter;
        // }
        /*
         * Dans le cas de la fermeture on retourne la old end
         */
         get_old_startORend(seria) {
            return seria.endMinute
        }
    }







    class PeriodPerson {
        constructor(o, main) {
            this.dateKey = o.dateKey;
            this.date = o.date;
            this.startMinute = o.startMinute;
            this.endMinute = o.endMinute;
            this.openingType = o.openingType
        }
    }
    /* @PlanningClass */
    class PlanningPerson {
        constructor(o, main) {
            this.periodSortedArr = o.periodSortedArr
            // Main.createPlanning(this,main,PeriodPerson)
        }
    }

    /* @GeneralPlanningClass */
    class GeneralPlanning {
        constructor(o, main) {
            // Main.createPlanning(this,main,class Period{
            //  constructor(o,main){
            //      this.dateKey = o.dateKey;
            //      this.date = o.date;
            //      this.startMinute = o.startMinute;
            //      this.endMinute = o.endMinute;
            //      this.openingType = 'OUVERTURE'
            //  }
            // })
        }
    }



    /* @PlanningNeedForEntityClass */
    class PlanningNeedForEntity {
        constructor(o, main) {
            this.periodSortedArr = o.periodSortedArr
            this.totalMinuteOpened = o.totalMinuteOpened
        }
    }

    /* @PersonSector*/
    class PersonSector {
        constructor(o, main) {
            var m = this;

            ///////
            m.wfId = o.wfId;
            m.idEnt = o.idEnt;
            m.ent = o.ent;
            m.wf = o.wf;
            ///////
            m.nb_minu_worked = 0;
            m.interventionMap = new MyMap;
            controls_enabled(()=>{
                if('sector_group' in o.ent){
                    my_throw(`Si il y a un secteur grp c'était le sector_grp qu'il fallait passer`)
                }
            })
        }

        update_nb_minu_worked(interv, plusMoins1) {
            if(!interv.is_counted_in_worktime()){
                return;
            }
            // if(interv.sector.id == "fake_for_abs"){
            //     debugger
            // }
            if(plusMoins1 < 0){
                //quand on enlève il faut prendre le temps tel qu'il était lors de l'ajout
                var diff = interv.finalTime.total_time
            }else{
                //sinon pour les interventions récupérées de la base on prend le temps
                //exact qui était en base
                if(interv.true_protected_information && interv.protected_reason != 'forced_pauses'){
                    var diff = interv.true_protected_information.time
                }else{
                    //sinon si il y a des ouvertures et des fermetures on ajoute
                    //unplaced_time
                    var diff = (interv.time + interv.unplaced_time())
                }
            }
            diff *= plusMoins1
            this.nb_minu_worked += diff
            // this.unplaced_time += unplaced_time * plusMoins1
            interv.listInfo.person.inc_nb_minu_worked(diff, interv.listInfo.date)
        }


        addInterv(intervToAdd, main) {
            var m = this;
            console.iDebug20 = (console.iDebug20 || 0) + 1
            // if(intervToAdd.type != 'PAUSE'){
                if(m.interventionMap.add(intervToAdd)) {
                    m._removeAddInter(intervToAdd, +1)
                    intervToAdd._debug_ai = {
                        startMinute: intervToAdd.startMinute,
                        endMinute: intervToAdd.endMinute,
                        nb_minu_worked: m.nb_minu_worked,
                        iDebug: console.iDebug
                    }
                }
            // }
        }

        _removeAddInter(interv, plusMoins1) {
            console.iidebug[98] = idnext('98')
            if(console.iidebug[98] == 22306){
                // debugger
            }
            var m = this;
            m.update_nb_minu_worked(interv, plusMoins1)
            var main = m.wf.main
            if(main) {
                //il faut mettre à jour les critère de rotation si l'intervention était sur une amplitude de rotation
                interv.forEachPeriod((period) => {
                    var rotation_has_changed = 0
                    forOf(main.rotations, function(rotation, rot_name) {
                        rotation_has_changed += rotation.inc_r(m.wf, {
                            person: m.wf,
                            period
                        }, plusMoins1)
                    })
                    if(rotation_has_changed) {
                        //mettre à jour la cellule de la linked list qui trie les personnes en commençant par celles qui ont le pire zscore(donc le plus grand)
                        // wf.cell_in_rotation_linked_list.update_position()
                    }
                })
            }
        }


        removeInterv(intervToRemove) {
            var m = this;
            var wasPresent = m.interventionMap.remove(intervToRemove)
            if(wasPresent) {
                // if(8586 == intervToRemove.addIntervDebug){
                //     debugger;
                // }
                // m.update_nb_minu_worked(intervToRemove,-1)
                m._removeAddInter(intervToRemove, -1)
            }
            intervToRemove._debug_aiRem = {
                startMinute: intervToRemove.startMinute,
                endMinute: intervToRemove.endMinute,
                nb_minu_worked: m.nb_minu_worked,
                iDebug: console.iDebug
            }
            return wasPresent
        }
    }

    //@pause_dispatcher
    class Pause_dispatcher {
        constructor(o) {
            this.pauses = []
            if(!Debug.is_not_test){
                class Meal extends Pause{
                    constructor(o,info_by_key,others){
                        super(o,info_by_key,others)
                    }

                    // // pour une pause non compté dans le temps de travail
                    // // il y a un temps qui lui est compté car on place d'abord une heure
                    // // au lieu de 40 minute, donc il faut soustraire 20 minute au temps
                    // // de travail
                    // time_counted_in_worktime(date,person){
                    //     let hc = this.get_hours_config(date,person)
                    //     let diff = hc.time - hc.real_time
                    //     if(diff && this.is_not_counted_in_worktime){
                    //         return diff
                    //     }
                    //     return 0
                    // }
                }
                class Noon extends Meal {
                     constructor(o) {
                         // const get_fake_sector = (key,p,is_not_counted_in_worktime_func)=>{
                         //     return new Sector({
                         //         is_not_counted_in_worktime: is_not_counted_in_worktime_func,
                         //         id: 'fake_for_pause_noon',
                         //         name: 'fake_for_pause',
                         //         // minTimeSector : 0,//p.fake_time,
                         //         // maxTimeSector : 0,//p.fake_time,
                         //         // recommendedTime: p.fake_time,
                         //         parentEntity: null,
                         //         pause_instance: p,
                         //         main: o.main
                         //     })
                         // }
                         var info_by_key = {
                             default: {
                                 min_start_pause: 11.5 * H,
                                 max_start_pause: 15 * H,
                                 real_time: 40,
                                 // time:  1 * H,
                                 fake_time: 1 * H,
                                 min_time_in_day: 5 * H,
                                 min_start_day: 0*H,
                                 max_start_day: 14 * H,
                                 min_time_in_day: 5 * H
                             },
                             // '2019-02-04': {
                             //     min_start_pause: 8 * H,
                             //     max_start_pause: 12 * H,
                             //     max_start_day: 14 * H,
                             //     // time:  1 * H,
                             //     fake_time: 1 * H,
                             //     min_time_in_day: 5 * H
                             // }
                         }
                         var p = super(o,info_by_key,{is_not_counted_in_worktime : true, uniq_name: 'fake_sector_for_Noon',pause_name: 'Noon'})
                     }

                     // is_pause_needed() {
                     //     return false
                     // }
                 }



                 //@afternoon
                 // Cette pause est compté dans le temps de travail
                 // elle dure 40 minute
                 // mais on va d'abord placer une heure
                 class Afternoon extends Pause {
                     constructor(o) {
                         var info_by_key = {
                             default: {
                                 min_start_pause: 18 * H,
                                 max_start_pause: 21.5 * H,
                                 real_time: 40,
                                 // time:  1 * H,
                                 fake_time: 1 * H,
                                 min_time_in_day: 5 * H,
                                 min_start_day: 14 * H + 1
                             },
                             // '2019-02-04': {
                             //     min_start_pause: 18 * H,
                             //     max_start_pause: 21.5 * H,
                             //     real_time: 40,
                             //     // time:  1 * H,
                             //     fake_time: 1 * H,
                             //     min_time_in_day: 5 * H,
                             //     min_start_day: 14 * H + 1
                             // }
                         }
                         var p = super(o,info_by_key,{is_not_counted_in_worktime: true,  uniq_name: 'fake_sector_for_Afternoon',pause_name: 'Afternoon'})
                         p.nb_max_by_day = 1;
                     }
                 }
                this.pauses = ['noon','afternoon']
                this.noon = new Noon(o)
                this.afternoon = new Afternoon(o)
            }else{
                forOf(o.params.pause_types  || {},(obj,name)=>{
                    this.pauses.push(name)
                    // if(name == 'LUNCH'){
                    //     name = 'noon'
                    // }else{
                    //     name = 'afternoon'
                    // }
                    this[name] = new Pause({
                        main: o.main
                    },obj.info_by_key,{is_not_counted_in_worktime: true,  uniq_name: 'fake_sector_for_'+name, pause_name: name})
                })
            }
            this.nb_max_by_day = 1;
            this.main = o.main
            this.exclusive_pause = true
        }

        affect_forced_pauses(forced_pauses){
            const m = this.main
            const pdi = this
            //@forced_pause
            forOf(forced_pauses,(pauses,wf_id)=>{
                let person = m.wfMap[wf_id]
                forOf(pauses, function(info_pause,date){
                    if(!info_pause){
                        return
                    }
                    let sector,pause_type,pt_name;
                    try{
                        pt_name = info_pause.pause_name
                        pause_type = pdi[pt_name]
                        sector = pause_type.get_hours_config(date, wf_id).fake_sector
                        if(!sector) throw 'inex'
                    }catch(e){
                        my_throw2(100,`le secteur est inexistant`)
                        let sector = null;
                    }

                    let real_time = info_pause.end_minute - info_pause.start_minute
                    var inter = new Intervention({
                        sector: sector,
                        startMinute: lower(info_pause.start_minute),
                        endMinute: upper(info_pause.end_minute),
                        protected: true,//on ne met pas fake_protected car contrairement
                        //aux autres pauses comme elle n'a aucune marge sur les heures il n'y a pas de raison de la supprimer
                        true_protected_information: {
                            start_minute: info_pause.start_minute,
                            end_minute: info_pause.end_minute,
                            time: real_time
                        },
                        date,
                        protected_reason: 'forced_pauses',
                        canFail: true//si la journée est impossible à travailler par exemple
                    })
                    assert_finite(inter.true_protected_information.start_minute,inter.true_protected_information.time)

                    if(pause_type){
                        pause_type.set_hours_config({person,date},{real_time},sector)
                    }
                    let wf = m.wfMap[wf_id]
                    if(wf){
                        let adi = wf.addInterv(inter)
                        assert_exists(pt_name)
                        if(adi.has_been_insert){
                            wf.listInfo_by_date[date].meal_type = pt_name
                        }
                    }
                    controls_enabled(()=>{
                        pause_type.get_hours_config(date,person)
                    })

                })
            })
        }


        /*
        * Retourne le temps réel de la pause, si celui-ci n'est pas compté dans le temps de travail
        * si la personne commence à travailler à start_minute et travail au total time_worked_this_day
         */
        get_time_pause_not_counted_in_worktime(start_minute,time_worked_this_day,date,person){
            controls_enabled(()=>{
                if(!person || !date){
                    my_throw("La personne ou la date est manquante")
                }
            })
            var end_minute = start_minute + time_worked_this_day
            let time = 0;
            let lbd = person.listInfo_by_date
            let d = lbd && lbd[date]
            for(let name_pause of this.pauses){
                const p = this[name_pause]
                if(p.type == 'MEAL'){
                    if(d && d.meal_type && d.meal_type !== p.pause_name){
                        continue
                    }
                }
                const hc = p.get_hours_config(date,person)
                if(start_minute < hc.max_start_pause && hc.min_start_pause < end_minute && time_worked_this_day > hc.min_time_in_day){
                    time += hc.real_time
                }
            }
            return time
        }
        // get_fake_sectors() {
        //     return [this.afternoon.fake_sector]
        // }

        go_real_time_mode() {
            this.is_real_time_mode = true
        }

        resolve_pause_unrespected(first_interv, opt5_bis) {
            let li = first_interv.listInfo
            let rules_pause = combine(li.rules_pause,opt5_bis.rules_pause)
            // let opt5 = {...opt5_bis, rules_pause: null} //crash sur v8js
            let opt5 = Object.assign({},opt5_bis,{rules_pause: null})
            console.iidebug[49] = idnext('49');
            if(console.iidebug[49] == 163145){
                debugger
            }
            var uip = {}
            for(let p_name of this.pauses){
                let pause = this[p_name]
                var ret;
                var opt_uip1 = {
                    force_to_remove: false
                }
                let f_uip = pause.unpause_illogic_pause.bind(pause,first_interv,opt_uip1)
                uip[p_name] = f_uip();
                if(!uip[p_name].pause_is_ok){
                    return {
                        pause_is_ok: false,
                        pause_is_placed: false,
                        all_pauses : []
                    }
                }
            }
            for(let p_name of this.pauses){
                let pause = this[p_name]
                var ret;
                var opt_uip1 = {
                    force_to_remove: false
                }
                /*
                var uip1 = pause.unpause_illogic_pause(first_interv,opt_uip1);
                var opt_uip2 = {
                    force_to_remove: this.exclusive_pause && uip1.need_pause//si droit à la pause du midi alors supprimer l'autre
                }
                var uip2 = this.afternoon.unpause_illogic_pause(first_interv,opt_uip2);
                if(!uip1.pause_is_ok || !uip2.pause_is_ok){
                    return {
                        pause_is_ok: false,
                        pause_is_placed: false,
                        all_pauses : []
                    }
                }*/

                var pause_info = pause.resolve_pause_unrespected(first_interv,
                    combine(opt5, opt_uip1, {rules_pause:{first_call: true} })
                )
                if(!pause_info.pause_is_ok && rules_pause && rules_pause.pause__do_not_respect_min_time_after_before){
                    pause_info = pause.resolve_pause_unrespected(first_interv, combine(opt5, opt_uip1, {rules_pause}))
                    li.rules_pause = rules_pause
                }else{
                    li.rules_pause = null
                }
                //si il y a une erreur pour placer la pause du midi ou que la pause du midi
                //a bien été inséré alors on va prendre le retour de la pause du midi
                var go_to_next_pause = false
                ret = pause_info
                ret.info = p_name
                if(!pause_info.pause_is_ok || pause_info.pause_is_placed) {
                    break
                }/*else{
                    go_to_next_pause = true
                }*/
            }

            // if(!this.exclusive_pause || go_to_next_pause){
            //     //par contre si la pause du midi n'était pas nécéssaire
            //     // c'est que celle de l'après midi est nécéssaire
            //     let opt5_bis1 = combine(opt5, opt_uip2,{rules_pause:{first_call: true}})
            //     ret = this.afternoon.resolve_pause_unrespected(first_interv, opt5_bis1)
            //     if(!ret.pause_is_ok && rules_pause && rules_pause.pause__do_not_respect_min_time_after_before){
            //         ret = this.afternoon.resolve_pause_unrespected(first_interv, combine(opt5, opt_uip2, {rules_pause}))
            //         li.rules_pause = rules_pause
            //     }else{
            //         li.rules_pause = null
            //     }
            //     ret.info = 'not_noon'
            // }
            return ret
        }
    }

    var rotation_wf_keys =  new Set(['id','type'])
    //empêche les NaN dans les ratio de rotation
    class Rotation_wf{
        constructor(o){
            const {type,main} = o
            controls_enabled(()=>{
                if(!type){
                    my_throw( 'necessaire')
                }
                if(!main){
                    my_throw( 'necessaire')
                }
                if(this.id){
                    my_throw( 'error')
                }
            })
            delete o.type;delete o.main;
            var m = this
            controls_enabled(()=>{
                m = proxy_creator(this,rotation_wf_keys,{
                    setter_check: (prop,val,target)=>{
                        if(main.rotations[prop]){
                            if(!isFinite(val)){
                                my_throw( `Seul des nombres finis sont autorisés`)
                            }
                        }
                        return true
                    },
                    is_allowed: (property)=>{
                        return main.rotations[property]
                    }
                })
            })
            clone(o,m)
            Object.defineProperty(m, 'id', {
                enumerable: false,//il ne faut pas boucler
                configurable: false,
                writable: false,
                value: idnext('no_name8')
            });
            Object.defineProperty(m, 'type', {
                enumerable: false,//il ne faut pas boucler
                configurable: false,
                writable: false,
                value: type
            });
            Object.defineProperty(m, 'main', {
                enumerable: false,//il ne faut pas boucler
                configurable: false,
                writable: false,
                value: main
            });
            return m
        }
    }



    launchTest();
    Debug.is_not_test = true;


    // pour le serveur //
    conf.minTime = 1
    var main = new Main

    if(!is_really_v8js) {
        window.main = window.planifObj = main
        //new Main
        // if(callbackTest) callbackTest();
    } else {
        return main
    }
})()
//# sourceURL=planifAutoV8.js
