'' + function(){
    /**
     * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
     */
    let Debug = {
        // utile pour comparer l'historique avec le mode débug et simulate_serveur
        // il faut que ce soit le même
        histo_function: false,
        iidebug_history: [],
        //nécessaire pour connaitre l'historique des interventions
        slow_debug: v8js ? false : false,//ne changer que après les : pas avant sinon ralentit le serveur
        // is_enabled: true,
        wf_id: [10606],
        date: '2018-03-26',
        between_start: 20 * H,
        between_end: 24 * H,
        interventions: {},
        timers: {

        },
        number_of_call: [],
        current_function: [],
        // contient l'id du moment ou on est passé d'un autre secteur à celui-ci
        // intervention commençant à une heure donnée pour un secteur donnée
        // ex: 10575_600: [{sec: 2522,aid:10}] pour 10h (10*60)
        // on ignore les restaurations d'intervention
        // car elle ne crée pas de situation elle ne font que les restaurer
        // et ne peuvent pas être la cause d'un problème
        change_to_sector :{},
        // dbg(i){
        //     return console.iidebug[i] = idnext('i')
        // },
        checker(obj) {
            // controls_enabled(()=>{
            //     var inter = Debug.getWf(10324).get_first_interv_at_date('2018-03-26')
            //     while (inter) {
            //         if(inter.protected && inter.startMinute == 630 && inter.endMinute == 660) {
            //             found = true
            //         }
            //         inter = inter.next
            //     }
            //     if(!found){
            //         my_throw('la protected')
            //     }
            // })
        },
        check_rotation: function(wf){
            controls_enabled(()=>{
                if(!wf){
                    main.all_wfs.forEach(function(wf){
                        if(!wf){
                            my_throw('error_check_rotation')
                            Debug.check_rotation(wf)
                        }
                    })
                    return;
                }
                forOf(wf.rotationMaps,(obj,rota_name)=>{
                    var nb_rota = 0;
                    forOf(obj,(map,key/*date*/)=>{
                        nb_rota += map.length > 0
                    })
                    if(nb_rota +  (wf.rotation_initial[rota_name]||0) != wf.rotation[rota_name]){
                        my_throw( 'le compteur de rotation ne fonctionne pas')
                    }
                })
            })
        },
        is_round(hour){
            controls_enabled(()=>{
                if(lower(hour) != hour){
                    console.log(lower(hour),hour)
                    my_throw( 'le temps demandé est mal arrondi')
                }
            })
            return true
        },
        getWf(wf_id) {
            var wf;
            main.all_wfs.some((wf1) => {
                if(wf1.wfId == wf_id) {
                    wf = wf1
                }
            })
            return wf
        },
        checkTime() {
            // if(Debug.getWf(10575).nb_minu_worked == 33.5) {
            //     console.log('là')
            // }
        },
        //@logDayWf
        logDayWf(inter_or_wf, date,recursive_call, force_display = false) {
            var endF = function(){
                if(!recursive_call){
                    console.log(`total dans la semaine ${wf.nb_minu_worked}`)
                    console.log(`console.iDebug = ${console.iDebug}`)
                }
            }
            var d = this
            if(!force_display && v8js) return;
            var table = []

            if(inter_or_wf instanceof Intervention) {
                ir = inter_or_wf.first_interv
                wf = ir.person
            } else {
                var wf = inter_or_wf
                if(isFinite(wf) || typeof wf == 'string') {
                    wf = Debug.getWf(+wf)
                }
                if(!date) {
                    var str = ''
                    wf.intervsRacines.forEach((inter4) => {
                        str += '[' + inter4.listInfo.first_minu_worked + '_' + inter4.listInfo.last_minu_worked + '] \n'
                        d.logDayWf(inter4,null,true)
                    })
                    endF()
                    console.log(str)
                    return;
                }
                var ir;
                wf.intervsRacines.forEach(function(ir1) {
                    if(ir1.date == date) {
                        ir = ir1
                    }
                })
            }

            let sum_unplaced_time = 0, sum_time = 0;
            if(ir) {
                console.log(`Debug.logDayWf('${ir.person.wfId}','${ir.date}')//day_minu_worked = ${wf.listInfo_by_date[ir.date].worked}`)
                controls_enabled(()=>{
                    if(ir.next) {
                        if(ir.next.prev != ir) {
                            my_throw( 'we have a probleme')
                        }
                    }
                    if(ir.prev) {
                        if(ir.prev && ir.prev.next != ir) {
                            my_throw( 'we have a probleme')
                        }
                    }
                })
                // console.log(date)
                var i = 0;
                while (ir) {
                    var minOk = ir.v2_respectMinTime()
                    if(!minOk) {
                        console.error('not min ok')
                        ////////////debugger
                    }
                    let unplaced_time = ir.unplaced_time()
                    table.push({
                        sect: ir.is_pause() ? 'PAUSE' : ir.sector ? ir.sector.id : ' None',
                        start: ir.startMinute,
                        end: ir.endMinute,
                        // date: ir.date,
                        addIntervDebug: ir.addIntervDebug,
                        interv: ir,
                        protected: ir.protected,
                        minTOk: minOk,
                        ori_addIntervDebug: ir.ori_addIntervDebug,
                        updateDebugId: ir.updateDebugId,
                        time: ir.time,
                        unplaced_time,
                        tot_time: unplaced_time + ir.time,
                        hasBeenInsert: ir.hasBeenInsert
                    })
                    // console.warn('sect: ' + (ir.sector ? ir.sector.id : ' None')
                    //     + ' st ' +ir.startMinute + ' end '+ ir.endMinute
                    //     )
                    if(ir.is_counted_in_worktime()){
                        sum_unplaced_time += unplaced_time
                        sum_time += ir.time
                    }
                    ir = ir.next
                }
            }
            table.push({
                sect: 'TOTAL',
                start: 0,
                end: 24,
                // date: ir.date,
                addIntervDebug: 0,
                interv: 0,
                protected: 0,
                minTOk:0,
                ori_addIntervDebug: 0,
                updateDebugId: 0,
                time: sum_time,
                unplaced_time: sum_unplaced_time,
                tot_time: sum_time + sum_unplaced_time
            })
            console.table(table)
            endF()
            if(console.iDebug == 61116) {
                ////////////////debugger
            }
        },
        not_close: {},
        perfStart: function(name) {
            controls_enabled(()=>{
                if(Debug.not_close[name]){
                    my_throw(`on ne peut faire un perfstart sur une fonction on l'on a déjà fait un perfstart sans jamais la fermer, En cas de récursivité faire un perfEnd avant l'appel récursif`)
                }
                Debug.not_close[name] = console.iDebug
                if(!name){
                    my_throw('Le name est obligatoire')
                }
                Debug.number_of_call[name] = fl(Debug.number_of_call[name]) + 1
                Debug.current_function.push(name)
                Debug.timers[name] = fl(Debug.timers[name]) - (new Date()).getTime()
            })
        },
        perfEnd: function(name, debugInfo) {
            Debug.not_close[name] = false
            controls_enabled(()=>{
                if(conf.disabled_controls_until == 0){
                    var current_function = this.current_function.pop(name)
                    if(current_function != name) {
                        console.error(debugInfo);
                        console.error(`you can't close this func because, it is not the last open ${current_function}`)
                        my_throw('')
                    }
                    Debug.timers[name] += (new Date()).getTime()
                }
            })
        },
        explain_time: function(){
            var arr=[]
            forOf(Debug.timers,function(time,name){
                arr.push({
                    name,time
                })
            })
            arr.sort(function(a,b){
                return a.time - b.time
            })
            for(let a of arr){
                console.log(`${a.name} : ${(a.time/Debug.timers.tot_time).toFixed(4)}%   time: ${a.time} `)
            }
        },
        get_inter_history(inter) {
            var intervs = []
            var ids_done = {}
            for (var old = inter.old_inter; old && ids_done[old.addIntervDebug] !== old; old = old.old_inter) {
                ids_done[old.addIntervDebug] = old;
                intervs.push(old)
            }
            return {
                intervs,
                inter_addIntervDebug: Object.keys(ids_done).map((v) => +v)
            }
        },
        get_interv_to_flush(wf, date) {
            var jsIntervArr = JSON.parse(main.getAllIntervToFlush())
            var filter2 = jsIntervArr.filter((i) => {
                return (!date || i.startDate == date) && (!wf || i.wfId == wf)
            })
            return filter2
        },
        inc_error_code(code,nb = 1) {
            if(Debug.is_not_test) {
                Debug.error_code[code] = (Debug.error_code[code] || 0) + nb
                if(code == 'success') {
                    var debug = 2;
                }
            }
        },
        rapport_error_code() {
            if(important_errors.size){
                console.error("Des erreurs importantes ont eu lieu désactiver do_not_my_throw_error pour générer les throws", important_errors)
            }
            console.log('erreurs les plus fréquente lors de l ajout d une intervention', Debug.error_code)
            console.log('erreurs les plus fréquente lors de l ajout du temps manquant', Debug.error_codes_place_missing_minutes_wf)
        },
        check_not_enough_worked(wf,strict){
            controls_enabled(()=>{
                if((strict ? wf.nbMinutesMin -  wf.nb_minu_worked : lower(wf.nbMinutesMin - wf.nb_minu_worked)) > 0){
                    my_throw2(100,'la personne ne travaille pas assez')
                    // my_throw( 'la personne ne travaille pas assez')
                }
            })
        },
        check_too_much_worked(wf,iidebug){
            controls_enabled(()=>{
                if(wf.nb_minu_worked > wf.nbMinutesMin){
                    my_throw2(70,'la personne travaille trop',iidebug)
                }
            })
        },
        check_nb_days_planif(wf,iidebug,infos){
            controls_enabled(()=>{
                if(wf.getNumberOfDaysRemained()){
                    if(today_str > '20181221'){
                        console.iidebug['infos'] = infos
                        my_throw2(14, "des jours ne sont pas placés",iidebug,'infos')
                    }
                }
            })
        },
        check_not_enough_worked_all_wfs(m,strict){
            controls_enabled(()=>{
                m.all_wfs.forEach((wf)=>{
                    console.info_debug = {
                        wf_id: wf.wfId
                    }
                    if(wf.getNumberOfDaysRemained()){
                        my_throw2(20, "des jours qui ne sont pas placés",'info_debug')
                    }
                    Debug.check_not_enough_worked(wf,strict)
                })
            })
        },
        check_too_much_worked_all_wfs(m){
            controls_enabled(()=>{
                m.all_wfs.forEach((wf)=>{
                    Debug.check_too_much_worked(wf)
                })
            })
        },
        check_intervention(m){
            m.all_wfs.forEach((wf)=>{
                wf.intervsRacines.forEach((inter)=>{
                    while(inter){
                        if(inter.endMinute <= inter.startMinute){
                            my_throw( "Ce n'est pas normal")
                        }
                        inter = inter.next
                    }
                })
            })
        },
        check_li(wf){
            controls_enabled(()=>{
                let is_wf = wf instanceof Person
                let f = (li)=>{
                    if(is_wf){
                        if(wf.listInfo_by_date[li.date] !== li){
                            my_throw2(100, "incohérence dans les données des listInfo")
                        }
                    }
                    if(li.first_interv){
                        let a = li.first_minu_possible  //ça lance des vérifications
                        let b = li.last_minu_possible//ça lance des vérifications
                    }
                    console.iidebug[120] = idnext('120')
                    // if(-388 == console.iidebug[120]){
                    //     debugger
                    // }
                    let {start_minute, end_minute} = li.amplitudes_to_include()
                    // if(start_minute > li.first_minu_worked){
                    //     my_throw2(25, `ce n'est pas normal que les contraintes ne soit pas respectées`)
                    // }
                    // if(end_minute < li.last_minu_worked){
                    //     my_throw2(25, `ce n'est pas normal que les contraintes ne soit pas respectées`)
                    // }
                }
                if(is_wf){
                    for(let li of wf.listInfo_sorted_poss){
                       f(li)
                    }
                }else if (wf instanceof ListInfo){
                    let li = wf
                    wf = li.person
                    f(li)
                }else{
                    my_throw2(15,`Mais qu'est ce tu passe en paramètre`)
                }
            })
        },
        check_to_inc(to_inc){
            controls_enabled(()=>{
                if(to_inc.start_minute == Number.NEGATIVE_INFINITY || to_inc.end_minute == Number.POSITIVE_INFINITY){
                    my_throw2(10, `comment on pourrait inclure une heure de fin infinie ou une date de début négativement infinie !!!`)
                }
            })
        },

        are_cmp(...args){
            controls_enabled(()=>{
                for(let arg of args){
                    if(!(arg < 0 || arg >= 0)){
                        my_throw2(100,`ceci n'est pas comparable avec un nombre comme Number.NEGATIVE_INFINITY`)
                    }
                }
            })
        },

        others_control(){
            return;
            let wf_id = 131
            Debug.check_too_much_worked(Debug.getWf(131))
            var inter1 = Debug.getWf(wf_id).listInfo_by_date['2018-03-30'].get_inter_with_startMinute(1050)
            var inter2 = Debug.getWf(wf_id).listInfo_by_date['2018-03-30'].get_inter_with_startMinute(1230)
            if(inter1.get_real_sector() && inter1.get_real_sector().id == 33
                && inter2.get_real_sector() && inter2.get_real_sector().id == 34
            ){
                console.log(console.iDebug)
                debugger
            }
        },
                // Debug.getWf(10324).listInfo_by_date['2018-03-28'].get_hours_rules_to_keep_same_pauses()
                // Debug.check_too_much_worked(10634)
                // return;
                // inc(Debug.check_protected_still_present,'nb_call')
                //
        check_protected_still_present(){
            controls_enabled(()=>{
                Debug.others_control()
                return;
                var info_debug = {
                    person: '17',
                    date: '2018-03-30',
                    start_minute: 1350,
                    end_minute: 1410,
                    protected: true
                }

                if(!Debug.addInter_running /*&& Debug.after_init_data*/){
                    var wf = main.wfMap[info_debug.person]
                    var inter = wf.listInfo_by_date[info_debug.date].get_inter_with_startMinute(info_debug.start_minute)

                    if(!inter.sector){
                        my_throw(`Aucune intervention avec secteur`)
                    }
                    if(info_debug.protected){
                        if(!inter.true_protected_information){
                            my_throw(`ça devrait être une protected`)
                        }else{
                            if(inter.startMinute != inter.true_protected_information.start_minute
                                || inter.true_protected_information.end_minute != inter.endMinute
                            ){
                                my_throw(`ça devrait avoir true_protected_information`)
                            }
                        }
                    }
                }

                let li2 = Debug.getWf(131).listInfo_by_date['2018-03-30']
                let int1 = li2.get_inter_with_startMinute(720)
                let int2 = li2.get_inter_with_startMinute(480)

                if(int2.get_real_sector() && !int1.get_real_sector()){
                    debugger;
                }


                // for(let wf of main.all_wfs){
                //     for(let li of wf.listInfo_sorted_worked){
                //         if(li.worked < li.min){
                //             my_throw2(`70`,`Cette personne ne respecte pas le temps minimal dans la journée`)
                //         }
                //     }
                // }
                // let li = Debug.getWf(17).listInfo_by_date["2018-03-26"]
                // if(!Debug.improve_day_extremities_running){
                //     if(li.last_minu_worked < 1350){
                //         my_throw2(70,`Mais comment la journée a été diminuée`)
                //     }
                // }

                // let li2 = Debug.getWf(362).listInfo_by_date["2018-03-28"]
                // if(li2.first_minu_worked > 660){
                //     my_throw2(70,`Non respect des max_start_minute`)
                // }

                // for(let wfId of [653,631]){
                //     let li = Debug.getWf(653).listInfo_by_date["2018-04-23"]
                //     if(!Debug.improve_day_extremities_running){
                //         if(li.first_minu_worked > 8 * H){
                //             my_throw2(70,`Mais comment la journée a été diminuée`)
                //         }
                //     }
                // }


                // let li = Debug.getWf(362).listInfo_by_date["2018-03-29"]
                // var a = li.last_minu_possible
                    // let cond1 = !!Debug.getWf(131).listInfo_by_date['2018-03-27'].get_inter_with_startMinute(17.5 * 60).sector
                    // let cond2 = !Debug.getWf(131).listInfo_by_date['2018-03-27'].get_inter_with_startMinute(17 * 60).sector
                    // if(cond1 && cond2){
                    //     my_throw2(100,`il ne faut pas de pause ici`)
                    // }

                // Debug.check_li( Debug.getWf(653))
                // Debug.check_li( Debug.getWf(631))

                // li = Debug.getWf(653).listInfo_by_date['2018-03-28']
                // li.get_hours_rules_to_keep_same_pauses()//verifie la cohérence des heures

                // li = Debug.getWf(631).listInfo_by_date['2018-03-26']
                // li.get_hours_rules_to_keep_same_pauses()//verifie la cohérence des heures



            })
        }
    }
    Debug.log_period_info = function(date,start_minute,end_minute){
        console.log(`Debug.log_period_info('${date}',${start_minute},${end_minute})`)
        var table = []
        var poses = main.getPosArrInArray(date, start_minute, end_minute)
        var sum_inf = {}
        var f4 = (amp2,obj,is_sum,id_ent)=>{
            if(id_ent){
                var id_sector = id_ent
            }else{
                var id_sector = amp2.v2_sector && (typeof amp2.v2_sector == 'string') ? amp2.v2_sector.id : 'global'
            }
            let zsc = ('zscor' in amp2) ? amp2.zscor : amp2.v2_zscore_period()
            let attr = ('attra' in amp2) ? amp2.attra : amp2.v2_attractivity()
            if(!is_sum){
                sum_inf[id_sector] = sum_inf[id_sector] || {zscor: 0, attra:0,need:0,have:0}
                sum_inf[id_sector].zscor += zsc
                sum_inf[id_sector].attra += attr
                sum_inf[id_sector].need +=  amp2.need
                sum_inf[id_sector].have += amp2.have
            }
            obj[`${id_sector}_need`] = amp2.need
            obj[`${id_sector}_have`] = amp2.have
            obj[`${id_sector}_attra`] = attr
            obj[`${id_sector}_zscor`] = zsc
        }
        poses.forEach((pos)=>{
            let amp = main.total_info_amp[pos]
            var zsc = amp.v2_zscore_period()
            var attr = amp.v2_attractivity()
            obj = {
                dateKey: amp.dateKey,
                pos: pos
            }
            f4(amp,obj,false,'global')
            table.push(obj)
            amp.periods.forEach(function(amp2){
                f4(amp2,obj,false,amp2.v2_sector.id)
            })
        })
        var obj = {
            dateKey: 'TOTAL'
        }
        forOf(sum_inf,(fake_amp, id_ent)=>{
            fake_amp.v2_sector = id_ent
            f4(fake_amp,obj,true,id_ent)
        })
        table.push(obj)
        console.table(table)
    }
    Debug.error_code = {}
    Debug.reload = function() {
        try {
            console.iDebug = -1; //générer une erreur
        } catch (e) {}
        window.location.reload()
    }

    // Debug.showNeedHave = function(sector, start_date_key, end_date_key) {
    //     var by_sector = {
    //         2520: [],
    //         2521: [],
    //         2522: []
    //     }
    //     planifObj.all_amp_open.forEach(function(amp) {
    //         amp.periods.forEach(function(per) {
    //             if((!start_date_key || per.dateKey >= start_date_key) && (!end_date_key || per.dateKey <= end_date_key)) {
    //                 by_sector[per.v2_sector.id].push({
    //                     dateKey: per.dateKey,
    //                     need: per.need,
    //                     have: per.have,
    //                     zscoreAllSector: amp.v2_sum_zscore_on_period(),
    //                     zscoreSector: per.v2_zscore_period()
    //                 })
    //             }
    //         })
    //     })
    //     console.table(by_sector[sector])
    // }

    Debug.check_all_wfs_pause = (main)=>{
        controls_enabled(()=>{
            main.all_wfs.forEach(function(wf){
                wf.listInfo_sorted.forEach((li)=>{
                    let fis = li.first_interv_sector
                    if(fis){
                        let pause_is_present = false
                        for(;fis;fis = fis.next){
                            if(fis.is_pause()){
                                pause_is_present = true
                                break;
                            }
                        }
                        if(!pause_is_present){
                            // Debug.logDayWf(wf,li.date)
                            console.error(`il manque une pause pour la personne ${wf.wfId} le ${li.date} `)
                        }
                    }
                })
            })
        })

    }
    return Debug
}
