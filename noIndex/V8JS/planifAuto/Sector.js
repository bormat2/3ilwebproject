/**
 * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
 */
'' + function(){

    var allow_keys_sector_group = new Set([
        'sectors','sector_group_name'
    ])
    /* @sectorClass */
    class Sectors_group {
        constructor(o) {
            var m = this
            assert_exists(o.sector_group_name)
            controls_enabled(()=>{
                m = proxy_creator(m,allow_keys_sector_group)
            })
            m.sector_group_name = o.sector_group_name
            m.sectors = new Set()

            this.personSectorMap = {}
            return m
        }

        get id(){
            return this.sector_group_name
        }

        set id(v){
            controls_enabled(()=>{
                my_throw('action impossible')
            })
        }
        add(sector){
            var m = this
            controls_enabled(()=>{
                if(m.sectors.has(sector)){
                    my_throw('secteur déjà présent')
                }
            })
            m.sectors.add(sector)
            sector.sector_group = m
        }
    }

    var allow_keys_sector = new Set([
        "id",
        "name",
        "personSectorMap",
        "parentEntity",
        "planning",
        "minTimeSector",
        "maxTimeSector",
        "recommendedTime",
        "main",
        "is_not_counted_in_worktime",
        "pause_instance",
        "is_real_sector",
        'sector_group',
        "max_one_around_pause", // une pause ne peut pas être entourée de 2 max_one_around_pause mais seulement de 0 ou 1
    ])
    class Sector {
        constructor(o) {
            const main = o.main
            // m.planningId = o.planningId
            // m.parentEntityId = o.parentEntityId || 0;
            var in_contructor = true
            var m = this
            controls_enabled(()=>{
                if(!o.main){
                    my_throw( 'main is missing')
                }
                m = proxy_creator(this,allow_keys_sector,{
                    setter_check: (property,value,target)=>{
                        if(property == "is_not_counted_in_worktime" && !in_contructor){
                            my_throw( "cette propriété ne peut pas être changée")
                        }
                        return true
                    }
                    // is_allowed: (property)=>{
                    // }
                })
            })
            m.id = o.id;
            m.name = o.name || 'noName';
            m.personSectorMap = {} //new MyMap;
            // m.interventionMap = new MyMap;
            m.parentEntity = o.parentEntity;
            m.planning = o.planning
            m.minTimeSector = o.minTimeSector || 0.5 * H
            m.maxTimeSector = o.maxTimeSector || 50 * H
            m.recommendedTime = o.recommendedTime
            m.main = main,
            m.is_not_counted_in_worktime = o.is_not_counted_in_worktime
            m.pause_instance = o.pause_instance
            m.is_real_sector = o.is_real_sector
            if(main && !main.is_test && (
                ! (o.is_not_counted_in_worktime instanceof Function ? o.is_not_counted_in_worktime() : o.is_not_counted_in_worktime)
            ) ){
                controls_enabled(()=>{
                    if(main.need_person_sectors.disabled){
                        "il est trop tard pour déclarer un secteur nécéssitant d'être lié au personnes car comptant dans le temps de travail"
                    }
                })
                if(o.sector_group_name){
                    if(!main.sector_group_names[o.sector_group_name]){
                        main.sector_group_names[o.sector_group_name] = new Sectors_group({
                            sector_group_name: o.sector_group_name
                        })
                    }
                    main.sector_group_names[o.sector_group_name].add(m)
                }
                main.need_person_sectors.add(m.sector_group || m)
            }
            in_contructor = false
            return m
        }
        get personSectorMap(){
            if(this.sector_group){
                return this.sector_group.personSectorMap
            }
            return this._personSectorMap
        }

        set personSectorMap(val){
            controls_enabled(()=>{
                if(this.sector_group){
                    my_throw(`On ne peut affecter le person secteur map d'un secteur appartemant à un group`)
                }

                if(this._personSectorMap){
                    my_throw(`normalement cette propriété ne doit être affecté qu'un seule fois`)
                }
            })
            this._personSectorMap = val
        }
    }

    return Sector
}