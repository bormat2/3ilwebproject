/**
 * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
 */
'' + function(){
    var allow_keys_listInfo = new Set([
        '_first_interv_sector',
        '_last_interv_sector',
        'exact_opening',
        'exact_closing',
        'date',
        'debug',
        'first_interv',
        'not_rounded',
        'from',
        'id',
        'rules_pause',//quand une pause est ajouté il faut parfois désactiver certaines règles et ne plus jamais les réappliquer
        'iRacine',
        'last_interv',
        'max_start_day',
        'min_start_day',
        'max',
        'min',
        'max_end_day',
        'min_end_day',
        'meal_type',
        'person',
        'worked',
        'autorise_because_of_protected',// si une protected était sur jour interdit on a autorisé les partie en commun avec la protected
        '_rules_pause',
        'working_mandatory',//la journée est obligatoirement travaillé si true
        'disabled_time_opening',//ne pas ajouter le quart d'heure ou 15 minute d'ouverture car une protected l'empêche
        'disabled_time_closing',//ne pas ajouter le quart d'heure ou 15 minute de fermeture car une protected l'empêche
        'is_initialised_by_create_listInfo',
        'absence',//probablement pas utilisé
    ]);
    // window.keys5 = {}

    /**
     * Toutes les interventions d'une même journée pour une personne donnée 
     * possède le même listInfo qui contient la date et la personne
     * ainsi que la première et dernière inter de la journée
     * et la première et dernière intervention de la journée avec secteur
     */
    class ListInfo{//@ListInfo
        constructor(o) {
            var m = this
            controls_enabled(()=>{
                m = proxy_creator(this,allow_keys_listInfo,{
                    setter_check:(prop,val,target)=>{
                        switch(prop){
                            case "max_end_day":
                                if(val == 600 && 9910 == target.id){
                                    debugger
                                }
                                // if(11340 == target.id && val == 555 ){
                                //     debugger
                                // }
                                break
                            case "min":case "max":
                                if(val && val < conf.minTime){
                                    my_throw2(100,"il semble que les données ne sont pas en minutes mais en heures")
                                }
                                break
                            case "max_end_day":
                                // if(val == 1140 && target.last_interv.endMinute != val){
                                //     debugger
                                // }
                                break
                            // case "min_end_day":
                            //     if(val == 1020 && target.id == 7398){
                            //         debugger
                            //     }
                            //     break
                            case "last_interv":
                                // if(val && val.endMinute == 1050 && target.listInfo.max_end_day == 1140){
                                //     debugger
                                // }
                                break;

                        }
                        return true
                    }
                 })
            })
            clone(o,m)
            m.debug = {}
            controls_enabled(()=>{
                if(o.from == 'getDataFromDataBase'){
                    if(!('working_mandatory' in m)){
                        my_throw( 'working_mandatory missing')
                    }
                }
            })
            controls_enabled(()=>{
                if(m.id){
                    my_throw( 'error')
                }
            })
            m.id = idnext('no_name11')
            // if(m.id == 7398){
            //     debugger
            // }
            // if(9910 == m.id){
            //     debugger
            // }
            return m
        }

        /**
         * rules pause peut contenir une règle 
         * désactivant les controle de temps minimum
         * pour qu'on puisse sauvegardr les règles facilement les règles 
         * sont immutable et l'objet ne peut pas avoir ses propriété qui changent
         */
        set rules_pause(val){
            if(!val){
                this._rules_pause = null;
            }else if(val !== this._rules_pause){
                this._rules_pause = immutable(val)
            }
        }

        get first_minu_possible(){
            return this.first_interv.startMinute
        }
        get last_minu_possible(){
            let res =  this.last_interv.endMinute
            controls_enabled(()=>{
                if(res != this.max_end_day){
                    my_throw2(50,'ces valeurs devraient être égale')
                }
            })
            return this.last_interv.endMinute
        }

        get max_finite(){
            return min_u(this.max, this.last_interv.endMinute - this.first_interv.startMinute)
        }

        max_possible_to_add(){
            let li = this
            let max_to_add_this_day;
            if(!li.first_interv_sector){
                max_to_add_this_day = 0
            }else{
                let max_worked = li.max_finite - li.worked
                let max_end = li.last_minu_possible - li.last_minu_worked
                let max_start = li.first_minu_worked - li.first_minu_possible
                let max_start_end = Math.max(max_end, max_start)
                max_to_add_this_day = Math.min(max_start_end,max_worked)
            }
            assert_finite(max_to_add_this_day)
            return max_to_add_this_day
        }

        get rules_pause(){
            return this._rules_pause
        }

        get meal_is_forced(){
            return this.meal_type != void 8
        }

        set meal_is_forced(val){
            my_throw2(0,`il faut setter meal_type et non le booléen`)
        }

        get_all_pauses_day(){
            const li = this
            const pauses = []
            for(var inte = li.first_interv;inte;inte = inte.next){
                if(inte.is_pause()){
                    pauses.push(inte)
                }
            }
            return pauses
        }

   
        /*
        * si il y a une journée avec que une absence elle 
        * n'est pas worked mais .worked n'est pas égale à 0
        **/
        is_worked(){
            var li = this;
            return li && li.first_interv_sector
        }
        get_v2_firstORlast_interv_with_sector(prop_name) {
            var m = this;
            var ii = m[prop_name]
            if(ii || !ii && ii !== 0) {
                controls_enabled(()=>{
                    if(ii && !(ii.hasBeenInsert && ii.get_real_sector())) {
                        my_throw( "si l'intervention n'est plus un secteur ou bien a été supprimé" +
                        "la valeur 0 aurait du être mise dans first_interv_sector")
                    }
                })
            } else {
                //si l'intervention a été supprimé ou n'a plus de secteur
                //alors il faut retrouver le dernier secteur
                if(prop_name == '_last_interv_sector') {
                    var ii = m.last_interv,
                    dir = 'prev'
                } else {
                    var ii = m.first_interv,
                    dir = 'next'
                }
                while (ii) {
                    if(ii.get_real_sector()) {
                        break
                    }
                    ii = ii[dir]
                }
                m[prop_name] = ii //donc undefined si il y en a pas
            }
            if(m.id == 78070 && m._last_interv_sector && m._last_interv_sector.addIntervDebug == 1387041){
                Debug.logDayWf(m.first_interv)
                //debugger;
            }
            return ii
        }
        set first_interv_sector(val) {
            var m = this
            controls_enabled(()=>{
                if(val && !val.hasBeenInsert) {
                    my_throw( "cette inter n'est pas insérée")
                }
                if(val.startMinute < m.startMinute){
                    my_throw( "ça ne devrait pas arriver")
                }
            })
            m._first_interv_sector = val
        }
        set last_interv_sector(val) {
            var m = this
            controls_enabled(()=>{
                if(val && !val.hasBeenInsert) {
                    my_throw( "cette inter n'est pas insérée")
                }
                if(val.endMinute > m.endMinute){
                    my_throw( "ça ne devrait pas arriver")
                }
            })
            m._last_interv_sector = val
        }

        get last_interv_sector() {
          return this.get_v2_firstORlast_interv_with_sector('_last_interv_sector')
        }

        get first_interv_sector() {
        return this.get_v2_firstORlast_interv_with_sector('_first_interv_sector')
        }

        get first_minu_worked(){
            return this.first_interv_sector && this.first_interv_sector.startMinute
        }

        get last_minu_worked(){
            return this.last_interv_sector && this.last_interv_sector.endMinute
        }

        get_inter_with_startMinute(start_minute){
            var next = this.first_interv
            while(next){
                if(next.startMinute <= start_minute && next.endMinute > start_minute){
                    return next
                }
                next = next.next
            }
            return null;
        }
        // /**
        //  * Sur quel horaire doit on planifier la journée à cause des interventions protected
        //  * et des pauses
        //  * @return {[type]} [description]
        //  */
        // amplitudes_to_include(){
        //     let start_minute,end_minute,inter;
        //     const main = this.person && this.person.main
        //     if(main){
        //         for(inter = this.first_interv;inter;inter = inter.next){
        //             if(inter.protected){
        //                 if(inter.is_pause()){
        //                     //il faut de la place autour de la pause
        //                     start_minute = inter.startMinute - main.blockArr[0].sectorEntArr[0].minTimeSector
        //                 }else if(inter.get_real_sector()){//donc pas une pas une absence mais une vrai affectation
        //                     start_minute = inter.startMinute
        //                 }
        //                 break;
        //             }
        //         }
        //         if(start_minute !== void 8){
        //             const inter2 = inter
        //             for(inter = this.last_interv;inter;inter = inter.prev){
        //                 if(inter.protected){
        //                     if(inter.is_pause()){
        //                         //il faut de la place autour de la pause
        //                         end_minute = inter.endMinute + main.blockArr[0].sectorEntArr[0].minTimeSector
        //                     }else if(inter.get_real_sector()){//donc pas une pas une absence mais une vrai affectation
        //                         end_minute = inter.endMinute
        //                     }
        //                     break
        //                 }
        //             }
        //         }

        //     }
        //     return start_minute !== void 8 && {start_minute,end_minute} 
        // }
        amplitudes_to_include(){
            console.iidebug[118] = idnext('118');
            if(console.iidebug[118] == 82094){
                debugger
            }
            let li = this
            let {max_start_day: start_minute, min_end_day: end_minute} = li
            let inter;
            const main = li.person && li.person.main

            if(main){
                let protected_exist = false;
                let start_minute0
                //trouver l'heure de début la plus grande où l'on peut commencer
                for(inter = li.first_interv;inter;inter = inter.next){
                    if(inter.protected){
                        if(inter.is_pause()){
                            //il faut de la place autour de la pause
                            //start_minute0 = inter.startMinute - main.blockArr[0].sectorEntArr[0].minTimeSector
                        }else if(inter.get_real_sector()){//donc pas une pas une absence mais une vrai affectation
                            start_minute0 = inter.startMinute
                        }
                        protected_exist = true
                        start_minute = min_u(start_minute0, start_minute)
                        break;
                    }
                }
                //trouver l'heure de fin minimum 
                if(protected_exist){
                    const inter2 = inter
                    for(inter = li.last_interv;inter;inter = inter.prev){
                        if(inter.protected){
                            let end_minute0
                            if(inter.is_pause()){
                                //il faut de la place autour de la pause
                               // end_minute0 = inter.endMinute + main.blockArr[0].sectorEntArr[0].minTimeSector
                            }else if(inter.get_real_sector()){//donc pas une pas une absence mais une vrai affectation
                                end_minute0 = inter.endMinute
                            }
                            end_minute = max_u(end_minute, end_minute0)
                            break
                        }
                    }
                }
            }

            // if(li.meal_is_forced){
            //     let hours = li.get_hours_rules_to_keep_same_pauses()
            //     start_minute = max_u(start_minute, hours.start_minute)
            //     end_minute = min_u(end_minute, hours.end_minute)
            // }

            if(start_minute !== void 8){
                let ret = {start_minute,end_minute} 
                Debug.check_to_inc(ret)
                return ret
            }

        }

        /**
         * Quelles sont les heures limite pour que la pause la mêre si on replanifie la journée
         *
         * liée  à day_hours
         */
        get_hours_rules_to_keep_same_pauses({debug_pause_can_be_bad} = {}){
            const li = this
            console.iidebug[120] = idnext(120);
            // if(console.iidebug[120] == 12533){
            //     debugger
            // }
            var min_start_day = Number.NEGATIVE_INFINITY, max_start_day = Number.POSITIVE_INFINITY
            var min_end_day = Number.NEGATIVE_INFINITY, max_end_day = Number.POSITIVE_INFINITY

            Debug.are_cmp(min_start_day,max_start_day,min_end_day,max_end_day)
            let all_pauses =  this.get_all_pauses_day()
            all_pauses.forEach((pause_inter)=>{
                var hc = pause_inter.get_hours_pause_config()
                let fis = pause_inter.first_interv_sector

                let blockArrs = li.person.main.blockArr
                if(blockArrs && blockArrs[0]){
                    let offset1 = blockArrs[0].sectorEntArr[0].minTimeSector || conf.minTime
                    let left_pause = pause_inter.startMinute - offset1 
                    let right_pause = pause_inter.endMinute + offset1 
                    if(left_pause < max_start_day) max_start_day = left_pause
                    if(right_pause > min_end_day) min_end_day = right_pause
                }
          
                if(hc.min_start_day > min_start_day){({min_start_day} = hc)}
                if(hc.min_end_day > min_end_day){({min_end_day} = hc)}
                if(hc.max_start_day < max_start_day){({max_start_day} = hc)}
                if(hc.max_end_day < max_end_day){({max_end_day} = hc)}
            })
            controls_enabled(()=>{
                var is_ok = !(max_start_day < min_start_day) && !(min_end_day > max_end_day)
                    && !(max_start_day > max_end_day) && !(min_start_day > max_end_day)
                if(!is_ok){
                    my_throw("Certaines inégalité ne sont pas logique")
                }
                if(!debug_pause_can_be_bad){
                    const fmw = li.first_minu_worked, lmw = li.last_minu_worked
                    if(fmw > max_start_day || lmw < min_end_day || fmw < min_start_day || lmw > max_end_day){
                        my_throw("Mais la pause est déjà mauvaise en faite")
                    }
                }
            })

            let ati = li.amplitudes_to_include() || {}

            let start_to_inc = min_u(ati.start_minute, max_start_day)
            let end_to_inc = max_u(ati.end_minute, min_end_day)
            let start_not_before = max_u(min_start_day, li.min_start_day)
            let end_not_after = min_u(max_end_day, li.max_end_day)
            let ret2 = {//min_start_day, max_start_day, min_end_day, max_end_day,
                amplitudes_to_include: {start_minute:  start_to_inc, end_minute: end_to_inc},
                opt_min_max: {start_not_before,end_not_after}
            }

            controls_enabled(()=>{
                if(li.meal_is_forced){
                    if(start_to_inc > end_to_inc){
                        my_throw2(70,`La pause en base récupérée via le working_model aurait du forcer une heure de début dans la journée maximum plus petit que l'heure de fin dans la journée minimum`)
                    }
                }
            })

            Debug.check_to_inc(ret2.amplitudes_to_include)
            Debug.are_cmp(start_to_inc,end_to_inc,start_not_before,end_not_after)
            return ret2
        }




        check_logic(){
            controls_enabled(()=>{
                var m = this;
                if(m.first_interv && m.first_interv.prev){
                    my_throw( 'la première ne peut pas avoir de précédent')
                }
                if(m.last_interv && m.last_interv.next){
                    my_throw( 'la dernière ne peut pas avoir de suivant')
                }
                var first_interv_sec = m._first_interv_sector
                    if(first_interv_sec){//si il y a au moins une fois où l'on est affecté à un secteur
                        var vliwsn = m._last_interv_sector.next
                    if(vliwsn && vliwsn.get_real_sector()){
                        window.vliwsn = vliwsn
                        my_throw( 'la dernière avec secteur ne peut pas pas avoir de suivante avec secteur')
                    }
                    var vfiwsp = first_interv_sec.prev
                    if(vfiwsp && vfiwsp.get_real_sector()){
                        window.vfiwsp = vfiwsp
                        my_throw( 'la dernière avec secteur ne peut pas pas avoir de précédente avec secteur')
                    }
                }
                // m.get_hours_rules_to_keep_same_pauses()
            })
        }


        update_opening_closing_time({add_to_update}){
            const lInfo = this
            if(lInfo.exact_opening && !lInfo.exact_opening.hasBeenInsert){
                lInfo.exact_opening = null
            }
            if(lInfo.exact_closing && !lInfo.exact_closing.hasBeenInsert){
                lInfo.exact_closing = null
            }

            if(lInfo.first_minu_worked){
                //c'était la première intervention à une ouverture mais maintenant il y en a d'autre avant 
                //alors la mettre à jour
                if(lInfo.exact_opening){
                    if(lInfo.first_interv_sector !== lInfo.exact_opening){
                        add_to_update(lInfo.exact_opening)
                        lInfo.exact_opening = lInfo.exact_opening = null
                    }
                }
                if(lInfo.exact_closing){
                    if(lInfo.last_interv_sector !== lInfo.exact_closing){
                        add_to_update(lInfo.exact_closing)
                        lInfo.exact_closing = null
                    }
                }
                const wf = lInfo.person
                if(wf){
                    const is_opening = wf.opening_info({start_minute: lInfo.first_minu_worked,date: lInfo.date}).is_supplement_time()
                    if(is_opening && lInfo.exact_opening != lInfo.first_interv_sector){
                        lInfo.exact_opening = lInfo.first_interv_sector
                        add_to_update(lInfo.exact_opening)
                    }
                    const is_closing = wf.closing_info({end_minute: lInfo.last_minu_worked,date: lInfo.date}).is_supplement_time()
                    if(is_closing && lInfo.exact_closing != lInfo.last_interv_sector){
                        lInfo.exact_closing = lInfo.last_interv_sector
                        add_to_update(lInfo.exact_closing)
                    }
                }
            }else{
                lInfo.exact_closing = lInfo.exact_opening = null
            }
        }
    }

    return ListInfo
}