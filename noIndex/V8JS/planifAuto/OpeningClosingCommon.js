/**
 * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
 */
 '' + function(){
    class OpeningClosingCommon extends Rotation {
        constructor(main) {
            controls_enabled(()=>{
                if(!main){
                    my_throw( 'missing main')
                }
            })
            super(main)
            controls_enabled(()=>{
                if(!OpeningClosingCommon.child_class){
                    OpeningClosingCommon.child_class = new WeakSet()
                }else{
                    OpeningClosingCommon.child_class.add(this.__proto__)
                    ;['get_limit_minute','get_names'].forEach((name) => {
                        if(void 8 == this[name]){
                            my_throw( `${name} does not exist in class ${this.constructor.name}`)
                        }
                    })
                }
            })
            this.tolerance = 1
            this.init()
        }

        // is_wf_date_rotation(wf,date,n){
        //     const first_minu_worked = wf[n.get_first_minu_worked]()
        //     if(!first_minu_worked) return false
        //     const is_rotation = wf[n.opening_info]({
        //         [n.start_minute]: first_minu_worked,
        //         date: date
        //     })[n.before_or_equal]('rotation')
        //     return is_rotation
        // }

        /*
         * si on est en ouverture ou veut l'intervention de la fin de la journée
         * alors qu'en fermeture ou veut la première de la journée
         */
         get_the_far_interv_of_same_day() {
            abstractError()
        }

        /*@depreciated*/
        reduce_rotation_for_wf_without_replanification(){
            lastORfirst_inter = rota.get_the_far_interv_of_same_day(theInter)
            // last_minute_worked = lastORfirst_inter.endMinute
            lastORfirst_inter_info = {
                start_minute: lastORfirst_inter.startMinute,
                end_minute: lastORfirst_inter.endMinute,
            }
            last_sector = lastORfirst_inter.sector

            //faire en sorte que la première intervention ne soit plus une ouverture
            if(replanif_the_day){

            }else{
                seria = theInter.v2_serialize()
                old_sector = theInter.sector
                old_time_opening = theInter.time
                beforeDelete = {
                    startMinute: theInter.startMinute,
                    endMinute: theInter.endMinute
                }
                if(!theInter.reset_inter()) {
                    echec = 'A2: une intervention protected bloque l\algo';
                    //pas besoin d'annuler on a rien fait
                    return;
                }
                old_startORend = rota.get_old_startORend(seria)
                nb_minute_to_remove = rota.update_seria_to_delete_the_rotation_on_first_person(seria)
                seria.rules = {
                    dontMoveExistingInterventionOrPause: true, // comme on a supprimé le secteur avant il y a normalement la place
                    v2_do_not_respect_min_time_after_before: true, // ne pas vérifier le temps restant après ou avant l'intervention
                    v2_break_if_min_time_not_ok: true //c'est le mintime de cette intervention que l'on va vérifier et si il est pas bon on n'ajoutera pas
                }
                seria.dontAffectNow = true

                //on remet l'intervention que l'on a enlevée mais avec des heures réduites pour ne plus être une ouverture
                inter4 = new Intervention(seria)
                adi4 = seria.person.addInterv(inter4) //si ça rate tampi on ajoutera plus le soir
                if(!theInter.is_day_ok({
                    ignore_working_time: true
                })) {
                    //si le début et la fin de la journée ne sont pas bon alors annuler
                    undoFunc_1()
                    console.error('à cause des début et fin de journée ou doit annuler le changement de rotation')
                    echec = true
                    return
                }
            }

            //mettre ce que l'on a enlevé le matin le soir
            ;
            (function() {
                //combien d'heure à t'on vraiment enlevé ?
                var nb_minute_to_really_remove_opening = adi4.has_been_insert ? nb_minute_to_remove : old_time_opening
                //ajouter ce que l'on a enlevé au début de la journée en fin de journée
                rota.update_seria_to_add_what_we_delete_at_the_other_side_of_the_same_day(seria,
                    lastORfirst_inter_info,
                    nb_minute_to_really_remove_opening
                    )
                // seria.startMinute = last_minute_worked
                // seria.endMinute = seria.startMinute + nb_minute_to_really_remove_opening
                seria.sector = last_sector
                var inte = new Intervention(seria)
                seria.person.addInterv(inte)
                if(!inte.hasBeenInsert) {
                    echec = 'ajout le soir (matin si closing) raté'
                }
                if(!theInter.is_day_ok({
                    ignore_working_time: true
                })) {
                    //si le début et la fin de la journée ne sont pas bon alors annuler
                    echec = 'à cause des début et fin de journée ou doit annuler le changement de rotation'
                    console.error(echec)
                    return
                }
            })();
            if(echec) {
                console.error('echec de inte ' + echec)
                undo_what_we_do()
                return;
            }
            var rotation_ratio_wf_aft = wf.rotation_ratio[rota_name] || 0

            console.log(
                "person " + wf.wfId +
                " déplacement de " + nb_minute_to_remove +
                " de " + date +
                " réduction de " + beforeDelete.startMinute + ' à ' + beforeDelete.endMinute + " vers " +
                adi.startMinute + ' à ' + adi.endMinute +
                ' ajout de ' + seria.startMinute + ' à ' + seria.endMinute +
                ' le ratio de rotation est ainsi passé de ' + rotation_ratio_wf_bef +
                ' à ' + rotation_ratio_wf_aft
                // + lastORfirst_inter_info.end_minute
                );

            //var is_pause_ok = main.main_resolve_pause_unrespected({
                //intervention: wf.get_first_interv_at_date(date)
            //}).pause_is_ok

            var rpatmid =  m.resolve_pause_and_time_in_day(inte,new opt_v2_schedule_planning({
                include_this_amplitude: {
                    start_minute: inter11.start_minute,
                    end_minute: inter11.end_minute
                }
            }))

            if(rpatmid.probleme) {
                console.error('echec de pause ')
                undo_what_we_do()
                return;
            }
        }

        /**
         * worked_this_day: ce qu'il y avait comme temps avant déplanification
         */
         replanif_wf_without_condition({date,wf,get_rotation_score,debug_callback}){
            const old_rota_score = get_rotation_score()
            const rota = this, m = rota.main, n = rota.names, lid = wf.listInfo_by_date[date], old_nb_minutes = lid.worked
            const zscore = m.global_score_ignoring_sectors()
            const rot_op_nam = rota.opposite_rota.names
            const {is_rotation_opening: is_rotation_closing,force_opening_rota: force_closing_rota,not_opening_rota: not_closing_rota} = rot_op_nam
            const was_closing = wf[is_rotation_closing](date);//la personne était en fermeture
            // let rota_before_not_enough_worked;
            var undoFunc_1bis = m.v2_try_a_change({person: wf,date})
            var undo_solve_not_enough_worked
            var undoFunc_1 = function(){
                undoFunc_1 = null;
                // if(rota_before_not_enough_worked != void 8){
                //     undo_solve_not_enough_worked()
                //     if(rota_before_not_enough_worked != get_rotation_score()){
                //         my_throw("L'annulation n'a pas fonctionné")
                //     }
                // }
                undoFunc_1bis && undoFunc_1bis()
            }
            console.iidebug[79] = idnext('79')
            if(console.iidebug[79] == 39992){
                debugger;
            }
            debug_callback()

            var max_in_day = wf.listInfo_by_date[date].worked
            wf.reset_date(date,{even_if_pause_fake_protected: true})
            const first_minu_worked = lid[n.first_minu_worked]
            if(first_minu_worked){
                let is_still_rotation = wf[n.is_rotation_opening](date)
                if(is_still_rotation){
                    //la journée est encore une ouverture malgrès le reset_date donc annuler
                    undoFunc_1 && undoFunc_1();
                    return {error_code: 1101}
                }
            }
            // Peut on ajouter une fermeture à la personne actuellement en ouverture
            // car elle risque de passer de l'un à l'autre
            let can_be_at_closing = was_closing || get_rotation_score.is_poss_add_rota(wf,n.closing_rot_name)

            const opt7 = {dates: [date],only_these_pers: [wf],get_dates_added: false,get_backup_functions: false}
            const amp_to_inc = lid.amplitudes_to_include()
            let already_reseted = true//la journée a été vidé de ce qui était possible

            let replanifP1 = function({nb_minutes, min_end_minute,not_rota}){
                let new_zscore1 =  m.global_score_ignoring_sectors()
                let free_of_params = nb_minutes === void 8 && min_end_minute === void 8 && not_rota === void 8
                console.iidebug['replanifP1'] = idnext('replanifP1')
                if(console.iidebug['replanifP1']  == 58612){
                    debugger
                }
                if(!already_reseted){
                    wf.reset_date(date,{even_if_pause_fake_protected: true})
                }
                already_reseted = false
                // Ne pas planifier plus que ce qu'il y avait au départ
                if(!(nb_minutes < max_in_day)){//@warning undefined
                    nb_minutes = max_in_day
                }
                if(amp_to_inc){//à cause d'une protected ou d'une pause forcée
                    opt7.include_this_amplitude = amp_to_inc
                    // debugger;//@TODO_fast
                    const diff = opt7.include_this_amplitude.end_minute - opt7.include_this_amplitude.start_minute
                    if(diff > nb_minutes){
                        nb_minutes = diff
                    }
                    //@todo_en vrai on devrait demander un minimum et pas un temps fixe à placer
                    if(nb_minutes > max_in_day){
                        nb_minutes = max_in_day
                    }
                }
                // opt7.minutes_to_place = nb_minutes
                let omm = opt7.opt_min_max = {
                    [n.not_closing_rota]: !can_be_at_closing
                }
                if(min_end_minute !== void 8){
                    omm[n.end_not_before] = min_end_minute
                }
                if(not_rota){
                    omm[n.not_opening_rota] = true
                }
                var ret_schel = m.v2_schedule_planning(opt7)
                if(!not_rota){
                    Debug.check_protected_still_present()
                }
                let new_zscore2 =  m.global_score_ignoring_sectors()
                controls_enabled(()=>{
                    if(free_of_params && new_zscore2 > new_zscore1){
                        my_throw2(5,`La replanification a empiré le score`)
                    }
                })

                return {
                    is_planif_ok: ret_schel.date_ok[opt7.dates[0]]
                }
            }



            let {is_planif_ok} = replanifP1({not_rota: true})
            let is_still_rotation = wf[n.is_rotation_opening](date)
            controls_enabled(()=>{
                if(is_still_rotation){
                    my_throw2(100,'anormal on a demandé le contraire')
                }
            })
            if(!is_planif_ok || is_still_rotation){
                //cette journée ne peut pas ne pas être en rotation
                undoFunc_1 && undoFunc_1();
                return {error_code: 1102}
            }
            //replanifier P1 au mieux
            replanifP1({})
            is_still_rotation = wf[n.is_rotation_opening](date)
            // si is_still_rotation on va chercher à mettre une autre personne à la place sinon
            // on va pas essayer car ça veut dire que le reste de la journée a plus besoin de monde
            // que l'ouverture
            Debug.replanif_wf_without_condition = true
            let new_zscore =  m.global_score_ignoring_sectors()
                // controls_enabled(()=>{
                //     if(new_zscore > zscore){
                //         my_throw2(5,`La replanification a empiré le score`)
                //     }
                // })

                return {
                    is_still_rotation,
                    undoFunc_1,
                    replanifP1,
                }
            }

        //@depreciated
        // reduce_rotation_for_wf_with_replanification({wf,date,get_rotation_score,debug_callback}){
        //     const old_rota_score = get_rotation_score()
        //     const rota = this, m = rota.main, n = rota.names, lid = wf.listInfo_by_date[date], old_nb_minutes = lid.worked
        //     const rot_op_nam = rota.opposite_rota.names
        //     const {is_rotation_opening: is_rotation_closing,force_opening_rota: force_closing_rota,not_opening_rota: not_closing_rota} = rot_op_nam
        //     const was_closing = wf[is_rotation_closing](date);//la personne était en fermeture
        //     let rota_before_not_enough_worked;
        //     var undoFunc_1bis = m.v2_try_a_change({person: wf,date})
        //     var undo_solve_not_enough_worked
        //     var undoFunc_1 = function(){
        //         undoFunc_1 = null;
        //         if(rota_before_not_enough_worked != void 8){
        //             undo_solve_not_enough_worked()
        //             if(rota_before_not_enough_worked != get_rotation_score()){
        //                 my_throw("L'annulation n'a pas fonctionné")
        //             }
        //         }
        //         undoFunc_1bis && undoFunc_1bis()
        //     }
        //     console.iidebug[79] = idnext('79')
        //     if(console.iidebug[79] == 97446){
        //         // debugger;
        //     }
        //     debug_callback()

        //     var max_in_day = wf.listInfo_by_date[date].worked
        //     wf.reset_date(date,{even_if_pause_fake_protected: true})
        //     const first_minu_worked = lid[n.first_minu_worked]
        //     if(first_minu_worked){
        //         let is_still_rotation = wf[n.is_rotation_opening](date)
        //         if(is_still_rotation){
        //             //la journée est encore une ouverture malgrès le reset_date donc annuler
        //             undoFunc_1 && undoFunc_1();
        //             return {error_code: 1101}
        //         }
        //     }




        //     const get_opt_min_max = (was_closing)=>{
        //         //empêcher de remettre en ouverture
        //         let opt_min_max = {[n.not_opening_rota]: true}
        //         // si la personne etait en ouverture et qu'on la sort de l'ouverture
        //         // il y a un risque qu'elle aille en fermeture, il faut voir si on a le droit
        //         if(was_closing){
        //             // il y a déjà une fermeture, ce qui veut dire que la personne travail
        //             // beaucoup aujourd'hui pour être en ouverture et en fermeture
        //             // cependant là on s'occupe des ouverture et pas des fermetures donc
        //             // on remet comme c'était
        //             // sinon il va aussi falloir trouver un remplaçant pour la fermeture
        //             opt_min_max[force_closing_rota] = true
        //         }else{
        //             if(!get_rotation_score.is_poss_add_rota(wf,n.closing_rot_name)){
        //                 // empêcher de mettre en fermeture seulement si la personne passerait au
        //                 // dessus de la moyenne de rotation_ratio_relative
        //                 // sinon on autorise si cela est mieux pour le score
        //                 opt_min_max[not_closing_rota] = true
        //             }
        //         }
        //         return opt_min_max
        //     }
        //     const opt7 = {dates: [date],only_these_pers: [wf],get_dates_added: false,get_backup_functions: false,opt_min_max: get_opt_min_max(was_closing)}
        //     let nb_minutes = lid.min
        //     const amp_to_inc = lid.amplitudes_to_include()
        //     if(amp_to_inc){//à cause d'une protected ou d'une pause forcée
        //         opt7.include_this_amplitude = amp_to_inc
        //         // debugger;//@TODO_fast
        //         const diff = opt7.include_this_amplitude.end_minute - opt7.include_this_amplitude.start_minute
        //         if(diff > nb_minutes){
        //             nb_minutes = diff
        //         }
        //         //@todo_en vrai on devrait demander un minimum et pas un temps fixe à placer
        //         if(nb_minutes > max_in_day){
        //             nb_minutes = max_in_day
        //         }
        //     }
        //     opt7.minutes_to_place = nb_minutes
        //     var ret_schel = m.v2_schedule_planning(opt7);
        //     if(upper(wf.nb_minu_worked) < wf.nbMinutesMin){
        //         console.warn('Annulation car on a pas pu placer le temps restant')
        //         undoFunc_1 && undoFunc_1();
        //         return {error_code: 1100}
        //     }
        //     controls_enabled(()=>{
        //         if(wf.getNumberOfDaysRemained()){
        //             console.error("la personne n'a pas été replanifié c'est étrange")
        //         }
        //     })
        //     controls_enabled(()=>{
        //         if(old_nb_minutes != nb_minutes && wf.listInfo_by_date[date].worked == old_nb_minutes){
        //             my_throw( "pk il n'y a pas de temps dispo alors qu'on a mis moins que avant")
        //         }
        //         if(get_rotation_score() >= old_rota_score){
        //             my_throw( "mais pk les rotations n'ont pas été améliorées")
        //         }
        //     })
        //     debug_callback()
        //     if(console.iidebug[79] == 77603){
        //         debugger;
        //     }
        //     //pour empêcher de mettre en rotation ouverture
        //     //on doit recalculer opt_min_max mais avec was_closing = false cette fois
        //     rota_before_not_enough_worked = get_rotation_score()
        //     var snew = m.solve_not_enough_worked([wf],{
        //         need_undo_func: true,
        //         opt_min_max: {
        //             do_not_make_rotation_worst: true,
        //             get_rotation_score,
        //         }
        //     })
        //     debug_callback()
        //     undo_solve_not_enough_worked = snew.undo_func;

        //     controls_enabled(()=>{
        //         if(wf[n.is_rotation_opening](date)){
        //             my_throw( `la personne est encore en ouverture ce n'est pas normal opt_min_max ne fait pas son job`)
        //         }
        //     })
        //     if(upper(wf.nb_minu_worked) < wf.nbMinutesMin){
        //         console.warn('Annulation car on a pas pu placer le temps restant')
        //         undoFunc_1 && undoFunc_1();
        //         return {error_code: 1100}
        //     }
        //     console.green('succes1100')
        //     Debug.success_reduce_rotation_for_wf_with_replanification
        //     return {success: true,undoFunc_1}
        // }



        /*
         * si la classe courrante est Closing alors pour lire les commentaire de code suivant il faut inverser closing et opening
         * Réduire le nombre d'ouverture faite par le worksfor et mettre cette ouverture à quelqu'un d'autre
         *
         */
         reduce_rotation_for_wf(wf, score, get_rotation_score,{debug_callback}) {
            const replanif_the_day = true,rota = this,rota_name = rota.name,m = rota.main;
            var echec = false,opennings_period = []
            console.iDebug14 = 1 + (console.iDebug14 || 0)
            forOf(wf.rotationMaps[rota.name], function(map) {
                if(map.length) {
                    //on récupère une intervention d'ouverture quelconque dans la journée
                    opennings_period.push(map.get_one())
                }
            })
            // maintenant que l'on a trouvé une période opening
            // il faut l'enlever comme il en a trop de ce type

            const iidebug8 = []
            const check_rotation = () =>  controls_enabled(()=>{
                if(score != get_rotation_score()){
                    my_throw("Le score de rotation n'a pas bien été réstoré")
                }
            })
            const next_some1 = function(){
                Debug.check_not_enough_worked_all_wfs(m)
                check_rotation()
            }
            const continue_or_stop_loop = function(){
                if(!get_rotation_score.is_poss_del_rota(wf,rota.name)){
                    return STOP_LOOP
                }else{
                    return !STOP_LOOP
                }
            }
            RandomGenerator.shuffle(opennings_period)
            some1: opennings_period.some((opening_period) => {
                Debug.check_not_enough_worked_all_wfs(m)
                debug_callback && debug_callback()
                let undoFunc_2,undoFunc_1,error_code
                //// debug_callback && debug_callback()
                var zscore_start_some = m.v2_get_zscore_global()
                var backup_week = wf.backup_week()
                echec = false
                console.iidebug[8] = idnext('8')
                iidebug8.push(console.iidebug[8])
                if(173821 == console.iidebug[8]){
                    controls_enabled(()=>{if(score != get_rotation_score()) {my_throw( 'some1_rotation_erreur') }})
                    debugger
                }

                var date = opening_period.date
                var info_someone_else,undoFunc2
                // var mega_undo = function() {
                //     undoFunc2 && undoFunc2()
                //     undoFunc_1 && undoFunc_1()
                //     controls_enabled(()=>{if(score != get_rotation_score()) {my_throw( 'some1_rotation_erreur2') }})
                // };
                //pour ce cas-ci on sauvegarde la journée entière
                var adj4,inter4
                console.iidebug[14] = idnext('14')
                if(70267 ===  console.iidebug[14]){
                    debugger
                }
                check_rotation()

                // undo_what_we_do = undoFunc_1
                var last_minute_worked, last_sector;
                var rotation_ratio_wf_bef = wf.rotation_ratio[rota_name] || 0
                var lastORfirst_inter, lastORfirst_inter_info, last_sector, seria, old_sector, old_time_opening, beforeDelete, old_startORend, nb_minute_to_remove, inter4;

                //  // enlever le matin
                let is_still_rotation;
                //enlever l'ouverture à la personne juste pour voir si c'est possible puis la replanifier dans la journée
                let rwwc = rota.replanif_wf_without_condition({date,wf,get_rotation_score,debug_callback})
                ;({error_code,undoFunc_1,is_still_rotation} = rwwc)
                if(error_code){
                    return next_some1();
                };
                let replace_by_a_new_person = is_still_rotation;//il faut mettre une autre personne à la place de la notre
                if(!replace_by_a_new_person){
                    // la personne a été enlevée de l'ouverture de manière naturelle
                    // car elle est plus utile ailleur
                    score = get_rotation_score()
                }else{
                    var new_score = get_rotation_score()
                    var success
                    if(new_score > score) {
                        echec = 'le score a empiré'
                        my_throw2(70,`Il est anormal que cet algo empire les rotations`)
                        undoFunc2 = null;
                    } else {
                        let {replanifP1} = rwwc
                        //debug_callback && debug_callback()
                        info_someone_else = rota.v2_place_someone_else_at_this_opening({replanifP1,undoFunc_1,rota_name,old_wf: wf,date,replanif_the_day,get_rotation_score,debug_callback})
                        ;({score} = info_someone_else)
                        debug_callback && debug_callback()
                        if(!info_someone_else.hasBeenInsert) {
                            echec = 'nobody else found to replace'
                        }
                    }
                    console.iidebug[12] = idnext('12')
                }
                next_some1()
                return continue_or_stop_loop()
            })
            debug_callback && debug_callback()
            controls_enabled(()=>{
                if(score != get_rotation_score()) {
                    my_throw( 'erreur')
                }
            })
            ++console.iidebug[5];
            return {
                score
            } //le score a été réduit ou bien est resté pareil donc il faut le retourner
        }


        get_replanif_p2(lid){
            let rota, wf,date,main,n,nb_call,include_this_amplitude,minutes_to_place;
            ;(()=>{
                rota = this;
                minutes_to_place = lid.worked
                wf = lid.person
                ;({date} = lid)
                ;({main} = rota)
                // undoFunc_2 = m.v2_try_a_change({person:wf,date})
                n = this.names
                nb_call = 0
                include_this_amplitude = lid.amplitudes_to_include()
                // const amp_to_inc = include_this_amplitude
                // if(amp_to_inc){//à cause d'une protected                       debugger;//@TODO_fast
                //     //@todo mette la bonne valeur pour diff qui prend en compte les pauses
                //     const diff = amp_to_inc.end_minute - amp_to_inc.start_minute
                //     if(diff > nb_minutes){
                //         nb_minutes = diff
                //     }
                // }
                //Le min end_minute permet que la personne2 ne finisse pas avant l'ancienne fin de la personne 1
            })()
            let replanifP2 = ({force_opening_rota,min_end_minute, do_not_check_forcing_rota}) => {
                let opt_min_max;
                ++nb_call;
                console.iidebug[121] = idnext('121')
                if(38674 == console.iidebug[121] ){
                    debugger
                }
                wf.reset_date(date,{
                    even_if_pause_fake_protected: true
                })
                if(force_opening_rota){
                    opt_min_max = {
                        [n.force_opening_rota]: true
                    }
                }
                controls_enabled(()=>{
                    if(nb_call == 2 && !force_opening_rota){
                        my_throw2(100,`anormalité replanif`)
                    }
                    if(nb_call > 2 && force_opening_rota){
                        my_throw2(100,`anormalité replanif 2`)
                    }
                })
                var opt15 = {
                    dates: [date],
                    only_these_pers: [wf],
                    get_dates_added: false,
                    get_backup_functions: false,
                    include_this_amplitude,
                    opt_min_max,//force l'ouverture
                    // minutes_to_place
                }

                if(min_end_minute !== void 8){
                    opt15[n.end_not_before] = min_end_minute
                }

                let sp = main.v2_schedule_planning(opt15)
                if(sp.date_ok[date]){
                    controls_enabled(()=>{
                        if(!do_not_check_forcing_rota){
                            let is_become_rotation_P2 = wf[n.is_rotation_opening](date)
                            if(!is_become_rotation_P2){
                                my_throw2(70,`echec forçage rota`)
                            }
                        }
                    })
                }
                return {
                    is_planif_ok: sp.date_ok[date]
                }
            }
            return replanifP2
        }

        /*
         * Placer quelqu'un à l'opening pour remplacer le "old_wf" qui a enlevé son intervention à "date"
         * et "old_start" du secteur "old_start"
         *
         * old_wf: la personne qui était à l'intervention que l'on a enlevée, on a besoin de le savoir pour ne pas mettre
         * date: // date de l'intervention que l'on a enlevé, c'est aussi celle on l'on va ajouter
         * replanif_the_day: c'est le nouveau mode qui replanifie la journée de ce que l'on modifie
         */
         v2_place_someone_else_at_this_opening({
            old_wf,
            date,
            get_rotation_score,
            replanif_the_day,
            debug_callback,
            replanifP1,
            undoFunc_1
        }) {
            undoFunc_1()
            let score;
            const init_score_rot = score = get_rotation_score();
            const refF = () => {
                info_someone_else = {
                    hasBeenInsert:error_code === 0,
                    error_code,
                    score,
                    wf: wf_to_return,
                    // undoFunc: error_code === 0 ? mega_undo : null //si on a déjà annulé à quoi bon transférer la fonction d'annulation
                } //si c'est faux c'est que l'on a parcouru toute les personne de la journée et qu'aucune ne pouvait déplacer la fin au début de la journée
                controls_enabled(()=>{
                    Debug.check_not_enough_worked_all_wfs(m)

                    // if(!info_someone_else.undoFunc && get_rotation_score() != init_score_rot){
                    //     my_throw("Si le score a changé il faut une fonction d'annulation pour remettre dans l'état initial si besoin ou bien il fallait appeler mega_undo lors de la création de l'erreur")
                    // }
                    //
                    let new_score4 = get_rotation_score()
                    if(new_score4 > init_score_rot){
                        my_throw2(100,`cette algo était supposé réduire et non augmenter`)
                    }
                    if(new_score4 !== score){
                        my_throw2(70,`il faut mettre à jour le score quand on fait une action suceptible de le réduire`)
                    }
                    assert_exists(old_wf)
                })
                return info_someone_else
            }
            var mega_undo = function({do_not_undo_p2} = {}){
                undoFunc_1()//en vrai ça sert juste a passer le test debug_callback
                undo_solve_not_enough_worked2 && undo_solve_not_enough_worked2()
                if(!undoFunc_2){
                    controls_enabled(()=>{
                        my_throw('undoFunc_2 devrait existé')
                    })
                    console.log(info_someone_else)
                }else{
                    if(do_not_undo_p2){
                        if(get_rotation_score() > init_score_rot){
                            my_throw('Le score est supposé réduire')
                        }
                    }else{
                        undoFunc_2()
                        controls_enabled(()=>{
                            if(get_rotation_score() != init_score_rot){
                                my_throw('La restauration a raté pour undoFunc_2')
                            }
                        })
                    }
                }
            }
            console.iidebug[82] = idnext('82')
            const rota = this, m = rota.main, n = rota.names;
            var undoFunc_2,error_code,undo_solve_not_enough_worked2, info_someone_else
            console.iDebug_v2_place_someone_else_at_this_opening = idnext('seato');
            let success_replacement = false; //est ce que l'on a pu trouver une personne à mettre à cette ouverture
            // Il faut maintenant trouver une autre personne à décaller
            // pour prendre la place de la personne que l'on a enlever
            // on va chercher la personne la plus proche dans le secteur courrant
            var person_that_cant_be_at_opening = new Set()
            // il ne faut pas que la personne que l'on prend soit au dessus de la moyenne pour les rotations
            // ou au dessus de nous
            m.all_wfs.forEach(function(wf2) {
                if(180212 == console.iidebug[82] && wf2.wfId == 10417){
                    debugger
                }
                //peut on ajouter une ouverture à la personne
                if(wf2[n.is_rotation_opening](date) || !get_rotation_score.is_poss_add_rota(wf2,rota.name)){
                    person_that_cant_be_at_opening.add(wf2)
                }
                //si la personne est en fermeture et qu'on la met en ouverture
                //on va lui enlever une fermeture donc il faut aussi regarder si
                //on a le droit de faire ça
                const is_rotation_closing = rota.opposite_rota.names.is_rotation_opening
                if(wf2[is_rotation_closing](date)){
                    if(!get_rotation_score.is_poss_del_rota(wf2,n.closing_rot_name)){
                        person_that_cant_be_at_opening.add(wf2)
                    }
                }
            })

            // on boucle sur les intervention de la periode de ce secteur
            // et on regarde si les personnes dans ces secteurs peuvent être avancer dans la journée
            // pour faire l'ouverture
            const firstORlast_interv_and_wfId = []
            m.all_wfs.forEach(function(wf3) {
                var wfId = wf3.wfId
                if(!person_that_cant_be_at_opening.has(wf3)) {
                    var inter = rota.get_firstORlast_interv_with_sector_at_date(wf3, date)
                    if(inter) {
                        firstORlast_interv_and_wfId.push({
                            firstORlast_interv: inter,
                            wf_id: wfId
                        })
                    }
                }
            })
            //Dans le cas de openning on met dans l'ordre des heures sinon l'inverse
            rota.sort_and_put_nearest_interv_first(firstORlast_interv_and_wfId)

            let wf_to_return;

            let get_lid = (wf)=>{
                const lid = wf.listInfo_by_date[date]
                if(!lid || !lid.is_worked()){
                    my_throw2(70,`cela aurait du être filtré dans quand on a rempli firstORlast_interv_and_wfId`)
                }
                return lid
            }
            // if(!replanif_the_day){
            //     // rota.v2_place_someone_else_at_this_opening_old()
            // }else{
                //on va placer ne nouvelle personne en ouverture
            // debug_callback && debug_callback()
            let debugs = []
            swap_loop : for(let wf_inter of firstORlast_interv_and_wfId){
                error_code = 0
                let zscore4 = m.global_score_ignoring_sectors()
                console.iidebug[89] = idnext('89')
                debugs.push(console.iidebug[89])
                if(54813 == console.iidebug[89]){
                    debugger
                }
                let wf_p2 = wf_inter.firstORlast_interv.person
                undoFunc_2 = m.v2_try_a_change({person:wf_p2,date})

                let lid = get_lid(wf_p2)
                let replanifP2 = rota.get_replanif_p2(lid)
                //juste replanifier P2 pour le placer au meilleur endroit et que le min_end_minute que l'on prenne est un sens
                let {is_planif_ok: is_planif_ok2 } = replanifP2({do_not_check_forcing_rota: true})

                if(!is_planif_ok2){
                    error_code = 1309
                    mega_undo()
                }else{
                    let min_end_minute = lid[n.last_minu_worked]

                    //on enlève p2 de la journée car on souhaite que p1 prennent sa place
                    wf_p2.reset_date(date,{
                        even_if_pause_fake_protected: true
                    })
                    assert_finite(min_end_minute)

                    get_lid(old_wf)//debug

                    // replanifier p1 en le faisant finir au plus tôt là où finissait p2
                    // et hors ouverture
                    let {is_planif_ok} = replanifP1({min_end_minute, not_rota: true})
                    if(!is_planif_ok){
                        //la condition min_end_minute a empêché de planifier p1
                        error_code = 1304
                        mega_undo()
                    }else{
                        controls_enabled(()=>{
                            let is_still_rotation_P1 = old_wf[n.is_rotation_opening](date)
                            if(is_still_rotation_P1){
                                my_throw2(100,`Ce n'est pas logique on avait testé à la création de replanifP1 que la personne pouvait être placé sans rotation`)
                            }
                        })
                        get_lid(old_wf)//debug
                        if(is_planif_ok){
                            get_lid(old_wf)//debug

                            /*if(37924 == console.iidebug[8]){
                                debugger
                            }*/
                            let sp,opt_min_max;
                            //replanifier P2 en forçant l'ouverture juste pour pour voir si c'est possible
                            let {is_planif_ok} = replanifP2({force_opening_rota: true})
                            if(! is_planif_ok){
                                // Mince p2 ne peut pas aller en ouverture
                                // Chercher une autre personne
                                mega_undo()
                                error_code = 1301
                            }else{
                                controls_enabled(()=>{
                                    let is_become_rotation_P2 = wf_p2[n.is_rotation_opening](date)
                                    if(!is_become_rotation_P2){
                                        my_throw2(`echec forçage rota`)
                                    }
                                })
                                // ce n'est pas parce que p2 peut aller en ouverture qu'il doit y aller
                                // si ce n'est pas le mieux pour le score, donc replanifier p2 et p1

                                // p1 étant déjà placé hors ouverture et se finit après ou pareil que P2
                                // le fait de replanifier p2 sans règle va sûrement mettre P2 en ouverture

                                replanifP2({})
                                Debug.check_not_enough_worked(wf_p2)
                                // comme P2 est sûrement placé en ouverture
                                // P1 était déjà en fin de journée mais là il va pouvoir se placer un peu plus tard dans la journée car il y a des heures
                                // de croisement commun entre P2 et P1
                                if(37924 == console.iidebug[8]){ debugger}
                                replanifP1({})
                                Debug.check_not_enough_worked(old_wf)

                                // Pas sur que ce soit utile mais on replace P2 car le changement de P1 a pu changer le meilleur endroit ou mettre P2
                                replanifP2({})
                                Debug.check_not_enough_worked(wf_p2)

                                let is_become_rotation_P2;
                                let is_still_rotation_P1 = old_wf[n.is_rotation_opening](date)
                                if(!is_still_rotation_P1){
                                    is_become_rotation_P2 = wf_p2[n.is_rotation_opening](date)
                                    if(is_become_rotation_P2){
                                        score = get_rotation_score()
                                        error_code == 0
                                    }
                                }else if(is_still_rotation_P1){
                                    undoFunc_1()
                                    replanifP2({})
                                    is_become_rotation_P2 = wf_p2[n.is_rotation_opening](date)
                                    if(!is_become_rotation_P2){
                                        // ni p1 ni p2 n'ont changé de rotation donc annuler le tout
                                        error_code = 1305
                                        mega_undo()
                                    }else if(is_become_rotation_P2){
                                        //P2 est en ouverture
                                        //mince ça n'a pas aidé p1 à partir de l'ouverture (ou fermeture selon la classe de rota)

                                        //l'erreur ne concerne que p1 où on a pas pu enlever l'ouverture
                                        //mais pas p2 où on a réussi à lui ajouter
                                        error_code = 1303
                                        let zscore5 = m.global_score_ignoring_sectors()
                                        let illogic = zscore5 > zscore4
                                        if(illogic){
                                            my_throw2(`Comment en replanifiant 2 personne de facon alternée on a pu empirer le score`)
                                        }
                                        // il n'y a pas de raison d'annuler P2 si celui-ci est passé en ouverture en plus de nous,
                                        // c'est que c'était nécessaire
                                        let do_not_undo_p2 = is_become_rotation_P2 && !illogic
                                        mega_undo({do_not_undo_p2})
                                        if(do_not_undo_p2){
                                            // comme p2 a changé il faut mettre à jour le score de rotation
                                            score = get_rotation_score()
                                        }//sinon le score n'a pas changé
                                    }
                                }

                                // laisser que le minimum d'heure pour P1 sans enlever dans l'ouverture et la fermeture
                                // replanifier les heures manquante dans la semaine
                                //@TODO
                                let optimise_time_in_week = true
                                if(optimise_time_in_week){
                                    // debugger
                                    try{
                                        for(let info of [{wf: old_wf, success: !is_still_rotation_P1, reduces:[n.reduce_start,n.reduce_end]},
                                            {wf: wf_p2, success: is_become_rotation_P2, reduces:[n.reduce_end,n.reduce_start]}]){
                                            controls_enabled(()=>{
                                                Debug.check_not_enough_worked(info.wf)
                                                Debug.check_too_much_worked(info.wf)
                                            })
                                            if(info.success){
                                                console.iidebug[136] = idnext(136)
                                                if(50221 === console.iidebug[136]){
                                                    debugger
                                                }
                                                let rscore = get_rotation_score()
                                                let backup_week = info.wf.backup_week()

                                                if(37924 == console.iidebug[8] && info.wf.wfId == 131){ debugger}
                                                // réduit les extrémités des journée puis replace le temps enlevé de manière optimale
                                                // TODO interdire de mêtre en ouverture p1
                                                m.improve_day_extremities({
                                                    wfs: [info.wf], facultatif: {
                                                        date_to_reduce: date, reduces: info.reduces,
                                                        // opt_min_max: {
                                                        //     [wf == old_wf ? n.not_opening_rota]: true
                                                        // }
                                                    }
                                                })
                                                let new_rscore = get_rotation_score({logTable: true})
                                                if(new_rscore > rscore){
                                                    backup_week.restore()
                                                    let new_rscore = get_rotation_score({logTable: true})
                                                    if(new_rscore > rscore){
                                                        my_throw2(100,`la restauration n'a pas fonctionnée`)
                                                    }
                                                }else{
                                                    score = new_rscore
                                                }
                                           }

                                           controls_enabled(()=>{
                                               Debug.check_not_enough_worked(info.wf)
                                               Debug.check_too_much_worked(info.wf)
                                           })
                                       }
                                    }catch(e){
                                        console.error(e)
                                        my_throw2(`regarder l'erreur e dans la console`)
                                    }
                                }
                                if(!error_code){
                                    wf_to_return = wf_p2
                                    undoFunc_1 = m.v2_try_a_change({
                                        person: old_wf,
                                        date
                                    })
                                }
                            }
                            get_lid(wf_p2)//vérifie que la personne travaille toujours
                        }

                        controls_enabled(()=>{
                            Debug.check_too_much_worked(old_wf)
                            Debug.check_not_enough_worked(old_wf)
                            Debug.check_too_much_worked(wf_p2)
                            Debug.check_not_enough_worked(wf_p2)
                        })
                    }
                }

                let success_swap = error_code === 0
                controls_enabled(()=>{
                    const info_someone_else2 = refF()
                    debug_callback && debug_callback()
                })
                if(success_swap){
                    break swap_loop
                }
            }

            debug_callback && debug_callback()
            info_someone_else = refF()
            if(error_code === 0){
                Debug.v2_place_someone_else_at_this_opening = true
            }
            return info_someone_else
        }
    }
    return OpeningClosingCommon
}
