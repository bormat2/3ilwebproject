/* @blockClass */
class Block {
    constructor(o) {
        var m = this;
        m.id = o.id;
        m.name = o.name || 'noName';
        // m.sectorArr = o.sectorArr || [];
        m.sectorEntArr = o.sectorEntArr || [];
        // m.wfIdArr = o.wfIdArr || [];
        m.wfArr = o.wfArr || [];
        m.parentEntity = o.parentEntity;
        m.planning = o.planning
    }

    // m.parentEntityId = o.parentEntityId || 0;
    // m.planningId = o.planningId
    // addPersonSector(o){
    //  this.personSector[o.id] = o;
    // }
}