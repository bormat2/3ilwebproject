/**
 * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
 */
 '' + function() {
    var main = new Main;
    main.is_test = true;
    var fake_sector = new Sector({
        id: 'fake_sec',
        is_real_sector: true,
        main
    })
    var get_fake_person = ()=>{
        var person = new Person;
        // persons.push(person)
        var inter = new Intervention({
            'startMinute': 8 * H,
            'endMinute': 12 * H,
            racine: true
        })
        // interventions.push(inter);
        inter.setNext(new Intervention({
            'next': null,
            'startMinute': 12 * H,
            'endMinute': 18 * H,
        }))

        // inter.listInfo.max_end_day = 18 * H
        // inter.listInfo.person = person
        // inter.next.listInfo = inter.listInfo
        let  li =  inter.listInfo
        li.max_end_day = 18 * H
        li.first_interv = inter
        li.last_interv = inter.next
        li.person = person
        inter.next.listInfo = li
        if(inter.next.last_interv.next){
            throw('il ne faut pas de suivant')
        }

        person.intervsRacines.push(inter)

        return person
    }
    // var persons = []
    // var interventions = [];
    // for (var i = 0; i < 200; ++i) {

    // }

    var test1 = function() {
        var person = get_fake_person()
        var inter = new Intervention({
            startMinute: 9 * H,
            endMinute: 13 * H,
            id: 5,
            person: person,
            sector: fake_sector,
            rules : {
                allow_to_change_hours:true
            }
        });
        //            person.addInterv(inter)

        var isOk = person.intervsRacines[0].startMinute == 8 * H && person.intervsRacines[0].endMinute == 9 * H &&
        person.intervsRacines[0].next.startMinute == 9 * H && person.intervsRacines[0].next.endMinute == 13 * H &&
        person.intervsRacines[0].next.next.startMinute == 13 * H && person.intervsRacines[0].next.next.endMinute == 18 * H
        if(!isOk) {
            console.error('probleme')
        } else {
            console.log('test Ok')
        }
    }

    var test2 = function() {
        var person = get_fake_person()
        var inter = new Intervention({
            startMinute: 8 * H,
            endMinute: 13 * H,
            id: 5,
            sector: fake_sector,
            rules : {
                allow_to_change_hours:true
            }
        });
        person.addInterv(inter)
        //             person.addInterv(inter)

        var isOk = person.intervsRacines[0].startMinute == 8 * H && person.intervsRacines[0].endMinute == 13 * H &&
        person.intervsRacines[0].next.startMinute == 13 * H && person.intervsRacines[0].next.endMinute == 18 * H
        if(!isOk) {
            console.error('probleme')
        } else {
            console.log('test Ok')
        }
    }

    var test3 = function() {
        var person = get_fake_person()
        var inter = new Intervention({
            startMinute: 14 * H,
            endMinute: 17 * H,
            id: 5,
            sector: fake_sector,
            rules : {
                allow_to_change_hours:true
            }
        });
        person.addInterv(inter)

        var isOk = person.intervsRacines[0].startMinute == 8 * H && person.intervsRacines[0].endMinute == 12 * H &&
        person.intervsRacines[0].next.startMinute == 12 * H && person.intervsRacines[0].next.endMinute == 14 * H &&
        person.intervsRacines[0].next.next.startMinute == 14 * H && person.intervsRacines[0].next.next.endMinute == 17 * H &&
        person.intervsRacines[0].next.next.next.startMinute == 17 * H && person.intervsRacines[0].next.next.next.endMinute == 18 * H
        if(!isOk) {
            console.error('probleme')
        } else {
            console.log('test Ok')
        }
    }

    var test4 = function() {
        var person = get_fake_person()
        var inter = new Intervention({
            startMinute: 14 * H,
            endMinute: 18 * H,
            id: 5,
            sector: fake_sector,
            rules : {
             allow_to_change_hours:true
         }
     });
        person.addInterv(inter)

        var isOk = person.intervsRacines[0].startMinute == 8 * H && person.intervsRacines[0].endMinute == 12 * H &&
        person.intervsRacines[0].next.startMinute == 12 * H && person.intervsRacines[0].next.endMinute == 14 * H &&
        person.intervsRacines[0].next.next.startMinute == 14 * H && person.intervsRacines[0].next.next.endMinute == 18 * H
        if(!isOk) {
            console.error('probleme')
        } else {
            console.log('test Ok')
        }
    }

    var test5 = function() {
        var person = get_fake_person()
        var inter = new Intervention({
            startMinute: 12 * H,
            endMinute: 18 * H,
            id: 5,
            sector: fake_sector,
        });

        inter.rules = {
         allow_to_change_hours:true
     }
     person.addInterv(inter)

     var isOk = person.intervsRacines[0].startMinute == 8 * H && person.intervsRacines[0].endMinute == 12 * H &&
     person.intervsRacines[0].next.startMinute == 12 * H && person.intervsRacines[0].next.endMinute == 18 * H &&
     person.intervsRacines[0].next.next == null
     if(!isOk) {
        console.error('probleme')
    } else {
        console.log('test Ok')
    }
}

var test6 = function() {
    var person = get_fake_person()
    var inter = new Intervention({
        startMinute: 12 * H,
        endMinute: 20 * H,
        id: 5,
        sector: fake_sector,
    });

    inter.rules = {
        allow_to_change_hours:true
    }
    var adi_inter = person.addInterv(inter)
    if(adi_inter.has_been_insert) {
        console.error("l'heure de fin est hors planning l'intervention aurait du être refusé")
    } else {
        console.log('test Ok')
    }
}

var test7 = function() {
    var person = get_fake_person()
    var inter = new Intervention({
        startMinute: 5 * H,
        endMinute: 12 * H,
        id: 5,
        sector: fake_sector,
    });
    inter.rules = {
        allow_to_change_hours:true
    }
    var adi_inter = person.addInterv(inter);
    if(adi_inter.has_been_insert) {
        console.error("l'heure de fin est hors planning l'intervention aurait du être refusé")
    } else {
        console.log('test Ok')
    }
}
var testMinTime = function() {
    conf.minTime = 0.5 * H
    fake_sector.minTimeSector = 3 * H
    var person = get_fake_person()
    var inter = new Intervention({
        startMinute: 8 * H,
        endMinute: 9 * H,
        id: 5,
        sector: fake_sector,
    });
    inter.rules = {
        allow_to_change_hours:true
    }
    person.addInterv(inter)
    if(inter.hasBeenInsert && inter.time != 3 * H || inter.endMinute !== 11 * H ) {
        console.error("l'intervention est trop courte, endMinute = " + inter.endMinute)
    } else {
        console.log('test Ok')
    }

    /////////////
    fake_sector.minTimeSector = 1.5 * H
    var person = new Person;
    // persons.push(person)
    var inter = new Intervention({
        'startMinute': 8 * H,
        'endMinute': 12 * H,
        //'person' : person,
        racine: true
    })
    // interventions.push(inter);
    inter.listInfo.max_end_day = 12 * H
    person.intervsRacines.push(inter)

    var inter = new Intervention({
        startMinute: 11 * H,
        endMinute: 12 * H,
        id: 5,
        sector: fake_sector,
        rules : {
            allow_to_change_hours:true  
        }
    });
    person.addInterv(inter)

    if(inter.time != 1.5 * H || inter.endMinute !== 12.0 * H || inter.startMinute !== 10.5 * H) {
        console.error("l'intervention ne respecte pas le temps minimal, startMinute = " + inter.startMinute)
    } else {
        console.log('test Ok')
    }

    fake_sector.minTimeSector = 2 * H
    var person = new Person;
    // persons.push(person)
    var inter = new Intervention({
        'startMinute': 8 * H,
        'endMinute': 12 * H,
        //'person' : person,
        racine: true
    })
    // interventions.push(inter);
    inter.listInfo.max_end_day = 12 * H
    person.intervsRacines.push(inter)
    ////////
    var inter = new Intervention({
        startMinute: 9.5 * H,
        endMinute: 11 * H,
        id: 5,
        sector: fake_sector
    });
    inter.rules = {
     allow_to_change_hours:true
 }
 person.addInterv(inter)
 if(inter.time != 2 * H) {
     console.error("l'intervention précédente résultante est trop courte, startMinute = " + inter.startMinute)
 } else {
     console.log('test Ok')
 }
 if(inter.endMinute != 11.5 * H) {
     console.error("l'intervention suivante résultante est trop courte, endMinute = " + inter.endMinute)
 } else {
     console.log('test Ok')
 }
}

var test_is_possible_to_add_this_new_day_as_worked = ()=>{
var wf = get_fake_person()
wf.listInfo_sorted = []
wf.listInfo_by_date = {}
wf.lis_absence = new Set()
wf.lis_mandatory = new Set()
var old_conf_force_to_have_2_consecutive_absences = conf.force_to_have_2_consecutive_absences;
conf.force_to_have_2_consecutive_absences = true
for(let i = 0; i < 7;++i){
    var date = '2018-03-0'+(i+1)

    var inter = new Intervention({
        startMinute: 8 * H,
        endMinute: 12 * H,
        racine: true,
        date
    })
    inter.listInfo.max_end_day = 18 * H
    inter.listInfo.person = wf
    wf.main = main
    var li = inter.listInfo
    li.iRacine = wf.intervsRacines.length
    wf.intervsRacines[li.iRacine] = inter
    wf.listInfo_by_date[date] = li
    wf.listInfo_sorted.push(li)
}
wf.nb_days_by_week = 5;
var m = wf.main
var iptatndaw = m.is_possible_to_add_this_new_day_as_worked(wf,['2018-03-01'])
if(!iptatndaw.is_possible){
    console.error('Pourquoi la personne ne peut pas être placé ce jour là !!!',iptatndaw)
}else{
 console.log('test_is_possible_to_add_this_new_day_as_worked success') 
}
conf.force_to_have_2_consecutive_absences = old_conf_force_to_have_2_consecutive_absences
const add_int = (date)=>{
var inter = new Intervention({
    startMinute: 8 * H,
    endMinute: 10 * H,
    sector: fake_sector,
    date,
    rules: {
        safe_insertion: true
    }
})
wf.addInterv(inter)
}

wf.lis_absence.add(wf.listInfo_by_date['2018-03-05'])

var iptatndaw = m.is_possible_to_add_this_new_day_as_worked(wf,['2018-03-05'])
if(iptatndaw.is_possible){
console.error("cette journée est interdite pourtant",iptatndaw)
}else{
console.log('test_is_possible_to_add_this_new_day_as_worked_2 success') 
}

add_int('2018-03-04')
if(!wf.listInfo_by_date['2018-03-04'].first_interv_sector){
my_throw( "la première inter avec secteur devrait être celle que l'on vient d'ajouter")
}
var iptatndaw = m.is_possible_to_add_this_new_day_as_worked(wf,['2018-03-06'])
if(iptatndaw.is_possible || iptatndaw.info.error_message != 'nb_absence_to_place_not_forced_is_1'){
console.error("Si on place à la fois le 04 et le 06 on ne pourra pas placé nos 5 jours de travail et deux jours de repos consécutifs",iptatndaw)
}else{
console.log('test_is_possible_to_add_this_new_day_as_worked_3 success') 
}

    //FORCER LE 05 EN ABSENCE
    wf.lis_absence.delete(wf.listInfo_by_date['2018-03-05'])
    
    var iptatndaw = m.is_possible_to_add_this_new_day_as_worked(wf,['2018-03-06'])
    if(!iptatndaw.is_possible){
        console.error("Il n'y a pas de raison que ses journées soient interdites",iptatndaw)
    }else{
     console.log('test_is_possible_to_add_this_new_day_as_worked_4 success') 
 }

 var iptatndaw = m.is_possible_to_add_this_new_day_as_worked(wf,['2018-03-01','2018-03-02','2018-03-03','2018-03-07'])
 if(!iptatndaw.is_possible){
    console.error("Il n'y a pas de raison que ses journées soient interdites",iptatndaw)
}else{
 console.log('test_is_possible_to_add_this_new_day_as_worked_5 success') 
}

var iptatndaw = m.is_possible_to_add_this_new_day_as_worked(wf,['2018-03-01','2018-03-03','2018-03-06'])

if(iptatndaw.is_possible || 'consecutive_days_is_not_respected' != iptatndaw.info.error_message ){
console.error("Il n'y a pas deux jours consécutifs ça aurait du être refusé",iptatndaw)
}else{
console.log('test_is_possible_to_add_this_new_day_as_worked_6 success') 
}

wf.lis_mandatory.add('2018-03-05')
add_int('2018-03-01')
add_int('2018-03-02')
add_int('2018-03-03')
var iptatndaw = m.is_possible_to_add_this_new_day_as_worked(wf,['2018-03-07'])
if(iptatndaw.is_possible || 'working_mandatory' != iptatndaw.info.error_message ){
console.error("Il ne reste qu'un jour à placer et on a dit que le 05 était obligatoire donc le 07 ne peut pas être autorisé",iptatndaw)
}else{
console.log('test_is_possible_to_add_this_new_day_as_worked_7 success') 
}
}

var testBinarySearsh = function() {
var arr = [{
    a: 1
},
{
    a: 2
},
{
    a: 3
},
{
    a: 4
},
{
    a: 5
},
{
    a: 6
},
{
    a: 7
},
];
var idx = sortedArrFunc.lowerBound(arr, {
    a: 4
}, (e) => {
    return e.a
})
if(idx == 3) {
    console.log('lowerBound ok')
} else {
    console.error('lowerBound not ok')
}

var idx = sortedArrFunc.lowerBound(arr, {
    a: 3.5
}, (e) => {
    return e.a
})
if(idx == 3) {
    console.log('lowerBound ok')
} else {
    console.error('lowerBound not ok')
}

var idx = sortedArrFunc.upperBound(arr, {
    a: 3.5
}, (e) => {
    return e.a
})
if(idx == 3) {
    console.log('upperBound ok')
} else {
    console.error('upperBound not ok')
}

var idx = sortedArrFunc.upperBound(arr, {
    a: 4
}, (e) => {
    return e.a
})
if(idx == 4) {
    console.log('upperBound ok')
} else {
    console.error('upperBound not ok')
}
}


var testDoubleLinkedList = () => {
var list = new SortedDoubleCircularLinkedList([10, 4, 5, 9], function(a, b) {
    return a - b
})
var expected = [4, 5, 9, 10]
var i = 0;
var result = []
var error = false
list.some(function(val) {
    if(expected[i] != val) {
        error = true
    }
    result.push(val)
    ++i
})
if(error || i != 4) {
    console.error(expected, result)
}
list.add(41)
if(list.first.next.next.next.next.next.elm != 41) {
    error = true
    console.error("add not working")
}
if(list.first.prev.elm != 41) {
    error = true
    console.error("add not working")
}
var cell = list.add(4.5)
if(cell.prev.elm != 4) {
    error = true
    console.error("add not working")
}
if(cell.next.elm != 5) {
    error = true
    console.error("add not working")
}
if(cell.next.prev.elm != 4.5) {
    error = true
    console.error("add not working")
}
if(cell.prev.next.elm != 4.5) {
    error = true
    console.error("add not working")
}
var cell = list.add(0)
if(!cell.prev.isFirst) {
    error = true
    console.error("add not working")
}
var result = []
expected = [0, 4, 4.5, 5, 9, 10, 41]
i = 0
list.someCell(function(cell) {
    if(expected[i] != cell.elm) {
        error = true
    }
    if(cell.prev.next != cell) {
        console.error("list not logic")
    }
    result.push(cell.elm)
    ++i
})
if(error || i != expected.length) {
    error = true
    console.error(expected, result)
}

if(!error) {
    console.log('linkedList tests passed')
}
}

var formatageTest = () => {
var form = Main.formatMinute(130)
if(form != '02:10') {
    console.error('02:10', form)
} else {
    console.log('le formatage fonctionne')
}
}


var pauseTest = function() {
var main = fake_sector.main;
main.is_test = true
        // main.rotations = {
        //     Opening: new Opening(main),
        //     Closing: new Closing(main),
        //     Saturday: new Saturday(main),
        // }
        var person = get_fake_person()
        person.main = main
        var inter = new Intervention({
            startMinute: 8 * H,
            endMinute: 18 * H,
            id: 5,
            sector: fake_sector,
            rules : {
             allow_to_change_hours:true
         }
     });
        person.addInterv(inter)
        controls_enabled(()=>{
            if(!inter.hasBeenInsert) {
                my_throw( 'problème')
            }
        })
        var f_mrpu =  main.main_resolve_pause_unrespected.bind(main,{
            intervention: inter
        })
        var is_pause_ok = f_mrpu().pause_is_ok
        if(!is_pause_ok) {
            console.error('la pause ne marche pas')
        } else {
            console.log('la pause marche')
        }
        var pause_found = false
        for (var next = inter.listInfo.first_interv;;) {
            if(next.is_pause()) {
                pause_found = true
                if(!next.prev.sector) {
                    console.error('une pause doit être précédé d\'un secteur')
                }
                if(!next.next.sector) {
                    console.error('une pause doit être suivit d\'un secteur')
                }
                if(next.time != 1 * H) {
                    console.error('la pause du midi devrait durer une heure')
                }
            }
            if(!(next = next.next)) break;
        }
        if(!pause_found) {
            console.error('En fait la pause n\'est pas présente')
        }
    }
    var resetTest = function() {
        var main = fake_sector.main
        var person = get_fake_person()
        person.main = main
        var inter = new Intervention({
            startMinute: 8 * H,
            endMinute: 18 * H,
            id: 5,
            sector: fake_sector,
            rules : {
                allow_to_change_hours:true
            }
        });
        person.addInterv(inter)
        controls_enabled(()=>{
            if(!inter.hasBeenInsert) {
                my_throw( 'problème')
            }
        })
        var a1 = inter.get_first_inter_with_sector_same_day()
        var a2 = inter.get_first_inter_with_sector_same_day()
        if(a1 != a2) {
            console.error('la fonction devrait renvoyer le même résultat')
        }
        if(!a2) {
            console.error('get_first_inter_with_sector_same_day ne marche pas')
        } else {
            console.log('get_first_inter_with_sector_same_day marche')
        }
        var is_pause_ok = main.main_resolve_pause_unrespected({
            intervention: inter
        }).pause_is_ok

        person.reset_date(inter.date, {
            even_if_pause_fake_protected: true
        })
        var a1 = inter.get_first_inter_with_sector_same_day()
        var a2 = inter.get_first_inter_with_sector_same_day()
        if(a1 != a2) {
            console.error('la fonction devrait renvoyer le même résultat')
        }
        if(a2) {
            console.error("la journée n'a pas été réinitialisé")
        } else {
            console.log('la journée a bien été réinitialisé')
        }
        for (inter = inter.listInfo.first_interv; inter; inter = inter.next) {
            if(inter.sector || inter.is_pause()) {
                console.error('certaines choses n\'ont pas été réinitialisé')
            }
        }
    }
    conf.minTime = 30;
    test_is_possible_to_add_this_new_day_as_worked()
    resetTest()
    pauseTest();
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    testBinarySearsh();
    testMinTime();
    testDoubleLinkedList()
    formatageTest()
    //test unnaffed TODO
    /**/
}