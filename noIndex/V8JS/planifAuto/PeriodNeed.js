/**
 * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
 */
'' + function(){
    var disabled_definitively = (per) => {
        // if(console.iDebug == 5751){
        ////     ////debugger;
        // }
        var error = function() {
            controls_enabled(()=>{
                my_throw( 'error il y a eu un changement du pas des besoins "change_step"celui-ci est donc désactivé')
            })
        }
        Object.defineProperty(per, 'have', {
            get: error,
            set: error
        });
        Object.defineProperty(per, 'need', {
            get: error,
            set: error
        });
        per.v2_sum_zscore_on_period__and__attractivity = error
        per.v2_sum_zscore_on_period = error
    }

//c@periodNeed @PeriodClass

    class PeriodNeed {
        constructor(o) {
            var m = this
            m.id = Main.getUniqId();
            m.main = o.main
            controls_enabled(()=>{
                if(m.startMinute < 0) {
                    my_throw( 'négatif')
                }
            })
            if(o.from_v2_total_need) return;
            // m.v2_sector = null // facultatif
            m.dateKey = o.dateKey;
            m.date = o.date;
            m.startMinute = o.startMinute;
            m.endMinute = o.endMinute;
            m.need = +o.need
            m.totalPeriod = null
            m.max_need = o.max_need
            m.min_need = o.min_need
            m.v2_sector = o.v2_sector //facultatif peut être mis après
            controls_enabled(()=>{
                if(!isFinite(o.startMinute) || !isFinite(o.endMinute)) {
                    my_throw( 'not finite')
                }
            })
            m.is_score_updtd = false;
            m.interventionMap = new MyMap({
                onAddRemove: m.onAddRemove.bind(m)
            }) // grace à la length on sait combien d'intervention sont présente, les pause n'étant pas stocké
            m.openingType = o.openingType
            m._last_zscore = 0;
            //++iDay;
        }

        get next_amp(){
            return this._next_amp
        }

        set next_amp(next_amp){
            if(next_amp){
                next_amp.prev_amp = this
            }
            this._next_amp = next_amp//même si c'est null il faut le faire à cause du test dans pos_in_periodSortedArr
        }

        disabled_definitively() {
            disabled_definitively(this)
            this.disabled_definitively = idnext('disabled_definitively')//pour débugger et empêcher que ça soit appelée deux fois
        }

        get have() {
            return this.interventionMap.length
        }

        set need(need){
            if(need > 0 && need < 1){
                this._need = 1
            }else{
                this._need = Math.round(need)
            }
        }

        get need(){
            return this._need
        }

        //ça va propager vers v2_total_need qui va lui même propager pour mettre à jour le zscore global
        // PeriodNeed@onAddRemove
        onAddRemove(signe, obj) {
            var m = this;
            m.is_score_updtd = false
            if(m.totalPeriod && (!obj || obj.get_personSector())) {
                m.totalPeriod.incHave(signe, m)
            }
        }

        set pos_in_periodSortedArr(val){
            controls_enabled(()=>{
                if(this.next_amp === void 8){
                    my_throw( 'il faut setter le suivant avant de vouloir mettre cette propriété')
                }
                if(this.planning === void 8){
                    my_throw( 'il faut le planning avant de vouloir mettre cette propriété')
                }
            })
            this._pos_in_periodSortedArr = val
        }

        get pos_in_periodSortedArr(){
            return this._pos_in_periodSortedArr
        }

        /*
         * C'est le score pour une amplitude et un secteur donnée
         * sauf si on est dans la classe v2_total_need là c'est comme si toute
         *  les entités partageait les besoins "need" et l'effectif "have" pour
         *  une demi-heure donnée
         *
         *  Le score ne suit pas toujours les mêmes règles, certain ratio comme le surrefectif
         *  doivent pour certain algorithms ^etre plus pénalisant que d'autres
         */

         v2_zscore_period() {
            var res = this.v2_zscore_and_attractivity()
            // console.iDebug;
            return res.zscore_period
        }

        v2_attractivity() {
            return this.v2_zscore_and_attractivity().attractivity
        }

        /**
         * Le zscore devient grand quand on fait des mauvais choix et diminue quand on fait des bons choix
         * l'attractivity dit ou c'est le mieux de rajouter du monde et est grand si il faut rajouter du monde
         */
        v2_zscore_and_attractivity() {
            // var round = function(val){
            //     //2²⁶ = 67 108 864
            //     var val2 = val + ''
            //     if(val.length > 7){
            //         val2 = val2.slice(0,6)
            //     }
            //     return +val2
            // }
            var m = this
            var main = m.main
            var f6 = (have)=>{
                var security_coef = 10000 * conf.minTime/30, //travailler avec des float donne des calcul approximatif donc on va travailler avec grand nombres
                zscore, attractivity = 0;
                if(!m.need) {
                    attractivity = zscore = 0 // il ne faut pas mettre des gens si le besoin est null
                } else {
                    var attractivity = Math.round(security_coef * Math.max(0, (m.need - have) / m.need)) //il vaut mieux éviter de retourner du négatif car ça pourrait avantager la baisse du score global en mettant du surrefectif

                    if(m.need && m.min_need && have < m.min_need) {
                        attractivity += Math.round(security_coef * Math.pow(m.min_need - have,4) * 100)

                        // privilégier de mettre des personnes aux extrémités plutôt qu'au
                        // centre surtout utile en cas d'égalité
                        // @todo : à supprimer peut être
                        attractivity += Math.abs(security_coef * (m.endMinute - 15.5 * H) / 24)
                    }
                    if(m.need && !have) {
                        attractivity += security_coef * 100000000
                    }

                    // attractivity = round(attractivity)
                    zscore = attractivity
                    //selon le mode le minimum non respecté est pénalisant
                    // ou bien le surrefectif est

                    if(main.is_penalise_overstaffing || main.is_penalise_overstaffing_attractivity) {
                        let s1,s2 = s1 = 0;
                        if(m.max_need && have > m.max_need) {
                            s1 += (security_coef * (have - m.max_need) / m.max_need)
                        }
                        if(have > m.need) {
                            s1 += (security_coef * (have - m.need) / m.need)
                        }
                        if(s1){
                            if(main.is_penalise_overstaffing){
                                zscore += Math.round(s1)
                            }
                            //quand on doit choisir entre 2 surreffectif ceci permet de choisir celui qui est le moins en surreffectif
                            if(main.is_penalise_overstaffing_attractivity){
                                attractivity += Math.round(s1/50)
                            }
                        }
                    }
                }
                m.is_score_updtd = true
                controls_enabled(()=>{
                    if(!isFinite(zscore)) {
                        my_throw('Number.isNaN(zscore)')
                    }
                })

                return m.last_zscore_on_period__and__attractivity = {
                    zscore_period: Math.round(zscore),
                    attractivity: Math.round(attractivity)
                }
            }

            var ret = f6(m.have)
            var ret2 = f6(m.have + 1)
            return {
                zscore_period: ret.zscore_period,
                //l'attractivité est en quelque sorte ce que l'on va diminuer au zscore
                //si on place une personne à cet endroit
                //sauf que contrairement au zscore l'attractivity prend en
                //compte le surrectif
                attractivity: ret.attractivity - ret2.attractivity
            }

        }

        //@blackbox
        // get startMinute() {
        //     return this.startMinute2;
        // }

        // set startMinute(s) {
        //     my_throw( "can not be changed")
        // }
    }
    return PeriodNeed
}
