'' +function(){
    /**
     * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
     */
     var allow_keys_person = new Set([
        "nbMinutesMin"
        ,"nb_days_by_week"
        ,"intervsRacines"
        ,"intervNotAffectedMap"
        ,"main"
        ,"name"
        ,"nb_minu_worked"
        ,"planning"
        ,"personSectorMap"
        ,"wfId"
        ,"previous_day"
        ,"next_day"
        ,"rotation_possible"
        ,"rotation_initial"
        ,"listInfo_sorted"
        ,"do_not_check_consecutive_abscence"//code mort ?
        ,"next_monday_not_worked"
        ,"previous_sunday_not_worked"
        ,"previous_week_with_consecutive_abs"
        ,"next_week_with_consecutive_abs"
        // ,"v2_nb_inter_worked_by_day"
        ,"rotation"
        ,"rotation_zscore"
        ,"rotation_ratio_relative_to_other_wf"
        ,"rotation_ratio"
        ,"listInfo_by_date"
        ,"$_nb_minu_worked_$"
        ,"nb_min_minute_fakely_worked"
        ,"$_nb_min_minute_fakely_worked_$"
        ,"rotationMaps"
        ,"unplaced_time"//code mort ?
        ,"zscore_rotation"
        ,"lis_mandatory"// Set listInfo qui sont obligatoirement travaillé
        ,"lis_absence"// Set listInfo qui sont obligatoirement NON travaillé
        ,"lis_mandatory"
        ,"time_opening"//le temps à ajouter avant et après les ouvertures fermetures
        // ,"real_nbMinutesMin"//le temps que la personne n'aurait pas due dépasser mais que l'on va toléré car elle a forcé des interventions
        ,'origin'// stocke les propriété comme nbMinutesMin avant qu'on les change
        //est ce utilisé ?
        ,"nbMinuteToDoTotal"//@depreciated
        ]);

     class PersonSectorMap_for_Person{
        constructor(o){
            var m = this
            controls_enabled(()=>{
                assert_exists(o.main)
                m = new Proxy(m,{
                    get(m,prop){
                        return m[prop]
                    },
                    set(m,prop,val){
                        if(m[prop]){
                            my_throw('Vous écraser une valeur existante')
                        }
                        if(o.main && !o.main.allow_to_fill_person_sector){
                            my_throw('Cela doit être fait après allow_to_fill_person_sector')
                        }
                        m[prop] = val
                        return true
                    }
                })
            })

            return m
        }

    }
//window.keys6 = []
/* @PersonClass @wf*/
class Person {
    constructor(o) {
        var m = this
        controls_enabled(()=>{
            m = proxy_creator(this,allow_keys_person,{
                setter_check: (prop,val,target)=>{
                    if(prop == 'personSectorMap' && !(val instanceof PersonSectorMap_for_Person) ){
                        my_throw('La classe est pas la bonne')
                    }
                    return true
                },
            })
        })
            //règle aléatoire pour mettre 10 ou 15 minute aux ouverture et fermeture
            m.time_opening = o ? o.wfId%2 ? 10 : 15 : 0
            m.unplaced_time = 0;
            m.nbMinutesMin = null;
            m.nb_days_by_week = null;
            m.intervsRacines = [];
            m.intervNotAffectedMap = {}; //tableau associatif id => intervention qui doit être affecté
            if(!o){return m}
                m.main = o.main
            m.name = o.name;
            m.nbMinuteToDoTotal = o.nbMinuteToDoTotal, //code mort ?
            m.nb_minu_worked = 0
            m.planning = o.planning;
            m.personSectorMap = new PersonSectorMap_for_Person({main: o.main})
            m.wfId = o.wfId
            m.rotation_initial = {}
            // m.v2_nb_inter_worked_by_day = {}
            m.rotation = new Rotation_wf({type: 'rotation',main})
            // plus il est grand moins cette personne est équitable avec les autres
            m.rotation_zscore = 0;
            m.rotation_ratio_relative_to_other_wf = new Rotation_wf({type: 'rotation_ratio_relative_to_other_wf',main})
            m.rotation_ratio = new Rotation_wf({type: 'rotation_ratio',main})
            m.listInfo_by_date = {
                // '2018-03-26': {
                //     min: 6 * H,
                //     max: 9 * H,
                //     worked: 0,
                //     min_start_day: 9.5 * H,
                //     max_start_day: 21 * H,
                //     min_end_day: 12 * H,
                //     max_end_day: 24 * H,
                // }
            }

            console.watch(m, 'nb_minu_worked', () => {
                if(m.nb_minu_worked < 0){
                    debugger
                }
                if(m.nb_minu_worked - m.nb_min_minute_fakely_worked > 35 * H) {
                    ////////////////debugger;
                    return true;
                }
            })
            console.watch(m, 'nb_min_minute_fakely_worked', () => {
                if(m.nb_minu_worked - m.nb_min_minute_fakely_worked > 35 * H) {
                    ////////////////debugger;
                    return true;
                }
            })
            // les map sont par clef donc
            const rotationMaps = {}
            forOf(m.main.rotations,function(rota,rota_name){
                rotationMaps[rota_name] = {}
            })
            m.rotationMaps = rotationMaps
            // console.watch(this,'nb_minu_worked',() => {
            //     // if(this.startMinute == 48 * H) return true;
            //     if(!console.found21){
            //         var p = m
            //         if(p){
            //             if(p.wfId == 10575){
            //                 var cond1 = true
            //             }
            //             if(p.nb_minu_worked == 33.5){
            //                 var cond2 = true
            //             }
            //             return console.found21 = /*affect*/
            //                 // this.startMinute == 14 && this.endMinute == 16
            //                 p && p.wfId == 10575 && p.nb_minu_worked  == 33.5
            //                 // && this.sector && this.sector.id ==2522
            //                 // && !this.type
            //                 // && this.date == '2018-03-26'
            //             }
            //         }
            // })
            return m
        }

        get listInfo_sorted_worked(){
            let wf = this
            return {
                *[Symbol.iterator](){
                    for(let li of wf.listInfo_sorted){
                        if(li.is_worked()){
                            yield li
                        }
                    }
                }
            }
        }

        get listInfo_sorted_poss(){
            let wf = this
            return {
                *[Symbol.iterator](){
                    for(let li of wf.listInfo_sorted){
                        if(li.first_interv){
                            yield li
                        }
                    }
                }
            }
        }


        respect_max_time(date){
            const wf = this, li = wf.listInfo_by_date[date]
            return li.worked <= li.max
        }
        /*
        * Permet de forcer ou interdire des ouvertures ou heures
        * Si whole_day alors on peut forcer de faire des ouvertures et fermeture
        * ce que l'on ne peut pas faire quand on fait que rajouter des heures
        * en êtrémité de journée
        * @opt_min_max
        */
        get_start_end_minu_error({opt_min_max,start_minute,end_minute,date,whole_day}){
            const m = this;
            //n'existe pas force_end_before_or_equal => end_not_after,
            // force_start_after => start_not_before_or_equal
            controls_enabled(()=>{
                const keys_autorised = new Set(['start_not_before','start_not_before_or_equal','not_opening_rota','force_opening_rota','force_start_before_or_equal','force_start_before','do_not_make_rotation_worst','end_not_after','end_not_after_or_equal','not_closing_rota','force_closing_rota','force_end_after','force_end_after_or_equal','do_not_make_rotation_worst','get_rotation_score'])
                forOf(opt_min_max,(value,key)=>{
                    if(!keys_autorised.has(key)){
                        my_throw( 'cette clef ne peut pas être utilisé')
                    }
                })
            })
            var wf = this
            if(wf.main){
                const min_max = wf.main.get_min_start_and_end({wf,date})
                if(start_minute < min_max.min_start_day || end_minute > min_max.max_end_day){
                    return 5030
                }
            }
            if(start_minute !== void 8){
                if(opt_min_max.start_not_before > start_minute){
                    return 5031
                }
                if(opt_min_max.start_not_before_or_equal <= start_minute){
                    return 5032
                }
                let will_be_opening_val;
                const will_be_opening = () => will_be_opening_val = will_be_opening_val || wf.is_rotation_opening(date,{start_minute})
                if(opt_min_max.not_opening_rota && will_be_opening() ){
                    return 5033
                }
                if(whole_day){
                    if(opt_min_max.force_opening_rota && !will_be_opening()){
                        return 5034
                    }
                    if(opt_min_max.force_start_before_or_equal < start_minute){
                        return 5035
                    }
                    if(opt_min_max.force_start_before <= start_minute){
                        return 5036
                    }
                }
                //si c'est pas une rotation et que ça en deviendrait une alors regardé si on peut faire ça
                if(opt_min_max.do_not_make_rotation_worst){
                    let was_opening = wf.is_rotation_opening(date)
                    if(!was_opening){
                        if(will_be_opening() && !opt_min_max.get_rotation_score.is_poss_add_rota(wf,'Opening')){
                            return 50361
                        }
                    }
                }
            }
            if(end_minute !== void 8){
                // if(opt_min_max.not_closing_rota){
                //     wf.is_rotation_closing(date,{start_minute,end_minute})
                // }
                if(opt_min_max.end_not_after < end_minute){
                    return 5037
                }
                if(opt_min_max.end_not_after_or_equal <= end_minute){
                    return 5038
                }
                let will_be_closing_value;
                const will_be_closing = () => will_be_closing_value = will_be_closing_value || wf.is_rotation_closing(date,{end_minute})
                if(opt_min_max.not_closing_rota && will_be_closing()  ){
                    return 5039
                }
                if(whole_day){
                    if(opt_min_max.force_closing_rota && !will_be_closing() ){
                        return 5040
                    }
                    if(opt_min_max.force_end_after >= end_minute){
                        return 5041
                    }
                    if(opt_min_max.force_end_after_or_equal > end_minute){
                        return 5042
                    }
                }
                if(opt_min_max.do_not_make_rotation_worst){
                    //si c'est pas une rotation et que ça en deviendrait une alors regardé si on peut faire ça
                    let was_closing = wf.is_rotation_closing(date)
                    if(!was_closing){
                        if(will_be_closing() && !opt_min_max.get_rotation_score.is_poss_add_rota(wf,'Closing')){
                            return 50421
                        }
                    }
                }
            }
        }

        update_rotation_ratio(name){
            var wf = this
            if(!wf.rotation_possible[name]){
                wf.rotation_ratio[name] = 0
            }else{
                wf.rotation_ratio[name] = wf.rotation[name] / wf.rotation_possible[name]
            }
        }
        set rotation(val){
            controls_enabled(()=>{
                if(val instanceof Rotation_wf == false){
                    my_throw( 'bad type')
                }
            })
            this._rotation = val
        }
        get rotation(){
            var wf = this//.proxy;
            return wf._rotation
        }

        opening_info(opt){
            return this.opening_closing_info(opt,'start_minute','end_minute','opening_minute','opening_minute_supplement_time','before_or_equal','block_opening')
            // var wf = this;
            // let obj = {
            //     is: function(p){
            //         controls_enabled((p)=>{
            //             if(p === void 8) my_throw( 'missing_arg')
            //         })
            //         if(p == 'supplement_time'){
            //             return obj.start_minute == opening_minute_supplement_time
            //         }else if(p=='rotation'){
            //             return obj.start_minute === conf.opening_minute
            //         }
            //     },
            //     before_or_equal: function(){
            //         return obj.start_minute <= conf.opening_minute
            //     },
            //     block_opening: function(){
            //         return obj.start_minute <= conf.opening_minute && obj.end_minute >= conf.opening_minute - wf.time_opening
            //     },
            // };
            // controls_enabled(()=>{
            //     if(void 8 == opt.start_minute) my_throw( 'missing arg start_minute')
            //         obj = proxy_creator(obj,new Set(['start_minute','end_minute','date']),{
            //             setter_check(prop,val){
            //                 if(!val){
            //                     my_throw( 'valeur nulle interdite')
            //                 }
            //                 return true
            //             }
            //         })
            // })
            // clone(opt,obj)
            // return obj
        }

        is_rotation_opening(date,_obj_minu){
            const first_minu_worked = _obj_minu ? _obj_minu.start_minute : this.get_first_minu_worked(date)
            if(!first_minu_worked) return false
                return this.opening_info({
                    start_minute: first_minu_worked,
                    date
                }).before_or_equal('rotation')
        }

        is_rotation_closing(date,_obj_minu){
            const last_minu_worked = _obj_minu ? _obj_minu.end_minute : this.get_last_minu_worked(date)
            if(!last_minu_worked) return false
                return this.closing_info({
                    end_minute: last_minu_worked,
                    date
                }).after_or_equal('rotation')
        }

        // retoune le temps réel travaillé plus le temps de la pause
        get_real_time_with_pause_if({date,fake_start_minute,fake_end_minute}){
            const m = this
            let time_in_day_with_pause = fake_end_minute - fake_start_minute
            assert_finite(time_in_day_with_pause)
            let unplaced_time = m.get_unplaced_time(fake_start_minute,time_in_day_with_pause,date)
            let time = time_in_day_with_pause + unplaced_time
            assert_finite(time)
            return time
        }

        opening_closing_info(opt,end_minute,start_minute,closing_minute,closing_minute_supplement_time,after_or_equal,block_closing){
            var wf = this;
            var sign = after_or_equal == 'after_or_equal' ? 1 : -1//1 pour closing et -1 pour opening
            let obj = {
                get_supplement_time: function(){
                    if(obj.is_supplement_time()){
                        return wf.time_opening
                    }
                },
                // il est a des heures qui rajoute un quart d'heure à la fin
                // ou avant
                is_supplement_time : function(){
                    // controls_enabled(()=>{
                    //     if(p === void 8) my_throw( 'missing_arg')
                    //     if(p !== 'supplement_time'){my_throw( 'impossible'})
                    // })
                    let lbd = wf.listInfo_by_date
                    let d = lbd && lbd[obj.date]
                    if(d){
                        if(sign > 0 && d.disabled_time_closing){
                            return false
                        }
                        if(sign < 0 && d.disabled_time_opening){
                            return false
                        }
                    }

                    return obj[end_minute] == conf[closing_minute_supplement_time]
                },
                //des protected peuvent empêcher le temps supplément d'être mis
                //block closing return alors true
                [block_closing] : function(p){
                    controls_enabled(()=>{
                        if(p!='supplement_time'){
                            my_throw( 'supplement_time')
                        }
                    })
                    return  (sign * obj[end_minute] >= sign * conf[closing_minute]) && (sign * obj[start_minute] <= sign * (conf[closing_minute] + wf.time_opening) )
                },
                //porte mal son nom car selon le paramètre border_rotation_is_in_rotation on va ou non inclure l'heure limite de rotation dans la rotation
                [after_or_equal]: function(p){
                    controls_enabled(()=>{
                        if(p!='rotation'){
                            my_throw( 'after_or_equal')
                        }
                    })
                    if(!conf.border_rotation_is_in_rotation){
                        return sign * obj[end_minute] > sign * conf[closing_minute]
                    }
                    return sign * obj[end_minute] >= sign * conf[closing_minute]
                }
            }
            controls_enabled(()=>{
                if(void 8 === opt[end_minute]) my_throw( `missing arg ${end_minute}`)
                    if(void 8 === opt['date']) my_throw( `missing arg ${date}`)
                        obj = proxy_creator(obj,new Set(['start_minute','end_minute','date']),{
                            setter_check(prop,val){
                                if(!val){
                                    my_throw( 'valeur nulle interdite')
                                }
                                if(val instanceof Function){
                                    my_throw( 'cette valeur est function ce qui est anormal')
                                }
                                return true
                            }
                        })
                })
            clone(opt,obj)
            return obj
        }

        closing_info(opt){
            return this.opening_closing_info(opt,'end_minute','start_minute','closing_minute','closing_minute_supplement_time','after_or_equal','block_closing')
        }

        /**
         * donne ne temps réel passé dans la journée à partir de la start_minute
         * arrondis et temps arrondis que l'on passe dans la journée dans l'entreprise(donc pause comprise)
         * time_in_day_with_pause est en faite la différence entre la fake_end_minute et la fake_start_minute
         */
         get_unplaced_time(start_minute,time_in_day_with_pause,date){
            controls_enabled(()=>{
                if(!date){
                    my_throw( 'date missing')
                }
                if(time_in_day_with_pause <= 0){
                    my_throw2(70,`Le temps ne peut pas être non positif`)
                }
            })
            var wf = this;
            var time = 0 ;
            const open_info = wf.opening_info({start_minute,date})
            if(open_info.is_supplement_time()){
                time += wf.time_opening
            }
            var end_minute = start_minute + time_in_day_with_pause - time
            const close_info = wf.closing_info({end_minute,date})
            if(close_info.is_supplement_time()){
                time += wf.time_opening
            }
            return time
        }

        set rotation_ratio(val){
            var wf = this//.proxy;
            controls_enabled(()=>{
                if(val instanceof Rotation_wf == false){
                    my_throw( 'bad type')
                }
            })
            wf._rotation_ratio = val
        }
        get rotation_ratio(){
            var wf = this//.proxy;
            return wf._rotation_ratio
        }

        set rotation_ratio_relative_to_other_wf(val){
            var wf = this//.proxy;
            controls_enabled(()=>{
                if(val instanceof Rotation_wf ==false){
                    my_throw( 'bad type')
                }
            })
            wf._rotation_ratio_relative_to_other_wf = val
        }
        get rotation_ratio_relative_to_other_wf(){
            return this._rotation_ratio_relative_to_other_wf
        }

        /*
        * Attention au absence qui possède du temps worked mais ne compte pas comme une journée
        * worked sans information complémentaire d'où la condition first_interv_sector
        */
        is_worked(date){
            var wf = this//.proxy;
            var li = wf.listInfo_by_date[date]
            return li.is_worked()
        }

        /**
        * Si il y a des protected d'absence ça n'empêche pas la fonction de
        * réussir à vider la journée de toutes affectation dans une équipe
        */
        reset_date_undofunc(date){
            var wf = this//.proxy;
            var m = wf.main
            var undo_func = m.v2_try_a_change({
                date,
                person:wf
            })
            wf.reset_date(date,{
                even_if_fake_protected: true
            })
            var d = wf.listInfo_by_date[date]
            //si il y a pas d le jour ne peut pas être travaillé mais du coup
            //on dit qu'on a quand même réussi à reset la journée
            if(d){
                var day_removed = !wf.is_worked(date)
                if(!day_removed){//on voulait une journée vide donc annuler
                    undo_func()
                }
            }
            return [undo_func,day_removed]
        }

        // Cette fonction supprime les secteur
        // des interventions de la journée mais en aucun
        // car ne modifie les référence des intervention présentes
        reset_date(date, opt = {}) {
            const wf = this
            let listInfo
            if(wf.listInfo_by_date){
                listInfo = wf.listInfo_by_date[date]
            }else{
                //pour les tests
                let inter = wf.get_first_interv_at_date(date)
                listInfo = inter && inter.listInfo
            }
            if(!listInfo) return true
                const inter_ori = listInfo.first_interv_sector;
            for (let inter = inter_ori;inter; inter = inter.next) {
                console.iidebug[61] = idnext('61')
                if(49086 == console.iidebug[61]){
                    // debugger
                }
                let was_protected = inter.protected && !inter.fakeProtected
                // ne pas s'arrêter en cas d'echec car si il faut c'est une intervention
                // protected d'absence qui ne peut pas être supprimé qui gène
                //  mais ce n'est pas forcément génant
                inter.reset_inter({
                    even_if_protected: opt.even_if_protected,
                    even_if_pause_fake_protected: opt.even_if_pause_fake_protected,
                    even_if_fake_protected: opt.even_if_fake_protected
                })
                controls_enabled(()=>{
                    if(was_protected && !opt.even_if_protected && !inter.protected){
                        my_throw( 'anormal')
                    }
                })
            }
            return !listInfo.is_worked()
        }

        backup_week(){
            var wf = this;
            var m = wf.main
            var backup = new Backup_week({wfs:[wf]})
            return backup
        }
        too_much_for_max_time(date) {
            var date_info = this.listInfo_by_date[date]
            return Math.max(0, date_info.worked - date_info.max)
        }
        missing_time(date) {
            var date_info = this.listInfo_by_date[date]
            return Math.max(0, date_info.min - date_info.worked)
        }

        inc_nb_minu_worked(diff, date) {
            controls_enabled(()=>{
                if(arguments.length !== 2){
                    my_throw( 'check the number of argument')
                }
            })
            var m = this//.proxy;
            m.nb_minu_worked += diff
            m.listInfo_by_date[date].worked += diff
            // m.v2_nb_inter_worked_by_day[date] = fl(m.v2_nb_inter_worked_by_day[date]) + (diff < 0 ? -1 : 1)
        }

        /* nombre de jour restant à placer */
        getNumberOfDaysRemained() {
            var m = this//.proxy;
            var nb_days_by_week = m.nb_days_by_week
            m.intervsRacines.forEach(function(inter){
                nb_days_by_week -= inter.is_day_worked() ? 1 : 0
            })

            // controls_enabled(()=>{
            //     var meth2 = m.nb_days_by_week - Object.values(m.v2_nb_inter_worked_by_day).filter((val) => val > 0).length
            //     if(meth2 != nb_days_by_week){
            //         my_throw( 'incohence des données')
            //     }
            // })
            return nb_days_by_week
        }

        addPersonSectorMap(o) {
            this.personSectorMap[o.id] = o;
        }


        initAllInterv(periodArr,wf,protected_info) {
            let ori_li_by_date = {}
            for(let li of Object.values(wf.listInfo_by_date)){
                ori_li_by_date[li.date] = {
                    min_start_day: li.min_start_day,
                    max_start_day: li.max_start_day,
                    max_end_day: li.max_end_day
                }
            }

                // on va fusionner les intervention qui se suivent
                // et remplir leur listInfo
                // if(protected_info.length){
                //     debugger
                // }
                if(conf.dont_care_about_amplitude){
                    periodArr = [] //car on se fiche des amplitudes maintenant seule les règles comptent
                    forOf(wf.listInfo_by_date,function(li){
                        periodArr.push({
                            date: li.date,
                            endMinute: li.max_end_day,
                            startMinute: li.min_start_day
                        })
                    })
                }




                var m = this;
                var inter,date_9,id = idnext('no_name12'),li;
                var listInfo_by_date = {}
                let add_to_listInfo_by_date = (li)=> {
                    if(!li.date){
                        debugger
                    }else{
                        li.person = wf
                        listInfo_by_date[li.date] = li
                    }
                }

                var get_listInfo = (inter)=>{
                    return listInfo_by_date[inter.date] || wf.listInfo_by_date[inter.date] || inter.listInfo
                }
                var createListeInfo = (inter,date,forced) => {
                    if(inter || forced) {
                        let li
                        if(forced){
                            li = new ListInfo({
                                person: wf,
                                date,
                                from: 'forced_init'
                            })
                        }else{
                            li = listInfo_by_date[date]
                        }
                        li.person = wf
                        li.date = date
                        li.worked = 0;
                        if(forced){
                            li.min_start_day = 0
                            li.max_end_day = 0
                            li.max = 0;
                            li.min = 0;
                            li.autorise_because_of_protected =  false
                        }else{
                            li.min = li.min || 0;
                            li.max = li.max || 0;
                            if(!conf.dont_care_about_amplitude){
                                li.min_start_day = li && li.min_start_day > inter.startMinute ? li.min_start_day : inter.startMinute
                                li.max_end_day = li && li.max_end_day < inter.endMinute ? li.max_end_day : inter.endMinute
                            }
                            if(void 8 == li.min_start_day){
                                li.min_start_day = inter.startMinute
                                li.max_end_day = inter.endMinute
                            }
                            inter.startMinute = upper(inter.startMinute)
                            inter.endMinute = lower(inter.endMinute)
                            li.first_interv = li.last_interv = inter
                            li.autorise_because_of_protected =  !not_autorise_because_of_protected
                            not_autorise_because_of_protected = true
                        }
                        li.min_end_day = li.min_end_day || 0
                        li.max_start_day = li.max_start_day || 0
                        li.is_initialised_by_create_listInfo = idnext('iibl')

                        if(li.max && (li.max_start_day + li.max < li.max_end_day) ){
                            li.max = Number.POSITIVE_INFINITY//on désactive une contrainte improbable
                            console.error('contrainte max désactivée')
                            //li.max_end_day = upper(li.max_start_day + li.max + conf.max_pause_time)
                        }

                        if(li.min_end_day - li.max - conf.max_pause_time > li.min_start_day){
                            li.max = Number.POSITIVE_INFINITY//on désactive une contrainte improbable
                            console.error('contrainte max désactivée')
                            // li.min_start_day = lower(li.min_end_day - li.max - conf.max_pause_time)
                        }

                        if(li.min_end_day > li.max_end_day){
                          console.error('contrainte min_max inversée')
                          [li.min_end_day,li.max_end_day] = [li.max_end_day,li.min_end_day]
                      }
                      if(li.min_start_day > li.max_start_day){
                          console.error('contrainte min_max inversée')
                          [li.min_start_day,li.max_start_day] = [li.max_start_day,li.min_start_day]
                      }
                      assert_finite(li.max_end_day)
                      assert_finite(li.min_start_day)
                      if(li.is_initialised_by_create_listInfo == 19416){
                        debugger
                    }
                        // let ori_info = ori_li_by_date[li.date]
                        // if(ori_info && li.max_end_day < ori_info.max_end_day){
                        //     console.warn('Attention on autorise moins que ce que la BDD demande')
                        // }
                        // Debug.check_li(li)
                        return li
                    }
                }
                let periodArr2 = (protected_info ? protected_info.concat(periodArr) : periodArr).sort((a,b)=>{
                    return (a.date > b.date ? 1 : (a.date < b.date ? -1 : 0))
                    || (a.startMinute - b.startMinute) || (a.endMinute - b.endMinute)
                })

                let not_autorise_because_of_protected = true
                periodArr2.forEach(function(per,k) {
                    console.iidebug[119] = idnext(119)
                    // attention per n'est pas forcément une période  mais peut aussi être un objet
                    // quelconque
                    let {openingType,date,endMinute,startMinute} = per
                    let date2
                    if(openingType == 'FERMETURE') return;
                    // if(per.protected) debugger
                    // console.log(
                    //si l'intervention est à la suite d'une autre alors les fusionner
                    //edit: maintenant il n'y a qu'une seule racine par jour même si il est
                    //coupé en deux par une pause
                    if(inter /*&& inter.endMinute == startMinute*/ && inter.date == date2) {
                        if(per.protected){
                            not_autorise_because_of_protected = false
                        }
                        inter.endMinute =  Math.max(inter.endMinute, endMinute)
                    } else {
                        //sinon remplir le champs listInfo de inter des boucles précédente
                        createListeInfo(inter,date_9)
                        if(per.protected){not_autorise_because_of_protected = false}
                            date2 = per.date
                        li = wf.listInfo_by_date[date2];
                        if(li || per.protected){

                            //Et créer une intervention à part
                            inter = new Intervention({
                                date: date2,
                                //@warning li peut être undefined
                                startMinute: li && li.min_start_day > startMinute ? li.min_start_day : startMinute,
                                endMinute: li && li.max_end_day < endMinute ? li.max_end_day : endMinute,
                                id: 'X' + ++id,
                            })
                            li = get_listInfo(inter)
                            add_to_listInfo_by_date(li)
                            inter.listInfo = li
                            // if(inter.id == 'X78173'){
                            //     debugger
                            // }
                            // controls_enabled(()=>{
                            //     if(inter.endMinute == 1410){
                            //         my_throw( 'error')
                            //     }
                            // })

                            if(inter.endMinute <= inter.startMinute){
                                inter = null;return;
                            }
                            m.intervsRacines.push(inter)
                            inter.hasBeenInsert = true
                            m.intervNotAffectedMap[inter.id] = inter;
                            date_9 = date2
                        }
                    }
                })
                createListeInfo(inter,date_9)

                for(let date of m.main.all_dates){
                    let li = listInfo_by_date[date]
                    if(!listInfo_by_date[date]){
                        add_to_listInfo_by_date(createListeInfo(null,date,true))
                        let lInfo = listInfo_by_date[date]
                        if(lInfo.first_interv){
                            let a = lInfo.last_minu_possible//ça lance des vérifications
                            let b = lInfo.last_minu_possible//ça lance des vérifications
                        }
                        li = listInfo_by_date[date]
                    }else{
                        li.not_rounded = {
                            min_start_day: li.min_start_day,
                            max_end_day: li.max_end_day
                        }
                        li.first_interv.startMinute = li.min_start_day = lower(li.min_start_day)
                        li.last_interv.endMinute =  li.max_end_day = upper(li.max_end_day)
                    }
                    Debug.check_li(li)
                }
                //contient aussi les jours non travaillés
                wf.listInfo_sorted = Object.values(listInfo_by_date).sort((a,b) => a.date > b.date ? 1 : -1)
                controls_enabled(()=>{
                    if(wf.listInfo_sorted.length != 7){
                        debugger
                    }
                })
                wf.listInfo_sorted.forEach((li)=>{
                    if(!li.first_interv){
                        //Rendre les données incohérentes pour être sûr que rien n'y soit planifié
                        li.max_end_day = 0
                        li.max = 0
                        li.min_start_day = 24 * H
                    }
                })
                let intervsRacines = [], iRacine = 0;
                wf.listInfo_sorted.forEach((li)=>{
                    let fi = li.first_interv
                    if(fi){
                        fi.listInfo = li
                        if(!isFinite(fi.listInfo.worked)){
                            debugger;
                        }
                        li.iRacine = iRacine
                        intervsRacines[iRacine] = fi
                        ++iRacine
                    }else{
                        delete listInfo_by_date[li.date]
                    }
                    Debug.check_li(li)

                })
                wf.intervsRacines = intervsRacines
                //contrairement à listInfo_by_date qui ne contient plus les jours non travaillés
                wf.listInfo_by_date = listInfo_by_date

                Debug.check_li(wf)
                if(protected_info){
                    // protected_info.forEach(function(inter){
                    //     var li = wf.listInfo_by_date[inter.date]
                    //     if(li.min_start_day > inter.startMinute){
                    //         li.min_start_day = inter.startMinute
                    //         li.first_interv.startMinute = upper(li.min_start_day)
                    //     }
                    //     if(wf.listInfo_by_date[inter.date].max_end_day < inter.endMinute){
                    //         li.max_end_day =  inter.endMinute
                    //         li.last_interv.endMinute = lower(li.max_end_day)
                    //     }
                    // })

                    controls_enabled(()=>{
                        protected_info.forEach(function(inter){
                            if(wf.listInfo_by_date[inter.date].min_start_day > inter.startMinute){
                                my_throw2(91,"Les heures autorisées n'englobe pas les protected")
                            }
                            if(wf.listInfo_by_date[inter.date].max_end_day < inter.endMinute){
                                my_throw2(91,"Les heures autorisées n'englobe pas les protected")
                            }
                        })
                    })
                }


                Debug.check_li(wf)

            }
            //d.date =="2017-09-05" && d.startMinute > 12 && d.person.wfId == 420 && d.sector.id == 1177

            get_first_interv_at_date(date) {
                var person = this;
                var obj = {
                    listInfo: { // on fake le listeInfo pour pouvoir comparer avec les autres interventions racine
                        min_start_day: -48 * H, //avant ça avait du sens car il pouvait y avoir plusieurs fois le même mais avec des heures différentes quand il y avait une pause alors que maintenant non car on doit choisir ou mettre la pause
                        max_end_day: 48 * H,
                        date: date || '2000-01-01',
                        person
                    }
                }
                if(person.listInfo_by_date){
                    return person.listInfo_by_date[date] && person.listInfo_by_date[date].first_interv
                }

                // On prend la date et l'heure de début de la linkedList pour comparer avec l'intervention que l'on veut ajouter
                // if  we don't divide by 10 '8' > '13' but if we divide by 10 '0.8' < '1.3'
                var getPropToCompare = (i) => i.listInfo.date //+ i.listInfo.min_start_day / 10;
                // var ddebug = true
                // if(ddebug == true){
                //     this.intervsRacines.forEach(function(inter){
                //         inter.debug_prop_to_compare = getPropToCompare(inter)
                //     })
                //     obj.debug_prop_to_compare = getPropToCompare(obj)
                //     getPropToCompare = (i) => {
                //         return i.debug_prop_to_compare
                //     }
                // }
                // on cherche la première intervention racine de la journée gràce à un algorithme performant
                // pour les tableau triés qui est binarySearsh
                // ça nous retourne la position ou il faudrait inserer notre intervention
                // mais comme on ne souhaite pas ajouter pas au tableau sortedArrFunc quand les interventions se suivent
                // on prend l'indice précédent et on va ajouter notre élément à cette linkedListe
                // @perf TODO: remplacer binary searsh par un calcul d'indice ou un objet indexé par date
                var iRacine = sortedArrFunc.upperBound(this.intervsRacines, obj, getPropToCompare) - 1;
                var inter = person.intervsRacines[iRacine]
                return inter && inter.date == date && inter
            }

            get_first_minu_worked(date) {
                const wf = this, lid = wf.listInfo_by_date[date]
                return lid && lid.first_minu_worked
            }

            get_last_minu_worked(date){
                const wf = this, lid = wf.listInfo_by_date[date]
                return lid && lid.last_minu_worked
            }
            /*
             * retourne undefined si pas de secteur dans la journée
             * ou que la journée n'est pas disponible du tout
             */
             get_first_interv_with_sector_at_date(date_or_first_inter) {
                let lid, wf = this;
                // if(date_or_first_inter.listInfo){
                //     lid = date_or_first_inter.listInfo
                // }else{
                    lid = wf.listInfo_by_date[date_or_first_inter]
                // }
                return lid && lid.first_interv_sector
                /*
                var person = this;
                if(!date_or_first_inter) {
                    return;
                }
                var first_interv = date_or_first_inter.listInfo ? date_or_first_inter : person.get_first_interv_at_date(date_or_first_inter)
                while (first_interv && !first_interv.sector) {
                    first_interv = first_interv.next
                }
                return first_interv && first_interv.sector && first_interv //undefined si pas de secteur dans la journée
                */
            }

            get_last_interv_with_sector_at_date(date_or_first_inter) {
                let lid,wf = this;
                // if(date_or_first_inter.listInfo){
                //     lid = date_or_first_inter.listInfo
                // }else{
                    lid = wf.listInfo_by_date[date_or_first_inter]
                // }
                return lid && lid.last_interv_sector
               /* var person = this;
                var last_interv = person.get_first_interv_at_date(date)
                if(!last_interv) return;
                while (last_interv.next) {
                    last_interv = last_interv.next
                }
                while (last_interv && !last_interv.sector) {
                    last_interv = last_interv.prev
                }
                return last_interv && last_interv.sector && last_interv //undefined si pas de secteur dans la journée
                */
            }

            //@TODO_fast: factoriser avec modifyIntervToAddMinute
            //v2_get_contiguous_interv_between
            v2_get_contiguous_interv_between(start_date, end_date, start_minute, end_minute, only_with_sector) {
                var person = this;
                controls_enabled(()=>{
                    if(start_date != end_date) {
                        my_throw( 'no need to be implemented')
                    }
                })
                var to_ret = []
                var no_minutes_cond = (typeof start_minute === 'undefined')
                for (var inter = person.get_first_interv_at_date(start_date); inter;) {
                    var date_ok = inter.listInfo.date <= end_date
                    var minutes_ok = no_minutes_cond ||
                    inter.startMinute < end_minute && inter.endMinute > start_minute
                    var sector_ok = (!only_with_sector || inter.get_real_sector())
                    if(date_ok && minutes_ok && sector_ok) {
                        to_ret.push(inter)
                    } else if(to_ret.length) {
                        break;
                    }
                    inter = inter.next /*|| this.intervsRacines[++iRacine]*/ //next si même jounée sinon ++iRacine pour journée suivante
                }
                return to_ret;
            }

            unsafe_reduce_start_day(date){
                var wf = this;
                var fmw = wf.get_first_minu_worked('2019-02-10')
                var inter = new Intervention({
                    startMinute: fmw ,
                    date,
                    endMinute: fmw + conf.minTime,
                    sector: null,
                    rules: {
                        megaForcedInsert : true
                    }
                })
                wf.addInterv(inter)
            }

            unsafe_reduce_end_day(date){
                var wf = this;
                var fmw = wf.get_last_minu_worked('2019-02-10')
                var inter = new Intervention({
                    date,
                    startMinute: fmw - conf.minTime,
                    endMinute: fmw,
                    sector: null,
                    rules: {
                        megaForcedInsert : true
                    }
                })
                wf.addInterv(inter)
            }

            update_intervention(intervToUpdate,iRacine) {
                var wf = this;
                if(!intervToUpdate.length) return;
                console.iidebug[24] = idnext('24')
                // if(console.iidebug[24] == 25166){
                //     debugger
                // }

                //main && main.logPersonSectorBug(true,3286)
                var alreadyDone = {}
                Debug.perfStart('addIntervP4')
                //le nombre d'heure travaillé va être instable tant que la boucle n'est pas terminé on désactive donc les contrôles en
                //ajoutant une tolérance de 10 * H minute
                wf.nb_min_minute_fakely_worked = (wf.nb_min_minute_fakely_worked || 0) + 1000 * H
                var inter0 = intervToUpdate[0]
                var lInfo = inter0.listInfo
                // si ça vaut 0 ça veut dire que l'on ne connait pas la premiere
                // ou dernière intervention avec secteur et donc on ne pourra pas mettre
                // la valeur à jour
                //en accord avec la promesse
                //i_will_preserve_the_integrity_of_last_inter_of_listInfo :
                //on ne peut pas utiliser first_interv_sector sans le underscore
                //car comme les données ne sont pas encore cohérentes ça va déclenché une erreur
                var fiws = lInfo._first_interv_sector
                var liws = lInfo._last_interv_sector
                var fiws_wasnt_0,liws_wasnt_0
                if(fiws !== 0){
                    fiws_wasnt_0 = true
                    if(fiws && !fiws.hasBeenInsert) {
                        lInfo.first_interv_sector = 0
                    }
                }
                if(liws !== 0){
                    liws_wasnt_0 = true
                    if(liws && !liws.hasBeenInsert) {
                        lInfo.last_interv_sector = 0
                    }
                }

                if(console.iiDebugInter == 14880) {
                    ////////debugger;
                }

                //on enlève les doublons
                // doublon : {
                //     var j = 0;
                //     for(var i = 0;i < intervToUpdate.length;++i){
                //         var inter = intervToUpdate[i]
                //         if(!inter.need_update){
                //             controls_enabled(()=>{
                //                 if(inter.listInfo != lInfo){
                //                     my_throw( "les interventions d'une même journée doivent avoir le même listInfo")
                //                 }
                //             })
                //             intervToUpdate[j++] = intervToUpdate[i]
                //             inter.need_update = true
                //         }
                //     }
                //     intervToUpdate.length = j
                // }

                intervToUpdate = new Set(intervToUpdate)
                //on a potentielement modifié les interventions voisine donc mettre tout ça à
                //jour en plus de notre intervention
                console.before_update_loop =  (console.before_update_loop || 0) + 1;
                if(console.before_update_loop == 1591){
                    // debugger;
                }

                const {exact_opening, exact_closing} = lInfo;
                //il faut trouver la dernière et première intervention
                //avec et sans secteur
                intervToUpdate.forEach((inter,jj) => {
                    inter.need_update = false;
                    controls_enabled(()=>{
                        // if(inter.endMinute == 1410){
                        //     my_throw( 'error')
                        // }
                        if(!isFinite(inter.startMinute) || !isFinite(inter.endMinute) || inter.startMinute < 0 || inter.endMinute < 0) {
                            my_throw( 'error sur les heures qui doivent êtres des entiers positifs')
                        }
                        if(inter.protected_reason == 'manager_forced' && !inter.true_protected_information){
                            my_throw( 'les deux propriétés doivent être présente ou bien aucune')
                        }
                        // ex: 10575_600: [{sec: 2522,aid:10}]
                        //DO NOT DELETE ceci est très utile pour savoir quand un changement de secteur à une heure donnée à eux lieu
                        var slow_debug = Debug.slow_debug
                        if(slow_debug){
                            for(var i = inter.startMinute;i < inter.endMinute;i += 30){
                                var key = inter.date + '_' + (inter.person ? inter.person.wfId : '0')+ '_' +i
                                Debug.change_to_sector[key] = Debug.change_to_sector[key] || []
                                var last_info = last( Debug.change_to_sector[key])
                                var last_sector = '' + ((last_info || {sec:'0'}).sec);
                                var new_sector = '' + (inter.sector ? inter.sector.id : '0')
                                if(Debug.undo){
                                    //annuler de l'historique tout ce qui était après la sauvegarde
                                    var arr = Debug.change_to_sector[key]
                                    if(arr){
                                        while(arr.length && last(arr).ludi > Debug.undo_until_ludi){
                                            --arr.length
                                        }
                                    }
                                }else{
                                    let ludi = console.addIntervDebug
                                    if(last_sector != new_sector){
                                        Debug.change_to_sector[key].push({
                                            sector: new_sector,
                                            ludi
                                        })
                                    }
                                    if(420685 == ludi){
                                        debugger
                                    }
                                }
                            }
                        }
                    })
                    if(!inter.prev) {
                        wf.intervsRacines[iRacine] = lInfo.first_interv = inter;
                    }
                    if(!inter.next) {
                        lInfo.last_interv = inter
                    }
                    inter.hasBeenInsert = true
                    if(inter.get_real_sector()) {
                        //en accord avec la promesse
                        //i_will_preserve_the_integrity_of_last_inter_of_listInfo :
                        if(fiws_wasnt_0){
                            var fi = lInfo._first_interv_sector
                            if(!fi || inter.startMinute <= fi.startMinute) {
                                lInfo.first_interv_sector = inter
                            }
                        }
                        if(liws_wasnt_0){
                            var li = lInfo._last_interv_sector
                            if(!li || inter.endMinute >= li.endMinute) {
                                lInfo.last_interv_sector = inter
                            }
                        }
                    }
                    controls_enabled(()=>{
                        if(wf.main && wf.main.is_test == false){
                            var date = inter.date
                            if(wf.rotationMaps[date].length && !wf.is_worked(date)){
                                my_throw( 'pourquoi il y a des éléments si la personne ne travaille pas ce samedi')
                            }
                            if(wf.listInfo_sorted){
                                var tot_worked = 0;
                                wf.listInfo_sorted.forEach((li1)=>{
                                 tot_worked += li1.worked
                             })
                                if(wf.nb_minu_worked != tot_worked){
                                    my_throw( 'illogic')
                                }
                            }
                        }
                    })
                })

            controls_enabled(()=>{
                var fiws = lInfo._first_interv_sector
                var liws = lInfo._last_interv_sector
            })

            const add_to_update = (inter)=>{
                if(!intervToUpdate.has(inter)){
                    inter.prepareToBeReinserted(main, {
                                        //on va bien préserver la dernière et première intervention
                                        //dans listInfo car on y touche pas
                                        i_will_preserve_the_integrity_of_last_inter_of_listInfo: true
                                    });
                    intervToUpdate.add(inter)
                    inter.hasBeenInsert = true
                }
            }

            lInfo.update_opening_closing_time({add_to_update})


            intervToUpdate.forEach((inter,jj) => {
                // if(11788 == console.iidebug[24]){
                //     debugger
                // }
                // pour la supppression il faut que les données définitive qui ont compté pour l'insertion soient stockées
                // finaltime est donc le startMinute et endMinute qui était là la dernière fois que les données était synchronisées
                // avec personSector et les sector.planning.periodarrsorted.interventionMap
                // cette synchronisation est faite par insertAtGoodPlace
                if(inter.is_pause()){
                    console.pause_id = idnext('pause_id')
                    if(18178 == console.pause_id){
                        debugger
                    }
                }
                inter.insertAtGoodPlace(main);
                inter.updateDebugId = ''//(inter.updateDebugId || '') + ',' + console.addIntervDebug // va aller dans jobcomment de la base de données
                inter.last_update_debug_id = console.addIntervDebug // va aller dans jobcomment de la base de données
            })


                controls_enabled(()=>{
                    if(wf.listInfo_by_date){
                        intervToUpdate.forEach(function(inter){
                            var found;
                            for(var inter2 = wf.listInfo_by_date[inter.date].first_interv;inter2;inter2 = inter2.next){
                                if(inter2 == inter){
                                    found = true
                                }
                                if(old_inter2 && old_inter2.endMinute != inter2.startMinute){
                                    my_throw( "les heures devraient être ordonnée dans cette liste chainée")
                                }
                                var old_inter2 = inter2
                            }
                            if(!found){
                                my_throw( 'Cette intervention devrait pourtant être dans la liste chainée')
                            }
                        })
                    }
                })
                inter0
                controls_enabled(()=>{
                    inter0 &&  inter0.listInfo.check_logic()
                })
                main && main.blockArr && main.logPersonSectorBug(false,wf,lInfo.date)
                wf.nb_min_minute_fakely_worked -= 1000 * H
                Debug.perfEnd('addIntervP4')
            }


            // // on cherche la première intervention racine de la journée gràce à un algorithme performant
            // // pour les tableau triés qui est binarySearsh
            // // ça nous retourne la position ou il faudrait inserer notre intervention
            // // mais comme on ne souhaite pas ajouter pas au tableau sortedArrFunc quand les interventions se suivent
            // // on prend l'indice précédent et on va ajouter notre élément à cette linkedListe
            // // TODO: remplacer binary searsh par un calcul d'indice
            // var iRacine = sortedArrFunc.upperBound(this.intervsRacines,obj,getPropToCompare) - 1;
            // if(iRacine < 0) return [];
            //@modifyIntervToAddMinute
            modifyIntervToAddMinute(intervToAdd, main, nbRecursivity, is_recursivity) {
                var to_unprotect = []
                var rules = intervToAdd.rules || {}
                if(intervToAdd.protected && !intervToAdd.fakeProtected) {
                    rules.megaForcedInsert = true
                }
                if(rules.safe_insertion) {
                    rules.break_if_left_mintime_not_respected = true
                    rules.break_if_right_mintime_not_respected = true
                    rules.v2_break_if_min_time_not_ok = true
                }
                if(rules.latest_rules_apply){
                    Object.assign(rules, rules.latest_rules_apply)
                }
                controls_enabled(()=>{
                    if(!rules.safe_insertion
                        // ne pas faire attention à la taille de l'intervention avant ou
                        // après celle qui va être insérée
                        && !rules.v2_do_not_respect_min_time_after_before
                        //De même mais pour un coté spécifique
                        && !(rules.break_if_left_mintime_not_respected && rules.break_if_right_mintime_not_respected)
                        //@depreciated
                        && !rules.allow_to_change_hours//agrandit le secteur d'avant ou d'après pour remplacer un secteur trop petit sur les bords mais crache avec les pauses
                        // insérer et ecraser ce qu'il a dessous que ce soit protégé ou non
                        // alors que normalement on écrase que des choses non protégé
                        // et en plus on ne vérifiera aucune règle
                        // il faut quand même que ce soit dans ses heures autorisées
                        && !rules.megaForcedInsert
                        //ne pas faire attention à la taille de l'intervention
                        && !rules.v2_break_if_min_time_not_ok === false
                        ){
                        my_throw( 'choisir une de ses options')
                    }


                    if(intervToAdd.endMinute%conf.minTime || intervToAdd.startMinute%conf.minTime){
                        debugger
                        my_throw('not round')
                    }
                })
                if(!is_recursivity) {
                    Debug.perfStart('modifyIntervToAddMinute')
                }
                var origine = {
                    startMinute: intervToAdd.startMinute,
                    endMinute: intervToAdd.endMinute
                }



                var args = arguments;
                var returnFalse = function(error_code) {
                    if(!is_recursivity) {
                        intervToAdd.error_code = error_code
                        Debug.perfEnd('modifyIntervToAddMinute')
                        Debug.inc_error_code(error_code)
                    }
                    return false; //ça sert à rien sauf à débugguer où on a quitter la fonction
                }

                var maxendMinute, minstartMinute, minTimeAllSector, minTime;
                controls_enabled(()=>{
                    if(!(intervToAdd instanceof Intervention)) {
                        my_throw( 'not_an_intervention')
                    }
                })
                var sctr = intervToAdd.sector
                if(sctr && sctr.parentEntity) {
                    minTimeAllSector = sctr.parentEntity.minTimeSectors
                } else {
                    minTime = minTimeAllSector = conf.minTime
                }
                if(sctr && 'minTimeSector' in sctr){
                    minTime = sctr.minTimeSector
                }
                if(rules.megaForcedInsert) {
                    //quand on est en brut force on se fiche que les interventions avant ou après respectent ou non le temps minimum
                    rules.v2_do_not_respect_min_time_after_before = true
                }
                var minTimeAllSectorAfterBefore;
                if(main && main.minTimeAllSectorIs0) {
                    minTime = minTimeAllSector = minTimeAllSectorAfterBefore = 0
                }
                if(rules.v2_do_not_respect_min_time_after_before) {
                    minTimeAllSectorAfterBefore = 0
                } else {
                    minTimeAllSectorAfterBefore = minTimeAllSector
                }
                var intervToUpdate = [intervToAdd]
                // if(minTime <= 0){
                //  my_throw( ('0 doit absolument être positif'))
                // }

                if(rules.megaForcedInsert) {
                    minTime = 0;
                }
                var person = this;
                var m = this;
                var delta, interv2,hours_out_of_limit, racine,lInfo, iRacine, getPropToCompare, getPropToCompare_endMinute;
                for (var i = 2; i--;) {
                    if(person.listInfo_by_date){//quand listInfo_by_date existe c'est plus rapide à utiliser
                        var d = person.listInfo_by_date[intervToAdd.date]
                    iRacine = d ? d.iRacine : -1
                }else{
                        intervToAdd.listInfo = { // on fake le listeInfo pour pouvoir comparer avec les autres interventions
                            min_start_day: intervToAdd.startMinute,
                            max_end_day: intervToAdd.endMinute,
                            date: intervToAdd.date || '2000-01-01',
                            person: person,
                            from : 'addInterv'
                        }
                        // On prend la date et l'heure de début de la linkedList pour comparer avec l'intervention que l'on veut ajouter
                        // if we don't divide by 10 '8' > '13' but if we divide by 10 '0.8' < '1.3'
                        getPropToCompare = (i) => i.listInfo.date //+ ('00000' + i.listInfo.min_start_day).slice(-5);
                        // getPropToCompare_endMinute = (i) => {return '' + i.listInfo.date + ('00000' + i.listInfo.max_end_day).slice(-5) ;}

                        // on cherche la première intervention racine de la journée gràce à un algorithme performant
                        // pour les tableau triés qui est binarySearsh
                        // ça nous retourne la position ou il faudrait inserer notre intervention
                        // TODO: remplacer binary searsh par un calcul d'indice
                        iRacine = sortedArrFunc.upperBound(this.intervsRacines, intervToAdd, getPropToCompare) - 1;
                    }
                    var ita = intervToAdd
                    if(iRacine >= 0){
                        racine = this.intervsRacines[iRacine]
                        lInfo = racine && racine.listInfo
                        if(  Debug.is_not_test ){
                            var hours_out_of_limit = 0;
                            if(lInfo.first_minu_possible > ita.startMinute){
                                hours_out_of_limit = 4
                            }
                            if(lInfo.last_minu_possible < ita.endMinute){
                                hours_out_of_limit = 5
                            }
                        }else{
                            var hours_out_of_limit = 0;
                            if(lInfo.min_start_day > ita.startMinute){
                                hours_out_of_limit = 2
                            }
                            if(lInfo.max_end_day < ita.endMinute){
                                hours_out_of_limit = 3
                            }
                        }

                        if(hours_out_of_limit && rules.allow_to_change_hours){
                            var time = ita.time
                            ita.startMinute = Math.max(lInfo.min_start_day, ita.startMinute )
                            if(!ita.end_minute_too_late){
                                ita.endMinute = Math.min(lInfo.max_end_day, ita.startMinute + time)
                                ita.startMinute = Math.max(lInfo.max_end_day, ita.endMinute - time )
                                //les heures peuvent être totalement différentes de ce que l'on avait demandé avec allow_to_change_hours
                                //on a essayé de garder le même temps d'intervention
                            }
                            hours_out_of_limit = ita.startMinute >= ita.endMinute ? 4 : 0
                        }
                    }
                    if(iRacine < 0 || racine.date < intervToAdd.date || hours_out_of_limit) {
                        if(!intervToAdd.end_minute_too_late || i == 0) {
                            controls_enabled(()=>{
                                if(intervToAdd.date.length > 10){
                                    my_throw( 'date trop longue')
                                }
                                if(rules.megaForcedInsert && !rules.canFail) {
                                    my_throw( 'can not failled')
                                }
                            })
                            returnFalse(901); //la personne ne peut pas travailler à ce moment là
                            return false;
                        }
                        //on laisse une tentative à end_minute_too_late pour corriger le fait que cette intervention finisse trop tard
                        intervToAdd.end_minute_too_late(intervToAdd, racine.listInfo.max_end_day)
                    } else {
                        break;
                    }
                }
                interv2 = racine;
                controls_enabled(()=>{
                    if(intervToAdd.date != racine.date) {
                        //normalement déjà géré par getPropToCompare_endMinute
                        my_throw( "l'object trouvé n'est pas à la date souhaitée")
                    }
                    if(racine.prev) {
                        my_throw( "la racine ne peut avoir de précédent")
                    }
                })
                intervToAdd.listInfo = lInfo; //on lui affecte le bon cette fois l'autre servait juste à la comparaison



                var interv2Origin = interv2;
                var rules = intervToAdd.rules
                while (interv2.endMinute < intervToAdd.endMinute) {
                    interv2 = interv2.next
                    controls_enabled(()=>{
                        if(!interv2) {
                            my_throw( '')
                        }
                        if(interv2 == interv2Origin) {
                            my_throw( "error recursion")
                        }
                    })
                }


                //une pause ne peut PAS ne PAS avoir d'intervention avec secteur avant
                if(!rules.megaForcedInsert) {
                    if(intervToAdd.is_pause()) {
                        var inter4 = interv2
                        while (inter4 && inter4.startMinute >= intervToAdd.startMinute) {
                            inter4 = inter4.prev
                        }
                        if(!inter4 || !inter4.sector) {
                            returnFalse(903);
                            return false;
                        }
                    }

                    // on parcourt les interventions qui vont croiser ou être collées à celles qu'on veut ajouter
                    // si elles sont du même secteur ça veut dire que l'on peut inclure son temps dans l'intervention que l'on va ajouter
                    // et peut être du coup que l'on respectera le temps minimum
                    //@TODO fusionner ce code avec getStartAndEndSameSector
                    if(intervToAdd.get_real_sector() /*!= 'PAUSE'*/ ) {
                        ['next', 'prev'].forEach((dir) => {
                            var interv3
                            interv3 = interv2;
                            if(dir == 'prev' && interv2.prev && interv2.startMinute == intervToAdd.startMinute) {
                                interv3 = interv2.prev
                            }
                            if(dir == 'next' && interv2.next && interv2.endMinute == intervToAdd.endMinute) {
                                interv3 = interv2.next
                            }
                            while (interv3.endMinute >= intervToAdd.startMinute && interv3.startMinute <= intervToAdd.endMinute) {
                                //tant que le secteur est le même inclure le temps dans notre intervention
                                if(!interv3.sector) {
                                    break;
                                }
                                if(interv3.sector == intervToAdd.sector) {
                                    intervToAdd.endMinute = Math.max(intervToAdd.endMinute, interv3.endMinute)
                                    intervToAdd.startMinute = Math.min(intervToAdd.startMinute, interv3.startMinute)
                                }
                                if(!(interv3 = interv3[dir])) break; //@affectation
                            }
                        })
                        //respecter le temps minimum
                        if(intervToAdd.endMinute - intervToAdd.startMinute < minTime) {
                            // si après avoir inclus ceux du même secteur il y a toujours par la place alors
                            // agrandir l'intervention par la fin
                            // on aurait très bien pu étendre par le début aussi
                            if(intervToAdd.endMinute - intervToAdd.startMinute < minTime) {
                                if(rules.v2_break_if_min_time_not_ok) {
                                    returnFalse(904);
                                    return false;
                                }
                                intervToAdd.endMinute = intervToAdd.startMinute + minTime
                                var hours_has_changed = true
                            }
                            // respecter le max_end_day
                            if(intervToAdd.endMinute > intervToAdd.listInfo.max_end_day) {
                                intervToAdd.endMinute = intervToAdd.listInfo.max_end_day
                                intervToAdd.startMinute = intervToAdd.endMinute - minTime
                                var hours_has_changed = true
                            }
                        }
                    }

                    if(!ita.megaForcedInsert){
                        var interv4 = interv2
                        var already_like_that = true
                        do{
                            if(ita.sector != interv4.sector){
                                already_like_that = false
                                break;
                            }
                            interv4 = interv4.prev
                        }while(interv4 && interv4.endMinute > ita.startMinute);

                        //l'ajout ne changera rien à la situation donc ne pas le faire
                        if(already_like_that){
                            // debugger
                            Debug.perfEnd('modifyIntervToAddMinute')
                            return {
                                already_like_that: true
                            }
                        }
                    }

                    if(intervToAdd.is_pause()) {
                        if(rules.v2_break_if_min_time_all_sector_not_ok_around_pause) {
                            var info87 = interv2.getStartAndEndWithSectorAroundMe(intervToAdd)
                            var diff = intervToAdd.startMinute - info87.startMinute
                            if(!diff || diff < minTimeAllSector) {
                                returnFalse(911);
                                return false
                            }
                            var diff2 = info87.endMinute - intervToAdd.endMinute
                            if(!diff2 || diff2 < minTimeAllSector) {
                                returnFalse(912);
                                return false
                            }
                        } else if(rules.v2_break_if_min_time_not_ok_around_pause) {
                            // var info88 = interv2.getStartAndEndSameSector(interv2)
                            // if(intervToAdd.startMinute - info88.startMinute < minTime ){
                            //     returnFalse(905);
                            //     return false
                            // }

                            // if(info88.endMinute - intervToAdd.endMinute < minTime){
                            //     returnFalse(906);
                            //     return false
                            // }
                        }


                    }

                    // on stoque les interventons protégées car il faudra les remettre à la fin
                    // car si une intervention de même secteur entoure sa plage horaire on va l'autoriser
                    // @warning slowPerf
                    // var alreadyProtected = {}
                    var props = ['next', 'prev']
                    var wm_protected = new WeakMap();
                    for (var i = 2; i--;) {
                        if(i == 1){
                            var interv3 = interv2
                        }else{
                            var interv3 = interv2[props[i]]
                            if(!interv3) continue;
                        }

                        //on parcourt les interventions qui vont croiser celle que l'on veut ajouter @cross
                        while (interv3.endMinute > intervToAdd.startMinute && interv3.startMinute < intervToAdd.endMinute) {
                            if(interv3.get_real_sector()) {
                                if(interv3.sector !== intervToAdd.sector) {
                                    if(interv3.protected && rules.forbid_to_be_arround_protected_with_real_sector){
                                        returnFalse(907);
                                        return false; // on ne peut pas écraser une intervention protected si on a pas le même secteur et quand on a le même secteur on remet la protected à la fin
                                    }
                                    if(rules.dontMoveExistingInterventionOrPause) {
                                        returnFalse(908);
                                        return false; //on ne prend pas la place d'une intervention déjà existante donc on ne peut insérer
                                    }
                                }
                            } else {
                                // On croise une période sans intervention
                                // donc ça étendrait le temps de travail et on ne veut
                                // pas ça
                                if(rules.v2_do_not_extend_workstime) {
                                    returnFalse(913)
                                    return false
                                }
                                if(rules.dontMoveExistingInterventionOrPause && interv3.is_pause()) {
                                    returnFalse(908);
                                    return false; //on ne prend pas la place d'une intervention déjà existante donc on ne peut insérer
                                }
                                if(interv3.is_pause()){
                                    if(rules.dont_move_existing_pause) {
                                        returnFalse(925);
                                        return false; //on ne prend pas la place d'une intervention déjà existante donc on ne peut insérer
                                    }

                                    //On ne peut pas entouré une pause de secteur vide
                                    if(!intervToAdd.get_real_sector()){
                                        returnFalse(926)
                                        return false; //on ne prend pas la place d'une intervention déjà existante donc on ne peut insérer
                                    }
                                }
                            }
                            if(interv3.protected) {
                                if(rules.break_if_protected_different_sector && intervToAdd.sector != interv3.sector){
                                    returnFalse(927)
                                    return false;
                                }
                                // il faut la déprotéger l'originale maintenant pour qu'elle soit traité
                                // comme une intervention normale et ne gène pas
                                // avant de la réinsérer à sa place
                                // interv3.unprotectIfProtected(protectedIntervArr)
                                if(!wm_protected.get(interv3)){//on l'a déjà traité ça ne sert à rien de le refaire
                                    to_unprotect.push(interv3)
                                wm_protected.set(interv3,true)
                            }
                        }
                            if(!(interv3 = interv3[props[i]])) break; //@affectation
                        }
                    }

                    //si à cause des protected le temp minimum n'est plus respecté alors ne pas faire l'ajout
                    var respect_min_time_with_protected = function() {
                        var hours_limits = [intervToAdd.startMinute]
                        to_unprotect.sort((a,b)=>{
                            return a.startMinute - b.startMinute
                        })
                        controls_enabled(()=>{
                            var last = null;
                            to_unprotect.forEach(function(a){
                                controls_enabled(()=>{
                                    if(a == last){
                                        my_throw( 'comment des doublons sont il arrivé')
                                    }
                                })
                                last = a
                            })
                        })
                        to_unprotect.forEach(function(pr_int) {
                            if(pr_int.sector != intervToAdd.sector) {
                                hours_limits.push(pr_int.startMinute, pr_int.endMinute)
                            }
                        })
                        hours_limits.push(intervToAdd.endMinute)


                        if(hours_limits.length) {
                            //on a un tableau d'heure de changement de secteur
                            //dans hours_limits si le secteur est différent de celui qu'on ajoute
                            //si notre intervention d'ajout va entourer plusieurs protected il
                            //faut vérifier que entre ces protected on respecte le temps minimal
                            //du secteur qu'on ajoute
                            // hours_limits = [8,10,12,15,16,18] si protected de 10 à 12 et 15 à 16 et qu'on veut ajouter
                            //de 8 à 18 notre nouvelle intervention
                            //on va donc vérifier entre 8 et 10, 12 et 15 et 16 et 18 que le temps minimum soit bon
                            var total_time_in_sector = 0;
                            for (var k = 1; k < hours_limits.length; k += 2) {
                                var time_in_new_sector = hours_limits[k] - hours_limits[k - 1]
                                total_time_in_sector += time_in_new_sector
                                if(time_in_new_sector < minTime) {
                                    returnFalse(917);
                                    return false;
                                }
                            }

                            if(total_time_in_sector == 0){
                                //il y a tellement de protected que notre ajout ne va rien ajouter'
                                returnFalse(918);
                                return false;
                            }
                        }
                    }
                    if(respect_min_time_with_protected() === false) return false;


                    //respecter le temps minimum
                    if(intervToAdd.endMinute - intervToAdd.startMinute < minTime) {
                        // si après avoir inclus ceux du même secteur il y a toujours par la place alors
                        // agrandir l'intervention par la fin
                        // on aurait très bien pu étendre par le début aussi
                        if(intervToAdd.endMinute - intervToAdd.startMinute < minTime) {
                            if(rules.v2_break_if_min_time_not_ok) {
                                returnFalse(904);
                                return false;
                            }
                            intervToAdd.endMinute = intervToAdd.startMinute + minTime
                            var hours_has_changed = true
                        }
                        // respecter le max_end_day
                        if(intervToAdd.endMinute > intervToAdd.listInfo.max_end_day) {
                            intervToAdd.endMinute = intervToAdd.listInfo.max_end_day
                            intervToAdd.startMinute = intervToAdd.endMinute - minTime
                            var hours_has_changed = true
                        }
                    }




                    //voir les commentaire de break_if_right_mintime_not_respected
                    for (var prev_inter = interv2; prev_inter && prev_inter.startMinute >= intervToAdd.startMinute;) {
                        prev_inter = prev_inter.prev
                    }
                    if(rules.break_if_left_mintime_not_respected) {
                        if(prev_inter && prev_inter.get_real_sector() && prev_inter.sector != intervToAdd.sector) {
                            var prev_info = prev_inter.getStartAndEndSameSector()
                            var time_before = intervToAdd.startMinute - prev_info.startMinute
                            //si suite à notre changement le temps min ne sera pas respecté alors ne rien faire
                            //sauf si le secteur précédent était déjà mauvais alors on s'en fiche
                            var was_already_not_good = prev_info.time < prev_inter.sector.minTimeSector
                            if(was_already_not_good) {
                                // console.error('le mintime du précédent est mauvais')
                            } else if(time_before > 0 && time_before < prev_inter.sector.minTimeSector) {
                                returnFalse(915);
                                return false;
                            }
                        }
                    }
                    // on ne veut pas de dégat collatérales en dehors de ce que l'on a demandé comme horaire donc juste bloquer
                    // on rappel que interv2 est l'intervention qui a ses minute de fin juste après (ou égale) celle que l'on veut insérer
                    for (var next_inter = interv2; next_inter && next_inter.endMinute <= intervToAdd.endMinute;) {
                        next_inter = next_inter.next
                    }
                    //next inter sera juste après notre intervention

                    //si notre intervention protected est autour d'une pause vérifier qu'il n'y a
                    //pas une autre autour de la pause dans le cas ou la pause doit être réduit après
                    //car la pause peut faire 40 minutes mais être d'abord mise à une heure
                    if(intervToAdd.protected_reason == 'strictNeed'){
                        if(next_inter && next_inter.startMinute == intervToAdd.endMinute && next_inter.is_pause() && next_inter.next && next_inter.next.protected_reason == 'strictNeed'){
                            var hc = next_inter.get_hours_pause_config()
                            if(hc.time != hc.real_time){
                                returnFalse(91151)
                                return false
                            }
                        }
                    }
                    if(intervToAdd.protected_reason == 'strictNeed'){
                        if(prev_inter && prev_inter.endMinute == intervToAdd.startMinute && prev_inter.is_pause() && prev_inter.prev && prev_inter.prev.protected_reason == 'strictNeed'){
                            var hc = prev_inter.get_hours_pause_config()
                            if(hc.time != hc.real_time){
                                returnFalse(91152)
                                return false
                            }
                        }
                    }

                    if(rules.break_if_right_mintime_not_respected) {
                        if(next_inter && next_inter.get_real_sector() && next_inter.sector != intervToAdd.sector) {
                            var nextInfo = next_inter.getStartAndEndSameSector()
                            var time_after = nextInfo.endMinute - intervToAdd.endMinute
                            //si suite à notre changement le temps min ne sera pas respecté alors ne rien faire
                            //sauf si le secteur précédent était déjà mauvais alors on s'en fiche
                            var was_already_not_good = nextInfo.time < next_inter.sector.minTimeSector
                            if(was_already_not_good) {
                                // console.error('le mintime du suivant est mauvais ' + next_inter.addIntervDebug)
                            } else if(time_after < next_inter.sector.minTimeSector) {
                                returnFalse(914);
                                return false;
                            }
                        }
                    }
                    // ne pas mettre deux exact need à la suite
                    // pour avoir plus d'amplitude pour bouger les pauses
                    if(!rules.allow_successive_strict_need){
                        if(intervToAdd.protected_reason == 'strictNeed'
                            && (
                                prev_inter && prev_inter.protected_reason == 'strictNeed'
                                || next_inter && next_inter.protected_reason == 'strictNeed'
                                )
                            ){
                            returnFalse(920)
                        return false
                    }
                }

                if(intervToAdd.sector && intervToAdd.sector.maxTimeSector && intervToAdd.time > intervToAdd.sector.maxTimeSector) {
                        //le temps max n'est pas respecté
                        console.max = 'yes'
                        returnFalse(916);
                        return false
                    }

                    //vérifier que la pause reste possible à diminuer d'un quart d'heure si c'est une pause d'une heure qui doit en fait
                    //faire 40 minutes au final mais qu'on met au début à une heure complête
                    var error_pause = false
                    var will_break_pause = function() {
                        var s1 = intervToAdd.sector
                        if(s1 && s1.minTimeSector == s1.maxTimeSector) {
                            var wbp = function(endMinute, startMinute, prev) {
                                //vérifions si la pause est avant notre inter
                                var just_before = interv2
                                while (just_before[endMinute] > interv2[startMinute]) {
                                    if(!(just_before = just_before.prev)) {
                                        break
                                    };
                                }
                                if(just_before && just_before.is_pause_that_need_to_be_reduced()) {
                                    //la pause fait une heure et il faut de la marge pour
                                    //la passer à 40 minute donc vérifier qu'on l'entoure
                                    //pas par des interventions stricte sachant que celle que l'on ajoute est une intervention stricte
                                    var nextORprev = just_before[prev]
                                    if(nextORprev && nextORprev.getMaxTime() && nextORprev.getMinTime() == nextORprev.getMaxTime()) {
                                        return true
                                    }
                                }
                            }
                            if(wbp('endMinute', 'startMinute', 'prev')) {
                                return true
                            }
                            return wbp('startMinute', 'endMinute', 'next')
                        }
                    }
                    if(!rules.dont_care_about_breaking_pause && will_break_pause()) {
                        returnFalse(919)
                        return false
                    }


                } //si !megaForcedInsert

                if(rules.megaForcedInsert && rules.restore_protected_at_end){
                    controls_enabled(()=>{
                        if(to_unprotect.length){
                            my_throw(`On ne devrait remplir ce tableau que ici quand on est en mode megaForcedInsert et restore_protected_at_end`)
                        }
                    })
                    for (let prop of ['prev', 'next']) {
                        let interv3;
                        if(prop == 'next'){
                            interv3 = interv2.next
                            if(!interv3) break
                        }else{
                            interv3 = interv2
                        }
                        //on parcourt les interventions qui vont croiser celle que l'on veut ajouter @cross
                        let cont = () => interv3.endMinute > intervToAdd.startMinute && interv3.startMinute < intervToAdd.endMinute
                        while (cont(interv3)) {
                            if(interv3.protected) {
                                to_unprotect.push(interv3)
                            }
                            if(!(interv3 = interv3[prop])) break; //@affectation
                        }
                    }
                }



                if(main && !main.is_test) {
                    if(console.iDebug == 14945) {
                        //////debugger
                    }
                    var opds = intervToAdd.getOpenAndCloseMinute(main)
                    if(!rules.megaForcedInsert) {
                        //// if(console.iDebug == 14946) ////debugger;
                        //refuser l'ajout si le magasin est fermé
                        var st = Math.max(opds.minstartMinute, intervToAdd.startMinute),
                        end = Math.min(opds.maxendMinute, intervToAdd.endMinute);
                        if(st >= end) {
                            returnFalse(909)
                            intervToAdd.startMinute_909 = st
                            intervToAdd.endMinute_909 = end
                            return false;
                        }
                    } else {
                        opds.minstartMinute = intervToAdd.startMinute
                        opds.maxendMinute = intervToAdd.endMinute
                    }
                }

                if(hours_has_changed && origine.startMinute != intervToAdd.startMinute || origine.endMinute != intervToAdd.endMinute) {
                    if(nbRecursivity == 4) {
                        return this.modifyIntervToAddMinute(intervToAdd, main, nbRecursivity + 1, true)
                    }
                    if(nbRecursivity > 3) {
                        console.error("impossible de statuer sur les horaires de cette intervention donc ne pas l'ajouter ")
                        console.error(intervToAdd.id)
                        returnFalse(910)
                        return false
                    }
                    //comme on a pas la même chose que ce que l'on avait demandé on est obligé de tous revérifier
                    // var to_ret =  this.modifyIntervToAddMinute(intervToAdd,main,nbRecursivity+1,protectedIntervArr,true)

                    var to_ret = this.modifyIntervToAddMinute(intervToAdd, main, nbRecursivity + 1, true)

                    if(!is_recursivity) {
                        Debug.perfEnd('modifyIntervToAddMinute')
                    }
                    return to_ret
                }
                if(!is_recursivity) {
                    Debug.perfEnd('modifyIntervToAddMinute')
                }
                //
                //maintenant que l'on est sûr que l'insertion va réussir on peut sauvegarder les protected et les déprotéger
                //pour pouvoir les écraser avec une intervention plus grande, mais on oubliera pas de les remettre après car
                //elles sont protégées
                var protectedIntervArr = []
                to_unprotect.forEach((inter_prot) => {
                    inter_prot.unprotectIfProtected(protectedIntervArr)
                })
                return {
                    iRacine: iRacine,
                    rules: rules,
                    interv2: interv2,
                    // minTime: minTime,
                    minTimeAllSector: minTimeAllSector,//c'est le minimum des temps minimum de tout secteur,
                    minTimeAllSectorAfterBefore,
                    intervToUpdate: intervToUpdate,
                    protectedIntervArr: protectedIntervArr,
                    maxendMinute: opds ? opds.maxendMinute : lInfo.max_end_day,
                    minstartMinute: opds ? opds.minstartMinute : lInfo.min_start_day
                }
            }
            /*return true  si on peut ajouter et remplit le champ hasBeenInsert
             * ajoute une intervention pour une personne
             * pousse les intervention déjà présente si il n'y a pas la place supprime les interventions adjacentes
             * cette fonction accèpte des règles dans l'attribut rules de intervention:
             *   dontMoveExistingInterventionOrPause: ne pas déplacer des intervention déjà existante qui possède un secteur (par défaut une intervention existe sans secteur si la zone horaire est affectable d'après son emploi du temps)
             *   megaForcedInsert: cette intervention doit absolument être ajouté car elle était déjà présente avant la planification automatique,
             *    même si elle ne respecte pas les temps minimum
             *
             * Une interventon avec .protected = true est une intervention qui était là à la base et donc elle ne peut pas être supprimer
             *       en fait elle peut être supprimer par une intervention de même secteur mais on va ensuite réinsérer la protected à sa place
             *
             * TODO : quand on insère une intervention et que l'on gère le temps minimal,
             *        on étend vers le futur mais il faudrait étendre vers le passé si on est suivit par une protected
             *
             * callerArgs est le arguments (tableau des paramètres) de celui qui a appelé en cas de récursivité
             *
             * main: l'instance de planification automatique (Main) utilisé
             *
             * person@addInter
             */
             addInterv(intervToAdd, main, callerArgs) {
                Debug.dont_check_start_end_logic = Debug.addInter_running = true
                var d, res, interReplacedCloned, delta, interv2, racine,
                badPrevendMinute, toReinsert, delta2, args, wf = this
                //@addIntervDebug
                if(console.iDebug == 256238 ) {
                    // debugger;
                };
                (function() {
                    controls_enabled(()=>{
                        if(intervToAdd.protected && intervToAdd.get_real_sector() && !intervToAdd.fakeProtected && ! ['manager_forced'].includes(intervToAdd.protected_reason) ){
                            my_throw2(`Comment se fait il que l'on ajoute des secteur en protected si ils ne viennent pas de la base `)
                        }

                        if(console.iDebug > 127635 && intervToAdd.sector && intervToAdd.sector.pause_instance){
                            if(intervToAdd.startMinute < 930 && intervToAdd.endMinute > 810 ){
                                d_f();
                            }
                        }
                        if(intervToAdd.hasBeenRemoved) {
                            my_throw( 'cette intervention a déjà servie et a mal été néttoyée pour être réinsérer')
                        }
                        if(intervToAdd.startMinute != intervToAdd.startMinute | 0) {
                            my_throw( 'les heures doivent être entière')
                        }
                        if(intervToAdd.startMinute >= intervToAdd.endMinute) {
                            my_throw( 'les heures doivent être ordonnée start < end_minute')
                        }
                        if(!(intervToAdd instanceof Intervention)) {
                            my_throw( 'not_an_intervention')
                        }
                        if(!intervToAdd.is_pause() && !intervToAdd.sector && intervToAdd.protected && intervToAdd.protected_reason != 'dont_planif') {
                            my_throw( 'seule les pauses et les affectations peuvent être protégées')
                        }
                        if(intervToAdd.hasBeenInsert) {
                            my_throw( "tentative d'ajout d'une intervention déjà ajouté sans l'avoir précédemment supprimé avec remove()")
                        }
                        // if(!intervToAdd.true_protected_information){
                            var startPos = intervToAdd.startMinute / conf.minTime
                            var endPos = intervToAdd.endMinute / conf.minTime
                            if(startPos != ~~startPos) {
                                my_throw( 'not an integer')
                            }
                            if(endPos != ~~endPos) {
                                my_throw( 'not an integer')
                            }
                        // }
                    })
                    if(intervToAdd.person && intervToAdd.person != wf) {
                        intervToAdd.reset_person()
                        // my_throw( 'Cette intervention était affecté à une autre personne ')
                        //     + ', il faut alors la sérialiser et recréer une intervention avec une autre personne'
                        //     + 'où utiliser reset_person'
                    }


                    main = main || wf.main //wf étant la personne
                    //peut être inutile @TODO remove
                    if(intervToAdd.listInfo) {
                        intervToAdd.listInfo.person = wf
                    }

                    var d = intervToAdd; //pour débugguer plus vite dans la console de chrome
                    Debug.perfStart('addInterv')
                    Debug.perfStart('addIntervP1')
                    // main && main.logPersonSectorBug()
                    // cet id va aller dans le commentaire de l'intervention pour un débuggage simplifié

                    if(1155116 == d.addIntervDebug) {
                        //// debugger
                        console.log('toto')
                    }
                    //main && main.logPersonSectorBug(true,3286)
                    intervToAdd.rules = intervToAdd.rules || {}
                    args = arguments;

                    //voir aussi console.min_time_pb
                    console.iiDebugInter = idnext('iiDebugInter')
                    if(console.iiDebugInter == 11555) {
                        ////// ////debugger;
                    }

                    if(wf.wfId == 10417 && d.sector && d.sector.id == 2521 && d.startMinute == 1050 && d.date == '2018-03-27') {
                        //// ////debugger;
                    }
                })();

                // on met une récursivité à 1 car une fois modifié les heures il faut revérifier qu'elle sont bonnes
                // cette fonction va nous dire si l'ajout de l'intervention est autorisé ou non
                // et avec quel horaire
                res = wf.modifyIntervToAddMinute(intervToAdd, main, 1)


                // var debug = function(){
                //     if(conf.debug && conf.debug.startMinute == 12.5){
                //         console.error('error')
                //         return true
                //     }
                // }
                // if(debug()){
                //     my_throw( 'err')
                // }
                controls_enabled(()=>{
                    if(!intervToAdd.person && main) {
                        my_throw( "error no person")
                    }
                })

                if(res == false) {
                    controls_enabled(()=>{
                        if(intervToAdd.rules && intervToAdd.rules.megaForcedInsert && !intervToAdd.rules.canFail) {
                            my_throw( 'un mégaforced insert devrait toujours réussir car il existait dans un état précédemment valide')
                        }
                    })
                    Debug.perfEnd('addIntervP1')
                    Debug.perfEnd('addInterv')
                    Debug.dont_check_start_end_logic = Debug.addInter_running = false
                    return {
                        has_been_insert: false
                    }
                }
                // à cause des protected il se peut que notre intervention soit plus
                // petite que ce que l'on voulait insérer car coupé en deux une partie non protégée et un
                // autre protégée.
                // le proxy va permettre d'obtenir les vrais heures au sein du secteur
                var ret = new InterProxy({
                    has_been_insert: true,
                    start_minute: intervToAdd.startMinute,
                    end_minute: intervToAdd.endMinute,
                    inter: intervToAdd,
                    sector: intervToAdd.sector
                })

                //l'ajout ne va rien changer donc retourner
                if(res.already_like_that){
                    Debug.perfEnd('addIntervP1')
                    Debug.perfEnd('addInterv')
                    Debug.inc_error_code('already_like_that')
                    Debug.dont_check_start_end_logic = Debug.addInter_running = false
                    return ret
                }

                const add_to_update = (inter)=>{
                    controls_enabled(()=>{
                        if(inter.endMinute > 24 * H || inter.startMinute > 24* H || inter.startMinute > inter.endMinute){
                            my_throw('not logic')
                        }
                    })
                    intervToUpdate.push(inter)
                }
                var iRacine,racine,lInfo,rules,nextInt,intervToUpdate,protectedIntervArr,
                minstartMinute,maxendMinute,minTimeAllSector,minTimeAllSectorAfterBefore;
                ;(function() {
                    if(intervToAdd.addIntervDebug == 328) {
                        // Debug.logDayWf(res.interv2)
                        // Debug.logDayWf('10416','2018-03-26')
                        // Debug.logDayWf(res.interv2.prev)
                        // Debug.logDayWf(res.interv2)
                        console.checkInterv2 = function(debug) {
                            // console.log(debug)
                            // Debug.logDayWf(res.interv2)
                        }
                        console.debug_interv2 = true;
                    } else {
                        console.debug_interv2 = false;
                    }
                    console.debug_interv2 && console.checkInterv2()
                    intervToAdd.before_add && intervToAdd.before_add();
                    idnext('no_name13');

                    iRacine = res.iRacine
                    racine = wf.intervsRacines[iRacine]
                    lInfo = racine.listInfo
                    rules = res.rules
                    nextInt = res.interv2
                    intervToUpdate = res.intervToUpdate
                    protectedIntervArr = res.protectedIntervArr
                    minstartMinute = res.minstartMinute //date d'ouverture du secteur
                    maxendMinute = res.maxendMinute // date de fermeture du secteur
                    minTimeAllSector = res.minTimeAllSector // date de fermeture du secteur
                    minTimeAllSectorAfterBefore = res.minTimeAllSectorAfterBefore // date de fermeture du secteur
                })();


                (function() {
                    // à partir d'ici on place l'intervention au milieu de intervention existante et on ajuste les horaires
                    // et supprime les interventions
                    // qui coise celle que l'on ajoute
                    // console.log(main && main.logPersonSectorBug())
                    // if(main && intervToAdd.startMinute >= 12 && wf.wfId == 420 && lInfo.date == '2017-09-05'){//on peut éditerl le code js en direct dans chrome donc jsute changer par l'id souhaité
                    // }
                    intervToAdd.setBetween(nextInt.prev, nextInt)
                    if(intervToAdd.addIntervDebug == 40305) {
                        ////////// ////debugger
                        // Debug.logDayWf('10416','2018-03-26')
                        // Debug.logDayWf(intervToAdd)
                        // Debug.logDayWf(nextInt.prev)
                        // Debug.logDayWf(nextInt)
                    }
                    //les précédentes ne peut commencer après notre intervention donc la supprimer
                    while (intervToAdd.prev && intervToAdd.prev.startMinute > intervToAdd.startMinute) {
                        intervToAdd.prev.remove({
                            i_will_preserve_the_integrity_of_last_inter_of_listInfo: true
                        })
                    }
                    // while(intervToAdd.next && intervToAdd.next.endMinute < intervToAdd.startMinute){
                    //  intervToAdd.prev.remove()
                    // }
                    // if(intervToAdd.rules.v2_do_not_respect_min_time_after_before){
                    //     minTimeAllSectorAfterBefore = 0
                    // }

                    if(intervToAdd.prev == null || intervToAdd.startMinute == lInfo.min_start_day) {
                        // notre intervention va remplacer l'intervention racine
                        // la première intervention racine doit contenir listInfo qui donne
                        // des informations sur toutes la liste
                        intervToAdd.listInfo = lInfo

                        // c'est la première intervention donc supprimer la précédente
                        // intervToAdd.removeAllPrev(main);
                        intervToAdd.prev && intervToAdd.prev.remove({
                            //on va bien préserver la dernière et première intervention
                            //dans listInfo car il seront mis à jour dans update_intervention
                            //de même pour les équivalents avec secteur
                            i_will_preserve_the_integrity_of_last_inter_of_listInfo: true
                        })

                        var delta = intervToAdd.startMinute - nextInt.startMinute // avec nextInt intervToAdd.next
                        if(delta) {
                            // si la suivante a une date de début < notre interv alors la mettre aussi avant en la clonant
                            // sauf si ça laisserait une intervention avant trop petite (delta < minTimeAllSector)
                            // cependant on le fait quand même si notre secteur n'est pas ouvert à l'heure de début de la précédente
                            // if(delta >= minTimeAllSectorAfterBefore || lInfo.min_start_day < minstartMinute /*|| intervToAdd.sector == intervToAdd.next.sector*/ ) {
                                if(delta > 0) {
                                    add_to_update(nextInt.setCloneBetween(intervToAdd.prev, intervToAdd))
                                }
                            //  else {
                            //     intervToAdd.startMinute = minstartMinute
                            // }
                        }
                    } else {
                        // notre intervention n'est pas la première
                        // si le delta entre me début de notre interv et l fin de la précédente
                        // est plus petit que la taille d'une affectation minimum, ne pas réinsérer le clone

                        // nextInt pourrait commencer avant intervToAdd et finir après intervToAdd
                        // du coup on garde l'nextInt d'origine après notre nouvelle intervention
                        // et on met un clone avant notre nouvelle intervention
                        delta = intervToAdd.startMinute - intervToAdd.prev.endMinute
                        if(delta && (delta >= minTimeAllSectorAfterBefore /*||  intervToAdd.sector == intervToAdd.next.sector*/ )) {
                            add_to_update(nextInt.setCloneBetween(intervToAdd.prev, intervToAdd))
                        }
                    }
                })();



                var checkNextOrPrevInterMinute = (dir, startOrEnd, EndOrStart) => {
                    if(intervToAdd[dir]) {
                        var badMinute = intervToAdd[dir][EndOrStart] != intervToAdd[startOrEnd]
                        if(badMinute) {
                            var haveNewInter = false;
                            Debug.perfStart('checkNextOrPrevInterMinute@unprotectIfProtected')
                            intervToAdd[dir].unprotectIfProtected(protectedIntervArr)
                            Debug.perfEnd('checkNextOrPrevInterMinute@unprotectIfProtected')
                            if(main && !main.is_test) {
                                if(intervToAdd[dir].sector) {
                                    Debug.perfStart('openCloseMinuteP6')
                                    // var opds = intervToAdd[dir].getOpenAndCloseMinute(main)
                                    Debug.perfEnd('openCloseMinuteP6')
                                    if(dir == 'next') {
                                        intervToAdd.next.startMinute = intervToAdd.endMinute
                                        // intervToAdd.next.startMinute = Math.max(intervToAdd.endMinute, opds.minstartMinute)
                                        //il semblerait qu'a cause des heure de fermeture des magasin on ne puisse pas étendre notre intervention
                                        //on va donc juste en mettre un vide au milieu
                                        if(intervToAdd.next.startMinute != intervToAdd.endMinute) {
                                            var newInter = new Intervention({
                                                startMinute: intervToAdd.endMinute,
                                                endMinute: intervToAdd.next.startMinute,
                                                date: intervToAdd.listInfo.date,
                                                person: intervToAdd.person,
                                                dontAffectNow: true
                                            })
                                            newInter.setBetween(intervToAdd, intervToAdd.next)
                                        }
                                    } else if(dir == 'prev') {
                                        intervToAdd.prev.endMinute = intervToAdd.startMinute
                                        // intervToAdd.prev.endMinute = Math.min(intervToAdd.startMinute, opds.maxendMinute)
                                        if(intervToAdd.prev.endMinute != intervToAdd.startMinute) {
                                            var newInter = new Intervention({
                                                startMinute: intervToAdd.startMinute,
                                                endMinute: intervToAdd.prev.endMinute,
                                                date: intervToAdd.listInfo.date,
                                                person: intervToAdd.person,
                                                dontAffectNow: true
                                            })
                                            newInter.setBetween(intervToAdd.prev, intervToAdd)
                                        }
                                    }
                                }
                            }

                            if(!newInter) {
                                intervToAdd[dir][EndOrStart] = intervToAdd[startOrEnd]
                            } else {
                                newInter.listInfo = intervToAdd.listInfo
                            }
                        }
                        // si l'intervention qui précède est trop petite alors
                        // la supprimer et avancer le début  de l'intervention à ajouter
                        // on ne peut pas l'avancer si l'entreprise était fermée à ce moment là (minstartMinute)
                        // @TODO ne pas calculer delta si v2_do_not_respect_min_time_after_before
                        delta = intervToAdd[dir].getTimeLinkedIntervSameSector()
                        if(!intervToAdd[dir].get_real_sector() || rules.v2_do_not_respect_min_time_after_before) {
                            var minTimeSector = 0
                        } else {
                            var minTimeSector = intervToAdd[dir].getMinTime();
                        }
                        var isDirectLinkedIntervEmpty = intervToAdd[dir].startMinute >= intervToAdd[dir].endMinute
                        var prevDontBeginBeforeIntervToAddCanBegin = (dir == 'next' || intervToAdd.prev.startMinute >= minstartMinute)
                        var linkedInterHasToBeDeleted = (!delta || isDirectLinkedIntervEmpty)
                        if(rules.allow_to_change_hours && delta < minTimeSector){
                            linkedInterHasToBeDeleted = true
                        }
                        if(linkedInterHasToBeDeleted && prevDontBeginBeforeIntervToAddCanBegin) {
                            if(intervToAdd.protected) {
                                if(intervToAdd[dir].endMinute <= intervToAdd[dir].startMinute) {
                                    intervToAdd[dir].remove({
                                        //on va bien préserver la dernière et première intervention
                                        //dans listInfo car il seront mis à jour dans update_intervention
                                        //de même pour les équivalents avec secteur
                                        i_will_preserve_the_integrity_of_last_inter_of_listInfo: true
                                    })
                                } else {
                                    if(badMinute) {
                                        toReinsert = intervToAdd[dir].prepareToBeReinserted(main, {
                                            //on va bien préserver la dernière et première intervention
                                            //dans listInfo car il seront mis à jour dans update_intervention
                                            //de même pour les équivalents avec secteur
                                            i_will_preserve_the_integrity_of_last_inter_of_listInfo: true
                                        });
                                        intervToUpdate.push(toReinsert)
                                    }
                                }
                            } else {
                                intervToAdd[startOrEnd] = intervToAdd[dir][startOrEnd]
                                intervToAdd[dir].remove({
                                    //on va bien préserver la dernière et première intervention
                                    //dans listInfo car il seront mis à jour dans update_intervention
                                    //de même pour les équivalents avec secteur
                                    i_will_preserve_the_integrity_of_last_inter_of_listInfo: true
                                })
                            }
                        } else {
                            // si l'heure de fin de la précédente ou suivante a été changé alors il faut la mettre à jour
                            if(badMinute) {
                                toReinsert = intervToAdd[dir].prepareToBeReinserted(main, {
                                    //on va bien préserver la dernière et première intervention
                                    //dans listInfo car il seront mis à jour dans update_intervention
                                    //de même pour les équivalents avec secteur
                                    i_will_preserve_the_integrity_of_last_inter_of_listInfo: true
                                });
                                intervToUpdate.push(toReinsert)
                                // if(toReinsert.is_pause()){
                                //     my_throw( 'la pause ne peut pas être étendu il manque le paramètre ')
                                //     + 'break_if_left_mintime_not_respected ou v2_do_not_respect_min_time_after_before dans rules'
                                // }
                            }
                        }
                    }
                }
                idnext('no_name14');
                Debug.perfEnd('addIntervP1')
                Debug.perfStart('addIntervP2')
                // if(protectedIntervArr[0] && protectedIntervArr[0].addIntervDebug == 125){
                ////////////// ////debugger
                // }
                checkNextOrPrevInterMinute('prev', 'startMinute', 'endMinute')
                // if(protectedIntervArr[0] && protectedIntervArr[0].addIntervDebug == 125){
                ////////////// ////debugger
                // }
                checkNextOrPrevInterMinute('next', 'endMinute', 'startMinute')
                controls_enabled(()=>{
                    let check_order = (inter)=>{
                        if(inter && (inter.startMinute >= inter.endMinute || inter.time < conf.minTime) ){
                            my_throw2(100,`Les heures ne sont pas logiques`, 'iiDebugInter')
                        }
                    }
                    check_order(intervToAdd.next)
                    check_order(intervToAdd.prev)
                    check_order(intervToAdd)
                })
                // if(protectedIntervArr[0] && protectedIntervArr[0].addIntervDebug == 125){
                ////////////// ////debugger
                // }
                Debug.perfEnd('addIntervP2')


                Debug.perfStart('addIntervP3')
                //TODO: à déplacer plus tard pour le mettre seulement quand il faut
                // for (var prev = intervToAdd; prev.prev; prev = prev.prev) {
                //     '';
                // }
                // wf.intervsRacines[iRacine] = prev;
                // les interventions qui ont changé d'heure de début ou de fin doivent être mise à jour
                // si elles ont un secteur

                Debug.perfEnd('addIntervP3')

                wf.update_intervention(intervToUpdate,iRacine)
                Debug.perfStart('addIntervP5')

                //on remet l'intervention protégé à sa place
                if(!rules.megaForcedInsert || rules.restore_protected_at_end && rules.megaForcedInsert) {
                    protectedIntervArr.forEach(function(proctInte) {
                        if(proctInte.id == intervToAdd.id) return; //anti récursivité infinie
                        if(
                            proctInte.startMinute >= intervToAdd.startMinute &&
                            proctInte.endMinute <= intervToAdd.endMinute &&
                            intervToAdd.protected &&
                            proctInte.date == intervToAdd.date &&
                            proctInte.sector == intervToAdd.sector
                            ){
                            return; //anti récursivité infinie
                    }

                    proctInte.rules = {
                        megaForcedInsert: true
                    }
                    var args2 = Array.prototype.slice.call(args)
                    args2[0] = proctInte;
                        args2[2] = args; //pour le débug c'est bien de savoir les paramètr de l'appelant
                        Debug.perfEnd('addIntervP5')
                        Debug.perfEnd('addInterv')
                        lInfo.person.addInterv.apply(lInfo.person, args2)
                        Debug.perfStart('addInterv')
                        Debug.perfStart('addIntervP5')
                    })
                }

                controls_enabled(()=>{
                    if(typeof intervToAdd.startMinute == 'undefined') {
                        my_throw( 'error')
                    }
                })
                intervToAdd.hasBeenRemoved = false
                delete intervToAdd.date // il est stocké dans le liste info maintenant
                Debug.interventions[console.addIntervDebug] = intervToAdd
                Debug.perfEnd('addIntervP5')
                Debug.perfEnd('addInterv', {
                    intervToAdd
                })
                var debug_wf = 10575,
                debug_date = '2018-03-26'
                if(intervToAdd.person && intervToAdd.person.wfId == debug_wf && intervToAdd.date == debug_date) {
                    //debugger
                    // Debug.logDayWf(debug_wf,debug_date)
                }
                var lInfo = intervToAdd.listInfo
                lInfo.person = wf
                Debug.inc_error_code('success')
                if(main && main.blockArr){
                    main.logPersonSectorBug(false,wf,lInfo.date)
                }

                // if(!intervToAdd.listInfo.person){
                //     debugger
                // }


                Debug.dont_check_start_end_logic = Debug.addInter_running = false
                return ret
            }
    } //endClass person

    Person.is_class_person = true
    return Person
}
