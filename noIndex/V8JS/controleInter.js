//mode rapide
	// controle sur la semaine
	// temps entre jours consécutif
	// temps max
/*


o{
	data: {
		nb_minutes_rrp_linked: le nombre de rrp en minute lié à ce jour
		nb_minutes_worked_this_week :
			{
				nb_days: le nombre de jour dans la semaine travaillé sans compter aujourd'hui
				tot_worked:  temps travaillé sans compter ce jour cette semaine
				dateday_will_be_counted: est ce que le jour compte dans les heures de la semaine certain dimanche au début ne compte pas
			}
		first_hour_tomorrow
		last_hour_yesterday
		amplitudesArr 
	}
	rules:{
		max_minutes_consecutive : le nombre d'heure consécutive au maximum
		min_minutes_consecutive  le nombre d'heure consécutive au minimum
		gap_interval:  yesterday and tomorrow le temps entre les heures d'aujourd'hui et de hier et demain
		nb_minutes_before_rest: le temps de travail dans la journée après lequel une pause est obligatoire
		min_minutes_rest: le temps min 	   d'une seule pause
		max_minutes_rest: le temps maximum d'une seule pause
		min_minutes_by_day: le temps min de travail par jour
		max_minutes_by_day: le temps max de travail par jour
		min_minutes_by_week: le temps min de travail par semaine
		max_minutes_by_week: le temps max de travail par semaine
	}
}
*/
// o.data.amplitudesArr = tableau d'amplitude
// o.nbHoursWorkedThisWeek sans prendre en compte la journée actuelle
// o.contract{min_seconds_consecutive,min_seconds_rest,max_seconds_rest,
// 	max_seconds_by_day,min_seconds_by_day,min_seconds_by_week,max_seconds_by_week,
// 	custom_validation_function}

(function(){
	var nb_days_warn = 15;
	try{
		var console = window.console
		var v8js = !epit.isNotV8Js;
	}catch(e){
		var v8js = true;
	}
	var error = function(code,params,blockValidation){
		m.infosCalculated.errors[code] = {
			errorCode: code,
			otherParams: params.otherParams,
			blockValidation: blockValidation
		}
	}

	var returnF = function(obj){
		if(v8js){
			return JSON.stringify(obj)
		}else{
			return obj
		}
	}

	var m = {
		days_diff: function(str_date_next, str_date_prev){
		  // Set to noon - avoid DST errors
		  var diff = new Date(str_date_next).setHours(12) - new Date(str_date_prev).setHours(12);
		  return Math.round(diff/8.64e7);//24* 60 * 60 * 1000
		},
		validatorIntervention: function(o){
			return returnF(m.validatorInterventionGeneral(o));
		},
		validatorInterventionCDI35: function(o){
			var  max5 = 42 * 60 ,max6 = 46 * 60
				,min_minutes_by_week = o.rules.min_minutes_by_week
			;
			delete o.rules.min_minutes_by_week;delete o.rules.max_minutes_by_week;
			var ret = m.validatorInterventionGeneral(o)
				,my = m.infosCalculated
				,epit = my.epit
				// le nombre de jours travaillés cette semaine en fonction du fait qu'il y ait des 
				// intervention aujourd'hui ou non
				,nbDays = o.data.nb_minutes_worked_this_week.nb_days + (ret.nbMinutesWorked > 0)
				,minutesWorkedThisWeek = ret.minutesWorkedThisWeek
				,errorWeek = (nbDays <= 5 && minutesWorkedThisWeek > max5 || minutesWorkedThisWeek > max6)
			 				|| minutesWorkedThisWeek < min_minutes_by_week
			;
			if(errorWeek){
				error('form.control.minutes_by_week56days', {
					otherParams: {
						isHour: epit.minuteToString(minutesWorkedThisWeek),
						isNbDays: nbDays,
						shouldBeMin: epit.minuteToString(min_minutes_by_week),
						shouldBeMax5: epit.minuteToString(max5),
						shouldBeMax6: epit.minuteToString(max6)
					}
				},false)
			}
			return returnF(ret)
		},
		validatorInterventionGeneral: function(o){
			try{
			if(v8js){
				var epit = o.epit;
			}else{
				var epit = window.epit
			}
			var my = m.infosCalculated = {
				errors: {},
				minutesWorkedThisWeek: null,
				nbMinutesWorked: null,
				epit: epit
			};
			var tmpConsecutif = 0;
			var tmpPause = 0;
			var allPause = [];
			var allConsecutive = [];
			var firstAmpProv;
			var lastAmpProv;


			var exist = function(val){
				if(arguments.length == 2){
					try{
						return eval('arguments[0]'+arguments[1]);
					}catch(e){return false}
				}
				var undef;
				return val !== null && val !== undef
			}
			//on veut pas de trou
			// if(['CANCELED','WITHDRAW','UNAVAILABLE','REFUSED_MANAGER'].indexOf(o.data.state) < 0){
			// 	o.data.amplitudesArr.forEach(function(amp,i,arr){
			// 		if(i && amp.startMinute != arr[i-1].endMinute){
			// 			var fillGap = {
			// 				startMinute: arr[i-1].endMinute,
			// 				endMinute: amp.startMinute,
			// 			}
			// 			fillGap.nbMinutes = fillGap.endMinute - fillGap.startMinute;
			// 			amplitudes.push(fillGap);
			// 		}
			// 		amp.nbMinutes = amp.endMinute - amp.startMinute
			// 		amplitudes.push(amp);
			// 	})
			// }else{
			// 	o.data.amplitudesArr = [];
			// }

			
			var get_amps_planified = function(amplitudesArr,state){
				var amplitudes = [];
				if(['CANCELED','WITHDRAW','UNAVAILABLE','REFUSED_MANAGER'].indexOf(state) < 0){
					o.data.amplitudesArr.forEach(function(amp,i,arr){
						if(i && amp.startMinute != arr[i-1].endMinute){
							var fillGap = {
								startMinute: arr[i-1].endMinute,
								endMinute: amp.startMinute,
							}
							fillGap.nbMinutes = fillGap.endMinute - fillGap.startMinute;
							amplitudes.push(fillGap);
						}
						amp.nbMinutes = amp.endMinute - amp.startMinute
						amplitudes.push(amp);
					})
				}
				return amplitudes
			}
			var amplitudes = get_amps_planified(o.data.amplitudesArr,o.data.state)

			
			amplitudes.forEach(function(amp,i){
				if(amp.prov_id){
					lastAmpProv = amp
					if(!firstAmpProv){firstAmpProv = amp;}
					allConsecutive[allConsecutive.length - (tmpConsecutif ? 1 : 0)] = (tmpConsecutif += amp.nbMinutes)// on incrémente ou ajoute un nouveau
					tmpPause = 0;
				}else{//voici un trou de pause
					if(!allConsecutive.length) return;//continue;//le temps avant le travail ne compte pas comme pause
					tmpConsecutif = 0;
					allPause[allPause.length - (tmpPause ? 1 : 0)] = (tmpPause += amp.nbMinutes) // on incrémente ou ajoute une nouvelle pause
				}
			})

			if(tmpPause) --allPause.length //la dernière pause si elle n'est suivit de rien ne compte pas

			//temps travaillé au total dans la journée = somme des interventions consécutives
			var nbMinutesWorked = allConsecutive.length && allConsecutive.reduce(function(nbMinutesWorked,tmpConsecutif){
				if(exist(o.rules.min_minutes_consecutive) && tmpConsecutif < o.rules.min_minutes_consecutive){
					error('form.control.min_consecutive',{
						otherParams: {shouldBe: epit.minuteToString(o.rules.min_minutes_consecutive), is: epit.minuteToString(tmpConsecutif)}
					},false)
				}
				if(exist(o.rules.max_minutes_consecutive) && tmpConsecutif > o.rules.max_minutes_consecutive){
					error('form.control.consecutive',{
						otherParams: {shouldBe: epit.minuteToString(o.rules.max_minutes_consecutive), is: epit.minuteToString(tmpConsecutif)}
					},false)
				}
				return nbMinutesWorked + tmpConsecutif
			})
			if(o.old_data){
				var old_amplitudes = get_amps_planified(o.old_data.amplitudesArr,o.old_data.state)
				var old_nb_minutes_total = old_amplitudes[0] ? old_amplitudes.reduce(function(a, b) {
				    return {nbMinutes: a.nbMinutes + b.nbMinutes}
				}).nbMinutes : 0;

				if(o.data.is_volunteering && old_nb_minutes_total != nbMinutesWorked){
					var diff_in_day = m.days_diff(o.data.date_inter,o.data.today)
					if(diff_in_day < nb_days_warn){
						error('form.control.very_soon_inter', {
							otherParams: {diff_wanted: nb_days_warn, diff_in_day: diff_in_day}
						})
					}
				}
			}

			if(exist(o.rules.min_minutes_by_day) && nbMinutesWorked && nbMinutesWorked < o.rules.min_minutes_by_day){
				error('form.control.min_minutes_by_day', {
					otherParams: {shouldBe: epit.minuteToString(o.rules.min_minutes_by_day) , is : epit.minuteToString(nbMinutesWorked)}
				},false)
			}else if(exist(o.rules.max_minutes_by_day) && nbMinutesWorked > o.rules.max_minutes_by_day){
				error('form.control.max_minutes_by_day', {
					otherParams: {shouldBe: epit.minuteToString(o.rules.max_minutes_by_day) , is :epit.minuteToString(nbMinutesWorked) }
				},false)
			}

			if(firstAmpProv && exist(o.rules.gap_interval)){
				var firstMinuteAllow,lastMinuteAllow
				if(firstAmpProv && exist(o.data.last_hour_yesterday)){
					firstMinuteAllow =  epit.stringToMinute(o.data.last_hour_yesterday) + o.rules.gap_interval - 24 * 60 
				}
				firstMinuteAllow = Math.max(amplitudes[0].startMinute,  firstMinuteAllow |0)

				if(lastAmpProv && exist(o.data.first_hour_tomorrow)){
					lastMinuteAllow = epit.stringToMinute(o.data.first_hour_tomorrow) - o.rules.gap_interval + 24 * 60
				}
				lastMinuteAllow = Math.min(lastMinuteAllow || 24 *60,amplitudes[amplitudes.length-1].endMinute)
				
				if(firstMinuteAllow > firstAmpProv.startMinute || lastAmpProv < o.rules.gap_interval){
					error('form.control.gap_interval', {
						otherParams: {shouldBeMin: epit.minuteToString(firstMinuteAllow), shouldBeMax: epit.minuteToString(lastMinuteAllow)}
					},false)
				}
			}

			allPause.sort(function(a,b){return b - a});//décroissant
			var biggerPause = allPause[0];
			if(biggerPause){
				if(exist(o.rules.no_rest_under_in_day)
					 && nbMinutesWorked <= o.rules.no_rest_under_in_day){
					error('form.control.no_rest_under_in_day', {
						otherParams: {
							is: epit.minuteToString(biggerPause), 
							shouldBe: epit.minuteToString(o.rules.no_rest_under_in_day)
						},
					},false)
				}else if( exist(o.rules.min_minutes_rest) && biggerPause < o.rules.min_minutes_rest
					  ||  exist(o.rules.max_minutes_rest) && biggerPause > o.rules.max_minutes_rest
				){
					error('form.control.minutes_rest', {
						otherParams: {
							is: epit.minuteToString(biggerPause), 
							shouldBeMin: epit.minuteToString(o.rules.min_minutes_rest || 0),
							shouldBeMax: epit.minuteToString(o.rules.max_minutes_rest || 24 * 60)
						},
					},false)
				}
			}else{
				if(exist(o.rules.nb_minutes_before_rest)){
					if(nbMinutesWorked >= o.rules.nb_minutes_before_rest){
						error('form.control.rest_should_exist', {
							otherParams: {
								time: epit.minuteToString(o.rules.nb_minutes_before_rest)
							}
						},false)
					}
				}
			}

			var nb_minutes_used = o.data.rrp_linked_info && o.data.rrp_linked_info.nb_minutes_used
			if(exist(nb_minutes_used)){
				var nbMinutesWorkedRrp = o.data.state != 'VALID' ? 0 : nbMinutesWorked;
				if(nbMinutesWorkedRrp < nb_minutes_used){
					var otherParams = {is: epit.minuteToString(nbMinutesWorkedRrp), shouldBe: epit.minuteToString(nb_minutes_used) }
					if(v8js){//on a pas l'objet translator en v8js
						var code = 'form.control.nb_minutes_rrp_linked';
					}else{
						var str = "<ul>"
						o.data.rrp_linked_info.rrp.forEach(function(rrp){
							var date = rrp.start_date.split('-').reverse().join('/')
							str += '<li>'+date + ' : ' + epit.minuteToString(rrp.nb_minutes_used) + '</li>'
						})
						str += '</ul>'
						var code = Translator.trans('form.control.nb_minutes_rrp_linked',otherParams) + str
						var otherParams = {};
					}
					error(code,{otherParams: otherParams},true);
				}
			}

			if(exist(o,'.data.nb_minutes_worked_this_week.dateday_will_be_counted')){
				var minutesWorkedThisWeek =  nbMinutesWorked + o.data.nb_minutes_worked_this_week.tot_worked
				if(exist(o.rules.min_minutes_by_week)){
					if( minutesWorkedThisWeek < o.rules.min_minutes_by_week ||
						exist(o.rules.max_minutes_by_week) && minutesWorkedThisWeek > o.rules.max_minutes_by_week ){
						error('form.control.minutes_by_week', {
							otherParams: {
								is: epit.minuteToString(minutesWorkedThisWeek),
								shouldBeMin: epit.minuteToString(o.rules.min_minutes_by_week | 0),
								shouldBeMax: epit.minuteToString(o.rules.max_minutes_by_week || 50 * 60)
							},
						},false);
					}
				}
			}
		
			//dateday_will_be_counted contient pour les dimanche le nombre de dimanche précédent et pour les autre jour un chiffre positif quelconque
			var nb_sunday = exist(o,'.rules.nb_sunday_volonteer') && o.data.nb_minutes_worked_this_week.is_sunday && ((nbMinutesWorked > 0) +  exist(o,'.data.nb_minutes_worked_this_week.dateday_will_be_counted') )
			if(nb_sunday > o.rules.nb_sunday_volonteer) {
				error('form.control.too_much_sunday', {
					otherParams: {
						is: nb_sunday,
						shouldBe: o.rules.nb_sunday_volonteer,
					},
				},false);
			}
			// //des infos qu'on exporte pour la fonction personnalisé parente
			// my.minutesWorkedThisWeek = minutesWorkedThisWeek
			// my.nbMinutesWorked = nbMinutesWorked

			return ({
				errors : my.errors,//{errorCode:"",otherParams:[]}
				nbMinutesWorked: nbMinutesWorked,
				minutesWorkedThisWeek: minutesWorkedThisWeek
			}) 

			}catch(e){
				if(!v8js){
					console.error(e)
				}
				throw 'error';
				// var_dump(e);
			}
		}
	}
	return m;
	//# sourceURL=controleInterJs.js
})()



