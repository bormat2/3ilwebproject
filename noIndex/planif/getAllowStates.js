(function(){
	"use strict";
	Array.prototype.has = String.prototype.has = function(a){return this.indexOf(a) > -1}
	var defaultConfig = /*ceci n'est qu'un exemple pas à jour utilisé que si la bdd n'est pas remplie*/
	{
		"ADMIN": {
			_AUTHORISED:	    ["WISHED", "PROPOSED","PROPOSED_SYSTEM", "VALID", "CANCELED","WITHDRAW","UNAVAILABLE", "REFUSED_MANAGER", "REFUSED_CONSULTANT"],
			_GPR1:				["_INIT","CANCELED","VALID"],
			_INIT:				["WISHED2","PROPOSED","WISHED"],
			WISHED:				["_GPR1", "REFUSED_MANAGER"],
			WISHED2:			["_GPR1", "REFUSED_MANAGER"],
			PROPOSED:			["_GPR1", "REFUSED_CONSULTANT"],
			PROPOSED_SYSTEM:	["_GPR1", "REFUSED_CONSULTANT"],
			VALID:				["UNAVAILABLE", "WITHDRAW"],
			CANCELED:			[],
			WITHDRAW:			[],
			UNAVAILABLE:		[],
			REFUSED_MANAGER:	[],
			REFUSED_CONSULTANT: [],
		}, 
		"MYSELF": {
			_AUTHORISED: 		["WISHED","VALID","CANCELED","REFUSED_CONSULTANT"],
			_GPR1:				["_INIT","CANCELED"],
			_INIT:				["WISHED2","WISHED"],
			WISHED:				["_GPR1"],
			WISHED2:			["_GPR1"],
			PROPOSED:			["","VALID","REFUSED_CONSULTANT"],
			PROPOSED_SYSTEM:	["","VALID","REFUSED_CONSULTANT"],
			VALID:				[""], /* si "" alors on ajoute pas les successeurs possibles de l'état initial et l'état initale comme autorisé*/
			CANCELED:			["","_GPR1"],
			WITHDRAW:			[""],
			UNAVAILABLE:		[""],
			REFUSED_MANAGER:	[""],
			REFUSED_CONSULTANT: [],
		},
		"ADMIN_LIMITED": {
			_INIT: [""], //sans état initial on ne peut rien faire
			_AUTHORISED:	    ["WISHED", "PROPOSED", "VALID","WISHED2"],
			WISHED:				["VALID"],
			WISHED2:			["VALID"],
			PROPOSED:			["VALID"],
			PROPOSED_SYSTEM:	["VALID"],
			VALID:				["@INIT_STATE"],//revenir à la version 1 de l'intervention
			CANCELED:			[""],
			WITHDRAW:			[""],
			UNAVAILABLE:		[""],
			REFUSED_MANAGER:	[""],
			REFUSED_CONSULTANT: [""],
		}, 
	}

	/**
	* config voir au dessus
	* initSt l'état initial
	* state l'état actuel
	* role ADMIN ou MYSELF
	* @return tableau de roles utilisés, il peut être de longueur 0 
	*/
	var getStates = function(o){
		// PHP.vardump(o)
		var config = o.config || defaultConfig ,initSt = o.initSt,state=o.state,role = o.role/*,dayWorked = o.dayWorked*/,
			daySelected = o.daySelected,/*isSundayCompatible = o.isSundayCompatible,*/addSuccessorOf,
			workedType = o.workedType,workedTypePers = o.workedTypePers,volunteerType = o.volunteerType; 

		var console1,browser = false;
		try{
			console1 = console || {}
			browser = true
		}catch(e){
			console1 = {
				error: function(e){
					var_dump(e)
				}
			};
		}
	   
	   	if(typeof volunteerType == 'undefined'){
	   		console1.error('I need volunteerType');
	   		throw 'error';
	   	}
	   	if(typeof workedTypePers == 'undefined'){
	   		console1.error('I need workedTypePers');
	   		throw '';
	   	}

	   	
	   	if(state == 'CANCELED'){
	   		state = null;
	   		initSt = null
	   	}else if(initSt == 'CANCELED'){
	   		initSt = null
	   	}

	 //    if(! (dayWorked == false && daySelected == 0 && isSundayCompatible)){
		// 	// PHP.vardump(allowStates);
	 //        return initSt ? ['VALID','CANCELED'] : ['VALID']
		// }

		var key = role;
		var statesNotVolunter;
		if(volunteerType && workedTypePers != 'WORKED'){
			key += '_VOLUNTEER'

			// PHP.vardump(allowStates);
	        // return initSt ? ['VALID','CANCELED'] : ['VALID']
		}else{
			o.hideStatesInPopup = true
		}

	    // config['ADMIN'] = defaultConfig['ADMIN_LIMITED'];
	    var theConfig = config[key]
	    if(!theConfig){
	    	return []
	    }

		if(config.charAt){
			eval('config='+config)//pour l'appel depuis V8JS
		}

		var needInclude,allowStates,state,inArray,addState,addAllowStates,i;
		inArray = {}// permet d'éviter des récursivité infinie si le json est mal fait
		allowStates = state ? [state]: [] 
		var lgAllowStates = allowStates.length
		i = 0
		addAllowStates = function(arr){
			Array.prototype.push.apply(allowStates,arr)
		}
		/*
		*	dès qu'un état contient un underscore on ajoute à la fin du tableau les états qui lui sont associès
		*/
		addSuccessorOf = function(state2){
			addAllowStates(theConfig[state2])
			for(;i < allowStates.length;++i){
				var state = allowStates[i]
				if(inArray[state] || state[0] == '_' || state[0] == '@'){
					allowStates[i] = '';//on met une chaine vide qui partira lors du filtrage car si ça commence par un _ ce n'est pas un vrai état mais un groupe de même pour @ qui est une règle
				}
				if(!inArray[state]){
					inArray[state] = true
					if(state[0] == '_'){//si ça commence par _ alors c'est un groupe à inclure
						addAllowStates(theConfig[state])
					}else if(state[0] == '@'){
						if(state == '@INIT_STATE'){
							addAllowStates([initSt]);
						}
					}
				}			
			}
		}
		addSuccessorOf(state || '_INIT')
		/* si allowStates[0] == "" alors on ajoute pas les successeur de l'état initial et l'état initale comme autorisé*/
		if(allowStates[lgAllowStates] !== "" && initSt){
			addSuccessorOf(initSt)
			if(!inArray[initSt]){
				allowStates.push(initSt)
			}
		}

		var ret = theConfig._AUTHORISED.filter(function(state){
			return allowStates.has(state)
		})
		if(o.state && o.state.length && !ret.has(o.state)){
			ret.push(o.state);//l'état actuel doit apparaitre dans la liste.
		}
		ret.hideStatesInPopup = o.hideStatesInPopup;
		return ret;
	}

	/** pour v8js */
	return typeof epit !== 'undefined' ? (epit.getStates = getStates) : getStates

})()

/*
*	nothing here
*/

// console.log(epit.getStates({
// 	config: null,
// 	initSt: 'WISHED',
// 	state : 'VALID',
// 	role  :	'MYSELF',
// 	dayWorked: false,
// 	daySelected: 0,
// 	isSundayCompatible: true
// }))
// console.log(epit.getStates({
// 	config: null,
// 	initSt: 'WISHED',
// 	state : 'VALID',
// 	role  :	'ADMIN',
// 	dayWorked: false,
// 	daySelected: 0,
// 	isSundayCompatible: true
// }))


