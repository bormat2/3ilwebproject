(function (){
   function getDataTestNeed(){
      var ent = 53
      if(ent = 53){
          var json = {
            "order":[  
               "business_grp",
               "business_grp_qty",
               "grp_sector",
               "grp_sector_entity",
               "workforce_ratio_for_one_qty",
               "entity_business_grp_participation",
               "equivalent_period",
               // "department",
               // "entity_department"
            ],
            "equivalent_period": [
               {
                  "id" : -1,
                  "name": "dontcare",
                  "start_date_old" : "2017-01-01",
                  "end_date_old" : "2017-12-31",
                  "start_date_new" : "2018-01-01",
                  "end_date_new" : "2018-12-31"
               }
            ],
            // "entity":[  
            //    {  
            //       "id":-1,
            //       "name":"e1",
            //       "state":"@",
            //       "master": true,
            //       "end_date": "2019-01-01",
            //       "parententity_id": 1,
            //       "type":"ENTITY"
            //    }
            //   ,{  
            //       "id":-2,
            //       "name":"e2",
            //       "state":"@",
            //       "master": true,
            //       "end_date": "2019-01-01",
            //       "parententity_id":-1,
            //       "type" : "SECTOR"
            //    },{  
            //       "id":-3,
            //       "name":"e3",
            //       "state":"@",
            //       "master": true,
            //       "end_date": "2019-01-01",
            //       "parententity_id":-1,
            //       "type":"SECTOR"
            //    }
            // ],
            "business_grp":[  
               {  
                  "id":-1,
                  "start_date":"2017-01-01",
                  "end_date":"2019-01-10"
               },

               {  
                  "id":-2,
                  "start_date":"2017-01-01",
                  "end_date":"2019-01-10"
               }
            ],
            "entity_business_grp_participation":[  
               {  
                  "id":-1,
                  "percent":1,
                  "start_date":"2017-01-01",
                  "end_date":"2019-01-10",
                  "entity_id": ent,
                  "business_grp_id": -1 
               },
               // {  
               //    "id":-2,
               //    "percent":0,
               //    "start_date":"2017-01-01",
               //    "end_date":"2019-01-10",
               //    "entity_id": -2,
               //    "business_grp_id": -2
               // },
               // {  
               //    "id":-3,
               //    "percent":0,
               //    "start_date":"2017-01-01",
               //    "end_date":"2019-01-10",
               //    "entity_id": -3,
               //    "business_grp_id": -1 
               // },
               // {  
               //    "id":-4,
               //    "percent":1,
               //    "start_date":"2017-01-01",
               //    "end_date":"2019-01-10",
               //    "entity_id": -3,
               //    "business_grp_id": -2
               // }
            ],
            "grp_sector":[  
               {  
                  "id":-1,
                  "name": "caisse",
                  "start_date":"2017-01-05",
                  "end_date":"2019-01-10"
               }
            ],
            "grp_sector_entity":[  
               {  
                   "id":-1,
                  "entity_id":ent,
                  "grp_sector_id":-1,
                  "start_date":"2017-01-01",
                  "end_date":"2019-01-10"
               },
               // {  
               //     "id":-2,
               //    "entity_id":-3,
               //    "grp_sector_id":-1,
               //    "start_date":"2017-01-01",
               //    "end_date":"2019-01-10"
               // }
            ],
            "workforce_ratio_for_one_qty":[  
               {  
                   "id":-1,
                  "ratio_in_sector_grp":1,
                  "productivity": 100,
                  "grp_sector_id":-1,
                  "business_grp_id":-1
               },
               {  
                   "id":-2,
                  "ratio_in_sector_grp":1,
                  "productivity": 100,
                  "grp_sector_id":-1,
                  "business_grp_id":-2
               }
            ],
            // "person":[  
            //    {  
            //       "id":-1,
            //       "civilite":"M",
            //       "last_name":"tutu",
            //       "first_name": "toto 1"
            //    },
            //    {  
            //       "id":-2,
            //       "civilite":"M",
            //       "last_name":"tutu",
            //       "first_name": "toto 2"
            //    },
            //    {  
            //       "id":-3,
            //       "civilite":"M",
            //       "last_name":"tutu",
            //       "first_name": "toto 1"
            //    },
            //    {  
            //       "id":-4,
            //       "civilite":"M",
            //       "last_name":"tutu",
            //       "first_name": "toto 2"
            //    }
            // ],
            // "worksfor":[ 
            //    {  
            //     "status_id" : 1,
            //     "work_role": "CONSULT",
            //     "end_date" : "2099-12-31",
            //     "start_date" : "2017-01-01",
            //     "contract_id":1,
            //     "id": -1,
            //       "person_id":-2,
            //       "entity_id":-1,
            //       "planning_id":-1,
            //       "in_planning" : true,
            //       "contract_id" : 55,
            //       "role_id" : 45726
            //    }, 
            //    {  
            //     "status_id" : 1,
            //     "work_role": "CONSULT",
            //     "work_role": 1,
            //     "start_date" : "2017-01-01",
            //     "end_date" : "2099-12-31",
            //     "contract_id":1,
            //     "id": -2,
            //       "person_id":-2,
            //       "entity_id":-1,
            //       "planning_id":-1,
            //     "in_planning" : true,
            //     "contract_id" : 55,
            //     "role_id" : 45726
            //    },
            //    {
            //     "status_id" : 1,
            //     "work_role": "CONSULT",
            //     "work_role": 1,
            //     "start_date" : "2017-01-01",
            //     "end_date" : "2099-12-31",
            //     "contract_id":1,
            //     "id": -3,
            //       "person_id":-3,
            //       "entity_id":-1,
            //       "planning_id":-1,
            //     "in_planning" : true,
            //     "contract_id" : 55,
            //     "role_id" : 45726
            //    },{              
            //     "status_id" : 1,
            //     "work_role": "CONSULT",
            //     "work_role": 1,
            //     "start_date" : "2017-01-01",
            //     "end_date" : "2099-12-31",
            //     "contract_id":1,
            //     "id": -4,
            //       "person_id":-4,
            //       "entity_id":-1,
            //       "planning_id":-1,
            //     "in_planning" : true,
            //     "contract_id" : 55,
            //     "role_id" : 45726
            //    }
            // ],
            // "planning":[  
            //    {  
            //       "id":-1,
            //       "isreusable": true,
            //       "name":"test",
            //       "type" : "ENTITY"
            //    }
            // ],
            // "periode":[  
            //    {  
            //       "id":-1,
            //       "level":1,
            //       "planning_id" : -1,
            //       "work_type":"WORKED",
            //       "start_date":"2018-03-04",
            //       "end_date":"2018-03-04",
            //       "frequency":"U",
            //        "isworked":true,
            //        "monday":false,
            //        "tuesday":false,
            //        "wednesday":false,
            //        "thursday":false,
            //        "friday":false,
            //        "saturday":false,
            //        "sunday":false
            //    },{  
            //       "id":-2,
            //       "level":1,
            //       "planning_id" : -1,
            //       "work_type":"WORKED",
            //       "start_date":"2018-03-05",
            //       "end_date":"2018-03-05",
            //       "frequency":"U",
            //        "isworked":true,
            //        "monday":false,
            //        "tuesday":false,
            //        "wednesday":false,
            //        "thursday":false,
            //        "friday":false,
            //        "saturday":false,
            //        "sunday":false
            //    }
            // ],
            // "amplitude":[
            //    {  
            //       "id" : -1,
            //       "start_hour":"9:00",
            //       "end_hour":"12:00",
            //       "periode_id" : -1,
            //       "rank":1,
            //       "worked_type" : null
            //    },
            //    {
            //       "id": -2,
            //       "periode_id" : -1,
            //       "start_hour":"14:00",
            //       "end_hour":"20:00",
            //      "rank":1,
            //      "worked_type" : null
            //    },
            //     {  
            //       "id" : -3,
            //       "start_hour":"9:00",
            //       "end_hour":"12:00",
            //       "periode_id" : -2,
            //       "rank":1,
            //       "worked_type" : null
            //    },
            //    {
            //       "id": -4,
            //       "periode_id" : -2,
            //       "start_hour":"14:00",
            //       "end_hour":"20:00",
            //      "rank":1,
            //      "worked_type" : null
            //    }],
            // "params_group" : [
            //    {  
            //       "id":-1,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":49
            //    },
            //    {  
            //       "id":-2,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":2,
            //       "namepar_id":8
            //    },
            //    {  
            //       "id":-3,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":12
            //    },
            //    {  
            //       "id":-4,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":1
            //    },
            //    {  
            //       "id":-5,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":2
            //    },
            //    {  
            //       "id":-6,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":3
            //    },
            //    {  
            //       "id":-7,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":4
            //    },
            //    {  
            //       "id":-8,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":5
            //    },
            //    {  
            //       "id":-9,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":6
            //    },
            //    {  
            //       "id":-10,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":7
            //    },
            //    {  
            //       "id":-11,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":9
            //    },
            //    {  
            //       "id":-12,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":10
            //    },
            //    {  
            //       "id":-13,
            //       "entity_id":-1,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":50
            //    },


            //     {
            //       "id":-21,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":49
            //    },
            //    {  
            //       "id":-22,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":2,
            //       "namepar_id":8
            //    },
            //    {  
            //       "id":-23,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":12
            //    },
            //    {  
            //       "id":-24,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":1
            //    },
            //    {  
            //       "id":-25,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":2
            //    },
            //    {  
            //       "id":-26,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":3
            //    },
            //    {  
            //       "id":-27,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":4
            //    },
            //    {  
            //       "id":-28,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":5
            //    },
            //    {  
            //       "id":-29,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":6
            //    },
            //    {  
            //       "id":-120,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":7
            //    },
            //    {  
            //       "id":-121,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":9
            //    },
            //    {  
            //       "id":-122,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":10
            //    },
            //    {  
            //       "id":-123,
            //       "entity_id":-2,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":50
            //    },


            //    {
            //       "id":-31,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":49
            //    },
            //    {  
            //       "id":-32,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":2,
            //       "namepar_id":8
            //    },
            //    {  
            //       "id":-33,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":12
            //    },
            //    {  
            //       "id":-34,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":1
            //    },
            //    {  
            //       "id":-35,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":2
            //    },
            //    {  
            //       "id":-36,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":3
            //    },
            //    {  
            //       "id":-37,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":4
            //    },
            //    {  
            //       "id":-38,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":5
            //    },
            //    {  
            //       "id":-39,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":6
            //    },
            //    {  
            //       "id":-130,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":7
            //    },
            //    {  
            //       "id":-131,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":9
            //    },
            //    {  
            //       "id":-132,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":10
            //    },
            //    {  
            //       "id":-133,
            //       "entity_id":-3,
            //       "edit_id":null,
            //       "start_date":"1999-01-01",
            //       "end_date":"2999-01-01",
            //       "table":null,
            //       "inheritfrom_id":1,
            //       "namepar_id":50
            //    }
            // ],
            // "custom_menu": [
            //     {"id":-1,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"HOME","item":"menu.home","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-2,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"COMPANY","item":"menu.fusionEntity","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-3,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"COMPANY","item":"menu.schedule","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-4,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"COMPANY","item":"menu.company","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-5,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"COMPANY","item":"menu.contacts","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-6,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"USER","item":"menu.user","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-7,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"PARTNERS","item":"menu.client","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-8,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"PARTNERS","item":"menu.provider","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-9,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"MISSION","item":"menu.missions","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-10,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"MISSION","item":"menu.Fusionmissions","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-11,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"MISSION","item":"menu.FusionIntervention","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-12,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"CONSULTANT","item":"menu.consultatives","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-13,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"ACTIVITY","item":"menu.inputAdminVal","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-14,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"ACTIVITY","item":"osf_person_fusion","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-21,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"MY_PLANNING","item":"menu.myPlanning","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-15,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"CONSULT","groupitem":"USER","item":"menu.user","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-16,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"CONSULT","groupitem":"USER","item":"menu.mySchedule","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-17,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"CONSULT","groupitem":"MISSION","item":"menu.missions","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-18,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"CONSULT","groupitem":"ACTIVITY","item":"menu.inputConsult","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-19,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"CONSULT","groupitem":"ACTIVITY","item":"osf_person_fusion","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
            //     {"id":-20,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"CONSULT","groupitem":"MY_PLANNING","item":"menu.myPlanning","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}
            // ],
            // "relclient" : [
            //    {
            //       "id": -1,
            //       "provider_id" : -1,
            //       "client_id" : -1,
            //       "owner_id" : -1
            //    },
            //    {
            //       "id": -2,
            //       "provider_id" : -2,
            //       "client_id" : -2,
            //       "owner_id" : -2
            //    },
            //    {
            //       "id": -3,
            //       "provider_id" : -3,
            //       "client_id" : -3,
            //       "owner_id" : -3
            //    }
            // ],
            // "mission" : [
            //    {  
            //       "id":-1,
            //       "relclient_id":-1,
            //       "type":"INTERNAL",
            //       "name":"-1",
            //       "start_date":"2016-01-01",
            //       "end_date":"2020-09-01",
            //       "staff_size_required":true,
            //       "state" : "VALID"
            //    },
            //    {  
            //       "id":-2,
            //       "relclient_id":-2,
            //       "type":"INTERNAL",
            //       "name":"-2",
            //       "start_date":"2016-01-01",
            //       "end_date":"2020-09-01",
            //       "staff_size_required":true,
            //       "state" : "VALID"
            //    },
            //    {  
            //       "id":-3,
            //       "relclient_id":-3,
            //       "type":"INTERNAL",
            //       "name":"-3",
            //       "start_date":"2016-01-01",
            //       "end_date":"2020-09-01",
            //       "staff_size_required":true,
            //         "state" : "VALID"
            //    }
            // ],
            // "subscription": [
            //    {
            //       "id": -1,
            //       "entity_id": -1,
            //       "creator_id": 1,
            //       "modifier_id": 1,
            //       "ref": "test",
            //       "type" : "OWNER",
            //       "start_date" : "2016-01-01",
            //       "end_date" : "2097-01-01",
            //       "create_date": "2097-01-01"
            //    },
            //    {
            //       "id": -2,
            //       "entity_id": -2,
            //       "creator_id": 1,
            //       "modifier_id": 1,
            //       "ref": "test",
            //       "type" : "OWNER",
            //       "start_date" : "2016-01-01",
            //       "end_date" : "2097-01-01",
            //       "create_date": "2097-01-01"
            //    },
            //    {
            //       "id": -3,
            //       "entity_id": -3,
            //       "creator_id": 1,
            //       "modifier_id": 1,
            //       "ref": "test",
            //       "type" : "OWNER",
            //       "start_date" : "2016-01-01",
            //       "end_date" : "2097-01-01",
            //       "create_date": "2097-01-01"
            //    }
            // ],
            // "employee_group": [{
            //    "id": -1,
            //    "start_date" : "1999-01-01",
            //    "end_date" : "2099-01-01",
            //    "paramsvalue_id" : 47859
            // }],
            "department": [{
                  "id": -1,
                  "name": "department1",
                  "type": "SECTOR"}],
            "entity_department": [
               {
                  "id": -1,
                  "entity_id": -1,
                  "start_date" : "1999-01-01",
                  "end_date" : "2099-01-01",
                  "department_id":-1,
                  "send" : true,
                  "receive" : true,
                  "manual_only" : false
               },
               // {
               //    "id": -2,
               //    "entity_id": -2,
               //    "start_date" : "1999-01-01",
               //    "end_date" : "2099-01-01",
               //    "department_id":-1,
               //    "send" : false,
               //    "receive" : true,
               //    "manual_only" : false
               // },
               // {
               //    "id": -3,
               //    "entity_id": -3,
               //    "start_date" : "1999-01-01",
               //    "end_date" : "2099-01-01",
               //    "department_id":-1,
               //    "send" : false,
               //    "receive" : true,
               //    "manual_only" : false
               // }
            ]
         }
      }else{
         var json = {
            "order":[  
               "entity",
               "business_grp",
               "business_grp_qty",
               "grp_sector",
               "grp_sector_entity",
               "workforce_ratio_for_one_qty",
               "entity_business_grp_participation",
               "person",
               "planning",
               "worksfor",
               "periode",
               "amplitude",
               "equivalent_period",
               "params_group",
               "custom_menu",
               "relclient",
               "mission",
               "subscription",
               "employee_group",
               "department",
               "entity_department"
            ],
            "equivalent_period": [
                  {
                     "id" : -1,
                     "name": "dontcare",
                     "start_date_old" : "2017-01-01",
                     "end_date_old" : "2017-12-31",
                     "start_date_new" : "2018-01-01",
                     "end_date_new" : "2018-12-31"
                  }
            ],
            "entity":[  
               {  
                  "id":-1,
                  "name":"e1",
                  "state":"@",
                  "master": true,
                  "end_date": "2019-01-01",
                  "parententity_id": 1,
                  "type":"ENTITY"
               }
              ,{  
                  "id":-2,
                  "name":"e2",
                  "state":"@",
                  "master": true,
                  "end_date": "2019-01-01",
                  "parententity_id":-1,
                  "type" : "SECTOR"
               },{  
                  "id":-3,
                  "name":"e3",
                  "state":"@",
                  "master": true,
                  "end_date": "2019-01-01",
                  "parententity_id":-1,
                  "type":"SECTOR"
               }
            ],
            "business_grp":[  
               {  
                  "id":-1,
                  "start_date":"2017-01-01",
                  "end_date":"2019-01-10"
               },

               {  
                  "id":-2,
                  "start_date":"2017-01-01",
                  "end_date":"2019-01-10"
               }
            ],
            "entity_business_grp_participation":[  
               {  
                  "id":-1,
                  "percent":1,
                  "start_date":"2017-01-01",
                  "end_date":"2019-01-10",
                  "entity_id": -2,
                  "business_grp_id": -1 
               },
               {  
                  "id":-2,
                  "percent":0,
                  "start_date":"2017-01-01",
                  "end_date":"2019-01-10",
                  "entity_id": -2,
                  "business_grp_id": -2
               },
               {  
                  "id":-3,
                  "percent":0,
                  "start_date":"2017-01-01",
                  "end_date":"2019-01-10",
                  "entity_id": -3,
                  "business_grp_id": -1 
               },
               {  
                  "id":-4,
                  "percent":1,
                  "start_date":"2017-01-01",
                  "end_date":"2019-01-10",
                  "entity_id": -3,
                  "business_grp_id": -2
               }
            ],
            "grp_sector":[  
               {  
                  "id":-1,
                  "name": "caisse",
                  "start_date":"2017-01-05",
                  "end_date":"2019-01-10"
               }
            ],
            "grp_sector_entity":[  
               {  
                   "id":-1,
                  "entity_id":-2,
                  "grp_sector_id":-1,
                  "start_date":"2017-01-01",
                  "end_date":"2019-01-10"
               },
               {  
                   "id":-2,
                  "entity_id":-3,
                  "grp_sector_id":-1,
                  "start_date":"2017-01-01",
                  "end_date":"2019-01-10"
               }
            ],
            "workforce_ratio_for_one_qty":[  
               {  
                   "id":-1,
                  "ratio_in_sector_grp":1,
                  "productivity": 100,
                  "grp_sector_id":-1,
                  "business_grp_id":-1
               },
               {  
                   "id":-2,
                  "ratio_in_sector_grp":1,
                  "productivity": 100,
                  "grp_sector_id":-1,
                  "business_grp_id":-2
               }
            ],
            "person":[  
               {  
                  "id":-1,
                  "civilite":"M",
                  "last_name":"tutu",
                  "first_name": "toto 1"
               },
               {  
                  "id":-2,
                  "civilite":"M",
                  "last_name":"tutu",
                  "first_name": "toto 2"
               },
               {  
                  "id":-3,
                  "civilite":"M",
                  "last_name":"tutu",
                  "first_name": "toto 1"
               },
               {  
                  "id":-4,
                  "civilite":"M",
                  "last_name":"tutu",
                  "first_name": "toto 2"
               }
            ],
            "worksfor":[ 
               {  
                "status_id" : 1,
                "work_role": "CONSULT",
                "end_date" : "2099-12-31",
                "start_date" : "2017-01-01",
                "contract_id":1,
                "id": -1,
                  "person_id":-2,
                  "entity_id":-1,
                  "planning_id":-1,
                  "in_planning" : true,
                  "contract_id" : 55,
                  "role_id" : 45726
               }, 
               {  
                "status_id" : 1,
                "work_role": "CONSULT",
                "work_role": 1,
                "start_date" : "2017-01-01",
                "end_date" : "2099-12-31",
                "contract_id":1,
                "id": -2,
                  "person_id":-2,
                  "entity_id":-1,
                  "planning_id":-1,
                "in_planning" : true,
                "contract_id" : 55,
                "role_id" : 45726
               },
               {
                "status_id" : 1,
                "work_role": "CONSULT",
                "work_role": 1,
                "start_date" : "2017-01-01",
                "end_date" : "2099-12-31",
                "contract_id":1,
                "id": -3,
                  "person_id":-3,
                  "entity_id":-1,
                  "planning_id":-1,
                "in_planning" : true,
                "contract_id" : 55,
                "role_id" : 45726
               },{              
                "status_id" : 1,
                "work_role": "CONSULT",
                "work_role": 1,
                "start_date" : "2017-01-01",
                "end_date" : "2099-12-31",
                "contract_id":1,
                "id": -4,
                  "person_id":-4,
                  "entity_id":-1,
                  "planning_id":-1,
                "in_planning" : true,
                "contract_id" : 55,
                "role_id" : 45726
               }
            ],
            "planning":[  
               {  
                  "id":-1,
                  "isreusable": true,
                  "name":"test",
                  "type" : "ENTITY"
               }
            ],
            "periode":[  
               {  
                  "id":-1,
                  "level":1,
                  "planning_id" : -1,
                  "work_type":"OPEN_new",
                  "start_date":"2018-03-04",
                  "end_date":"2018-03-04",
                  "frequency":"U",
                   "isworked":true,
                   "monday":false,
                   "tuesday":false,
                   "wednesday":false,
                   "thursday":false,
                   "friday":false,
                   "saturday":false,
                   "sunday":false
               },{  
                  "id":-2,
                  "level":1,
                  "planning_id" : -1,
                  "work_type":"OPEN_new",
                  "start_date":"2018-03-05",
                  "end_date":"2018-03-05",
                  "frequency":"U",
                   "isworked":true,
                   "monday":false,
                   "tuesday":false,
                   "wednesday":false,
                   "thursday":false,
                   "friday":false,
                   "saturday":false,
                   "sunday":false
               }
            ],
            "amplitude":[
               {  
                  "id" : -1,
                  "start_hour":"9:00",
                  "end_hour":"12:00",
                  "periode_id" : -1,
                  "rank":1,
                  "worked_type" : null
               },
               {
                  "id": -2,
                  "periode_id" : -1,
                  "start_hour":"14:00",
                  "end_hour":"20:00",
                 "rank":1,
                 "worked_type" : null
               },
                {  
                  "id" : -3,
                  "start_hour":"9:00",
                  "end_hour":"12:00",
                  "periode_id" : -2,
                  "rank":1,
                  "worked_type" : null
               },
               {
                  "id": -4,
                  "periode_id" : -2,
                  "start_hour":"14:00",
                  "end_hour":"20:00",
                 "rank":1,
                 "worked_type" : null
               }],
            "params_group" : [
               {  
                  "id":-1,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":49
               },
               {  
                  "id":-2,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":2,
                  "namepar_id":8
               },
               {  
                  "id":-3,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":12
               },
               {  
                  "id":-4,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":1
               },
               {  
                  "id":-5,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":2
               },
               {  
                  "id":-6,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":3
               },
               {  
                  "id":-7,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":4
               },
               {  
                  "id":-8,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":5
               },
               {  
                  "id":-9,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":6
               },
               {  
                  "id":-10,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":7
               },
               {  
                  "id":-11,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":9
               },
               {  
                  "id":-12,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":10
               },
               {  
                  "id":-13,
                  "entity_id":-1,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":50
               },


                {
                  "id":-21,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":49
               },
               {  
                  "id":-22,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":2,
                  "namepar_id":8
               },
               {  
                  "id":-23,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":12
               },
               {  
                  "id":-24,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":1
               },
               {  
                  "id":-25,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":2
               },
               {  
                  "id":-26,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":3
               },
               {  
                  "id":-27,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":4
               },
               {  
                  "id":-28,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":5
               },
               {  
                  "id":-29,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":6
               },
               {  
                  "id":-120,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":7
               },
               {  
                  "id":-121,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":9
               },
               {  
                  "id":-122,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":10
               },
               {  
                  "id":-123,
                  "entity_id":-2,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":50
               },


               {
                  "id":-31,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":49
               },
               {  
                  "id":-32,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":2,
                  "namepar_id":8
               },
               {  
                  "id":-33,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":12
               },
               {  
                  "id":-34,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":1
               },
               {  
                  "id":-35,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":2
               },
               {  
                  "id":-36,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":3
               },
               {  
                  "id":-37,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":4
               },
               {  
                  "id":-38,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":5
               },
               {  
                  "id":-39,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":6
               },
               {  
                  "id":-130,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":7
               },
               {  
                  "id":-131,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":9
               },
               {  
                  "id":-132,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":10
               },
               {  
                  "id":-133,
                  "entity_id":-3,
                  "edit_id":null,
                  "start_date":"1999-01-01",
                  "end_date":"2999-01-01",
                  "table":null,
                  "inheritfrom_id":1,
                  "namepar_id":50
               }
            ],
            "custom_menu": [
                {"id":-1,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"HOME","item":"menu.home","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-2,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"COMPANY","item":"menu.fusionEntity","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-3,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"COMPANY","item":"menu.schedule","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-4,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"COMPANY","item":"menu.company","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-5,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"COMPANY","item":"menu.contacts","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-6,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"USER","item":"menu.user","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-7,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"PARTNERS","item":"menu.client","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-8,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"PARTNERS","item":"menu.provider","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-9,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"MISSION","item":"menu.missions","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-10,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"MISSION","item":"menu.Fusionmissions","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-11,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"MISSION","item":"menu.FusionIntervention","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-12,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"CONSULTANT","item":"menu.consultatives","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-13,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"ACTIVITY","item":"menu.inputAdminVal","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-14,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"ACTIVITY","item":"osf_person_fusion","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-21,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"ADMIN","groupitem":"MY_PLANNING","item":"menu.myPlanning","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-15,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"CONSULT","groupitem":"USER","item":"menu.user","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-16,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"CONSULT","groupitem":"USER","item":"menu.mySchedule","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-17,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"CONSULT","groupitem":"MISSION","item":"menu.missions","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-18,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"CONSULT","groupitem":"ACTIVITY","item":"menu.inputConsult","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-19,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"CONSULT","groupitem":"ACTIVITY","item":"osf_person_fusion","enabled":false,"update_date":null,"create_date":"2017-02-01T00:00:00"}, 
                {"id":-20,"entity_id":-1,"creator_id":1,"modifier_id":null,"profil":"CONSULT","groupitem":"MY_PLANNING","item":"menu.myPlanning","enabled":true,"update_date":null,"create_date":"2017-02-01T00:00:00"}
            ],
            "relclient" : [
               {
                  "id": -1,
                  "provider_id" : -1,
                  "client_id" : -1,
                  "owner_id" : -1
               },
               {
                  "id": -2,
                  "provider_id" : -2,
                  "client_id" : -2,
                  "owner_id" : -2
               },
               {
                  "id": -3,
                  "provider_id" : -3,
                  "client_id" : -3,
                  "owner_id" : -3
               }
            ],
            "mission" : [
               {  
                  "id":-1,
                  "relclient_id":-1,
                  "type":"INTERNAL",
                  "name":"-1",
                  "start_date":"2016-01-01",
                  "end_date":"2020-09-01",
                  "staff_size_required":true,
                  "state" : "VALID"
               },
               {  
                  "id":-2,
                  "relclient_id":-2,
                  "type":"INTERNAL",
                  "name":"-2",
                  "start_date":"2016-01-01",
                  "end_date":"2020-09-01",
                  "staff_size_required":true,
                  "state" : "VALID"
               },
               {  
                  "id":-3,
                  "relclient_id":-3,
                  "type":"INTERNAL",
                  "name":"-3",
                  "start_date":"2016-01-01",
                  "end_date":"2020-09-01",
                  "staff_size_required":true,
                    "state" : "VALID"
               }
            ],
            "subscription": [
               {
                  "id": -1,
                  "entity_id": -1,
                  "creator_id": 1,
                  "modifier_id": 1,
                  "ref": "test",
                  "type" : "OWNER",
                  "start_date" : "2016-01-01",
                  "end_date" : "2097-01-01",
                  "create_date": "2097-01-01"
               },
               {
                  "id": -2,
                  "entity_id": -2,
                  "creator_id": 1,
                  "modifier_id": 1,
                  "ref": "test",
                  "type" : "OWNER",
                  "start_date" : "2016-01-01",
                  "end_date" : "2097-01-01",
                  "create_date": "2097-01-01"
               },
               {
                  "id": -3,
                  "entity_id": -3,
                  "creator_id": 1,
                  "modifier_id": 1,
                  "ref": "test",
                  "type" : "OWNER",
                  "start_date" : "2016-01-01",
                  "end_date" : "2097-01-01",
                  "create_date": "2097-01-01"
               }
            ],
            "employee_group": [{
               "id": -1,
               "start_date" : "1999-01-01",
               "end_date" : "2099-01-01",
               "paramsvalue_id" : 47859
            }],
            "department": [{
                  "id": -1,
                  "name": "department1",
                  "type": "SECTOR"}],
            "entity_department": [
               {
                  "id": -1,
                  "entity_id": -1,
                  "start_date" : "1999-01-01",
                  "end_date" : "2099-01-01",
                  "department_id":-1,
                  "send" : true,
                  "receive" : false,
                  "manual_only" : false
               },
               {
                  "id": -2,
                  "entity_id": -2,
                  "start_date" : "1999-01-01",
                  "end_date" : "2099-01-01",
                  "department_id":-1,
                  "send" : false,
                  "receive" : true,
                  "manual_only" : false
               },
               {
                  "id": -3,
                  "entity_id": -3,
                  "start_date" : "1999-01-01",
                  "end_date" : "2099-01-01",
                  "department_id":-1,
                  "send" : false,
                  "receive" : true,
                  "manual_only" : false
               }
            ]
         }
      }


      var arr = []
      var id = -1;
      var hours = ['08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30',
      '13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00',
      '18:30','19:00','19:30','20:00','20:30','21:00'];
      for(var iDay = 1; iDay < 30;++iDay){
         hours.forEach(function(hour,i){
            var nextHour = hours[i+1]
            var day = ('0'+iDay)
            day = day.slice(day.length-2,day.length)
            if(hour < '09:00' || hour >= '12:00' && hour < '14:00' || hour >= '20:00'){
               return;
            }
            var qty = 400;
            // if(iDay == 5 ){
            //    qty = 800
            // }

            var o1 = {  
              "id": --id,
              "start_date":"2017-03-"+iDay,
              "start_hour":hour,
              "end_hour": nextHour,
              "qty":qty,
              "business_grp_id":-1
           }
           var o2 = JSON.parse(JSON.stringify(o1));
           o2.id = --id;
           o2.business_grp_id = -2
           arr.push(o1,o2);
         })
      }

      json.business_grp_qty = arr;
     // var_dump(json);exit;
      return JSON.stringify(json);
   }

   return typeof window !== 'undefined' ? getDataTestNeed() : getDataTestNeed
})()




// quand les amplitudes correspondent toutes
// ça fonctionne 


/* maintenant le lundi va être 2 fois plus performant que les autres jours

*/