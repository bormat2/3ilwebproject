//Des nombres pseudo aléatoire
{
	m_w : 123456789,
	m_z : 987654321,
	mask : 0xffffffff,
	// Takes any integer
	seed : function(i) {
	    this.m_w = i;
	    this.m_z = 987654321;
	},
	// Returns number between 0 (inclusive) and 1.0 (exclusive),
	// just like Math.random().
	random : function(){
	    this.m_z = (36969 * (this.m_z & 65535) + (this.m_z >> 16)) & this.mask;
	    this.m_w = (18000 * (this.m_w & 65535) + (this.m_w >> 16)) & this.mask;
	    var result = ((this.m_z << 16) + this.m_w) & this.mask;
	    result /= 4294967296;
	    return result + 0.5;
	},
	//mélange le tableau
	shuffle: function(arr){
	    return arr.sort(() => this.random() - 0.5)
	}
}
