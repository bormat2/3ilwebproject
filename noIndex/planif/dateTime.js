var func = {
	formatHour : (floatHour) =>{
		if(floatHour > 24){
			print('erreur');
		}
		var hour = ('0'+(floatHour|0)).slice(-2);// le slice c'est pour avoir 08 au lieu de 008 et 13 au lieu de 013
		var unit = '0'+ Math.round((floatHour - hour) * 60)
		return hour + ':' + unit.slice(-2)
	},
	hourToFloorNumber(hour){
		var arr = hour.split(/[h\:]/); //le séparateur est un 'h' ou ':'
		return +arr[0] + arr[1]/60
	}
};
func;
