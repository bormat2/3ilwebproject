/**
 * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
 */
'' + function(){
    //La classe InterProxy représente non pas ce qui est réellement inséré mais ce que l'on voulait inséré agrandit des 
    //intervention déjà présente du même secteur
    //Quand il y a des protected elles peuvent au final écraser notre intervention et lui mettre son hasBeenInsert à false
    //et il faut donc rerécupéré une référence valide avant d'effectuer 
    //l'appel à des fonction sur l'intervention
    class InterProxy{
        constructor(o) {
            var m = this;
            ['has_been_insert','start_minute','end_minute','inter','sector'].forEach(function(name){
                m[name] = o[name]
            })
        }
        get_valid_inter(){
            var m = this;
            if(!m.inter.hasBeenInsert){
                //// debugger;
            }
            if(m.inter.hasBeenInsert) return m.inter
                var int = m.inter.listInfo.first_interv
            while(int){
                if(int.sector == m.sector && m.start_minute < int.endMinute && m.end_minute > int.startMinute){
                    return m.inter = int
                }
                int = int.next
            }
            controls_enabled(()=>{
                my_throw( "il ne faut pas appeler get_valid_inter si l'intervention n'a pas été insérée")
            })
        }
        getStartAndEndSameSector(){
            return this.get_valid_inter().getStartAndEndSameSector()
        }

        getMinTime(){
            return this.get_valid_inter().getMinTime()
        }
    }


    return InterProxy
}