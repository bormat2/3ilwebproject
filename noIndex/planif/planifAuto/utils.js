  '' + function(){
  /**
   * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
   */
    var clone = function(source, destination) {
        var o = source;
        var keys = Object.keys(o);
        var lg = keys.length;
        for (var i = 0; i < lg; i++) {
            var key = keys[i];
            destination[key] = o[key];
        }
        return destination
    }
    var copy_properties = (source,destination,...properties) => {
        for(property of properties){
            destination[property] = source[property]
        }
        return destination
    }

    var combine = function(...objs){
        let objs2 = []
        for(let obj of objs){
            if(obj instanceof Object){
                objs2.push(obj)
            }
        }
        return Object.assign({},...objs2)
    }

    var forOf = function(o, func) {
        if(o instanceof MyMap) {
            o = o.objs
        }
        var keys = Object.keys(o);
        var lg = keys.length;
        for (var i = 0; i < lg; i++) {
            var key = keys[i];
            if(key != 'length') {
                func(o[key], key);
            }
        }
    }

    var someOf = function(o, func) {
        if(o instanceof MyMap) {
            o = o.objs
        }
        var keys = Object.keys(o);
        var lg = keys.length;

        var stop_the_loop = function() {
            stop_the_loop = true
        }

        for (var i = 0; i < lg; i++) {
            var key = keys[i];
            if(key != 'length') {
                func(o[key], key, stop_the_loop)
            }
            if(stop_the_loop === true) {
                return;
            }
        }
    }

    const inc = function(obj,key,incre = 1){
        obj[key] = incre + (obj[key] || 0)
    }
    const is_between = (x, min, max) => x >= min && x <= max

    let handler = {
        set: function(){
            my_throw('cet objet est immutable et doit être recrée si on change des valeur')
        }
    }
    const immutable = function(obj){
        controls_enabled(()=>{
            obj = new Proxy(obj,handler)
        })
        return obj
    }
    const old_if_finite = Number.isFinite;
    const not_null = function(val){
        if(val === void 8 || val === null){
            my_throw('not_null')
        }
        return val
    }
    const isFinite = function(elm){
        if(!elm){
            return elm === 0
        }
        if(elm instanceof Object/* || elm[isProxy]*/){
            return false;
        }
        return old_if_finite.apply(this,arguments)
    }

    //une manière de pouvoir executé une fonction après chaque tour
    //attention res peut être un objet différent de true ou false
    var special_some = function(array,callback,after_each_callback_debug){
        array.some(function(){
            var res = callback.apply(null,arguments)
            controls_enabled(()=>{
                if(res === void 8){
                    my_throw("pour éviter des sortie de boucle non voulu il faut explicitement retourner false pour continuer la boucle et un truc vrai pour l'arrêter")
                }
                //on ne passe pas i car les tableau si ils ont été trié dans la boucle 
                //ne sont plus dans le même ordre
                after_each_callback_debug(res,arguments[0])
            })
            if(res) return true
        })
    }

    //on veut du positif
    const modul = (a,n) => {
        var res =  a%n + (a < 0 && Math.abs(n));
        controls_enabled(()=>{
            if(!isFinite(res)){
                my_throw( 'not finite')
            }
        })
        return res || 0
    }

    const lower = (hour,displayMinTime = conf.displayMinTime) => {
        //si hour = -15, -15 - 15 = - 30 ; si hour = 15, 15 - 15 = 0
        var hour2 = hour - (displayMinTime ? modul(hour,displayMinTime) : 0)
        controls_enabled(()=>{
            if(hour2 != ~~hour2){
                my_throw( "pas un entier")
            }
        })
        return hour2
    }
    const upper = (hour) => {
        var _lower = lower(hour)
        return _lower != hour ? _lower + conf.displayMinTime : hour
    }

    /* permet de mettre allowed_to_be_sorted pour dire que le tableau peut être trié dans un controls_enabled */
    const array_create = ({from})=>{
        let arr
        if(from){
            arr = from.slice()
        }else{
            arr = []
        }
        arr.allowed_to_be_sorted = 1
        return arr
    }

    const filter_workable = (amp2) => (
        amp2.openingType !== 'FERMETURE'
    );
    const STOP_LOOP = true; //comme cela on peut écrire return STOP_LOOP dans les fonction "some"
    const fl = (nb) => (Math.floor(nb) || 0)
    const equal_fl = function(x1, x2) {
        return x1 - x2 < 0.00000000001
    }
    const def = (m, name, def) => {
        if(m[name] === void 8) m[name] = def
        return m[name]
    }
    const first_def = function(){//ne pas utiliser de row function => à cause de arguments
        for(var i = 0; i < arguments.length;++i){
            if(arguments[i] !== void 8){
                return arguments[i]
            }
        }
    }
    const abstractError = function() {
        controls_enabled(()=>{
            my_throw( 'this method is abstract and should be implemented')
        })
    }
    String.prototype.has = Array.prototype.has = function(value) {
        return this.indexOf(value) >= 0
    };
    Array.prototype.reverse_some = function(callback) {
        for (var lg = this.length; lg--;) {
            if(callback(this[lg])) {
                return;
            }
        }
    }

    return {clone,forOf,someOf,inc,is_between,immutable,not_null,isFinite,special_some,modul,
        lower,upper,array_create,filter_workable,STOP_LOOP,fl,combine,equal_fl,def,first_def,abstractError, copy_properties}
}