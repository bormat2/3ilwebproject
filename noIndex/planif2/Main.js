'' + function(){
    /**
     * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
     */
    const main_keys = new Set([
        'rotations','pause_dispatcher','v2_total_needs_to_updtd','pauses_obj','minTimeAllSectorIs0',
        'blockArr','planifStartDate','planifEndDate',
        'need_person_sectors','sector_group_names','is_real_time_mode','is_test',
        "is_penalise_overstaffing_attractivity",
        "allow_to_fill_person_sector",
        "wfMap",
        "sectorMap",
        "all_wfs",
        "total_info_amp",
        "all_amp_open",
        "zscoreGlobal",
        "old_v2_sum_zscore_on_period_by_id_amp",
        "minTimeAllSectorIs0",
        "exact_need_running",
        "unplaced_time_disabled",
        "add_time_for_openning_closing",
        'is_penalise_overstaffing',//depreciated
    ])
    class Main{
         constructor(o) {
            var m = this;//pour utiliser le proxy à la place et faire des vérifs

            controls_enabled(()=>{
                m = proxy_creator(m,main_keys,{})
            })

            m.need_person_sectors = new Set()
            m.sector_group_names = {}

            m.v2_total_needs_to_updtd = new MyMap();
            m.rotations = {
                Opening: new Opening(m),
                Closing: new Closing(m),
                Saturday: new Saturday(m)
            }
            m.rotations.Opening.opposite_rota = m.rotations.Closing
            m.rotations.Closing.opposite_rota = m.rotations.Opening

            m.pause_dispatcher = new Pause_dispatcher({
                main: m
            })

            m.pauses_obj = {
                noon: m.pause_dispatcher.noon,
                afternoon: m.pause_dispatcher.afternoon
            }
            return m
        }


        main_ScheduleMyEntity(paramsSchedule) {
            const m = this;
            m.is_penalise_overstaffing_attractivity = true
            const algo = {}
            const zscores = []
            const get_rotation_score = m.get_instance_of_get_rotation_score()
            var f = () => {
                var move_extremity = (rules,opt) => {
                    m.improve_day_extremities();
                    // var get_zscore = opt.get_zscore || (() => m.v2_get_zscore_global())
                    // var g5,g3 = g5 = get_zscore()
                    // for(var i = 0;i<5;++i){
                    //     var g4 = g3
                    //     //en fait tant que la rotation n'est pas pire que avant la boucle ça va
                    //     // si il y a dont_make_rotation_worst
                    //     m.v2_move_multiple_extremity({
                    //         rules: new Set(rules),
                    //         // get_rotation_score: info.get_rotation_score,
                    //         zscore_rotation: get_rotation_score()
                    //     })
                    //     g3 = get_zscore()
                    //     if(!( (g3 < g4))) break
                    // }
                    // return g5 > g3 // si true on a amélioré
                }
                // console.error('test2');my_throw( 'test2')
                var mode = 'exact'
                Debug.perfStart('total_time')
                Debug.perfStart('main_ScheduleMyEntity_1')
                var params = paramsSchedule ? JSON.parse(paramsSchedule) : fakeData
                conf.minTime = 30 //params.minTime;
                conf.planifStartDate = params.startDate;
                conf.planifEndDate = params.endDate;
                var improve_sector = function(opt) {
                    if(mode == 'exact') {
                        m.handle_exact_need(opt)
                    } else {
                        m.v2_todo_multiple_change_sector({
                            do_not_let_too_small_inter: true
                        })
                        m.v2_todo_multiple_change_sector({
                            do_not_let_too_small_inter: false
                        })
                    }
                }
                var data = m.getDataFromDataBase(params);

                // m.pause_dispatcher.get_fake_sectors().forEach((fake_sector) => {
                //     data.all_wfs.forEach(function(wf) {
                //         m.create_personSector(fake_sector, wf)
                //     })
                // })
                


                // Pour compter la participation dans le temps 
                // de travail il faut un lien entre le secteur
                // et la personne
                // Comme l'absence compte dans le temps de travail
                // c'est aussi un secteur possédant des PersonSector
                // mais on peut le différencier avec is_real_sector
                m.allow_to_fill_person_sector = true;
                m.need_person_sectors.forEach((fake_sector) => {
                    data.all_wfs.forEach(function(wf) {
                        m.create_personSector(fake_sector, wf)
                    })
                })
                m.need_person_sectors.disabled = true

                // m.v2_allNeedsInfo = data.v2_allNeedsInfo;
                if(data.nothingTodo) {
                    m.blockArr = [];
                    return false;
                }
                const {wfMap,sectorMap,blockArr,all_wfs,total_info_amp,protectedIntervArr} = data
                clone({wfMap,sectorMap,blockArr,all_wfs,total_info_amp},m)
                m.addPersonSectorToSectorAndPerson()
                Debug.check_rotation()
                m.affectMinTimeBlock()
                m.change_step(30)
                Debug.check_rotation()

                //dire les zone horaire ou la personne peut travailler
                m.createInterventionFromPlanning()
                Debug.check_rotation()
                m.generate_forced_absence_and_presence()
                Debug.check_rotation()

                // saturday_test:{
                //     //faire croire que 10606 a fait plus de samedi que les autre 
                //     //en proportion des samedis possibles
                //     var dwfs = Debug.getWf(10606)
                //     dwfs.rotation.Saturday += 50
                //     dwfs.rotation_initial.Saturday += 50
                //     dwfs.rotation_possible.Saturday += 5
                //     dwfs.update_rotation_ratio('Saturday')
                //     var debug_forbid_saturday_wf = [];
                //     var saturdays = ['2018-03-31','2018-04-07']
                //     debug_forbid_saturday_wf.forEach(function(wf_id){
                //         saturdays.forEach((sat_date)=>{
                //             var wf = m.wfMap[wf_id]
                //             var li = wf.listInfo_sorted[5/*samedi*/]
                //             wf.lis_absence.add(li)//simuler une interdiction le samedi de travailler
                //         })
                //     })
                // }

                Debug.check_rotation()

                //affecter ce qui a été placer par le manager
                m.affectProtectedIntervs(protectedIntervArr)
                Debug.check_rotation()

                Debug.check_too_much_worked_all_wfs(m)
                // Debug.getWf(10575).rotation.Saturday = -1
                // Debug.getWf(10574).rotation.Saturday = -2
                // Debug.getWf(10191).rotation.Saturday = -1
                




                //affecter la semaine de reference si possible
                // m.affect_ref_intervs(params.inter_refs||{})
                zscores.push(m.v2_get_zscore_global())
                if(at(zscores,-1) >= at(zscores,-2)){
                    algo.affect_ref_intervs = at(zscores,-1) > at(zscores,-2) ? "LE SCORE A EMPIRÉ CE N'EST PAS NORMAL" : "LE SCORE EST INCHANGÉ"
                }
                Debug.check_too_much_worked_all_wfs(m)

                Debug.perfEnd('main_ScheduleMyEntity_1')
                // m.fillPossibleNeed();
                Debug.perfStart('v2_schedule_planning')
                Debug.check_rotation()
                //pénaliser le sous effectif en ignorant le surreffectif
                m.penalize_understaffing_unpenalise_overstaffing()
                Debug.checkTime()

                Debug.check_too_much_worked_all_wfs(m)
                m.v2_schedule_planning({
                    initial_planning: true,
                    // on a récupérer des journée de référence et il ne faut pas recalculer ces journées
                    // on s'autorise quand même à déplacer les extrémité une fois la semaine planifié si il y a trop ou pas assez d'heure faite
                    do_not_planif_on_planified_days: true, 
                    allow_to_move_time_between_days: true,
                    // debug_forbid_saturday_wf
                })
                Debug.check_all_wfs_pause(m)
                Debug.check_not_enough_worked_all_wfs(main)
                improve_sector({first_call: true})
                throw 'error';
                if(0){
                    zscores.push(m.v2_get_zscore_global())
                    if(at(zscores,-1) >= at(zscores,-2)){
                        algo.v2_schedule_planning = at(zscores,-1) > at(zscores,-2) ? "LE SCORE A EMPIRÉ CE N'EST PAS NORMAL" : "LE SCORE EST INCHANGÉ"
                    }

                    // m.unpenalize_understaffing_penalise_overstaffing();zscores.push(m.v2_get_zscore_global())

                    Debug.check_too_much_worked_all_wfs(m)
                    Debug.check_not_enough_worked_all_wfs(m)
                    //on a interdit des samedi à des fins de tests maintenant on les réautorise 
                    //pour voir si les rotation des samedis marchent
                    // fake_saturday_remove:{
                    //     m.generate_forced_absence_and_presence()
                    // }

                    Debug.checker()
                    //améliorer en déplaçant les extrémité des journées sans 
                    //tenir compte des secteurs
                    //@todo: uncomment
                    const r10 = get_rotation_score({logTable:true})
                    m.improve_rotation_all_wfs({get_rotation_score})
                    Debug.check_all_wfs_pause(m)
                    get_rotation_score({logTable:true})
                    // my_throw( 'stop')
                    const r11 = get_rotation_score()
                    zscores.push(m.v2_get_zscore_global())
                    if(r11 >= r10){
                        algo.v2_improve_rotation = (r11 > r10) ? "LES ROTATIONS ONT EMPIRÉ CE N'EST PAS NORMAL" : "LES ROTATIONS N'ONT PAS ÉTÉ AMÉLIORÉES"
                    }else{
                        if(at(zscores,-1) >= at(zscores,-2)){
                            algo.v2_improve_rotation = at(zscores,-1) > at(zscores,-2) ? "LE SCORE A EMPIRÉ MAIS C'EST NORMAL POUR AMÉLIORÉR LES ROTATIONS" : "LE SCORE EST INCHANGÉ"
                        }
                    }
                    Debug.check_all_wfs_pause(m)
                    Debug.check_not_enough_worked_all_wfs(main)
                    // à cause des rotation des samedi ou de la récupération d'un planning
                    // déjà existant certains jours sont en manque de personne, on va changer les 
                    // jours travaillés tout en gardant les heures
                    for(let j = 0;j < 2;++j){
                        let idw = m.improve_days_worked({
                            dont_make_rotation_worst: true,
                            get_zscore: () => m.global_score_ignoring_sectors()

                        })
                        if(!idw.improvement) break;
                    }
                    // throw 'stop';

                    zscores.push(m.v2_get_zscore_global())
                    if(at(zscores,-1) >= at(zscores,-2)){
                        algo.improve_days_worked = at(zscores,-1) > at(zscores,-2) ? "LE SCORE A EMPIRÉ CE N'EST PAS NORMAL" : "LE SCORE EST INCHANGÉ"
                    }
                    Debug.check_all_wfs_pause(m)
                    Debug.check_not_enough_worked_all_wfs(m)
                    //placer les personnes dans les secteurs
                    // return;
                    improve_sector({first_call: true})
                    zscores.push(m.v2_get_zscore_global())
                    if(at(zscores,-1) >= at(zscores,-2)){
                        algo.improve_sector_first_call = at(zscores,-1) > at(zscores,-2) ? "LE SCORE A EMPIRÉ CE N'EST PAS NORMAL" : "LE SCORE EST INCHANGÉ"
                    }
                    // controls_enabled(()=>{
                    //     if(window.per1 && window.per1.have != 1){
                    //         my_throw( 'erreur de besoin')
                    //     }
                    // })
                    Debug.check_not_enough_worked_all_wfs(m)

                        //réaméliorer le planning sans réduire les rotations
                        //d'abord dans la même journée
                        // move_extremity(['same_day'/*in_the_week*/, 'dont_make_rotation_worst'],{})
                        // Debug.check_not_enough_worked_all_wfs(m)

                        //puis dans la semaine
                    // for(let i = 0;i < 5;++i){
                    //     let is_better = move_extremity(['in_the_week','move_rotation'],{get_zscore: () => m.global_score_ignoring_sectors()})
                    //     // let is_better = move_extremity(['in_the_week'/*same_day*/, 'dont_make_rotation_worst'],{})
                    //     if(!is_better){
                    //         break
                    //     }
                    //     improve_sector({first_call: false})
                    // }
                    // zscores.push(m.v2_get_zscore_global())
                    // if(at(zscores,-1) >= at(zscores,-2)){
                    //     algo.move_extremity = at(zscores,-1) > at(zscores,-2) ? "LE SCORE A EMPIRÉ CE N'EST PAS NORMAL" : "LE SCORE EST INCHANGÉ"
                    // }
                    m.optimise_pause()
                    m.improve_day_extremities()
                    zscores.push(m.v2_get_zscore_global())
                    if(at(zscores,-1) >= at(zscores,-2)){
                        algo.move_extremity_v2 = at(zscores,-1) > at(zscores,-2) ? "LE SCORE A EMPIRÉ CE N'EST PAS NORMAL" : "LE SCORE EST INCHANGÉ"
                    }
                    m.optimise_pause()
                    zscores.push(m.v2_get_zscore_global())
                    if(at(zscores,-1) >= at(zscores,-2)){
                        algo.optimize_pause2 = at(zscores,-1) > at(zscores,-2) ? "LE SCORE A EMPIRÉ CE N'EST PAS NORMAL" : "LE SCORE EST INCHANGÉ"
                    }


                    Debug.check_not_enough_worked_all_wfs(m)
                    //on passe maintenant à un pas de 5 minutes
                    main.is_real_time_mode = true; 
                    m.change_step(5);zscores.push(m.v2_get_zscore_global());

                    //mettre les vrais horaires pour les protected
                    m.restore_protected_true_time(data.true_protected_informations)
                    // * les problèmes de pause de 15 minutes et les 10 ou 15 minutes
                    m.solve_too_long_pause()//pause de 1 heure vers 40 minutes
                    // * les ouvertures ainsi que les 10,15,20 minutes qui peuvent ne 
                    //pas être placé

                    m.place_missing_minutes_due_to_openning_closing()
                    zscores.push(m.v2_get_zscore_global())
                    if(at(zscores,-1) >= at(zscores,-2)){
                        algo.place_missing_minutes_due_to_openning_closing = at(zscores,-1) > at(zscores,-2) ? "LE SCORE A EMPIRÉ CE N'EST PAS NORMAL" : "LE SCORE EST INCHANGÉ"
                    }

                    Debug.check_not_enough_worked_all_wfs(m)          
                    Debug.perfEnd('v2_schedule_planning')
                        // if(true) {
                        //     // Debug.checkTime()
                        //     m.unpenalize_understaffing_penalise_overstaffing()
                        //     // on change l'entité de la personne
                        //     // sans laisser sur les bords de trop petite intervention
                        //     Debug.checker()
                        //     m.handle_exact_need()
                        //     Debug.perfStart('v2_todo_multiple_change_sector')
                        //     improve_sector({
                        //         do_not_let_too_small_inter: true
                        //     })
                        //     Debug.perfEnd('v2_todo_multiple_change_sector')
                            // if(true) {
                            //     console.iDebug19 = 0

                            //     var zscore_rotation = info.zscore_rotation

                            //     improve_sector({last_call: true})
                            //     var solve_overstaffing_problem = false //car on utilise handle_exact_need
                            //     if(solve_overstaffing_problem) {
                            //         m.solve_overstaffing_problem({
                            //             methods: ['extendTheSecondTowardLeft', 'move_pause'],
                            //             order: 'chronological'
                            //         })
                            //         m.solve_overstaffing_problem({
                            //             methods: ['extendTheFirstTowardRight', 'move_pause'],
                            //             order: 'reverse_chronological'
                            //         })
                            //         move_extremity(['in_the_week', 'dont_make_rotation_worst']);
                            //         m.solve_overstaffing_problem({
                            //             methods: ['extendTheSecondTowardLeft', 'move_pause'],
                            //             order: 'chronological',
                            //             debug: {
                            //                 startMinute: 1140,
                            //                 sector: 2522,
                            //                 time: 120,
                            //                 date: '2018-03-31'
                            //             }
                            //         })
                            //     }
                            // }
                        // }
                    // Debug.checkTime()
                    Debug.perfStart('reportProblem')
                    Debug.perfEnd('reportProblem')
                    m.logPersonSectorBug();
                    Debug.perfEnd('total_time') //@tott
                    console.green(Debug.timers['total_time'])
                    Debug.rapport_error_code()
                    m.create_report()
                    Debug.check_rotation()
                    Report.log()
                    Debug.check_not_enough_worked_all_wfs(m,/*strict*/ true)
                    Debug.check_intervention(m)
                    if(Report.length){
                        console.log(Report)
                    }
                    console.log(algo)
                    // utile pour comparer l'historique entre le mode débug et simulate_serveur
                    // il faut que ce soit le même
                    //@main2
                }
                if(Debug.histo_function){
                    console.log(JSON.stringify(Debug.iidebug_history).replace(/\{"j/g,'\n{"j'))
                }
                Debug.check_all_wfs_pause(m)
                Debug.check_not_enough_worked_all_wfs(m)
               
            }

            // pour v8js si on catch pas les erreurs parfois elle s'affiche pas
            if(v8js) {
                try {
                    f()
                } catch (e) {
                    console.error('erreur débugguer le script <a href="../../public/js/V8JS/indexplanif.html">ici </a>')
                    // console.error('erreur')
                    console.error(e.message)
                    console.error(e.stack)
                    return JSON.stringify({
                        errors: e.message
                    })
                }
            } else {
                f()
                console.log(planifObj)
            }
        }

        optimise_pause({all_wfs = this.all_wfs, listInfos} = {}){
            const m = this;
            const get_score = () => m.global_score_ignoring_sectors()

            const listInfo_pause = (li)=>{
                console.iidebug[87] = idnext(87)
                if(624340 == console.iidebug[87]){
                    Debug.logDayWf(li.first_interv)
                    Debug.log_period_info(li.date,10*60,18*60)
                    debugger
                }
                var score = get_score()
                for(var inter = li.first_interv;inter;inter = inter.next){
                    if(inter.is_pause_fake_protected()){
                        inter.unPause()
                        var rpatid = m.resolve_pause_and_time_in_day(inter,new opt_v2_schedule_planning({}))
                        break
                    }
                }
                controls_enabled(()=>{
                    const new_score = get_score()
                    if(new_score > score){
                        console.error(`Mais on a empiré en voulant améliorer les pauses console.iidebug[87] = ${console.iidebug[87]}`)
                    }else if(new_score < score){
                        console.green('succès optimisation pause')
                    }
                })
            }
            if(listInfos){
                listInfos.forEach(listInfo_pause)
            }else{
                let new_score = get_score()
                //réaméliorer les pause tant que ça diminue le score
                for(let i = 0;i < 5;++i){
                    let old_score = new_score
                    all_wfs.forEach(function(wf){
                        forOf(wf.listInfo_by_date,listInfo_pause)
                    })
                    new_score = get_score()
                    if(all_wfs.length <= 1 || new_score === old_score){
                        break
                    }
                }
            }
        }

        /**
        * Place au mieux le temps manquant avec des blocs d'au maximum max_bloc_size
        * Les gros bloc sont privilégié au petit sauf si l'extremité d'un gros bloc peu être déplacer en améliorant le score
        */
        optimum_schedule_on_extremity({
            max_bloc_size: ori_max_bloc_size, wfs = this.all_wfs,step = conf.minTime,score,
            date,
            upper_nb_minutes_wanted_at_final: ori_upper_nb_minutes_wanted_at_final,
            do_not_check_pause_and_time = false,//ne pas appeler resolve_pause_and_time_in_day
            avoid_closing_opening_problem = false,
            put_in_main_sector = true,//pour l'instant que true est possible
            opt_min_max,//min et max pour les heures ou interdire des ouverture
            only_these_date,
            opt_min_max_by_date,
            repair_pause_at_end = false,
            can_be_worse = false,
            max_recursivity = 1//nombre de fois que la fonction peut se rappeler
        }){
            const recursive_call = ()=>{
                if(arguments[0].max_recursivity <= 0){
                    return false
                }
                --arguments[0].max_recursivity
                m.optimum_schedule_on_extremity.apply(m,arguments)
            }
            const retF = (error_code)=>{ return {error_code}}
            controls_enabled(()=>{
                if(opt_min_max && opt_min_max_by_date){
                    my_throw('il faut choisir entre les deux')
                }
                if(!can_be_worse && !ori_max_bloc_size) {
                    my_throw("Quand on utilise le mode can_be_worse à false il faut dire la taille du SEUL et UNIQUE bloc qui a été enlevé")
                }
            })
            const get_score = () => m.global_score_ignoring_sectors()
            const m = this;
            const next_pers = function(){}
            var cpt_deb = 0;
            wfs.forEach((wf)=>{
                controls_enabled(()=>{
                    if(wf.getNumberOfDaysRemained()){
                        my_throw(1103)
                    }
                })
                const upd_original_minu = function(li){
                    if(li.is_worked()){
                        original_minutes[li.date] = {start_minute: li.first_minu_worked, end_minute: li.last_minu_worked}
                    }
                }
                const original_minutes = {}
                if(date){
                    upd_original_minu(wf.listInfo_by_date[date])
                }else{
                    wf.listInfo_sorted.forEach(upd_original_minu)
                }
                var backup_week = wf.backup_week()
                Debug.ii98 = console.iDebug
                if(530127 == Debug.ii98){debugger}
                let max_bloc_size = 0;
                const upper_nb_minutes_wanted_at_final = ori_upper_nb_minutes_wanted_at_final || wf.nbMinutesMin
                forOf(wf.listInfo_by_date, (li) => max_bloc_size = Math.max(max_bloc_size,li.max - li.worked) );
                max_bloc_size = lower(ori_max_bloc_size < max_bloc_size ? ori_max_bloc_size : max_bloc_size);
                const score_ori = controls_enabled(()=> get_score())
                const optimal_schedule = (bloc_size,max_bloc_size_is_also_min_size,nb_supt) => {
                    const {starts,ends} = m.get_amps_on_extremity(wf,bloc_size,date,only_these_date)
                    for(var i=0;i < 2;++i){
                        console.iidebug[93] = idnext(93)
                        if(530632 == console.iidebug[93]){
                            debugger
                        }
                        var ret = m.place_missing_minutes_wf({wf,starts,ends,max_bloc_size: bloc_size, max_bloc_size_is_also_min_size,
                            step,opt_min_max,opt_min_max_by_date,avoid_closing_opening_problem,upper_nb_minutes_wanted_at_final,nb_supt,can_be_worse})
                        
                        // si le temps restant à la personne est trop grand alors c'est peut être du à la condition nb_supt que
                        // l'on désactive
                        if(nb_supt !== void 8 && bloc_size == conf.minTime && lower(wf.nbMinutesMin - wf.nb_minu_worked) > 0){
                            nb_supt = void 8
                        }else{
                            break
                        }
                    }
                    return ret
                }
                const get_hashcode = ()=>{
                    let str = ''
                    forOf(wf.listInfo_by_date,(li) => {
                        str += li.first_minu_worked+'_'+li.last_minu_worked
                    })
                }
                // for1: for(let k = 0; k < 3;++k){
                let diff = upper_nb_minutes_wanted_at_final - upper(wf.nb_minu_worked)
                if(diff <= 0) {
                    return
                    // break for1;
                }else if(diff < max_bloc_size){
                    max_bloc_size = lower(diff)
                }
                //le plus gros bloc que l'on peut placer dans une journée d'un coup
                Debug.check_too_much_worked(wf)
                let {new_max_bloc_size} = optimal_schedule(max_bloc_size,false)
                controls_enabled(()=>{
                    if(!can_be_worse){
                        const new_score = get_score()
                        // console.log(cpt_deb)
                        if(new_score > score){
                            my_throw( "Si le score a empiré (augmenté) c'est que vous avez déplanifié plus que un seul bloc avant d'appeler optimum_schedule_on_extremity ou bien déplanifier autour d'une pause")
                        }
                    }
                })
                Debug.check_too_much_worked(wf)
                //il faut pouvoir comparer si il y a eu des changement donc créer unechaine unique
                let hashcode = get_hashcode()
                for(let bloc_size = max_bloc_size;bloc_size >= 0;){
                    ++cpt_deb;
                    if(console.iidebug[92] == 38588 && cpt_deb == 1){
                        debugger
                    }
                    
                    Debug.check_too_much_worked(wf)
                    // let nb_supt = wf.optimal_schedule_on_extremity__nb_supplement_time(wf)
                    // enlever sur toutes les extrémités que l'on a agrandi un temps de unplanif_time sans enlever plus que ce l'on avait ajouté
                    let {nb_sub_t} = m.optimum_schedule_on_extremity__unplanif_extremity({wf,do_not_increase_supt: true,original_minutes,unplanif_time: bloc_size })
                    // replacer au mieux en des blocs plus petit, ça peut potentiellement revenir là ou c'était déjà
                    // si c'est l'optimum
                    optimal_schedule(bloc_size,true,nb_sub_t)
                    // if(bloc_size == step && upper(wf.nb_minu_worked) < wf.nbMinutesMin){
                    //     debugger
                    // }
                    Debug.check_too_much_worked(wf)
                    let new_hashcode = get_hashcode()
                    if(new_hashcode === hashcode){//on est stable pour la taille de bloc bloc_size maintenant passer à des blocs plus petits
                        hashcode = new_hashcode
                        bloc_size -= step
                    }
                }
                controls_enabled(()=>{
                    if(!can_be_worse){
                        const new_score = get_score()
                        // console.log(cpt_deb)
                        if(new_score > score){
                            my_throw( "Si le score a empiré (augmenté) c'est que vous avez déplanifié plus que un seul bloc avant d'appeler optimum_schedule_on_extremity ou bien déplanifier autour d'une pause")
                        }
                    }
                })
                if(!do_not_check_pause_and_time){
                    if(upper(wf.nb_minu_worked) < wf.nbMinutesMin){
                        if(wf.getNumberOfDaysRemained()){
                            return retF(1103)
                        }
                        return retF(1102)
                    }
                    // controls_enabled(()=>{
                    //     if(upper(wf.nb_minu_worked) < wf.nbMinutesMin){
                    //         my_throw('pas assez de minute')
                    //     }
                    // })
                    controls_enabled(()=>{
                        if(upper(wf.nb_minu_worked) < wf.nbMinutesMin){
                            my_throw('pas assez de minute')
                        }
                    })
                    controls_enabled(()=>{
                        if(lower(wf.nb_minu_worked - wf.nbMinutesMin) > 0){
                            my_throw('trop de minute')
                        }
                    })
                    forOf(wf.listInfo_by_date, (li) => {
                        console.iidebug[84] = idnext(84)
                        if(163143 ==  console.iidebug[84]){
                            debugger
                        }
                        const check_ori_minu = function(){
                            controls_enabled(()=>{
                                const ori_info = original_minutes[li.date]
                                if(ori_info){
                                    controls_enabled(()=>{
                                        if(not_null(ori_info.start_minute) < li.first_minu_worked){
                                            my_throw('unplanif a été trop puissant 3')
                                        }
                                        if(ori_info.end_minute > li.last_minu_worked){
                                            my_throw('unplanif a été trop puissant 4')
                                        }
                                    })
                                }
                            })
                        }
                        check_ori_minu()
                        if(repair_pause_at_end){
                            m.main_resolve_pause_unrespected({intervention: wf.listInfo_by_date[li.date]})
                            if(lower(wf.nb_minu_worked - wf.nbMinutesMin) > 0){
                                recursive_call()
                            }
                        }
                        controls_enabled(()=>{
                            if(lower(wf.nb_minu_worked - wf.nbMinutesMin) > 0){
                                my_throw('trop de minute')
                            }
                        })
                        // var rpatmid = m.resolve_pause_and_time_in_day(li.first_interv,new opt_v2_schedule_planning({
                        //     from: "optimum_schedule_on_extremity",
                        //     do_not_call_optimum_schedule_on_extremity: true,//évite la récursivité croisé
                        //     include_this_amplitude: original_minutes[li.date]
                        // }))
                        // check_ori_minu()
                    })
                    if(score < get_score()){
                        backup_week.restore()
                    }
                }
                controls_enabled(()=>{
                    if(!isFinite(score_ori) || score_ori < get_score()){
                        console.error('On a empiré le score')
                    }
                })                
            // }
            })
            return retF(0)
        }


        // optimal_schedule_on_extremity__nb_supplement_time(wf){
        //     var nb_open_close_supp_time = 0;
        //     wf.listInfo_sorted.forEach(function(li){
        //         if(li.first_interv_sector){
        //             const open_info = wf.opening_info({start_minute: li.first_minu_worked,date,date: li.date})
        //             if(open_info.is_supplement_time()){
        //                 ++nb_open_close_supp_time
        //             }
        //             const close_info = wf.closing_info({end_minute: li.last_minu_worked,date: li.date}) 
        //             if(close_info.is_supplement_time()){
        //                 ++nb_open_close_supp_time
        //             }
        //         }
        //     })
        //     //retourne le nombre de fermeture ou ouverture avec temps supplémentaire possible
        //     return nb_open_close_supp_time + ~~((wf.nbMinutesMin - wf.nb_minute_worked) / o.time_opening)
        // }
        /*
        * ORIGINAL_minute contient pour chaque date les heure d'origine que l'on ne doit pas déplanifier
         */
        optimum_schedule_on_extremity__unplanif_extremity({wf,original_minutes,unplanif_time,do_not_increase_supt}){
            const refF = ()=>{return {nb_sub_t} }
            if(unplanif_time <= 0) return refF();
            var nb_sub_t = 0;//nombre d'extremités de journée necessitant un temps supplémentaire que l'on a le droit de replacer
            forOf(original_minutes, function(origi,date){
                const li = wf.listInfo_by_date[date]
                const unplanif = (start_minute,end_minute) => {
                    let inter = new Intervention({
                        date,
                        sector: null,
                        startMinute: start_minute,
                        endMinute: end_minute,
                        rules: {safe_insertion: true}
                    })
                    wf.addInterv(inter)
                }
                // On déplanifie mais sans enlever les heures entre start_minute et end_minute de l'origine
                const end_minute = li.first_minu_worked + unplanif_time
                if(end_minute <= origi.start_minute){
                    let unplanif_start = true
                    if(do_not_increase_supt){
                        let open_info = wf.opening_info({start_minute: li.first_minu_worked,date: li.date})
                        let was_supt = open_info.is_supplement_time()
                        nb_sub_t += !!was_supt
                        if(!was_supt){
                            open_info = wf.opening_info({start_minute: end_minute,date: li.date})
                            if(open_info.is_supplement_time()){
                                unplanif_start = false
                            }
                        }
                    }
                    if(unplanif_start){
                        unplanif(li.first_interv.startMinute, end_minute)
                    }
                }
                const start_minute = li.last_minu_worked - unplanif_time
                controls_enabled(()=>{
                    if(!isFinite(start_minute) || !isFinite(end_minute)){
                        my_throw( 'not finite')
                    }
                })
                if(start_minute >= origi.end_minute){
                    let unplanif_end = true
                    if(do_not_increase_supt){
                        let close_info = wf.closing_info({end_minute: li.last_minu_worked,date: li.date})
                        let was_supt = close_info.is_supplement_time()
                        nb_sub_t += !!was_supt
                        if(!was_supt){
                            close_info = wf.closing_info({end_minute: start_minute,date: li.date})
                            if(close_info.is_supplement_time()){
                                unplanif_end = false
                            }
                        }
                    }
                    if(unplanif_end){
                        unplanif(start_minute, li.last_interv.endMinute)
                    }
                }
                controls_enabled(()=>{
                    if(not_null(origi.start_minute) < li.first_minu_worked){
                        my_throw('unplanif a été trop puissant')
                    }
                    if(not_null(origi.end_minute) > li.last_minu_worked){
                        my_throw('unplanif a été trop puissant 2')
                    }
                })
            })
            return refF();
        }

        get_amps_on_extremity(wf,max_bloc_size, date,only_these_date){
            const date_ori = date
            const starts = {}, ends = {}, m = this;

            wf.listInfo_sorted.forEach(function(li){
                let dt = li.date
                if(date_ori && date_ori != dt || only_these_date && !only_these_date.includes(dt)){
                    return;
                }
                var fmw = li.first_minu_worked
                if(fmw){
                    let poses_start = m.getPosArrInArray(dt, li.first_interv.startMinute, fmw)
                    let common_start_end = (amp,diff,arr)=>{
                        //si au moins un secteur est ouvert
                        if(amp.startMinute < li.min_start_day || amp.endMinute > li.max_end_day){
                            controls_enabled(()=> my_throw('Ceci est censé avoir déjà était filtré'))
                        }
                        let entity_is_open = amp.periods.some(filter_workable)
                        if(!entity_is_open) return true
                        if( !(diff > 0 && diff <= max_bloc_size) ){
                            return true
                        }
                        if(!arr[dt]) arr[dt] = []
                        arr[dt].push(amp)
                    }
                    poses_start.forEach((pos)=>{
                        let amp = m.total_info_amp[pos]
                        var diff_start = li.first_minu_worked  -  amp.startMinute
                        common_start_end(amp,diff_start,starts)
                    })
                    let poses_end = m.getPosArrInArray(dt, li.last_minu_worked, li.last_interv.endMinute)
                    poses_end.forEach((pos)=>{
                        let amp = m.total_info_amp[pos]
                        var diff_end =  amp.endMinute - li.last_minu_worked
                        common_start_end(amp,diff_end,ends)
                    })
                }
            })
            // m.v2_get_all_amp_open().forEach(function(amp){
            //     if(date_ori && date_ori != amp.date || only_these_date && !only_these_date.includes(amp.date)){
            //         return;
            //     }
            //     const {date} = amp, lid = wf.listInfo_by_date[date]
            //     if(!lid || !lid.first_minu_worked) return false;//la journée doit être travaillé
            //     //les horaires doivent être autorisé par le planning de la personne
            //     if(amp.startMinute < lid.min_start_day || amp.endMinute > lid.max_end_day) return false;
            //     var diff_start = lid.first_minu_worked  -  amp.startMinute
            //     var diff_end =  amp.endMinute - lid.last_minu_worked
            //     if(diff_start > 0 && diff_start <= max_bloc_size){
            //         ;(starts[date] = starts[date] || []).push(amp)
            //     } 
            //     if(diff_end > 0 && diff_end <= max_bloc_size){
            //         ;(ends[date] = ends[date] || []).push(amp)
            //     }
            // })
            return {starts,ends}
        }

        /*
        * place les plus gros blocs de minutes possibles pour chaque taille de bloc on prend le meilleur
         */
        place_missing_minutes_wf({wf,starts,ends,max_bloc_size,max_bloc_size_is_also_min_size,step = conf.minTime,
            opt_min_max = {}, avoid_closing_opening_problem,upper_nb_minutes_wanted_at_final,nb_supt,can_be_worse,opt_min_max_by_date }){
            const lower_remaining_time = () => lower(upper_nb_minutes_wanted_at_final - wf.nb_minu_worked)
            const stop_cond = (time = 0) => {
                const lower_remaining_time_v = lower_remaining_time()
                return lower_remaining_time_v < time || lower_remaining_time_v <= 0
            }

            //retourne la somme de la productivité sur les extremités des journées
            var sum = (array,max_bloc_size)=>{
                let sum_v = 0;
                let bloc_size = 0
                array.some((amp)=>{
                    if(bloc_size >= max_bloc_size) return STOP_LOOP
                    bloc_size += amp.endMinute - amp.startMinute
                    sum_v += amp.v2_attractivity()
                })
                return sum_v
            }
            // const _order = []
            var lid = wf.listInfo_by_date;
            // const dates_done = new Set();
            Debug.check_too_much_worked(wf)
            console.iidebug[90] = idnext('90')
            for(var bloc_size = max_bloc_size; bloc_size > 0;){
                console.iidebug[90] = idnext('90')
                if(617265 == console.iidebug[90] || Debug.debug88 && bloc_size == step){
                    debugger
                }
                Debug.check_too_much_worked(wf)
                const start_arr = [],end_arr = []
                // max_i = Math.bloc_size/step
                forOf(starts,(start,date)=>{
                    start_arr.push({
                        date,
                        posi: 'start',
                        startMinute: 
                        bloc_size,
                        attractivity: sum(start,bloc_size)
                    })
                })
                forOf(ends,(end,date)=>{
                    end_arr.push({
                        date,
                        posi: 'end',
                        bloc_size,
                        attractivity: sum(end.reverse(),bloc_size)
                    })
                })
                var all_arr = start_arr.concat(end_arr)
                all_arr.sort((a,b)=> b.attractivity - a.attractivity)//plus forte attractivité au début
                let stop_cond_last;
                const some3 = (STOP_LOOP)=>{Debug.check_too_much_worked(wf); return STOP_LOOP}
                some3: all_arr.some(function(o){
                    console.iidebug[83] = idnext('83')
                    if(29930 == console.iidebug[83]){
                        debugger;
                    }
                    //placer les plus grosse intervention possible
                    //et prendre la zone horaire avec la meilleur productivité parmis ces grosses interventions
                    var obj_inter = {
                        date: o.date,
                        sector: main.blockArr[0].sectorEntArr[0],
                        rules: {
                            safe_insertion: true,
                        }
                    }
                    if(o.posi == 'start'){
                        obj_inter.endMinute = wf.listInfo_by_date[o.date].first_minu_worked
                        obj_inter.startMinute = obj_inter.endMinute - bloc_size
                    }else if(o.posi == 'end'){
                        obj_inter.startMinute = wf.listInfo_by_date[o.date].last_minu_worked
                        obj_inter.endMinute = obj_inter.startMinute + bloc_size
                    }
                    if(opt_min_max_by_date){
                        opt_min_max = opt_min_max_by_date[o.date]
                    }
                    let error_code = wf.get_start_end_minu_error({opt_min_max,start_minute: obj_inter.startMinute,
                        end_minute: obj_inter.endMinute,date :o.date,whole_day:false})
                    if(error_code){
                        if(max_bloc_size == conf.minTime){
                           inc(def(Debug,'error_codes_place_missing_minutes_wf',{}),error_code)
                        }
                    }
                    const respect_rules = ()=> lower_remaining_time() >= 0 && wf.respect_max_time(o.date)
                    if(!error_code && respect_rules()){
                        let was_supt = false
                        if(avoid_closing_opening_problem || nb_supt !== void 8){
                            const open_info = wf.opening_info({
                                date:obj_inter.date,
                                start_minute: obj_inter.startMinute
                            })
                            const close_info = wf.closing_info({
                                date:obj_inter.date,
                                end_minute: obj_inter.endMinute
                            })
                            //was_supt est vrai si la journée était une ouverture ou fermeture pour le temps 
                            //supplémentaire et que l'on gère le nombre de temps supplémentaire autorisé 
                            was_supt = nb_supt !== void 8
                            if(nb_supt > 0 == false){//attention subt est facultatif donc ne pas mettre nb_supt <= 0 à la place
                                //si ça nous met sur une heure ou il faut ajouter du temps ne pas le faire
                                if(open_info.is_supplement_time() || close_info.is_supplement_time()){
                                    return some3()
                                }

                                //on peut placer un temps supplémentaire en ouverture ou fermeture de moins
                                if(was_supt){
                                    --nb_supt
                                }
                            }
                        }
                        const interv2 = new Intervention(obj_inter)
                        const adi_inter = wf.addInterv(interv2)
                        if(! respect_rules()){
                            // annuler le dernier ajout qui à cause d'un quart d'heure
                            // en fermeture surement à fait dépasser le temps autorisé
                            // 
                            // ou bien le temps max dans la journée est dépassé
                            obj_inter.sector = null
                            
                            //on doit forcer à remettre comme avant même si ça respecte pas les règles
                            obj_inter.rules = {
                                megaForcedInsert: true, // ne vérifier aucune règle
                            }
                            const interv3 = new Intervention(obj_inter)
                            const adi_inter3 = wf.addInterv(interv3)
                            if(!respect_rules()){
                                controls_enabled(()=>{
                                    my_throw( "le temps ajouté en trop n'arrive pas à être enlevé")
                                })
                            }else if(was_supt){
                                ++nb_supt
                            }
                        }else{
                            //on ne peut plus placer de bloc de bloc_size
                            if(stop_cond(bloc_size)) return some3(STOP_LOOP);
                        }
                        // console.green('error code ')
                    }
                    some3()
                })
                // if(max_bloc_size_is_also_min_size){
                //     controls_enabled(()=>{
                //         my_throw( "c'est étrange que l'on ai pas réussi à placer toute les heures alors qu'elles étaient déjà présentes")
                //     })
                // }
                if(max_bloc_size_is_also_min_size){
                    break
                }
                bloc_size = Math.min(bloc_size - step, lower_remaining_time())
                Debug.check_too_much_worked(wf)
            }
            Debug.check_too_much_worked(wf)
            return {}
        }

        improve_day_extremities({wfs = this.all_wfs,debugu} = {}){
            const m = this;
            const get_score = () => m.global_score_ignoring_sectors()

            var mega_ligth_activated = false
            if(mega_ligth_activated){
                const mega_ligth = (wf)=>{
                    wf.listInfo_sorted.forEach((li,i)=>{

                        const date = li.date
                        var inter = li.first_interv_sector

                        if(inter /* && !debug || (debug && [0].includes(i))*/ ){
                            const restore_day = m.v2_try_a_change({person:wf,date})
                            const score_ori = get_score()
                            const nb_minu = wf.nb_minu_worked
                            let opt25 = {
                                inter: li.first_interv,
                                time_to_rem_by_side: conf.minTime,
                                let_space_around_pause: true,
                                reduce_end: true, //ne réduire que la fin
                            }
                            console.iidebug[91] = idnext('91')
                            if(console.iidebug[91] == 0){
                                debugger;
                            }
                            m.reduce_start_or_end_of_day(opt25)
                            let osoe = m.optimum_schedule_on_extremity({wfs: [wf],score: score_ori,only_these_date:[date],can_be_worse: false, max_bloc_size: lower(nb_minu - wf.nb_minu_worked) }) 
                            const new_score = get_score()
                            if(new_score > score_ori){
                                console.error('On a empiré le score avec une demi-heure')
                                restore_day()
                            }
                        }
                    })
                }
            }
            // m.all_wfs.forEach(function(wf){
            //     mega_ligth(wf)
            // })

            const method_ligth = (wf)=>{
                Debug.check_not_enough_worked_all_wfs(m)
                const backup_week = wf.backup_week()
                const score_ori = get_score()
                var debug = false;
                var only_these_date
                // enlever une demi-heure à chaque extremité
                console.iidebug[88] = idnext('88')
                if([331156].includes(console.iidebug[88])){
                    Debug.logDayWf(wf)
                    Debug.debug88 = true
                    debug = true
                    // only_these_date = ['2019-02-04']
                    debugger;
                }else{
                    Debug.debug88 = false
                }
                debugu && debugu(wf)
                let opt_min_max_by_date = {}
                wf.listInfo_sorted.forEach((li,i)=>{
                    var inter = li.first_interv_sector
                    if(inter /* && !debug || (debug && [0].includes(i))*/ ){
                        // On optimise la pause car cela va peut être éviter de modifier les heures
                        // d'arrivées et de départ des personnes pour la personne suivante
                        m.optimise_pause({listInfos: [li]})
                        var hours_limit = li.get_hours_rules_to_keep_same_pauses()
                        opt_min_max_by_date[li.date] = hours_limit.opt_min_max

                        var nb_minu = wf.nb_minu_worked
                        m.reduce_start_or_end_of_day({
                            inter,
                            opt3: new opt_v2_schedule_planning({
                                include_this_amplitude: hours_limit.amplitudes_to_include
                            }),
                            reduce_both_side: true,
                            time_to_rem_by_side: conf.minTime,
                        })
                        // if(nb_minu <= wf.nb_minu_worked){
                        //     controls_enabled(()=>{
                        //         my_throw("le temps n'a pas été réduit")
                        //     })
                        // }
                    }
                })
                // puis les replacer en commençant par des gros blocs
                let osoe = m.optimum_schedule_on_extremity({wfs: [wf],score: score_ori,only_these_date,
                    can_be_worse: true,opt_min_max_by_date})
                const new_score = get_score()
                if(new_score > score_ori){
                    backup_week.restore()
                    // my_throw( "Mais il est nul cet algo il empire le score qu'on veut diminuer")
                }else if(new_score < score_ori){
                    console.green('On a amélioré le score 1')
                }
                Debug.check_not_enough_worked_all_wfs(m)
            }


            //enlever des demi-heures partout puis les replacer en bloc plus gros ou pas
            wfs.forEach((wf)=>{
                method_ligth(wf)
            })
            const not_enough_worked = (wf)=>{
                if(upper(wf.nb_minu_worked) < wf.nbMinutesMin){
                    controls_enabled(()=>{
                        my_throw("Ceci n'est pas normal")
                    })
                    m.solve_not_enough_worked([wf])
                }
            }

            //enlève un bloc du meilleur coté de la journée puis le décompose si c'est mieux
            wfs.forEach((wf)=>{
                not_enough_worked(wf)
                Debug.check_not_enough_worked_all_wfs(m)
                const score_ori = get_score()
                const backup_week = wf.backup_week()
                special_some(wf.listInfo_sorted,(li)=>{
                    not_enough_worked(wf)
                    // On optimise la pause car cela va peut être éviter de modifier les heures
                    // d'arrivées et de départ des personnes pour la personne suivante
                    m.optimise_pause({listInfos: [li]})
                    if(li.first_interv_sector){
                        console.iidebug[92] = idnext('92')
                        if(console.iidebug[92] == 512826){
                            Debug.logDayWf(wf)
                            debugger;
                        }
                        Debug.check_too_much_worked(wf)
                        const score_ori2 = get_score()
                        var {first_minu_worked,last_minu_worked} = li
                        var hours_limit = li.get_hours_rules_to_keep_same_pauses()
                        var {opt_min_max} = hours_limit
                        //réduire la journée à son minimal en enlevant en priorité au coté qui en a le moins besoins
                        let opt25 = {
                            inter: li.first_interv,
                            let_space_around_pause: true,
                            reduce_time_at_minimal: true,
                            do_not_reduce_both_side: true, //ne pas réduire par la fin et le début
                            do_not_increase_supt: true,
                            opt3: new opt_v2_schedule_planning({
                               include_this_amplitude: hours_limit.amplitudes_to_include
                            })
                        }
                        const nb_minu = wf.nb_minu_worked
                        m.reduce_start_or_end_of_day(opt25)
                        controls_enabled(()=>{
                            if(first_minu_worked != li.first_minu_worked && last_minu_worked != li.last_minu_worked){
                                my_throw("on avait demandé à ne réduire qu'un seul coté")
                            }
                        })
                        let max_bloc_size = lower(nb_minu - wf.nb_minu_worked)
                        let osoe;
                        if(max_bloc_size){
                            // puis replanifier ce temps sur la semaine, can_be_worse est à false car quand on ne déplanifie 
                            // qu'une seul extremité l'algo ne peut par empirer le score s'il est bien construit
                            osoe = m.optimum_schedule_on_extremity({wfs: [wf],score: score_ori,can_be_worse: false, max_bloc_size,opt_min_max})
                            Debug.check_too_much_worked(wf)
                        }
                        controls_enabled(()=>{
                            const new_score = get_score()
                            if( upper(wf.nb_minu_worked) != upper(wf.nbMinutesMin)){
                                my_throw('mais pk toutes les minutes ne sont placés')
                            }
                            if(new_score > score_ori2 ){
                                my_throw( "Mais il est nul cet algo il empire le score qu'on veut diminuer")
                            }else if(new_score < score_ori2){
                                console.green('On a amélioré le score 2')
                            }
                        })
                    }
                    return false
                },()=>{
                    not_enough_worked(wf)
                    console.iidebug[93] = idnext('93')
                })
                const new_score = get_score()
                if(new_score > score_ori){
                    backup_week.restore()
                    my_throw( "Mais il est nul cet algo il empire le score qu'on veut diminuer")
                }else if (new_score < score_ori){
                    console.green('method 2 improvement extremities')
                }
                Debug.check_not_enough_worked_all_wfs(m)
            })
        }

        create_report(){
            var m = this
            too_much_worked_and_consecutive_absence:{
                m.all_wfs.forEach((wf)=>{
                    if(wf.nb_minu_worked > wf.nbMinutesMin){
                        Report.add({
                            name: 'too_much_worked',
                            args: {wf_id:wf.wfId}
                        })
                    }
                    if(wf.nb_minu_worked < wf.nbMinutesMin){
                        Report.add({
                            name: 'not_enough_worked',
                            args: {wf_id:wf.wfId}
                        })
                    }
                    var is_abs_ok = m.is_possible_to_add_this_new_day_as_worked(wf,[])
                    if(!is_abs_ok){
                     Report.add({
                         name: 'consecutive_abs',
                         args: {wf_id:wf.wfId}
                     }) 
                 }
             })
            }
            needs_not_optimal: {
                m.v2_get_all_amp_open().forEach((amp) => {
                    amp.periods.forEach((per2) => {
                        if(per2.startMinute%30 !== 0) return 
                        if(per2.min_need !== per2.max_need) return
                        var sctr = per2.v2_sector
                        if(sctr.minTimeSector != sctr.maxTimeSector) return
                        if(per2.min_need == per2.max_need){
                            if(per2.have > per2.need){
                                Report.add({
                                    name: 'too_much_person',
                                    args: {start_hour: per2.startMinute,sector: per2.v2_sector.id}
                                })
                            }else if(per2.have < per2.need){
                                Report.add({
                                    name: 'not_enough_person',
                                    args: {start_hour: per2.startMinute,sector: per2.v2_sector.id}
                                })
                            }                     
                        }
                    })
                })
            }
        }
        improve_rotation_all_wfs({get_rotation_score}){
            var m = this;
            var backups = {}
            m.all_wfs.forEach((wf)=>{
                wf.intervsRacines.forEach((inter)=>{
                    const undo_day = m.v2_try_a_change({
                        // get_array_of_backup: true,
                        date: inter.date,
                        person: inter.person
                    })
                    if(inter.first_minu_worked){
                        backups[`${inter.person.wfId}_${inter.date}`] = {
                            is_opening: wf.opening_info({start_minute: inter.first_minu_worked,date: inter.date}).before_or_equal('rotation') ,
                            is_closing: wf.closing_info({end_minute: inter.last_minu_worked,date: inter.date}).after_or_equal('rotation'),
                            last_minu_worked: inter.last_minu_worked,
                            first_minu_worked: inter.first_minu_worked,
                            undo_day
                        }
                    }
                })
            })

            const detect_incoherence = function(callback){
                m.all_wfs.forEach((wf)=>{
                    // Pour les rotations:
                    //     Si au final sur une journée travaillé qui était déjà travaillés avant 
                    //         les personnes ont eu leurs horaires changé alors qu'ils ont le même nombre d'ouverture et fermeture dans la journée alors
                    //     remettre les horaires d'avant pour cette journée
                    wf.intervsRacines.forEach((inter)=>{
                        var backup = backups[`${inter.person.wfId}_${inter.date}`]
                        if(backup !== void 8 && inter.first_minu_worked){//si le jour était travaillé avant et qu'il est encore
                            //si les heures ont été réduite sur une journée alors que les rotations n'ont pas changé ce n'est pas normal
                            if(backup.last_minu_worked > inter.last_minu_worked || backup.first_minu_worked < inter.first_minu_worked){
                               //mais que ça n'a rien changé aux ouvertures alors restorer la journée
                                if(wf.is_rotation_opening(inter.date) == backup.is_opening && backup.is_closing == wf.is_rotation_closing(inter.date) ){
                                    callback(backup,inter)
                                }
                            }
                        }
                    })
                })
            }
            // move_extremity(['in_the_week','move_rotation'],{get_zscore: () => m.global_score_ignoring_sectors()})
            //améliorer les rotation tant que c'est possible
            const debug_callback = ()=>{
                controls_enabled(()=>{
                    detect_incoherence((backup,inter)=>{
                        my_throw("Les rotations ont été changées pour qu'une personne ne fasse que recevoir un type de rotation ou bien que envoyer donc ceci n'est pas possible")
                    })
                })
                // if(m.wfMap[10324].rotation['Opening'] == 5){
                //     debugger
                // }
            }
            for(var l=0;l < 3;++l) {
                var info = m.v2_improve_rotation({
                    logTable: true,
                    log_table_before: true,
                    get_rotation_score,
                    debug_callback
                }) //améliore la rotation des ouverture et des fermeture et samedi
                idnext('no_name16')
                // my_throw( 'stop')
                // console.error('iDebug : ' + console.iDebug19)
                info.get_rotation_score({
                    logTable: false
                })
                if(!info.improvement) break
            }
            debug_callback()
            //annuler ci des changements inutile ont eux lieu malgrès toutes nos précautions
            detect_incoherence((backup,inter)=>{
                backup.undo_day()
            })
            //juste au cas où on va vérifier et corriger les heures des personnes
            m.solve_not_enough_worked(m.all_wfs,{})
            m.reduce_time_worked_to_respect_max_time(m.all_wfs)
        }

        /**
         * pénaliser le fait d'être en surreffectif par rapport au fait de ne pas respecter le minimum
         */
        unpenalize_understaffing_penalise_overstaffing() {
            var m = this;
            if(!m.is_penalise_overstaffing) {
                m.is_penalise_overstaffing = true;
                // m._understaffing_ratio = m.min__understaffing_ratio * 2
                m.set_zscore_as_false()
            }
        }

        get_between(val, min, max){
            return Math.max(Math.min(val, max), min)
        }
        /**
         * Dépénaliser le fait d'être en surreffectif par rapport au fait de ne pas respecter le minimum
         */
         penalize_understaffing_unpenalise_overstaffing() {
            var m = this;
            if(m.is_penalise_overstaffing) {
                m.is_penalise_overstaffing = false;
                // m._understaffing_ratio = 10
                m.set_zscore_as_false();
            }
        }

        get_args_for_schdl_plan(wf,dates_to_remove,except_eaonp = []){
            console.iidebug[72] = idnext('gafsp')
            var t;
            var days_worked = {},exact_amplitude_or_not_planified = {},forced_ampli = {}
            ;dates_to_remove.forEach(function(date){
                console.iidebug[73] = idnext('gafsp'+wf.wfId+'__'+date)
                t = wf.listInfo_by_date[date]
                if(t && t.is_worked()){
                    days_worked[date] = {
                        worked : t && t.worked,
                        first_minute_worked: t && t.first_minu_worked,
                        last_minute_worked: t && t.last_minu_worked
                    };
                    var key = t.first_minu_worked + '_'+t.last_minu_worked
                    forced_ampli[key] = forced_ampli[key] || {
                        start_minute : t.first_minu_worked,
                        end_minute : t.last_minu_worked,
                        nb_to_place: 0,
                        nb_minutes: t.worked
                    }
                    ++forced_ampli[key].nb_to_place
                    //le samedi est interdit donc ça ne peut pas être une date
                    //sur laquel il faudrait restaurer les heures
                    if(!except_eaonp.has(date)){
                        var eaonp = exact_amplitude_or_not_planified[date] = clone(days_worked[date] ,{})
                        eaonp.forced_ampli_key = key
                        eaonp.date = date 
                    }
                }
            })

            forOf(exact_amplitude_or_not_planified,(eaonp,date) =>{
                var [undof, success_remove] = wf.reset_date_undofunc(date)
                if(success_remove){
                    eaonp.place_like_before = undof
                    controls_enabled(()=>{
                        if(eaonp.place_like_before instanceof Function == false){
                            my_throw( 'not normal')
                        }
                    })
                }else{
                    delete exact_amplitude_or_not_planified[date]
                    const fa = forced_ampli[eaonp.forced_ampli_key]
                    if(--fa.nb_to_place == 0){
                        delete forced_ampli[eaonp.forced_ampli_key]
                    }
                }
                delete eaonp.forced_ampli_key // elle n'a plus d'utilité
            })

            var undo_modif = (ret_schel)=>{
                if(ret_schel){
                    ret_schel.backup_functions.forEach((undo_func)=>{
                        undo_func()
                    })
                }else{
                    forOf(exact_amplitude_or_not_planified,(eaonp,date) =>{
                        eaonp.place_like_before()
                    })
                }
            }
            return {undo_modif,days_worked,exact_amplitude_or_not_planified,forced_ampli}
        }
        /**
         * Changer les journée travaillées, mais conserver les horaires si on planifie sur une journée qui était déjà planifiée
         * Au final les amplitudes horaires seront les mêmes qu'au début mais sur des journées différentes
         */
         improve_days_worked({dont_make_rotation_worst,get_zscore}){
            const m = this, init_zscore = get_zscore(), get_rotation_score = m.get_instance_of_get_rotation_score()
            let zscore = init_zscore, rota_score = get_rotation_score();
            console.iidebug[74] = idnext('74')
            var debug_hours;//vérifier que les heures ne change pas
            const get_k = (li)=> li.first_minu_worked+'_'+li.last_minu_worked
            const all_wfs = m.all_wfs//.slice()
            special_some(m.all_wfs,(wf) => {
                controls_enabled(()=>{
                    debug_hours = {}
                    forOf(wf.listInfo_by_date,(li)=>{
                        inc(debug_hours,get_k(li))
                    })
                })
                Debug.check_not_enough_worked_all_wfs(main)
                console.iidebug[74] = idnext('74')
                if(console.iidebug[74] == 118653){
                    debugger;
                }
                const dates = wf.listInfo_sorted.map((li)=>li.date);//on replanifie la semaine entière

                const gafsp = m.get_args_for_schdl_plan(wf,dates,[])
                const {undo_modif,days_worked,forced_ampli,exact_amplitude_or_not_planified} = gafsp
                // Maintenant planifions cette personne en ne changeant que les jours et pas les horaires
                const ret_schel = m.v2_schedule_planning({
                    //Si la journée que l'on veut planifiée est dans exact_amplitude 
                    //alors remettre la journée tel qu'on la connaissait
                    exact_amplitude_or_not_planified, 
                    //différentes amplitudes qui sont autorisés indépendemment des jours, 
                    //les autres sont interdites, pour chacune dire combien de fois exactement
                    //elle doit être placé
                    forced_ampli, 
                    only_these_pers: [wf],
                    get_dates_added: true,
                    get_backup_functions: true,
                    do_not_planif_on_planified_days: true
                });

                const undo_all = ()=>{
                    undo_modif(ret_schel)
                    controls_enabled(()=>{
                        if(zscore !=  get_zscore()){
                            my_throw( 'error zscore')
                        }
                        if(rota_score != get_rotation_score()){
                            my_throw( 'error rota_score')
                        }
                    })
                }
                let new_zscore = get_zscore()
                if(!ret_schel.success || new_zscore >= zscore){
                    undo_all()
                }else{
                    //c'est pour éviter de créer des déséquilibre le samedi
                    const new_rota_score = get_rotation_score()
                    if(!dont_make_rotation_worst && new_rota_score >= rota_score){
                        undo_all()
                    }else{
                        zscore = new_zscore
                        rota_score = new_rota_score
                    }
                }
                return false
            },(end_loop,wf)=>{
                forOf(wf.listInfo_by_date,(li)=>{
                    inc(debug_hours,get_k(li),-1)
                })
                forOf(debug_hours,(nb_time, key_hours)=>{
                    if(nb_time !== 0){
                        my_throw('Les heures ont été changées')
                    }
                })
                Debug.check_not_enough_worked_all_wfs(main)
            })
            return {
                improvement: zscore < init_zscore
            }
        }

        //changer le pas minimum
        change_step(step) {
            var m = this;
            m.all_amp_open = null
            conf.displayMinTime = step;
            conf.minTime = step //logiquement on a pas d'intervention plus petite
            var total_info_amp = []
            var by_sector_periodNeed = {}

            //sauvegarder les interventions et tout vider
            var backups = []
            m.all_wfs.forEach(function(wf){
                wf.intervsRacines.forEach((inter)=>{
                    backups.push(m.v2_try_a_change({
                        person:wf,
                        date: inter.date
                    }))
                    wf.reset_date(inter.date,{
                        even_if_protected:true
                    })
                })
            })

            // var pos_in_periodSortedArr = 0;
            main.total_info_amp.forEach((tot_amp) => {
                var nb_loop = (tot_amp.endMinute - tot_amp.startMinute) / step
                controls_enabled(()=>{
                    if(nb_loop != ~~nb_loop) {
                        my_throw( 'not integer')
                    }
                })
                for (var i = 0; i < nb_loop; ++i) {
                    var st = tot_amp.startMinute + (i) * step
                    var end = tot_amp.startMinute + (i + 1) * step
                    var obj = {
                        main: tot_amp.main,
                        startMinute: st,
                        endMinute: end,
                        // need:tot_amp.need,
                        // max_need:tot_amp.max_need,
                        // min_need:tot_amp.min_need,
                        openingType: tot_amp.openingType,
                        date: tot_amp.date,
                        // have:tot_amp.have,
                        dateKey: Main.getDateKey(tot_amp.date, st, end)
                    }
                    var totN = new v2_total_need(obj)
                    // totN.pos_in_periodSortedArr = pos_in_periodSortedArr
                    var pns = []
                    total_info_amp.push(totN)
                    var need = 0;
                    tot_amp.periods.forEach((per) => {
                        need += (obj.need = per.need)
                        obj.min_need = per.min_need
                        obj.max_need = per.max_need
                        obj.have = per.have
                        obj.v2_sector = per.v2_sector
                        obj.openingType = per.openingType
                        obj.from_v2_total_need = false
                        var pn = new PeriodNeed(obj)
                        controls_enabled(()=>{
                            if(!pn.dateKey || !pn.v2_sector) {
                                my_throw( 'no dateKey')
                            }
                        })
                        pns.push(pn)
                        var id_sec = per.v2_sector.id
                        by_sector_periodNeed[id_sec] = by_sector_periodNeed[id_sec] || []
                        by_sector_periodNeed[id_sec].push(pn)
                        // pn.totalPeriod = totN
                        totN.addAmp(pn)
                        // pn.pos_in_periodSortedArr = pos_in_periodSortedArr
                        if(i == nb_loop - 1) {
                            per.disabled_definitively()
                        }
                    })
                    controls_enabled(()=>{
                        if(need != totN.need || tot_amp.need != totN.need) {
                            my_throw( 'erreur sur le besoin')
                        }
                    })
                    totN.periods = pns
                    totN.periods_not_update.add_multiple(totN.periods)
                }
                // ++pos_in_periodSortedArr
                tot_amp.disabled_definitively()
            })
            forOf(m.blockArr[0].sectorEntArr, (sector) => {
                var planning = sector.planning
                var periods = planning.periodSortedArr = by_sector_periodNeed[sector.id]
                periods.forEach(function(per,pos_in_periodSortedArr){
                    per.next_amp = periods[pos_in_periodSortedArr + 1] || null
                    per.planning = planning
                    per.pos_in_periodSortedArr = pos_in_periodSortedArr
                })
            })
            main.total_info_amp = total_info_amp
            main.v2_total_needs_to_updtd.add_multiple(total_info_amp)
            total_info_amp.forEach(function(tot_amp,pos_in_periodSortedArr) {
                tot_amp.planning = null;
                tot_amp.periods_not_update.add_multiple(tot_amp.periods)
                tot_amp.next_amp = total_info_amp[pos_in_periodSortedArr + 1] || null
                tot_amp.pos_in_periodSortedArr = pos_in_periodSortedArr
                tot_amp.periods.forEach(function(amp) {
                    amp.is_score_updtd = false
                })
                tot_amp.is_score_updtd = false
            })

            //pareil_pour_les personne
            main.all_wfs.forEach((wf) => {
                var periodsPers = []
                var pos_in_periodSortedArr2 = 0;
                wf.planning.periodSortedArr.forEach((amp_pers) => {
                    var nb_loop = (amp_pers.endMinute - amp_pers.startMinute) / step
                    controls_enabled(()=>{
                        if(nb_loop != ~~nb_loop) {
                            my_throw( 'not integer')
                        }
                    })
                    for (var i = 0; i < nb_loop; ++i) {
                        var st = amp_pers.startMinute + (i) * step,
                        end = amp_pers.startMinute + (i + 1) * step,
                        obj = {
                            main: amp_pers.main,
                            startMinute: st,
                            endMinute: end,
                            need: amp_pers.need,
                            max_need: amp_pers.max_need,
                            min_need: amp_pers.min_need,
                            openingType: amp_pers.openingType,
                            date: amp_pers.date,
                            have: amp_pers.have,
                            dateKey: Main.getDateKey(amp_pers.date, st, end)
                        }
                        var periodPerson = new PeriodPerson(obj);
                        periodsPers.push(periodPerson)
                    }
                })
                wf.planning.periodSortedArr = periodsPers
                periodsPers.pos_in_periodSortedArr = pos_in_periodSortedArr2
                ++pos_in_periodSortedArr2
            })

            m.set_zscore_as_false()
            backups.forEach((func,i)=>{
                func()
            })
            // if(m.v2_total_needs_to_updtd.length != pos_in_periodSortedArr && total_info_amp.length == pos_in_periodSortedArr) {
            //     my_throw( 'la taille de v2_total_needs_to_updtd devrait contenir toutes les periodes car comme on a changé de pas il faut tout recalculer')
            // }
        }
        create_personSector(sector, wf) {
            var idEnt = sector.id
            var idWf = wf.wfId
            controls_enabled(()=>{
                if(wf.personSectorMap[idEnt]){
                    my_throw(`Cette relation entre une personne et un secteur existe déjà, le nom du secteur n'est sûrement pas assez unique`)
                }
            })

            let ps = new PersonSector({
                idEnt: idEnt,
                wfId: idWf,
                wf: wf,
                ent: sector
            })

            let psm = wf.personSectorMap
            psm[idEnt] = ps
            sector.personSectorMap[idWf] = ps
        }

        // on dit qu'il va falloir recalculer tous les scores
        set_zscore_as_false() {
            var m = this;
            // m.do_not_optimise_propagation = true

            m.v2_total_needs_to_updtd.reset();
            m.zscoreGlobal = 0;
            m.v2_get_all_amp_open().forEach(function(total_amp) {
                total_amp.set_zscore_as_false()
            })
            //amplitude par heure regroupant le total de plusieurs entités
            m.v2_total_needs_to_updtd.add_multiple(m.total_info_amp)
            m.old_v2_sum_zscore_on_period_by_id_amp = {}
            // m.do_not_optimise_propagation = false
        }

        static getUniqId() {
            Main.uniqId = 1 + fl(Main.uniqId)
            if(Main.uniqId == 648) {
                ////////////// ////debugger;
            }
            // console.log(Main.uniqId)
            return 'uniq' + Main.uniqId;
        }
        static stringToMinute(minute) {
            idnext('no_name17');
            var arr = minute.split(/[h:]/)
            return +arr[0] * H + arr[1] * 1
        }
        static format(d) {
            var onTwoChar = function(val) {
                return val < 10 ? '0' + val : val;
            }
            return d.getFullYear() + '-' + onTwoChar(d.getMonth() + 1) + '-' + onTwoChar(d.getDate())
        }
        static formatPeriod(startMinute, endMinute) {
            return startMinute + ' ' + endMinute
        }
        static formatMinute(floatMinute) {
            var hour = ('0' + fl(floatMinute / 60)).slice(-2); // le slice c'est pour avoir 08 au lieu de 008 et 13 * H au lieu de 013 * H
            var minute = ('0' + Math.round(floatMinute - hour * H)).slice(-2)
            return hour + ':' + minute.slice(-2)
        }
        // static formatMinute(floatMinute){
        //     var minute = ('0'+fl(floatMinute)).slice(-2);// le slice c'est pour avoir 08 au lieu de 008 et 13 * H au lieu de 013 * H
        //     var unit = '0'+ Math.round((floatMinute - minute) * H)
        //     return minute + 'h' + unit.slice(-2)
        // }
        static getDateKey(date, floatstartMinute, floatendMinute = 25) {
            return date + ' ' + ('00000' + floatstartMinute).slice(-5) + ' ' + ('00000' + floatendMinute).slice(-5)
        }

        static createPlanning(m, main, Period) {
            // m.periodSortedArr = [];
            // var amplitudeArr = simulator.amplitudeArr
            // var startDate = new Date(main.planifStartDate);
            // var endDate = new Date(main.planifEndDate);
            // for(var d = startDate; d <= endDate; d.setDate(d.getDate() + 1)){
            //  for(var j = 0,jmax = amplitudeArr.length - 1; j < jmax;++j){

            //      if(amplitudeArr[j] == '12h00') continue;
            //      //m.period['2017-01-01 8h30-9h00'] = new Period();
            //      var date =  Main.format(d);
            //      var dateKey = date + ' ' + Main.formatPeriod(amplitudeArr[j],amplitudeArr[j+1]);
            //      m.periodSortedArr.push(new Period({
            //          dateKey: dateKey,
            //          date: date,
            //          startMinute: Main.stringToMinute(amplitudeArr[j]),
            //          endMinute : Main.stringToMinute(amplitudeArr[j + 1])
            //      }));
            //  }
            // }

            // /* pourrait être commenté vu que c'est déjà dans l'ordre */
            // m.periodSortedArr.sort((a,b) => {
            //  if(a.dateKey == b.dateKey) return 0;
            //  return a.dateKey > b.dateKey ? 1 : -1
            // })
        }

        

        /*
         * renvoit un tableau de v2_total_need qui possède donc le besoin total sur la plage concernée pour tous les secteurs
         */
         v2_get_all_amp_open(opt4 = {}) {
            var m = this,
            // si il y a au moins un secteur qui est ouvert alors mettre la periode dans la liste des périodes ouverte
            // garder cette liste en cache car elle ne va pas changer
            all_amp = m.all_amp_open = m.all_amp_open || m.total_info_amp.filter((per, i) => {
                return per.periods.some(filter_workable)
            });
            controls_enabled(()=>{
                if(!all_amp.length) {
                    my_throw( 'empty array')
                }
            })
            if(opt4.dates) {
                all_amp = all_amp.filter((per) => opt4.dates.has(per.date))
            } else if(opt4.forbidden_dates) {
                all_amp = all_amp.filter((per) => !opt4.forbidden_dates[per.date])
            }
            return all_amp
        }

        /*
         * C'est le score ultime il prend tout en compte (sauf rotation rotation)
         * Il prend en compte le score de chaque demi-heures
         * Pour des raison de performance, seul les demi-heure qui ont changé sont mise dans le
         * tableau "v2_total_needs_to_updtd" et on a donc juste à mettre à jour en fonction
         * des différences sur ces demi-heures
         */
         v2_get_zscore_global() {
            var m = this;
            Debug.perfStart('v2_get_zscore_global')
            var zscoreGlobal = m.zscoreGlobal || 0;
            def(m, 'old_v2_sum_zscore_on_period_by_id_amp', {})
            var debug10 = idnext('no_name18')
            //Le score global prend en compte le score de chaque demi-heure
            var dif = 0;
            m.v2_total_needs_to_updtd.some((totalPeriod) => {
                debug10;
                var old_v2_sum_zscore_on_period = Math.round(m.old_v2_sum_zscore_on_period_by_id_amp[totalPeriod.id] || 0)
                var new_v2_sum_zscore_on_period = Math.round(totalPeriod.v2_sum_zscore_on_period() || 0)
                dif += Math.round(new_v2_sum_zscore_on_period - old_v2_sum_zscore_on_period)
                controls_enabled(()=>{
                    if(Number.isNaN(zscoreGlobal)) {
                        console.error('NaN');
                        my_throw( '')
                    }
                })
                m.old_v2_sum_zscore_on_period_by_id_amp[totalPeriod.id] = new_v2_sum_zscore_on_period
            })
            // if(36840 == console.iidebug[21]){
            //     console.warn(dif)
            // }
            zscoreGlobal = Math.round(dif + zscoreGlobal)
            // une fois que l'on a traiter les demi-heure qui avait changées depuis le dernier
            // calcul alors vider la liste
            m.v2_total_needs_to_updtd.reset()
            Debug.perfEnd('v2_get_zscore_global')
            return m.zscoreGlobal = zscoreGlobal
        }

        /**
        * Ajoute le temps manquant en début ou fin de journée attention, on ne l'ajoute pas
        * forcément au meilleur endroit
        * il est donc conseillé d'appellé v2_move_multiple_extremity pour rerépartir les
        * extrémités des journées de façon optimal
        */
        solve_not_enough_worked(wfs,{
            date,
            upper_nb_minutes_wanted_at_final,
            step, //les intervention ajouté doivent avoir un temps multiple de celui-ci
            do_not_check_pause_and_time = false,//ne pas appeler resolve_pause_and_time_in_day,
            allow_to_change_pause = false,//parfois on ne peut pas ajouter d'heure sans que la pause ne passe du soir au midi
            avoid_closing_opening_problem,
            put_in_main_sector,
            opt_min_max,//min et max pour les heures ou interdire des ouverture
            need_undo_func = false
        }) {
            const m = this;
            const old_function = false
            if(!old_function){
                var backups = []
                //todo sauvegarder que les jours modifier
                wfs.forEach(function(wf){
                    controls_enabled(()=>{
                        if(wf.getNumberOfDaysRemained()){
                            my_throw(1103)
                        }
                    })
                    if(need_undo_func){
                        backups.push(wf.backup_week())
                    }
                    var max_bloc_size = lower(wf.nbMinutesMin - wf.nb_minu_worked)
                    if(max_bloc_size > 0){
                        let opt_min_max_by_date;
                        if(!allow_to_change_pause && !do_not_check_pause_and_time){
                            opt_min_max_by_date = {}
                            wf.listInfo_sorted.forEach((li)=>{
                                if(li.first_interv_sector){
                                    var hours_limit = li.get_hours_rules_to_keep_same_pauses()
                                    opt_min_max_by_date[li.date] = hours_limit.opt_min_max
                                }
                            })
                        }
                        m.optimum_schedule_on_extremity({
                            wfs, step, upper_nb_minutes_wanted_at_final,date,do_not_check_pause_and_time,
                            avoid_closing_opening_problem,
                            put_in_main_sector,
                            opt_min_max,
                            max_bloc_size,
                            repair_pause_at_end: true,
                            opt_min_max_by_date,
                            max_recursivity : 1//la fonction pourra se rappeler une fois
                        })
                    }
                })

                return {
                    undo_func: function(){
                        backups.reverse_some(function(backup){
                            backup.restore()
                        })
                    }
                }  
                
            }


            /*****
            
            ANCIEN MODE 

            *****/





            // if(upper_nb_minutes_wanted_at_final === void 8){
            //     debugger
            //     console.error("c'est upper_nb_minutes_wanted_at_final maintenant")
            //     upper_nb_minutes_wanted_at_final = arguments[1].nb_minutes_wanted_at_final
            // }
            var o = {upper_nb_minutes_wanted_at_final}
            var wfs = wfs || m.all_wfs;
            var undo_funcs = []
            wfs.forEach(function(wf) {
                console.iidebug[67] = idnext('67')
                if(console.iidebug[67] == 64311){
                    debugger
                }
                var get_upper_nb_minutes_wanted_at_final = ()=> upper_nb_minutes_wanted_at_final || wf.nbMinutesMin
                if(get_upper_nb_minutes_wanted_at_final() && get_upper_nb_minutes_wanted_at_final() > wf.nbMinutesMin){
                    debugger
                    upper_nb_minutes_wanted_at_final = wf.nbMinutesMin
                    console.error("c'est trop grand")
                }
                controls_enabled(()=>{
                    if(!wf.nb_minu_worked){
                        console.error('impossible')
                    }
                })
                if(!wf.nb_minu_worked){
                    return;
                }
                var get_missing_time = () => lower(get_upper_nb_minutes_wanted_at_final() - wf.nb_minu_worked)
                var missing_time = get_missing_time()
                if(missing_time > 0) {
                    var time_to_place = lower(missing_time)
                    while (time_to_place > 0) {
                        console.iidebug[43] = idnext('43')
                        if(console.iidebug[43] == 165837){
                            debugger;
                        }
                        let opt4 = {
                            // zscoreG: null,//on se moque d'augmenter ou diminuer le zscore
                            time: time_to_place,
                            wf,
                            dont_care_about_score: true,
                            inter_of_the_day: date && wf.get_first_interv_at_date(date),
                            put_in_main_sector,
                            /*les paramètres suivant sont pour ajouter les quart d'heures manquants*/
                            step,
                            do_not_check_pause_and_time,
                            avoid_closing_opening_problem,
                            opt_min_max
                            // inter_of_the_day: opt.rules.has('same_day') ? inter : null
                        }
                        var info_extrem = m.add_inter_on_extremity(opt4)
                        info_extrem.undo_func && undo_funcs.push(info_extrem.undo_func)
                        //si l'ajout n'a pas fonctionné alors retenter avec un temps plus petit
                        if(!info_extrem.has_added_minute) {
                            time_to_place -= (step || conf.displayMinTime)
                            // if(time_to_place <= 0) break;
                        } else {
                            missing_time = get_missing_time()
                            if(time_to_place > missing_time){
                                time_to_place = lower(missing_time)
                            }
                        }
                    }
                }
                if(missing_time > 0) {
                    const msg = ` console.iidebug[67] ==  ${console.iidebug[67]}`
                    console.error("Impossible d'ajouter le temps manquant " + wf.wfId + msg)
                    controls_enabled(()=>{
                        // debugger
                        var missing_t = get_missing_time()
                        // my_throw( `erreur placement ${missing_time} ${missing_t} placé: ${wf.nb_minu_worked}`)
                    })
                } else {
                    // console.green('Oui cette personne a été placé avec la méthode ' + (time_to_place ? 1 : 2))
                }
            })
            return {
                undo_func : function(){
                    undo_funcs.reverse_some(function(undof){
                        undof()
                    })
                }
            }
        }
        affect_ref_intervs(interv_by_wf){
            // un tableau avec les id des personnes qui ont des interventions
            // d'une semaine de référence
            // var person_ids_with_reference_interv = []
            var m = this;
            forOf(interv_by_wf,(inter_info_s,wf_id)=>{
                var wf = m.wfMap[wf_id]
                inter_info_s.forEach(function(ii){
                    console.iidebug[69] = idnext('69')
                    // if(console.iidebug[69] == 35924){
                    //     debugger;
                    // }
                    m.logPersonSectorBug(false,wf)
                    var inter = new Intervention({
                        dontAffectNow:true,
                        person: m.wfMap[ii.wf_id],
                        sector: m.sectorMap[ii.sector_id],
                        date: ii.start_date,
                        startMinute:  upper(Main.stringToMinute(ii.start_hour)),
                        endMinute:  lower(Main.stringToMinute(ii.end_hour)),
                        rules: {
                            //selon les contrainte on peut totalement déplacer cette intervention
                            //dans la journée
                            allow_to_change_hours: true
                        }
                    })
                    var adi_inter = wf.addInterv(inter)
                    // if(!v8js && !adi_inter.has_been_insert){
                    //     my_throw( 'echec de réinsertion des interventions de la semaine de référence')
                    // }
                })
                //maintenant on va réparer les journée qui pourrait ne pas être complête
                //pour cette personne
                wf.intervsRacines.forEach((inter2)=>{
                    if(inter2.date == '2018-03-26' && inter2.person.wfId == 10324){
                        debugger
                    }
                    m.logPersonSectorBug(false,wf)
                    var is_worked = inter2.first_interv_sector
                    if(!is_worked) return
                        var pause;
                    var date = inter2.date
                    //il ne faut pas de trou dans la journée donc on affecte d'un bloc
                    //puis on remet ce que l'on peut
                    var backups_partial_restore = m.v2_try_a_change({
                        get_array_of_backup: true,
                        date: date,
                        person: wf
                    })
                    var first_minu_worked = inter2.first_minu_worked
                    ,last_minu_worked = inter2.last_minu_worked
                    ,sector = m.blockArr[0].sectorEntArr[0]

                    //tant pis pour les trous dans la journée et les pauses
                    //si la personne fait déjà trop d'heures
                    if((wf.real_nbMinutesMin || wf.nbMinutesMin) > wf.nb_minu_worked){
                        return;
                    }
                    var restore_day = m.v2_try_a_change({
                        date,
                        person: wf
                    })
                    wf.reset_date(date, {
                        even_if_protected: true
                    })
                    var inter = new Intervention({
                        dontAffectNow:true,
                        person: wf,
                        sector: sector,
                        date,
                        startMinute: upper(first_minu_worked) ,
                        endMinute:  lower(last_minu_worked),
                        rules: {
                            megaForcedInsert: true, // ne vérifier aucune règle
                        },
                        canFail: true
                    })
                    var adi_inter = wf.addInterv(inter)
                    controls_enabled(()=>{
                        if(!adi_inter.has_been_insert){
                            my_throw( 'anormal')
                        }
                        if(adi_inter.true_protected_information && adi_inter.true_protected_information.debug_moi){
                            debugger;
                        }
                    })
                    backups_partial_restore.forEach(function(inter){
                        if(inter.protected || inter.get_real_sector() || inter.is_pause()){
                            inter.rules = {
                                //si le magasin change d'heure ainsi ça marchera
                                allow_to_change_hours: true
                            }
                            inter.startMinute = upper(inter.startMinute)
                            inter.endMinute = lower(inter.endMinute)
                            var is_protected = inter.protected
                            wf.addInterv(inter)
                            controls_enabled(()=>{
                                if(is_protected && !inter.hasBeenInsert){
                                    my_throw( "comment c'est possible qu'une protected rate")
                                }
                            })
                        }
                    })
                    console.iidebug[47] = idnext('47')
                    // if(24496 == console.iidebug[47] ){
                    //     debugger;
                    // }
                    var rpatmid = m.resolve_pause_and_time_in_day(inter,new opt_v2_schedule_planning({
                        from: "affect_ref_intervs"
                    }))
                    //il se peut que la personne travail trop dans la semaine
                    //alors réduire
                    m.reduce_time_worked_to_respect_max_time([wf])
                    //on a fait dépassé le temps de la personne donc lui remettre sa journée
                    //toute troué
                    if((wf.real_nbMinutesMin || wf.nbMinutesMin) < wf.nb_minu_worked){
                        controls_enabled(()=>{
                            if(!rpatmid.probleme && wf.nbMinutesMin < wf.nb_minu_worked){
                                my_throw('rpatmid renvoie un mauvais résultat')
                            }
                        })
                        restore_day();
                    }
                    if(rpatmid.probleme){
                        controls_enabled(()=>{
                         console.error("Comment se fait-il que l'on ne puisse pas mettre de pause ou régler le min_time et max_time, peut être que c'est à cause des interventions que l'on a demandé à ne pas toucher (protected)")
                     })
                    }
                })
            })

        }



        //on avait arrondis les heures des interventions protégé maintenant on les remets
        restore_protected_true_time(true_protected_informations){
            true_protected_informations.forEach(function(inter_info){
                let wf = inter_info.person
                let {date} = inter_info
                let d = wf.listInfo_by_date[date]
                if(d){
                    console.iidebug[70] = idnext('70')
                    if(console.iidebug[70] == 141094){
                        Debug.logDayWf(wf,date)
                        debugger
                    }
                    let {nb_minu_worked} = wf
                    //il ne faut pas que les protected puissent rater leur insertion donc agrandir les horaires
                    //autorisé si c'est le cas
                    let d = wf.listInfo_by_date[date]
                    if(d){
                        if(inter_info.end_minute > d.max_end_day ){
                            d.max_end_day = lower(inter_info.end_minute)
                            d.last_interv.endMinute = d.max_end_day
                        }
                        if(inter_info.start_minute < d.min_start_day ){
                            d.min_start_day = upper(inter_info.start_minute)
                            d.first_interv.startMinute = d.min_start_day
                        }
                    }
                    
                    //Comme la protected est plus petite que l'intervention fictive que l'on a placer on va d'abord 
                    //mettre cette zone horaire dans l'équipe principale
                    for(var inte = wf.listInfo_by_date[date].first_interv;;){
                        if(inte.true_protected_information === inter_info){
                            inte.put_in_main_sector({
                                even_if_protected_and_force_insertion: true,
                            })
                            break
                        }
                        inte = inte.next
                        if(!inte){
                            controls_enabled(()=>{
                                my_throw(`la protected n'a pas été retrouvé`)
                            })
                            break;
                        }
                    }

                    // Puis on met la protected à sa vrai taille
                    let inter = new Intervention({
                        startMinute: inter_info.start_minute,
                        date,
                        endMinute: inter_info.end_minute,
                        protected: true,
                        true_protected_information: inter_info,
                        protected_reason: inter_info.protected_reason,
                        sector: inter_info.sector,
                    })
                    wf.addInterv(inter)
                    controls_enabled(()=>{
                        if(nb_minu_worked != wf.nb_minu_worked){
                            Debug.logDayWf(wf,date)
                            my_throw( `le temps n'aurait pas du changer`)
                        }
                    })
                }
            })
        }
        /**
         * On va placer les quart d'heures aux extremités des interventions 
         * ainsi que résoudre le temps restant non placé
         * @return {[type]} [description]
         */
         place_missing_minutes_due_to_openning_closing(){
            var m = this;
            //arrêter de compter un quart d'heure en plus pour les ouvertures
            m.logPersonSectorBug()
            m.all_wfs.forEach((wf)=>{
                var step = wf.time_opening
                // if(10179 == wf.wfId){
                //     debugger
                // }
                m.solve_not_enough_worked([wf],{
                    step,
                    do_not_check_pause_and_time : true,
                    avoid_closing_opening_problem: true,
                    put_in_main_sector: false
                })
                m.logPersonSectorBug(false,[wf])
            })

            m.logPersonSectorBug()
            m.all_wfs.forEach(function(wf){
                var step = wf.time_opening
                wf.listInfo_sorted.forEach(function(li){
                    console.iidebug[68] = idnext('68')
                    if(177581 == console.iidebug[68]){
                        debugger
                    }
                    m.unplaced_time_disabled = false
                    m.logPersonSectorBug(false,[wf],li.date)
                    // ne plus rajouter automatiquement du temps pour 
                    // les débuts et fin de journée
                    m.unplaced_time_disabled = true
                    if(li.first_interv_sector){
                        var lil = li.last_interv_sector
                        var lif = li.first_interv_sector
                        // console.log(li.worked,lif.startMinute,lil.endMinute)
                        
                        const open_info = wf.opening_info({
                            start_minute: lif.startMinute,
                            date: lif.date
                        })
                        // si c'est une ouverture qui n'est pas récupéré de la bdd
                        // alors commencer un peu plus tôt
                        if(open_info.is_supplement_time() && !lif.first_interv_sector.true_protected_information){
                            lif.startMinute -= step
                        }
                        const close_info = wf.closing_info({
                            end_minute: lil.endMinute,
                            date: lil.date
                        })
                        // si c'est une fermeture qui n'est pas récupéré de la bdd
                        // alors commencer un peu plus tard
                        if(close_info.is_supplement_time() && !lif.last_interv_sector.true_protected_information){
                            lil.endMinute += step
                        }
                    }
                    m.logPersonSectorBug(false,[wf],li.date)
                })
            })
            m.add_time_for_openning_closing = false;           
            // getArrayGlobalZscoreByDay(time = 7, wf, opt4 = {}) {
            }

            get_two_intervs_ordered(per) {
                var intervs = [];
                per.interventionMap.some((inter) => {
                    intervs.push(inter)
                })
                if(intervs[0].startMinute > intervs[1].startMinute) {
                    intervs = [intervs[1], intervs[0]]
                }
                return intervs
            }

        //on regarde le besoin glabal et les personnes présente mais au global pas par secteur
        global_score_ignoring_sectors() {
            //@to_optimize comme pour le score général
            var score = 0
            var m = this;
            m.all_amp_open.forEach((amp) => {
                score += amp.v2_zscore_and_attractivity().zscore_period
            })
            return score
        }

        /**
         * Résoudre le temps minimum de l'intervention suivante ou précédente
         *
         * il se peut que du temps soit supprimer des extrémité et que l'on arrive pas à les replacer on laissera alors
         * cette responsabilité à la fonction appelante, ce temps sera dans time_removed_to_place
         */
         solve_min_time(new_interv, prevORnext, opt9) {
            var m = this;
            var ret = {}
            if(!new_interv){
                return ret
            }
            var wf = new_interv.person
            var inte2 = new_interv[prevORnext]
            controls_enabled(()=>{
                if(inte2 && (!inte2.hasBeenInsert || inte2[prevORnext] && !inte2[prevORnext].hasBeenInsert)) {
                    my_throw( 'ces interventions devrait être insérées')
                }
            })
            if(!inte2){
                ret.error_code_706 = false
                return ret
            }
            var is_timeok = inte2.solve_min_time_not_respected()
            if(is_timeok){
                ret.error_code_706 = false
                return ret
            }
            //mince le temps minimum n'a pas pu être résolu
            //on va bouger l'extremité si c'en est une
            var func = prevORnext == 'prev' ? 'get_first_inter_with_sector_same_day' : 'get_last_inter_with_sector_same_day'

            if(inte2[prevORnext] && inte2[prevORnext].is_pause()) {
                //est ce que la pause est responsable du problème
                //var pause_info = m.main_resolve_pause_unrespected({
                    //intervention: inte2[prevORnext],
                    //do_not_put_pause_on: opt9.do_not_put_pause_on,
                    //include_this_amplitude: opt9.include_this_amplitude
                //})
                var rpatmid =  m.resolve_pause_and_time_in_day(inte2,new opt_v2_schedule_planning({
                    include_this_amplitude: opt9.include_this_amplitude
                }))

                if(!rpatmid.probleme) {
                    ret.error_code_706 = true
                    return ret
                }
            }
            // si un clone a été fait de la protected on possède une référence morte
            // donc récupérer la dernière
            new_interv = new_interv.getLastProtectedCloneOrOriginal()
            var inte2 = new_interv[prevORnext]
            if(inte2 == new_interv[func]()) {
                var time_to_place = inte2.time
                //libérer du temp à l'extremité
                if(!inte2.reset_inter()) {
                    return false //echec
                } else {
                    var time_before_extremity_move = wf.nb_minu_worked
                    //et le placer ailleur
                    // var info_extrem = m.add_inter_on_extremity({
                    //     time: time_to_place,
                    //     wf,
                    //     dont_care_about_score: true,
                    //     put_in_main_sector: true,
                    //     inter_of_the_day: inte2 //on force à placer les heures dans la même journée
                    // })
                    m.solve_not_enough_worked([wf],{
                        // upper_nb_minutes_wanted_at_final: wf.nbMinutesMin, //lower(wf.nb_minu_worked + time_removed_to_place),
                        date: inte2.date,
                        put_in_main_sector: true,
                    })
                    ret.time_removed_to_place = wf.nb_minu_worked - time_before_extremity_move
                    return ret//on a résolu le temps minimum que add_inter_on_extremity est marché ou non
                        // mais il se peut que l'on ait du temp implaçable que l'on placera un autre jour
                    //info_extrem.has_added_minute //echec si false
                }
            }
            ret.error_code_706 = true
            return ret
        }

        //on va peut être croiser des intervention et si on ne les supprime pas
        //avant elles vont créer un surreffectif
        handle_exact_need_remove_intervention_crossed(per){
            var minTime = per.v2_sector.minTimeSector
            var endMinute = per.endMinute + minTime
            for(;per && per.endMinute < endMinute;per = per.next){
                per.interventionMap.some((inter44)=>{
                    inter44.put_in_main_sector()
                })
            }
        }
        handle_exact_need_wf_handle_protected(wf, per, new_interv,opt={}) {
            var m = this;

            // on ne peut pas utiliser le même backup car ajouter deux fois une même intervention
            // risque d'entrainer plein de problème
            // le partial backup va permettre de restorer ce que l'on peut dans les nouvelles horaires
            var backups_partial_restore = m.v2_try_a_change({
                get_array_of_backup: true,
                date: per.date,
                person: wf
            })

            var impossible = false
            //si une des interventions déjà présente croise notre intervention new_interv alors
            //vérifier si sa disparition pose problème
            //
            //on va aussi récupérer la première heure protégée et la dernière heure protégée
            let {startMinute: start_minute, endMinute: end_minute} = new_interv
            var protected_intervs = []
            var backups_without_pause = []
            //faire un backup sans les pauses et période vide
            let forced_pause_start_minute,forced_pause_end_minute;
            //@Warning elle ne sont pas dans l'ordre
            backups_partial_restore.some((inter) => {
                //elle peut être protected soit car c'est l'utilisateur qui l'a rentré soit car elle est sur un strict need
                if(!inter.true_protected_information){
                    if(inter.is_pause_fake_protected()) {
                        return;
                    }
                }else{
                    if(inter.is_pause()){
                        forced_pause_start_minute = Math.min(inter.startMinute,start_minute)
                        forced_pause_end_minute =  Math.max(inter.endMinute,end_minute)
                    }
                }
                var real_sector = inter.get_real_sector()
                if(real_sector) {
                    backups_without_pause.push(inter)
                    if(inter.protected) {
                        if(inter.startMinute < start_minute){
                            start_minute = inter.startMinute
                        }
                        if(inter.endMinute > end_minute){
                            end_minute = inter.endMinute
                        }
                    }
                }

                if(inter.protected) {
                    protected_intervs.push(inter)
                    //une protégé croise l'intervention que l'on veut mettre donc impossible
                    if(inter.endMinute > new_interv.startMinute && inter.startMinute < new_interv.endMinute) {
                        return impossible = 708
                    }
                    if(!opt.allow_successive_strict_need){
                        if(inter.endMinute == new_interv.startMinute || inter.startMinute == new_interv.endMinute){
                            //'ne pas mettre deux protected exact_need à la suite'
                            return impossible = 707
                        }
                    }
                }
            })
            if(protected_intervs.length) {
                //si on a une pause forcée prévoir de la place avant et après si il y en a pas déjà
                if(forced_pause_start_minute !== void 8){
                    let smaller = forced_pause_start_minute <= start_minute
                    let greater = forced_pause_end_minute >= endMinute
                    if(smaller || greater){
                        let minTime = m.blockArr[0].minTimeSectors
                        if(smaller){
                            start_minute = forced_pause_start_minute - minTime
                        }
                        if(greater){
                            end_minute = forced_pause_end_minute + minTime
                        }
                        debugger
                    }
                }
            }
            //@TODO
            //Si il y a une pause protected il faut un endMinute qui soit au p
            return {
                impossible,
                start_minute,//plus petite start_minute que l'on doit inclure à cause des protected
                end_minute,//plus grande end_minute que l'on doit inclure à cause des protected
                backups_partial_restore: backups_without_pause,
                protected_intervs
            }
        }

        /*
        *   Remettre les interventions d'avant sans dépassé start et end
        */
        partial_restore(backups_partial_restore, start, end, new_interv, wf,opt) {
            var m = this;
            var impossible = false
            backups_partial_restore.forEach((inter) => {
                if(inter.hasBeenInsert) return
                if(inter.is_pause()) {
                    return;
                }
                let st = Math.max(start, inter.startMinute)
                let en = Math.min(end, inter.endMinute)
                if(st >= en) return;
                //normalement ça sert à rien vu qu'elle est protected
                if(!(en > new_interv.startMinute && st < new_interv.endMinute)) {
                    inter.rules = {
                        //on ne veut pas d'intervention au dela
                        //devs nouvelles heures que l'on vient de planifier
                        v2_do_not_extend_workstime: true,
                        //les heures données doivent être respectées
                        break_if_left_mintime_not_respected: true,
                        break_if_right_mintime_not_respected: true,
                        v2_break_if_min_time_not_ok: true,
                        allow_successive_strict_need: opt.allow_successive_strict_need
                    }
                    //les protected elles ne peuvent que réussir sauf si ce sont des false protected
                    inter.set_minu(st,en)
                    wf.addInterv(inter)
                    // deb1 && Debug.logDayWf(wf,amp.date)
                    if(!inter.hasBeenInsert && inter.protected && !inter.is_pause_fake_protected()) {
                        impossible = true
                    }
                }
            })
            return {
                impossible
            }
        }

        //place une personne donnée sur un secteur strict
        handle_exact_need_wf(wf, per, error_codes,opt = {}) {
            const {direction, create_new_interv} = opt
            if(wf.wfId == 10634 && per.startMinute == 20 * H && opt.hard_reschedule && per.date == '2018-03-30'){
                Debug.deb145 = true
                // debugger
            }else{
                Debug.deb145 = false
            }
            if(10575 == wf.wfId && is_between(per.startMinute,19.5 * H, 21.5 * H) && /*per.v2_sector.id == 2522 &&*/ per.date == '2018-03-31'){
                // debugger
            }
            Debug.check_not_enough_worked(wf)
            var m = this;
            let can_worked_this_day;
            var checkTooMuch = function(wf) {
                controls_enabled(()=>{
                    if(wf.nb_minu_worked  > wf.nbMinutesMin) {
                        my_throw( 'la personne travail trop')
                    }
                })
            }
            checkTooMuch(wf);
            var ret = (wf,other_info = {},someone_has_been_found = false) => {
                controls_enabled(()=>{
                    m.checkDoublePause(wf, per.date)
                    checkTooMuch(wf)
                    Debug.checker()
                    inc(error_codes, other_info.error_code || 0)
                });
                other_info.can_worked_this_day = can_worked_this_day
                return {
                    someone_has_been_found,
                    other_info//comme les raisons de l'echec
                };
            }
            var d = wf.listInfo_by_date[per.date]
            //si on ne l'a pas déjà vérifé voir si la personne peut travailler sur cette période
            if(!opt.can_worked_this_day){
                if(!d || !d.first_interv_sector) return ret(wf,{
                    error_code: 702,
                })
                if(per.startMinute < d.min_start_day || per.endMinute > d.max_end_day){
                    if(60656 == console.iDebug){
                        console.log(d.min_start_day,per.startMinute,per.endMinute ,d.max_end_day)
                    }
                    return ret(wf,{
                        error_code: 709,
                    })
                }
            }
            can_worked_this_day = true
            if(!opt.hard_reschedule){
                //comme on ne fait que changer le secteur d'intervention déjà placé
                //regardé que les horaire correspondent
                if(d.first_minu_worked > per.startMinute || d.last_minu_worked < per.endMinute){
                    return ret(wf,{
                        error_code: 710,
                    })
                }
            }

            var someone_has_been_found = false;
            m.checkDoublePause(wf, per.date)
            Debug.check_too_much_worked(wf)
            console.iidebug[26] = idnext('26');
            if(console.iidebug[26] == 103372) {
                debugger;
                var deb1 = true
                // Debug.logDayWf(wf, new_interv.date)
            }
            //créer une intervention sur la période per
            //et qui dure le temps minimum du secteur
            
            let ani = create_new_interv.args_new_inter
            const {max_end_day,min_start_day} = m.get_min_start_and_end({wf,date: per.date})
            if(ani.startMinute < min_start_day || ani.endMinute > max_end_day){
                return ret(wf,{
                    //si on commence trop tot le lendemain ça peut nous empêcher d'aller à 23h par exemple
                    failed_because_of_min_max_possible: true
                },false)
            }

            //si l'intervention n'est pas possible à cause de la proximité d'un strictNeed
            //alors ne pas tenter le hard rescedule
            //il sera tenté plus tard sans allow_successive_strict_need
            if(!opt.failed_because_of_successive_has_been_checked){
                if(!opt.allow_successive_strict_need){
                    for(var next = d.first_interv_sector;next;next = next.next){
                        if( is_between(ani.endMinute, next.startMinute, next.endMinute)
                            || is_between(ani.startMinute, next.startMinute, next.endMinute)
                            && next.protected_reason == 'strictNeed'){
                                return ret(wf,{
                                    failed_because_of_successive: true
                                },false)
                        }
                    }
                }
            }

            var new_interv = create_new_interv()
            var adi_new_inter = wf.addInterv(new_interv)
            if(adi_new_inter.has_been_insert){
                // console.green('directement marché')
                // someone_has_been_found = true
                return ret(wf,{},true); //on a réussi à ajouter la personne
            }else{
                // controls_enabled(()=>{
                //     if([913,916].includes(new_interv.error_code)){
                //         console.error(`cette erreur aurait du être controlé avant`)
                //         debugger
                //     }
                // })
            }

           
            //il va falloir replanifier la personne comme le mode soft n'a pas fonctionné
            if(opt.hard_reschedule){
                //faire une sauvegarde des interventions de la journée
                var real_restore = m.v2_try_a_change({
                    date: per.date,
                    person: wf
                })
                return m.handle_exact_need_wf_hard_reschedule({create_new_interv,per,opt,ret,wf,real_restore,error_codes,allow_successive_strict_need: opt.allow_successive_strict_need})
            }
            return ret(wf,{
                error_code: 715
            },false); //on n'a pas réussi à ajouter la personne
        }

        handle_exact_need_wf_hard_reschedule({create_new_interv,per,opt,ret,wf,real_restore,error_codes,allow_successive_strict_need}){
            Debug.check_not_enough_worked(wf)
            var m = this,deb1;
            var someone_has_been_found = false
            var ret2 = (info)=>{
                if(info.error_code){
                    return ret(wf,{
                        error_code: info.error_code,
                        error_code_impossible: henwhp.impossible
                    },false)
                }else{
                    return ret(wf,{},true)
                }
            }
            console.iidebug[25] = idnext('25')
            if(console.iidebug[25] == 121063){
                debugger
            }
            var new_interv = create_new_interv()
            // if(wf.wfId == 10606 && per.date == '2018-03-28' && per.startMinute == 20 * H && per.v2_sector.id == 2522) {
            //     debugger
            // }
            if(136807 == console.iidebug[25]){
                deb1 = true
                debugger
                deb1 && Debug.logDayWf(wf, per.date)
            } else {
                deb1 = false
            }
            var henwhp = m.handle_exact_need_wf_handle_protected(wf, per, new_interv,{
                allow_successive_strict_need: opt.allow_successive_strict_need
            })
            if(henwhp.impossible) return ret2({
                error_code: 702,
                error_code_impossible: henwhp.impossible
            })
                var start_minute = henwhp.start_minute,
                end_minute = henwhp.end_minute,
                backups_partial_restore = henwhp.backups_partial_restore,
                protected_intervs = henwhp.protected_intervs;

                if(protected_intervs[0]){
                    let d = wf.listInfo_by_date[per.date]
                    var time = d.last_minu_worked - d.first_minu_worked
                    if(protected_intervs[0].startMinute + time < per.endMinute){
                    // il semblerait qu'une protected soit trop tôt dans la journée 
                    // ne laissant aucune chance de pouvoir placer la personne sur "per"
                    // @warning TODO non prise en compte de remaining_time qui sont les temps au ouverture et fermeture
                    // ainsi que du temps non utilisée par la personne
                    return ret(wf,{
                        not_possible_because_of_protected: true
                    },false)
                }
            }

            // on va vider la journée de la personne
            // même les protected car on va les remettre après
            // et on a vérifier que les protected ne croisaient pas notre nouvelle intervention
            var nb_minu_worked1 = wf.nb_minu_worked

            // if(107928 == console.iidebug[25]){
            //     Debug.logDayWf(+wf.wfId,per.date)
            //     debugger
            // }
            Debug.check_too_much_worked(wf)

            // si la journée n'est pas vide v2_schedule_planning ne trouvera pas la meilleur position
            var minutes_to_place =  wf.listInfo_by_date[per.date].worked
            wf.reset_date(per.date, {
                even_if_protected: true
            })
            Debug.check_too_much_worked(wf)
            //puis la replanifier là où c'est le mieux
            protected_intervs.push(new_interv)
            var opt9 = {
                dates: [per.date],
                include_this_amplitude: {
                    start_minute,
                    end_minute
                }, // start_minute est null alors  endMinute l'est aussi et c'est comme si la condition n'était pas
                only_these_pers: [wf],
                minutes_to_place,
                // minutes_to_place: nb_minu_worked1 - wf.nb_minu_worked,
                do_not_put_pause_on: protected_intervs,
                get_dates_added: false,
                get_backup_functions: false
            }
            


            var ret_schel = m.v2_schedule_planning(opt9);
            if(!ret_schel.date_ok[per.date]) {
                // debugger
                // ASTUCE DEBUG regarder dans @cibip la raison de l'echec  et le code d'erreur de ret_schel
                Debug.check_too_much_worked(wf)
                real_restore()
                Debug.check_too_much_worked(wf)
                return ret2({
                    error_code: 704,
                    errors_codes: ret_schel.error_codes
                })
            }
            //on remet maintenant les protected
            var adi_protected = []
            protected_intervs.forEach(function(pr_inter){
                if(pr_inter != new_interv){
                    pr_inter.rules = {
                        //elle était déjà présente donc elle peuvent
                        //être remise sans controle il faudra parcontre
                        //vérifier le temps avant la première et après
                        //la dernière de la journée
                        megaForcedInsert: true
                    }
                    adi_protected.push(wf.addInterv(pr_inter))
                    controls_enabled(()=>{
                        if(!pr_inter.protected){
                            my_throw( 'cette intervention devrait être protected')
                        }
                        if(!pr_inter.hasBeenInsert){
                            my_throw( 'comment une protected peut rater son insertion')
                        }
                    })
                }
            });
            controls_enabled(()=>{
                var inte78 = wf.listInfo_by_date[per.date].first_interv_sector;
                while(inte78.next){
                    if(inte78.is_pause()){
                        let hc = inte78.get_hours_pause_config()
                        if(inte78.endMinute - inte78.startMinute != hc.fake_time){
                            my_throw( 'cette pause semble avoir une mauvaise durée')
                        }
                    }
                    inte78 = inte78.next
                }
            })

            idnext('noName')
            deb1 && Debug.logDayWf(wf, per.date)
            Debug.check_too_much_worked(wf,25)
            if(deb1){
                new_interv.logDayWf(wf)
            }


            
            var last_minute_worked = ret_schel.last_inter.last_minu_worked
            var first_minute_worked = ret_schel.last_inter.first_minu_worked
            controls_enabled(()=>{
                if(last_minute_worked < end_minute || first_minute_worked > start_minute) {
                    my_throw( 'On avait demandé à encadrer ces amplitudes pourtant')
                }
            })
            Debug.reset_and_replanif_success = true
            var start = first_minute_worked //start_inter.get_first_inter_with_sector_same_day().startMinute
            var end = last_minute_worked //start_inter.get_last_inter_with_sector_same_day().endMinute

            new_interv.rules = {
                v2_do_not_respect_min_time_after_before: true/*true*/,
                break_if_left_mintime_not_respected: false,
                break_if_right_mintime_not_respected: false,
                v2_break_if_min_time_not_ok: true,
                do_not_care_about_pause_even_fake_protected: true,
                allow_successive_strict_need
            }
            if(deb1){
                new_interv.logDayWf(wf)
            }
            console.iidebug[44]= idnext('44')
            if(console.iidebug[44] == 91334){
                debugger
            }
            var nb_minu_wanted = wf.nb_minu_worked;
            var adi_new_interv = wf.addInterv(new_interv)

            new_interv.debugInfo = 'handle_exact_need1'
            if(!adi_new_interv.has_been_insert) {
                // debugger
                var errc = new_interv.error_code
                error_codes[errc] = (error_codes[errc] || 0) + 1
                real_restore();
                return ret2({
                    error_code: 700,
                    error_code_inter: errc,
                    iidebug_25: console.iidebug[25] 
                })
            }
            //avec toutes ses protected et les temps minimal à respecter
            //la pause n'est pas forcément bien placé
            var rpatmid = m.resolve_pause_and_time_in_day(new_interv,new opt_v2_schedule_planning(opt9),void 8,{nb_minutes_wanted_at_final: nb_minu_wanted});
            if(rpatmid.probleme){
                //La pause ne peut pas être bien placé
                real_restore();
                return ret2({
                    error_code: 710
                });
            }

            new_interv = adi_new_interv.get_valid_inter()
            Debug.new_interv_insertion = true
            //on essaie de remettre ce que l'on peut de la journée
            //sans dépasser la nouvelle plage horaire choisie avec v2_schedule_planning
            // console.green('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
            deb1 && Debug.logDayWf(wf, per.date)
            // deb1 && console.green('BBBBBBBBBBBBBBBBBBBBBBBB')
            var pr = m.partial_restore(backups_partial_restore, start, end, new_interv, wf,{
                allow_successive_strict_need: opt.allow_successive_strict_need
            })
            // deb1 && console.green('CCCCCCCCCCCCCCC')
            if(pr.impossible) {
                // pour une raison inconnue on a pas pu ajouter l'intervention
                // donc on restore
                real_restore();
                return ret2({
                    error_code: 705
                })
            }
            Debug.protected_have_been_restored = true
            var min_time_info = {}
            var time_removed_to_place = 0;
            var mintime_pb = function(adi_inter,dir){
                if(min_time_info.error_code_706) return;
                var inter = adi_inter.get_valid_inter()
                min_time_info = m.solve_min_time(inter, dir || 'prev', opt9)
                /*time_removed_to_place += */min_time_info.time_removed_to_place || 0
                if(dir || min_time_info.error_code_706) return;
                inter = adi_inter.get_valid_inter()
                min_time_info = m.solve_min_time(inter, 'next', opt9)
                /*time_removed_to_place +=*/ min_time_info.time_removed_to_place || 0
            }
            mintime_pb(adi_new_interv)
            adi_protected.sort(function(a,b){
                return a.startMinute - b.startMinute
            })
            if(adi_protected.length){
                mintime_pb(adi_protected[0],'prev')
                mintime_pb(adi_protected.pop(),'next')
            }
            if(min_time_info.error_code_706){
                real_restore();
                return ret2({
                    error_code: 706
                })
            }
            
            // il se peut que la personne ne fasse plus assez d'heure on va donc les 
            // replacer dans la journée puis dans la semaine
            var is_not_enough_worked = ()=> upper(wf.nb_minu_worked) < wf.nbMinutesMin
            if(is_not_enough_worked() /*time_removed_to_place*/){
                //tenter de remettre la somme de ce que l'on a enlever dans la même journée
                var snew1 = m.solve_not_enough_worked([wf],{
                    upper_nb_minutes_wanted_at_final: wf.nbMinutesMin, //lower(wf.nb_minu_worked + time_removed_to_place),
                    date: new_interv.date,
                    put_in_main_sector: true,
                    need_undo_func: true
                })
                var snew2;
                if(is_not_enough_worked()){
                    //c'est toujours pas bon alors mettre ailleur dans la semaine
                    snew2 = m.solve_not_enough_worked([wf],{
                        upper_nb_minutes_wanted_at_final: wf.nbMinutesMin,//lower(wf.nb_minu_worked + time_removed_to_place),
                        put_in_main_sector: true,
                        need_undo_func: true
                    })
                }
                if(is_not_enough_worked()){
                    //on annule car on a pu placer les heures nulle part dans la semaine
                    snew2 && snew2.undo_func()
                    snew1.undo_func()
                    real_restore();
                    return ret2({
                        error_code: 707
                    })
                }
            }
            return ret2({})
            
        }

        checkDoublePause(wf, date) {
            controls_enabled(()=>{
                var inter = wf.get_first_interv_at_date(date)
                var nb_pause = 0;
                while (inter) {
                    if(inter.is_pause()) {
                        ++nb_pause
                    }
                    inter = inter.next
                }
                if(nb_pause > 1) {
                    my_throw( 'error too much pause')
                }
            })
        }

        // //on récupère l'heure de début de la première intervention
        // //qui est sur une amplitude stricte (où le maximum est égale au minimum
        // //  et le mintime dans le secteur égale au maxtime dans le secteur)
        // var start_minute
        // backups.some(function(inter){
        //     var start_minute2 = inter.get_firstORlast_minute_where_need_is_absolute('first')
        //     if(start_minute2 !== null){
        //         start_minute = start_minute2
        //         return true
        //     }
        // })
        // //de même avec l'heure de fin
        // var end_minute
        // backups.reverse_some(function(inter){
        //     var end_minute2 = inter.get_firstORlast_minute_where_need_is_absolute('last')
        //     if(end_minute2 !== null){
        //         end_minute = end_minute2
        //     }
        // })

        handle_exact_need_amp_for_of(wfArr, per,{minTimeSector, intervToExtend, sctr, direction, error_codes}){
            var m = this;
            sctr.minTimeSector = sctr.maxTimeSector = minTimeSector
            var last_wf;
            // les wf qui n'ont pas été placé car ça aurait fait deux secteurs
            // successifs dont le temps et le besoin sont stricts
            var failed_because_of_successive = []
            var wfArr2 = []//filtré sur ceux qui pourrait être placé avec un algo plus performant      
            var henf;
            //paramètre de la nouvelle intervention
            var ani = {
                date: per.date,
                sector: per.v2_sector,
                protected: true,
                fakeProtected: true, //on met une protected pour ne pas casser cet intervention on la déprotègera à la fin
                protected_reason: 'strictNeed',
                rules: {
                    safe_insertion: true,
                    v2_do_not_extend_workstime: true
                }
            }
          
            if(direction == +1){
                ani.startMinute = (intervToExtend || per).startMinute
                ani.endMinute = ani.startMinute + minTimeSector
            }else{
                ani.endMinute = (intervToExtend || per).endMinute
                ani.startMinute = ani.endMinute - minTimeSector
            }
            assert_finite(ani.endMinute)
            var create_new_interv = ()=>{
                return new Intervention(ani);
            }
            create_new_interv.args_new_inter = ani




            var check_logic = (per,before_algo,henf)=>{
                if(henf){
                    if(!henf.someone_has_been_found && per.have >= per.need){
                        my_throw( "someone_has_been_found ne fonctionne pas" )
                    }
                }else{
                    if(before_algo){
                        if(per.have == per.need){
                            my_throw( "it is already good")
                        }
                    }
                    if(per.have > per.need){
                        my_throw( "too much person")
                    }
                }
            }

            wfArr.some((wf) => {
                last_wf = wf
                check_logic(per,true)
                if(wf.wfId == 10108 && per.date == '2018-03-27' && per.startMinute == 20 * H){
                    // debugger
                }
                henf = m.handle_exact_need_wf(wf, per, error_codes,{
                    hard_reschedule: false,
                    direction,
                    create_new_interv
                })
                let oi = henf.other_info
                if(oi.failed_because_of_successive){
                    failed_because_of_successive.push(wf)
                }else if(oi.can_worked_this_day && !oi.failed_because_of_min_max_possible){
                    wfArr2.push(wf)
                }
                check_logic(per,false,henf)
                return henf.someone_has_been_found
            })

            var iidebug = []
            if(1){
                if(!henf.someone_has_been_found){
                    wfArr2.some((wf) => {
                        // debugger
                        // if(console.iidebug[28] == 62187 && wf.wfId == 10324) {
                        //     debugger
                        // }
                        iidebug.push(idnext('no_name1'))
                        if(console.iDebug == 66208){
                            debugger
                        }
                        check_logic(per,true)
                        last_wf = wf
                        henf = m.handle_exact_need_wf(wf, per, error_codes,{
                            hard_reschedule: true,
                            can_worked_this_day: true,
                            failed_because_of_successive_has_been_checked: true,
                            direction,
                            create_new_interv
                        })
                        check_logic(per,false,henf)
                        return henf.someone_has_been_found
                    })
                }
                if(!henf.someone_has_been_found){
                    failed_because_of_successive.some((wf) => {
                        last_wf = wf
                        check_logic(per,true)
                        henf = m.handle_exact_need_wf(wf, per, error_codes,{
                            hard_reschedule: true,
                            allow_successive_strict_need: true,
                            failed_because_of_successive_has_been_checked: true,
                            direction,
                            can_worked_this_day: true,
                            create_new_interv
                        })
                        check_logic(per,false,henf)
                        return henf.someone_has_been_found
                    })
                }
            }
            Debug.checker()
            controls_enabled(()=>{
                if(per.have > per.need){
                    my_throw( 'surreffectif')
                }
                if(!henf.someone_has_been_found) {
                    if(per.startMinute == 23.5 * H){
                        debugger
                    }
                    // if(per.startMinute < 18.5 * H){
                        console.error('personne à cette amplitude console.iidebug[28] ' + console.iidebug[28],per.date,per.startMinute,{
                            reset_and_replanif_success : Debug.reset_and_replanif_success,
                            new_interv_insertion : Debug.new_interv_insertion,
                            protected_have_been_restored : Debug.protected_have_been_restored,
                            error_codes
                        })
                    // }
                } else {
                    if(per.have < 1){
                        console.log('console.iidebug[26]'+console.iidebug[26])
                        my_throw( "Pourquoi someone_has_been_found est true si le besoin n'est toujours pas Ok ")
                    }
                    // console.green('OK AMP')
                }
            })

            return {
                someone_has_been_found: henf.someone_has_been_found,
                last_wf
            }
        }

        handle_exact_need_amp(per,direction = +1,progress){//si direction = +1 cela veut dire que l'on a déjà traité le passé juste avant, si -1 on a déjà traiter le futur juste après
            var sctr = per.v2_sector, not_strict = 0, stop = 0, error_code;
            var retF = ()=>{
                return {not_strict,stop,error_code}
            }
            not_a_strict_need: {
                if(per.need == 0 || per.min_need !== per.max_need || sctr.minTimeSector != sctr.maxTimeSector){
                    not_strict = 1
                    return retF()
                }
            }

            if(direction == -1 && ( (per.endMinute - sctr.minTimeSector) < progress.startMinute ) ){
                stop = 1
                return retF();
            }
            var m = this;
            var old_min_max_time = sctr.minTimeSector

            // on a parcouru la journée par la fin et par le début et là on se retrouve en milieu
            // de journée avec un impossibilité de mettre une intervention qui fasse pile minTimeSector
            // on va donc devoir abandonner la règle sur le temps minimum et maximum
            if(direction == +1 && (per.startMinute + sctr.minTimeSector > progress.endMinute ) ){
                stop = 1
            }

            // combien de temps avant le prochain besoin à complété
            // on n'ira pas chercher plus loin que 2 * old_min_max_time
            let get_time_til_need_nobody = function(per, dir){
                var nb_loop = 2 * old_min_max_time / conf.minTime, t = ['startMinute','endMinute'];
                const [startMinute,endMinute] = dir > 0 ? t : t.reverse()
                const next = dir > 0 ? 'next_amp' : 'prev_amp'
                var a = per
                while(--nb_loop && a.need !== a.have && a[next]){
                    a = a[next]
                }
                return dir * (a[startMinute] - per[startMinute])
            }
            let diff = get_time_til_need_nobody(per, direction)
            assert_finite(diff)
            let diff_modu = diff%old_min_max_time
            var fixed_min_time_sector;
            if(diff_modu === 0){
                fixed_min_time_sector = old_min_max_time
            }else if(diff_modu * 2 > old_min_max_time){
                //on va ajouter une nouvelle personne
                fixed_min_time_sector = diff_modu
            }

            var get_minTimeSector_possible = ()=>{
                return {
                    *[Symbol.iterator]() {
                        if(fixed_min_time_sector !== void 8){
                            yield fixed_min_time_sector
                            return;
                        }
                        //il n'y a l'emplacement que pour une personne 
                        if(diff_modu <= old_min_max_time){
                            yield diff_modu
                            return;
                        } 

                        // il y a l'emplacement pour 2 personne donc tester plusieurs tailles
                        // en privilégiant une qui fasse la moitié du temps à couvrir
                        // on autoriser à allonger les personnes autour de cet horaire pour 
                        // que elles le couvrent
                        // let nb_loop = (old_min_max_time + old_min_max_time / 2) / conf.minTime
                        let middle = upper(diff/2)
                        yield middle
                        let a,b = a = middle;
                        let not_a = false, not_b = false;
                        if(middle > old_min_max_time){
                            for(var i = 1;; ++i){
                                a += conf.minTime
                                if(a < old_min_max_time * 2){
                                    yield a
                                }else{
                                    break
                                }
                            }
                        }else{
                            if(middle > old_min_max_time){
                                for(var i = 1;; ++i){
                                    b -= conf.minTime
                                    if(b >= conf.minTime){
                                        yield b
                                    }else{
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // if(1){
            //     // debugger;
            //     let diff = progress.endMinute - progress.startMinute
            //     sctr.maxTimeSector = sctr.minTimeSector = diff
            // }


            
            console.iidebug[31] = idnext('31');
            // if(console.iidebug[31] == 516640) {
            //   //// debugger;
            // }
            Debug.checker()
            //on va enlever l'intervention qui commence le plus tard en cas de surreffectif
            if(per.have > per.need){
                var intervs77 = per.interventionMap.getValues().sort(function(inter1,inter2){
                    return direction * (inter2.startMinute - inter1.startMinute)//décroissant si direction est 1
                }).some((int77)=>{
                    var new_inter = int77.v2_clone()
                    //supprimer pour ne plus dépasser le max_need
                    int77.reset_inter({even_if_fake_protected:true})
                    new_inter.sector = m.blockArr[0].sectorEntArr[0]
                    new_inter.rules = {
                        safe_insertion: true
                    }
                    int77.person.addInterv(new_inter)
                    return per.have <= per.need
                })
            }
            if(per.have == per.need) {
                //si le besoin est respecté il faut protéger les interventions qui sont dessus cette amplitude de secteur
                per.interventionMap.some((inter8) => {
                    if(inter8.get_real_sector() && !inter8.protected) {
                        inter8.protected = true
                        inter8.fakeProtected = true
                        inter8.protected_reason = 'strictNeed'
                    }
                })
                return retF(); //rien d'autre à faire pour cette amplitude le besoin est déjà respecté
            }
            if(per.have >= per.need){
                return retF(); //rien d'autre à faire pour cette amplitude le besoin est trop grand et on a pas pu le réduire
            }

            //le besoin strict de 1 n'est pas couvert, il faut résoudre ce problème
            //supprimer ce qui va géner comme intervention
            m.handle_exact_need_remove_intervention_crossed(per)

            // on trie de façon à avoir ceux qui n'ont pas été beaucoup affecté à ce secteur au début
            // on va aussi prendre en compte si cela change les horaires de la personne
            // return 1 si a doit être après b
            // return -1 si b doit être après a 
            const wfArr = m.all_wfs.filter((wf)=> wf.listInfo_by_date[per.date]).sort((a, b) => {
                const d_wfa = a.listInfo_by_date[per.date]
                const d_wfb = b.listInfo_by_date[per.date]
                //Est ce que les deux worksfor peuvent mettre l'intervention sans changer leurs horaires
                const a_is_ok = per.startMinute >= d_wfa.first_minu_worked && d_wfa.last_minu_worked >= per.endMinute
                const b_is_ok = per.startMinute >= d_wfb.first_minu_worked && d_wfb.last_minu_worked >= per.endMinute

                if(a_is_ok !== b_is_ok){
                    //celui qui a les bonnes horaires en premier si a_is_ok = 1 et b_is_ok = 0 alors return -1 ce qui veut dire que a est avant b 
                    return b_is_ok - a_is_ok 
                }

                const get_dist = (d_wf)=>{
                    const start_diff = d_wf.first_minu_worked - per.startMinute
                    if(start_diff > 0){
                        return start_diff
                    }else{
                        return per.endMinute - d_wf.last_minu_worked
                    }
                }
                //mettre en premier celui qui va le moins devoir se décaller si aucun des deux n'a les bonnes horaires
                if(!a_is_ok && !b_is_ok){
                    return get_dist(d_wfa) - get_dist(d_wfb)
                }
                //sinon traiter-> en fonction de la participation au sein du secteur
                const nbMinuteA = a.personSectorMap[sctr.id].nb_minu_worked / a.nb_minu_worked
                const nbMinuteB = b.personSectorMap[sctr.id].nb_minu_worked / b.nb_minu_worked
                return nbMinuteA - nbMinuteB // celui qui a fait le plus de minute dans ce secteur à la fin
            })

            // RandomGenerator.shuffle(wfArr)
            Debug.check_protected_still_present()

            console.iidebug[28] = idnext('28');
            if(console.iidebug[28] == 185718){
                debugger
            }
            
            // pour chaque personne  selon la personne qui a le moins fait le secteur en ratio
            //                     revoir ses horaires en engloblant les heures où elle est dans des besoins stricts passés
            //                         en ayant le meilleur score et en comprenant les horaires où on veut la placer
            var henf = {}
            var error_codes = {}
            var last_wf, someone_has_been_found;
            Debug.checker()
            if(fixed_min_time_sector !== void 8){
                let minTimeSector = sctr.minTimeSector = sctr.maxTimeSector = fixed_min_time_sector
                ;({someone_has_been_found,last_wf} = m.handle_exact_need_amp_for_of(wfArr, per, {sctr,minTimeSector,error_codes,direction}));
            }

            if(fixed_min_time_sector === void 8){
                // le temps étant plus petit que 1.5 fois le temps dans le old_min_max_time
                // on va allonger la personne juste avant cette horaire
                let wfArr2 = [];
                let minTimeSector_possible = get_minTimeSector_possible()
                forOf(per[direction > 0 ? 'prev_amp' : 'next_amp'].interventionMap,(interv)=>{
                    for(let minTimeSector of minTimeSector_possible){
                        sctr.minTimeSector = sctr.maxTimeSector = minTimeSector
                        ;({someone_has_been_found,last_wf} = m.handle_exact_need_amp_for_of([interv.person], per, {sctr,minTimeSector,direction,intervToExtend: interv,error_codes} ));
                        if(someone_has_been_found) break
                    }
                }) 
            }
            //restaurer les règles sur les secteurs
            sctr.minTimeSector = sctr.maxTimeSector = old_min_max_time


            controls_enabled(()=>{
                //vérifier les passé
                var all_amp = m.v2_get_all_amp_open();
                all_amp.forEach((amp) => {
                    if(amp.date != per.date) return;
                    amp.periods.some((per2) => {
                        if(per2.v2_sector != per.v2_sector) return;
                        if(direction * (per2.startMinute - per.startMinute) > 0 ) return true
                        if(per2.min_need == per2.max_need && per2.need == 1 && per2.have > 1){
                            my_throw( 'surreffectif')
                        }
                        if(per2.min_need == per2.max_need && per2.need == 1 && per2.have < 1){
                            console.error('sous effectf')
                        }
                    })
                })
            })
            
            return retF()
            
        }

        //certaines équipes ont un minimum et un maximum à 1 mais ont aussi un temps min et max de temps
        //d'intervention égaux ce qui restreint les possibilité de combler les sous effectf en étirant
        //des interventions, et de réduire les surreffectifs en diminuant une interventon
        //on va donc faire un parcours de gauche à droite, en trouvant à chaque fois une personne
        // à mettre durant ce min ou max time
        handle_exact_need(opt4 = {}) {
            var m = this;
            m.exact_need_running = true
            console.iidebug[29] = idnext('29');
            var generators = []
            var progress_by_date = {}
            create_generators: {
                var all_amp = m.v2_get_all_amp_open();
                let get_generator = function(all_amp, start, end, progress, inc, callback, stop_cond){
                    var _generator = function* improve_strict_need(){
                       for(var i = start; i != end;  i+= inc){
                            console.iidebug[96] = idnext('96')
                            let amp = all_amp[i];
                            if(stop_cond(amp)){
                                return false;
                            }
                            console.iidebug[30] = idnext('30');
                            // if(console.iidebug[30] == 62185){
                            //     debugger;
                            // }
                            Debug.reset_and_replanif_success = false
                            Debug.new_interv_insertion = false
                            Debug.protected_have_been_restored = false
                            //per est la période pour un secteur particulier
                            let nb_strict_need = 0
                            let must_stop = 0;
                            amp.periods.some((per) => {
                                console.iidebug[97] = idnext('97')
                                if(452875 == console.iidebug[97]){
                                    debugger
                                }
                                let hena = m.handle_exact_need_amp(per, inc,progress)
                                if(!(hena instanceof Object)){
                                    controls_enabled(()=>my_throw('Not an object'))
                                }
                                // assert_key_exist(hena,'need_has_been_improved')
                                nb_strict_need += !(hena instanceof Object) || !hena.not_strict
                                must_stop += hena.stop
                                assert_finite(must_stop)
                                if(hena.not_strict == 0 && per.min_need != per.max_need){
                                    debugger
                                }
                            })

                            if(must_stop){
                                return;
                            }
                            callback(amp)
                            if(nb_strict_need){
                                yield {value: {}};
                            }
                        }
                    }
                    let instance_of_generator = _generator()
                    return instance_of_generator
                }
                //séparer les amplitudes par jour
                let arrays_by_d = {}
                all_amp.forEach(function(amp){
                    arrays_by_d[amp.date] = arrays_by_d[amp.date] || []
                    arrays_by_d[amp.date].push(amp)
                })
                forOf(arrays_by_d,(amps,date)=>{
                    var progress = progress_by_date[date] = {
                        startMinute: amps[0].startMinute,
                        endMinute: last(amps).endMinute
                    }
                    var check4 = ()=>{
                        controls_enabled(()=>{
                            if(progress.endMinute < progress.startMinute){
                                my_throw(`La progression aurait du s'arrêté au croisement de la boucle ordinaire et inversé donc en milieu de journée`)
                            }
                        })
                    }
                    generators.push(get_generator(amps, 0,amps.length,progress, 1, (amp)=>{
                        progress.startMinute = amp.endMinute
                        check4()
                    }, (amp)=>{
                        return amp.endMinute == progress.endMinute
                    }))
                    generators.push(get_generator(amps, amps.length - 1, -1,progress, -1, (amp)=>{
                        progress.endMinute = amp.startMinute
                        check4()
                    }, (amp)=>{
                        return amp.endMinute == progress.startMinute
                    }))
                })
            }

            //On parcourt les amplitudes pour chaque jour du début à la fin et de la fin vers le début
            //on continue tant que tout les générateur ne se sont pas croisé au milieur de la journée
            for(let nb_done; nb_done != generators.length;){
                nb_done = 0;
                generators.forEach(function(generator){
                    let result = generator.next()//améliorer le besoin sur le secteur qui a un besoin strict
                    nb_done += result.done
                })
            }
            console.log(progress_by_date)
            debugger;
            m.exact_need_running = false
        }

        // comme on a travaillé à la demi-heure les pause font 20 minute de trop
        solve_too_long_pause() {
            var m = this;
            // forOf(m.pauses_obj, function(pause_ins) {
            //     pause_ins.go_real_time_mode()
            // })
            var restore_interv = m.backup_all_interv()
            m.free_the_planning()
            restore_interv()
            m.all_wfs.forEach(function(wf) {
                wf.intervsRacines.forEach(function(first_interv_day) {
                    var inter = first_interv_day
                    while (inter) {
                        if(inter.sector && inter.sector.pause_instance) {
                            inter.sector.pause_instance.reduce_time_pause(inter)
                            return; //journée suivante
                        }
                        inter = inter.next
                    }
                })
            })
        }


        backup_all_interv(){
            var m = this;
            var undo_funcs = []
            m.all_wfs.forEach(function(wf){
                wf.intervsRacines.forEach(function(inter){
                    undo_funcs.push(m.v2_try_a_change({
                        person:wf,
                        date: inter.date
                    }))
                })
            })
            var restore = function(){
                undo_funcs.forEach(function(restoreF){
                    restoreF()
                })
            }
            return restore
        }

        free_the_planning(){
            var m = this;
            var undo_funcs = []
            m.all_wfs.forEach(function(wf){
                wf.intervsRacines.forEach(function(inter){
                    inter.remove_all_interv_of_day()
                })
            })
        }
        /*
         *
         * Résouds les problèmes de surreffectif dans une équipe où il ne devrait y avoir qu'une personne
         */
         solve_overstaffing_problem(opt = {}) {
            // return;
            var m = this;
            var all_amp = m.v2_get_all_amp_open();
            var stop = false;

            all_amp[opt.order == 'reverse_chronological' ? 'reverse_some' : 'some']((amp) => {
                if(stop) return true
                    amp.periods.some((per) => {
                        if(stop) return true
                            if(per.max_need && per.have > 1 && per.have > per.max_need) {
                        // if(opt.debug){
                        //     Debug.logDayWf('10575','2018-03-26')
                        // }
                        var intervs = m.get_two_intervs_ordered(per)
                        var sector = m.blockArr[0].sectorEntArr[0]
                        var minTimeSector = sector.minTimeSector
                        //retardons la seconde intervention
                        var seria0 = intervs[0].v3_serialize()
                        var seria1 = intervs[1].v3_serialize()

                        var sector_with_too_much_person = seria0.sector
                        seria1.sector = sector
                        seria0.rules = seria1.rules = {
                            v2_do_not_extend_workstime: true, // ne pas changer les horaires travaillés

                            //ne pas essayer de changer les heures données si l'ajout n'est pas possible
                            //les heures peuvent quand même être différentes de ce qui a été demandée car on a fusionné avec ceux du même secteur
                            // safe_insertion: true
                        }

                        var undoFunc_p0 = m.v2_try_a_change({
                            date: intervs[0].date,
                            person: intervs[0].person
                        })
                        var undoFunc_p1 = m.v2_try_a_change({
                            date: intervs[1].date,
                            person: intervs[1].person
                        })
                        var message;
                        var addInterNextThenPrev = (mode) => {
                            controls_enabled(()=>{
                                if(seria0_startMinute > seria1_startMinute) {
                                    my_throw( 'not logic')
                                }
                            })
                            
                            var success;
                            if(console.iDebug == 137555) {
                                ////////////debugger
                            }
                            // quand on étend une intervention c'est pour enlever l'autre qui est en même
                            // temps et la mettre dans un autre sector sector
                            message = console.iDebug + '@a0 Le ' + per.date + ' La première personne ' + seria0.person.wfId + ' était présente de ' + seria0_startMinute + ' à ' + seria0_endMinute +
                            '\nEt la seconde personne ' + seria1.person.wfId + ' était présente de ' + seria1_startMinute + ' à ' + seria1_endMinute +
                            '\nLes 2 dans le secteur ' + sector_with_too_much_person.id +
                            ' avec la méthode ' + mode

                            var message2;
                            if(mode == 'extendTheFirstTowardRight') {
                                seria0.startMinute = seria0_startMinute
                                seria0.endMinute = seria0_endMinute + time
                                seria0.sector = sector_with_too_much_person
                                message2 = ' dans le même secteur '
                            } else if(mode == 'extendTheSecondTowardLeft') {
                                seria0.endMinute = seria0_endMinute
                                seria0.startMinute = seria0_endMinute - time
                                seria0.sector = sector
                                message2 = ' dans un autre secteur '
                            }

                            message += ' \n@a1 La première personne ' + seria0.person.wfId + ' est maintenant présente de ' + seria0.startMinute + ' à ' + seria0.endMinute +
                            message2 + seria0.sector.id;
                            var interPrev = new Intervention(seria0)
                            var adi_interPrev = intervs[0].person.addInterv(interPrev)
                            if(adi_interPrev.has_been_insert) {
                                // if(interPrev.startMinute != seria0.startMinute){
                                //     my_throw( 'not what I asked for')
                                // }
                                // if(interPrev.endMinute != seria0.endMinute){
                                //     my_throw( 'not what I asked for')
                                // }
                                if(mode == 'extendTheFirstTowardRight') {
                                    seria1.startMinute = seria1_startMinute
                                    seria1.endMinute = seria0.endMinute
                                    seria1.sector = sector
                                    var message2 = ' dans un autre secteur '
                                } else if(mode == 'extendTheSecondTowardLeft') {
                                    seria1.startMinute = seria0.startMinute
                                    seria1.endMinute = seria1_endMinute
                                    seria1.sector = sector_with_too_much_person
                                    var message2 = ' dans le même secteur '
                                }
                                message += '\n@a2 La seconde personne ' + seria1.person.wfId + ' est maintenant présente de ' + seria1.startMinute + ' à ' + seria1.endMinute +
                                message2 + seria1.sector.id
                                var interNext = new Intervention(seria1)
                                var adi_inter_next = intervs[1].person.addInterv(interNext)
                                success = adi_inter_next.has_been_insert
                            }
                            return {
                                success
                            }
                        }
                        var seria0_startMinute = seria0.startMinute
                        var seria0_endMinute = seria0.endMinute
                        var seria1_startMinute = seria1.startMinute
                        var seria1_endMinute = seria1.endMinute
                        var max_time = Math.max(seria1.endMinute, seria0.endMinute) -
                        Math.min(seria0.startMinute, seria0.startMinute)
                        // var minTimeAllSector = per.v2_sector.parentEntity.minTimeSectors
                        //on va essayer d'en étendre une et d'en réduire une autre
                        for (var time = 0; time <= max_time; time += conf.displayMinTime) {
                            if(opt.debug && opt.debug.startMinute == per.startMinute &&
                                per.v2_sector.id == opt.debug.sector &&
                                time == opt.debug.time &&
                                per.date == opt.debug.date
                                ) {
                                ////////////debugger;
                        }
                        if(opt.methods.has('extendTheFirstTowardRight')) {
                                //tenter de décaler la fin de la première droite
                                var info = addInterNextThenPrev('extendTheFirstTowardRight')
                                if(info.success) break;
                            }
                            if(opt.methods.has('extendTheSecondTowardLeft')) {
                                //tenter de décaler la fin de la première gauche
                                info = addInterNextThenPrev('extendTheSecondTowardLeft')
                                if(info.success) break;
                            }
                        }
                        if(info.success) {
                            console.green('overstaff solve for ' + per.dateKey)
                            console.log(
                                message
                                )
                            if(opt.debug) {
                                Debug.logDayWf(10575, '2018-04-01')
                                Debug.logDayWf(10179, '2018-04-01')
                            }
                            console.green('ok')
                            // stop = true
                        } else {
                            undoFunc_p0()
                            undoFunc_p1()
                            if(opt.methods.has('move_pause')) {
                                if(!m.solve_overstaffing_problem_by_moving_pause(per).success) {
                                    console.error('overstaff not solve for ' + per.dateKey)
                                }
                            }
                        }
                    }
                })
            })
        } //end solve_overstaffing_problem

    /**
     * En plus de rendre la pause bonne on corrige le temps travaillé ce jour-ci
     * pour qu'il soit égale à nb_minutes_wanted_at_final, on va essayé d'enlever le plus possible
     * au début et à la fin de la journée pour atteindre ce nombre on va garder le meilleure entre beaucoup au début et un peu à la fin 
     * ou beaucoup à la fin et un peu au début
     * car quand on change de pause par exemple on peut passer d'une pause compté à une pas compté dans le temps de travail
     * inter est une intervention quelconque de la journée que l'on doit corriger elle permet d'avoir la personne et la date
     */
    resolve_pause_and_time_in_day(inter,opt3 = new opt_v2_schedule_planning({}),internal_arguments={},{do_not_call_optimum_schedule_on_extremity,nb_minutes_wanted_at_final}= {}){//internal_arguments{nbRecursivity,dont_care_about_min_time}
        var args1 = arguments;
        var wf = inter.person
            ,m = this, max_minute_wanted,min_minute_wanted
            ,opt_min_max = opt3.opt_min_max //min et max des heure
            ,to_inc = opt3.include_this_amplitude
            ,probleme
            ,returnF = () => {
                if(opt3.from != 'affect_ref_intervs'){
                    if(!probleme){
                        Debug.check_too_much_worked(wf)
                    }
                }
                return {probleme}
            }
        ;
        const recursive_call = () => m.resolve_pause_and_time_in_day.apply(m,args1)
        controls_enabled(()=>{
            if(opt3 instanceof opt_v2_schedule_planning == false){
                my_throw("opt3 doit être une instance de opt_v2_schedule_planning")
            }
        })

        max_minute_wanted = min_minute_wanted = nb_minutes_wanted_at_final//facultatif
            
        switch(internal_arguments.nbRecursivity){
            case 1: console.iidebug[58] = idnext('58');break
            case 2: console.iidebug[59] = idnext('59');break
            case 3: console.iidebug[60] = idnext('60');break
            case 4: console.iidebug[61] = idnext('61');break
            case 5: console.iidebug[62] = idnext('62');break
            default:console.iidebug[36] = idnext('36');break
        }

        if(internal_arguments.nbRecursivity > 4) {
            if(v8js){
                probleme = 'anti boucle infinie'
                return returnF()
            }
            my_throw( 'anti boucle infinie')
        }
        if(console.iidebug[36]  == 163674){
            debugger;
        }
        var check_pb = (logDayWf, dont_check_time, pause_can_on_extremity) => {
            controls_enabled(()=>{
                if(to_inc) {
                    var fi = inter.first_interv_sector
                    var li = inter.last_interv_sector
                        //ce n'est pas encore stable la pause à le droit d'être encore sur une extremité
                        if(pause_can_on_extremity) {
                            if(fi.prev && fi.prev.is_pause()) {
                                fi = fi.prev
                            }
                            if(li.next && li.next.is_pause()) {
                                li = li.next
                            }
                        }
                        var pb3 = to_inc.start_minute < fi.startMinute
                        var pb4 = to_inc.end_minute > li.endMinute
                        var pb5 = pb3 || pb4
                    }
                    logDayWf && Debug.logDayWf(inter)
                    if(pb5) {
                        my_throw( "mais ce n'est pas dans les amplitudes demandées")
                    }
                    if(!dont_check_time) {
                        if(wf.nb_minu_worked  > wf.nbMinutesMin) {
                            my_throw( 'pb')
                        }
                    }
                })
        }

        check_pb(false, true, true)
        var pause_info = m.main_resolve_pause_unrespected({
            intervention: inter,
            do_not_put_pause_on: opt3.do_not_put_pause_on
        })
        if(!pause_info.pause_is_ok) {
            probleme = 515
        } else {
                //soit disant la pause serait bonne vérifions cela avec checkpb
                check_pb(false, true, false)
                var d =  inter.get_listInfo_by_date()
                if(!isFinite(nb_minutes_wanted_at_final)){
                    // on ne peut pas travailler plus que nbMinutesMin dans la semaine
                    // et on ne peut pas dépasser d.max dans la journée
                    max_minute_wanted = Math.min(wf.nb_minu_worked - d.worked + d.max,wf.nbMinutesMin)
                    //on ne peut pas faire moins que d.min dans la journée
                    min_minute_wanted = wf.nb_minu_worked - d.worked + d.min
                    // controls_enabled(()=>{
                    //     if(min_minute_wanted > max_minute_wanted){
                    //         my_throw( "ça va pas être possible")
                    //     }
                    // })
                }
                const not_enough_cond = ()=> upper(wf.nb_minu_worked - min_minute_wanted) < 0
                const too_much_cond =() => wf.nb_minu_worked > max_minute_wanted
                const stop_func_cond = ()=> (probleme || not_enough_cond() || too_much_cond())
                if(too_much_cond()) {
                    var rsoeod = m.reduce_start_or_end_of_day({inter,nb_minutes_wanted_at_final: max_minute_wanted,
                        opt3,internal_arguments,allow_pause_on_extremity: true, 
                        v2_do_not_respect_min_time_after_before: true
                    })
                    probleme = rsoeod.probleme
                    if(stop_func_cond()){
                        if(!probleme) probleme = 517
                        return returnF()//comme on a pas pu réduire les heure l'action n'est pas possible
                    }
                    // my_throw( "1 : ce n'est pas une erreur je check que ça y passe bien")
                    internal_arguments.nbRecursivity = (internal_arguments.nbRecursivity || 0) + 1
                    internal_arguments.dont_care_about_min_time = rsoeod.dont_care_about_min_time
                    return recursive_call()
                }else if( not_enough_cond() ){
                    console.iidebug[65] = idnext('65')
                    if(console.iidebug[65] == 21114){
                        // debugger
                    }
                    if(!do_not_call_optimum_schedule_on_extremity){
                        m.solve_not_enough_worked([wf],{
                            upper_nb_minutes_wanted_at_final: nb_minutes_wanted_at_final,
                            date: inter.date,
                            put_in_main_sector: true,
                            // on va éviter des récursivités alternées
                            // entre solve_not_enough_worked et 
                            // resolve_pause_and_time_in_day
                            do_not_check_pause_and_time: true,
                            opt_min_max
                        })
                        if(stop_func_cond()){
                            probleme = 516
                            return returnF()//comme on a pas pu réduire les heure l'action n'est pas possible
                        }
                        return recursive_call()
                    }
                }
            }
            return returnF()
        }

        reduce_start_or_end_of_day({
            inter,nb_minutes_wanted_at_final,
            opt3 = new opt_v2_schedule_planning({}),
            internal_arguments = {},
            reduce_time_at_minimal = false,
            do_not_reduce_both_side,
            let_space_around_pause,
            reduce_both_side,
            time_to_rem_by_side,
            reduce_end,
            reduce_start,do_not_increase_supt,
            allow_pause_on_extremity = false,
            v2_do_not_respect_min_time_after_before = false
        }){
            console.iidebug[46] = idnext('46')
            if(console.iidebug[46] == 420471){
                debugger;
            }
            controls_enabled(()=>{if(opt3 instanceof opt_v2_schedule_planning == false){}})
            var probleme = 0;
            var ret = ()=>{
                return {
                    probleme,
                    // dont_care_about_min_time
                }
            }
            var check_pb = (logDayWf, dont_check_time, pause_can_on_extremity) => {
                controls_enabled(()=>{
                   var pb = to_inc
                   if(pb) {
                       var fi = inter.first_interv_sector
                       var li = inter.last_interv_sector
                       //ce n'est pas encore stable la pause à le droit d'être encore sur une extremité
                       if(pause_can_on_extremity) {
                           if(fi.prev && fi.prev.is_pause()) {
                               fi = fi.prev
                           }
                           if(li.next && li.next.is_pause()) {
                               li = li.next
                           }
                       }
                       var pb3 = to_inc.start_minute < fi.startMinute
                       var pb4 = to_inc.end_minute > li.endMinute
                       var pb5 = pb3 || pb4
                   }
                   logDayWf && Debug.logDayWf(inter)
                   if(pb5) {
                       my_throw( "mais ce n'est pas dans les amplitudes demandées")
                   }
                   if(!dont_check_time) {
                       if(wf.nb_minu_worked  > wf.nbMinutesMin) {
                           my_throw( 'pb')
                       }
                   } 
               })
            }
            var m = this
                ,wf = inter.person
                ,to_inc = opt3.include_this_amplitude
                ,seria_desa = inter.v3_serialize()
                ,end_day = inter.last_minu_worked
                ,start_day = inter.first_minu_worked
                ,desa
                ,d = inter.get_listInfo_by_date()
                // ,dont_care_about_min_time = opt3.dont_care_about_min_time
            ;

            if(!to_inc && !allow_pause_on_extremity){
                to_inc = inter.listInfo.amplitudes_to_include()
            }
            console.iidebug[46];
            if(console.iidebug[46] == 600372){
                debugger
            }
            controls_enabled(()=>{if(start_day === void 8){my_throw( 'not worked date')}})
            
            const get_unplanif_time = ()=>{
                if(reduce_both_side !== void 8){
                    var to_unplanif_time = time_to_rem_by_side
                }else if(nb_minutes_wanted_at_final){
                    var to_unplanif_time = upper(wf.nb_minu_worked - nb_minutes_wanted_at_final)
                    if(time_to_rem_by_side < to_unplanif_time){
                        to_unplanif_time = time_to_rem_by_side
                    }
                }
                assert_finite(to_unplanif_time)
                return to_unplanif_time && upper(to_unplanif_time)
            }

            if(reduce_time_at_minimal){
                let li = wf.listInfo_by_date[inter.date]
                var to_unplanif_time = time_to_rem_by_side = li.worked - li.min
                to_unplanif_time = upper(to_unplanif_time)
                nb_minutes_wanted_at_final = wf.nb_minu_worked - to_unplanif_time
            }else{
                if(reduce_start || reduce_end){
                    var to_unplanif_time = time_to_rem_by_side
                }else{
                    var to_unplanif_time = get_unplanif_time()
                }
                assert_finite(to_unplanif_time)
                to_unplanif_time = upper(to_unplanif_time)
                nb_minutes_wanted_at_final = wf.nb_minu_worked - to_unplanif_time
                if(reduce_both_side){
                    nb_minutes_wanted_at_final = wf.nb_minu_worked - to_unplanif_time
                }
            }
            controls_enabled(()=>{
                if(!isFinite(nb_minutes_wanted_at_final)){
                    throw "n'est finite"
                }
            })
            // if(to_unplanif_time < conf.minTime && wf.nb_minu_worked <= wf.nbMinutesMin){
            //     return ret()
            // }
            var before_common_start_end = () => {
                desa = new Intervention(seria_desa);
                desa.sector = null
                desa.rules = {
                    v2_break_if_min_time_not_ok: true
                }
                //comme on desaffecte cela concerne l'intervention précédent la désaffection ou bien qui est après
                if(v2_do_not_respect_min_time_after_before){
                    desa.rules.v2_do_not_respect_min_time_after_before = true
                }else{
                    desa.rules.safe_insertion = true
                }
                if(time_to_rem_by_side !== void 8 && reduce_both_side){
                    return time_to_rem_by_side
                }
                return get_unplanif_time()// return to_unplanif_time
            }
            var undo_is_useless;
            var after_common_start_end = function(st,en) {
                undo_is_useless = true
                if(st >= en) return;
                desa.set_minu(st, en)
                if(desa.time > 0) {
                    var asked_start = desa.startMinute
                    var asked_end = desa.endMinute
                    var adi_desa = wf.addInterv(desa)
                    if(adi_desa.has_been_insert) {
                        undo_is_useless = false
                        if(desa.startMinute != asked_start || asked_end != desa.endMinute) {
                            console.iidebug[22];
                            probleme = 511
                            console.error('la désaffection n\'a pas été faite comme demandée console.iidebug[46] =' + console.iidebug[46] )
                        }
                    }

                }
            }
            //on ne peut pas déplanifier n'importe comment
            //si il y a to_inc = opt3.include_this_amplitude
            //on va donc ne pas déplanifier à l'intérieure de l'amplitude
            var unplanif_end = function() {
                let unplanif_end = true
                var to_unplanif_time = before_common_start_end()
                let st = end_day - upper(to_unplanif_time)
                let en = end_day
                if(do_not_increase_supt){
                    var li = wf.listInfo_by_date[desa.date]
                    let close_info = wf.closing_info({end_minute: li.last_minu_worked,date: li.date})
                    let was_supt = close_info.is_supplement_time()
                    if(!was_supt){
                        close_info = wf.closing_info({end_minute: st,date: li.date})
                        if(close_info.is_supplement_time()){
                            unplanif_end = false
                        }
                    }
                }
                if(unplanif_end){
                    if(to_inc && st < to_inc.end_minute) {
                        st = to_inc.end_minute
                    }
                    if(!internal_arguments.dont_care_about_min_time){
                        st = Math.max(desa.startMinute,d.min_end_day)
                    }
                    after_common_start_end(st,en)
                }
            }
            var unplanif_start = function() {
                var to_unplanif_time = before_common_start_end()
                let st = start_day
                let en = start_day + to_unplanif_time
                let unplanif_start = true
                if(do_not_increase_supt){
                    var li = wf.listInfo_by_date[desa.date]
                    let open_info = wf.opening_info({start_minute: li.first_minu_worked,date: li.date})
                    let was_supt = open_info.is_supplement_time()
                    if(!was_supt){
                        open_info = wf.opening_info({start_minute: en,date: li.date})
                        if(open_info.is_supplement_time()){
                            unplanif_start = false
                        }
                    }
                }
                if(unplanif_start){
                    if(to_inc && en > to_inc.start_minute) {
                        en = to_inc.start_minute
                    }
                    if(!internal_arguments.dont_care_about_min_time){
                        en = Math.min(en,d.max_end_day)
                    }
                    after_common_start_end(st,en)
                }
            }
            var undo = m.v2_try_a_change({
                date: inter.date,
                person: wf
            })
            var zscore_global1 = m.global_score_ignoring_sectors()
            if(!reduce_end){
                unplanif_start()
            }
            var to_unplanif_time2 = wf.nb_minu_worked - nb_minutes_wanted_at_final
            var zscore_global2 = m.global_score_ignoring_sectors()
            if(!reduce_end && !undo_is_useless){
                undo()
            }
            if(!reduce_start){
                unplanif_end()
            }
            var zscore_global3 = m.global_score_ignoring_sectors()
            var to_unplanif_time3 = wf.nb_minu_worked - nb_minutes_wanted_at_final

            //quel était le meilleur coté
            var denosc = (to_unplanif_time - to_unplanif_time2)
            var denoend = (to_unplanif_time - to_unplanif_time3)
            if(denosc && denoend) {
                var startsc = (zscore_global1 - zscore_global2) / denosc
                var endsc = (zscore_global1 - zscore_global3) / denoend
                var start_is_best = startsc > endsc
            } else {
                var start_is_best = denosc > 0
            }
            if(start_is_best) {
                !undo_is_useless && undo()
                unplanif_start()
                if(!do_not_reduce_both_side){
                    unplanif_end()
                }
            } else {
                if(!do_not_reduce_both_side){
                    //on a déjà fait le unplanif_end
                    unplanif_start()
                }
            }
            if(!reduce_time_at_minimal){
                if(nb_minutes_wanted_at_final < wf.nb_minu_worked){
                    // apparemment on ne peut pas enlever assez
                    probleme = true
                    if(opt3.allow_to_go_more_that_nb_minutes_wanted_at_final){
                        // ce n'est grâve que si cela augmente le temps
                        // de travail au dela du temps autorisé si allow_to_go_more_that_nb_minutes_wanted_at_final
                        if(wf.nb_minu_worked > wf.nbMinutesMin && opt3.allow_to_move_time_between_days){
                            m.reduce_time_worked_to_respect_max_time([wf])
                            if(wf.nb_minu_worked > wf.nbMinutesMin){
                                // dont_care_about_min_time = true
                            }else{
                                probleme = false
                            }
                        }
                    }
                }
            }

            //comme on a enlever une heure la pause est peut être mal
            //placé, par exemple la journée pourrait finir par une pause
            check_pb(false, true, true)
            return ret()
        }

        solve_overstaffing_problem_by_moving_pause(per) {
            var m = this;
            var intervs = m.get_two_intervs_ordered(per)
            var person = intervs[0].person
            var next = intervs[0].next
            var success = false
            if(next.is_pause()) {
                // on va d'abord essayer de décaller la pause de la première personne un peu plus à gauche
                // pour enlever le surreffectif
                var old_pause = next
                var seria_old_pause = old_pause_interv.v3_serialize()

                var pause = new Intervention(seria_old_pause)
                var pause_time = pause_interv.time;
                pause_interv.startMinute = per.startMinute
                pause_interv.endMinute = per.startMinute + pause_time

                // var prev_seria = intervs[0].v3_serialize()
                // var prev_pause = new Intervention(prev_seria)
                // prev_pause_interv.endMinute = pause_interv.startMinute
                // before_pause_interv.rules = {}

                //après la pause on étend l'intervention déjà présente pour qu'elle commence juste après la nouvelle pause
                var after_pause_seria = old_pause_interv.next.v3_serialize()
                var after_pause = new Intervention(after_pause_seria)
                my_throw( "cette fonction n'a pas l'air finie after_pause_interv n'est pas définie")
                after_pause_interv.startMinute = pause_interv.endMinute
                after_pause_interv.rules = {
                    v2_break_if_min_time_not_ok: true
                }
                old_pause_interv.no_more_a_pause()
                //la pause ne sera pas bonne à la suite de cette ajout c'est pour cela qu'on l'a dépausé juste avant
                person.addInterv(after_pause)
                if(after_pause_interv.hasBeenInsert) {
                    person.addInterv(pause)
                    if(pause_interv.hasBeenInsert) {
                        success = true
                        console.green('overstaff solve via pause for ' + per.dateKey)
                    }
                }
            }
            return {
                success
            }
        }

        sort_array_by_score(a1,a2,_wf){
            if(_wf){
                let date1 = a1.amp.date, date2 = a2.amp.date, lbd = _wf.listInfo_by_date
                //si la journée est déjà travaillée la mettre au début de la liste
                //On ne peut pas juste faire lbd[date1].worked à cause des absences
                var worked_d1 = (_wf.is_worked(date1) && lbd[date1].worked) || 0
                var worked_d2 = (_wf.is_worked(date2) && lbd[date2].worked) || 0
                if(worked_d1 !== 0 || 0 !== worked_d2){
                    return !worked_d1 - !worked_d2
                }                    
            }
            return (a1.zscore - a2.zscore) || ( (a1.nb_recursivity || 0) - (a2.nb_recursivity|| 0) ) // le plus grand à la fin
        }
        /**
         * ArrayOfArrayByDay contient des tableau ou le dernier élément est l'amplitude(startMinute à endMinute) la meilleure pour 
         * améliorer le score 
         * Chaque tableau de ArrayOfArrayByDay est pour un jour particulier
         * Attention c'est un tri in place qui va modifier le tableau d'origine
         * On a donc 
         *     [    
         *         // Lundi
         *         [{
         *             {amp:..., zscore: 12},
         *             {amp:..., zscore: 19},
         *             {amp:..., zscore: 35}
         *         },
         *         // Mardi, le jour est contenu dans amp ainsi que l'heure de début, pour avoir l'heure de fin il faut ajouter
         *         // le temps pour lequel on a calculer les scores
         *         {
         *             {amp:..., zscore: 15},
         *             {amp:..., zscore: 18},
         *             {amp:..., zscore: 22},
         *             {amp:..., zscore: 47} // comme 47 > 35  Mardi est après lundi
         *         }]
         *     ]
         */
         sort_arrays(ArrayOfArrayByDay,_wf) {
            var m =this;
            // Créer un tableau de tableau de sort_arrays
            // score par jour trié selon le meilleure score de chaque sous tableau
            // Cela permet de commencer par la journée qui a le plus besoin d'aide
            var j = 0;
            ArrayOfArrayByDay.forEach(function(array_score) {
                if(array_score.length > 0){
                    ArrayOfArrayByDay[j++] = array_score
                }
            })
            ArrayOfArrayByDay.length = j
            ArrayOfArrayByDay.sort(function(array_score1, array_score2) {
                var a1 = last(array_score1), a2 = last(array_score2)
                return m.sort_array_by_score(a1,a2,_wf)
            })
            return ArrayOfArrayByDay;
        }


        // on calcule la somme des scores à réduire pour toute les zones possibles
        // mais comme on est intelligent à chaque fois que l'on se décale d'une demi-heure
        // on enlève
        // juste le score de la demi-_heure décalé et on ajoute celui de celle ajouté
        // le but étant de placer la personne d'un seul bloc sur la journée
        // la fin des tableau est là ou il faut ajouter du monde
        getArrayGlobalZscoreByDay(time = 7, wf, opt4 = {}) {
            /* @TODO optimize */
            var m = this;
            // m.total_info_amp.forEach(function(amp /*PeriodNeed*/){
            //  amp.v2_last_zscore = amp.v2_zscore_period()
            //  if(Number.isNaN(amp.v2_last_zscore)){
            //      console.error('nan');my_throw( '';;)
            //  }
            // })
            var m = this;
            controls_enabled(()=>{
                if(lower(time) != time){
                    my_throw( 'le temps demandé est mal arrondi')
                }
            })
            // var pena = Rotation.penalizePeriodForWf(m.all_wfs)
            var nb_minute_by_day = time
            var nb_minutes_in_zone;
            var i_deb, deb_amp;
            var i_bestZoneToPlacePlanif = null;
            var greaterZscore_by_date = null;
            var i_best_zone_to_place_by_date = {}
            var z_score_zone = []

            var all_amp_open = m.v2_get_all_amp_open(opt4)
            var prev_amp ;
            for (var j = 0; j < all_amp_open.length; ++j) {
                console.iidebug[50] = idnext('50')
                var amp = all_amp_open[j]
                controls_enabled(()=>{
                    if(amp.startMinute === undefined){
                        my_throw( 'error')
                    }
                })
                
                if(opt4.dates && opt4.dates.indexOf(amp.date) < 0){
                    debugger
                    continue
                }
                if(opt4.start_minute !== undefined && amp.startMinute < opt4.start_minute){
                    continue
                }
                if(opt4.end_minute !== undefined && amp.endMinute > opt4.end_minute){
                    continue
                }
                var d = wf.listInfo_by_date[amp.date]
                if(!d || wf.lis_absence.has(d) || opt4.do_not_planif_on_planified_days && d.first_interv_sector){
                    continue //la personne travail déjà ce jour là on ne va pas donc l'y planifier
                }
                
                //var prev_amp = all_amp_open[j - 1]
                idnext('no_name2')

                /*
                 * z_score_zone contient pour chaque indice d'amplitude de all_amp_open la somme des zscores des
                 * amplitudes de tel sorte que la zone soit de time heure
                 * Par exemple pour un cdi 35h sur 5 jour ce sera souvent la somme des
                 * zscores de 7 + 1 heures consécutive de 10 à 18 de 10.5 à 18.5 ... de 12 à 20
                 */
                 if(!prev_amp || prev_amp.date != amp.date) {
                    nb_minutes_in_zone = 0; //il y a pour le moment 0 heures d''amplitude dans la zone que l'on veut sommer
                    i_deb = j // cette zone commence à j car on a changer de jour
                    z_score_zone[i_deb] = 0
                    deb_amp = amp
                }
                //si on a atteind la fin de la zone du bloc alors on décale de une demis heure notre zone
                if(nb_minutes_in_zone >= nb_minute_by_day && nb_minute_by_day > 0) {
                    var old_start_of_zone = i_deb
                    ++i_deb
                    z_score_zone[i_deb] = z_score_zone[old_start_of_zone] - all_amp_open[old_start_of_zone].v2_attractivity() + amp.v2_attractivity()
                } else {
                    //sinon on ajoute à notre zone le score de l'amplitude courante
                    var zsc = amp.v2_attractivity()//.v2_sum_attractivity_on_period()
                    z_score_zone[i_deb] += zsc
                    nb_minutes_in_zone = amp.endMinute - deb_amp.startMinute
                }
                if(!zone_info || zone_info.i_deb != i_deb) {
                    var iDebug8 = console.iDebug
                    var zone_info = {
                        zscore2: z_score_zone[i_deb],
                        i_deb: i_deb,
                        amp: all_amp_open[i_deb],
                        is_zone_info: true
                    }
                    controls_enabled(()=>{
                        if(Number.isNaN(zone_info.zscore2)) {
                            my_throw( 'error NaN')
                        }
                    })
                    if(!i_best_zone_to_place_by_date[amp.date]) {
                        i_best_zone_to_place_by_date[amp.date] = [zone_info]
                    } else {
                        i_best_zone_to_place_by_date[amp.date].push(zone_info)
                    }
                } else {
                    zone_info.zscore2 = z_score_zone[i_deb]
                }
                prev_amp = amp
            }

            // pour chaque jour trier par score, plus c'est haut plus il faut affecter
            forOf(i_best_zone_to_place_by_date, function(array_score, date) {
                //il se peut que plus tard on ait à comparer des array_score avec différents time 
                //entre eux et pour cela on a besoin que l'attractivité en fonction du temps
                array_score.forEach((a)=>{
                    a.zscore = a.zscore2 / time
                })
                //Pour chaque jour mettre à la fin les horaires qui permettent de réduire le plus les besoins
                array_score.sort((a, b) => a.zscore - b.zscore /*petit debut -> grand fin*/ )
            })
            return i_best_zone_to_place_by_date
        }

        get_first_last_inter_same_day(theInter, nextORprev) {
            if(nextORprev == 'next'){
                return theInter.first_interv_sector
            }
            return theInter.last_interv_sector
        }

        /**
         * Chaque instance à son  propre mean_rotation_ratio
         */
        get_instance_of_get_rotation_score(){
            const m = this
            const mean_rotation_ratio = {}
            let update_get_ratio_relative;
            let mean_has_been_init = false;
            const init_mean = () => Object.keys(m.rotations).forEach((name) => mean_rotation_ratio[name] = 0)
            init_mean()
            const f = function(opt = {}){
                let {do_not_update_mean} = opt
                if(Debug.controls_enabled_running){
                    do_not_update_mean = true
                }
                var all_wfs = array_create({from: m.all_wfs}).sort(function(wf1,wf2){
                    return wf1.wfId - wf2.wfId
                })
                //les moyennes des rotation ne doivent parfois pas être mise à jour car on a pas terminer de remettre toutes
                //les heures aux personnes
                if(opt.force_to_update_mean || !mean_has_been_init ) {
                    mean_has_been_init = true
                    init_mean()
                    var is_not_null = 0;
                    var debug_ratio = []
                    all_wfs.forEach(function(wf) {
                        forOf(mean_rotation_ratio, function(val, name) {
                            mean_rotation_ratio[name] += wf.rotation_ratio[name] || 0
                            is_not_null += wf.rotation_ratio[name] || 0
                            debug_ratio.push(mean_rotation_ratio[name])
                            controls_enabled(()=>{
                                if(Number.isNaN(mean_rotation_ratio[name])) {
                                    my_throw( 'error NaN')
                                }
                            })
                        })
                    })
                    if(opt.debug_ratio) {
                        console.log(debug_ratio)
                    }
                    if(!is_not_null) {
                        console.error('les rotations ne semble pas à jour') //fakecomment
                    }
                    forOf(mean_rotation_ratio, function(val, name) {
                        mean_rotation_ratio[name] = mean_rotation_ratio[name] / all_wfs.length
                        controls_enabled(()=>{
                            if(Number.isNaN(mean_rotation_ratio[name])) {
                                my_throw( 'error NaN')
                            }
                        })
                    })
                }
                // @TODO: DELETE
                //     if(wf.rotation.Saturday){
                //         all_wfs[0].rotation_ratio.Saturday = all_wfs[0].rotation.Saturday + 5
                //         return true
                //     }
                // })

                update_get_ratio_relative = function(wf,name){
                    let rota_diff_corrected;
                    const power = 2 /*doit être un nombre pair car on multiple par signe*/
                    const mean_rat = mean_rotation_ratio[name]
                    const rot_poss =  wf.rotation_possible[name]
                    if(!rot_poss){
                        wf.rotation_ratio_relative_to_other_wf[name] = 0;
                    }else{
                        var rota_diff = (wf.rotation_ratio[name] || 0) - (mean_rotation_ratio[name] || 0)
                        var rota_diff_tolerance =  rota_diff - (m.rotations[name].tolerance / rot_poss)
                        //si en enlevant la tolérance on passe en dessous de la moyenne c'est qu'on est presque à la moyenne
                        //il faut donc considérer que notre ratio d'écart est null, et ne pas essayer d'enlever des rotation
                        if(rota_diff > 0 && rota_diff_tolerance < 0){
                            rota_diff_corrected = 0
                        }else{
                            var signe = rota_diff < 0 ? -1 : 1
                            rota_diff_corrected = signe * Math.pow(rota_diff, power)
                        }
                        wf.rotation_ratio_relative_to_other_wf[name] = rota_diff_corrected
                    }
                    return {
                        rota_diff_tolerance: wf.rotation_ratio_relative_to_other_wf[name],
                        rota_diff,
                        rota_diff_corrected
                    }
                }
                //on obtient pour chaque rotation la différence par rapport à la moyenne
                all_wfs.forEach(function(wf) {
                    forOf(mean_rotation_ratio, function(val, name) {
                        //ça marche mieux de réduire le carré que la valeur absolue
                        var rot_rat = wf.rotation_ratio[name]
                        controls_enabled(()=>{
                            if(isNaN(rot_rat)){
                                my_throw( 'error NaN')
                            }
                        })
                        if(wf.wfId == 10416){
                            // debugger
                        }
                        update_get_ratio_relative(wf,name)
                        controls_enabled(()=>{
                            if(Number.isNaN(wf.rotation_ratio_relative_to_other_wf[name])) {
                                my_throw( 'error NaN')
                            }
                        })
                    })
                })

                all_wfs.forEach(function(wf) {
                    wf.zscore_rotation =
                    Math.max(wf.rotation_ratio_relative_to_other_wf.Opening || 0, 0) +
                    Math.max(wf.rotation_ratio_relative_to_other_wf.Closing || 0, 0) +
                    Math.max(wf.rotation_ratio_relative_to_other_wf.Saturday || 0, 0)
                })

                //ici c'est bien m.all_wfs et non all_wfs car c'est utilisé plus tard
                if(!Debug.controls_enabled_running){
                    m.all_wfs.sort(function(wf1, wf2) {
                        return wf2.zscore_rotation - wf1.zscore_rotation // le pire (plus grand au début)
                    })
                }
                var rotation_score = 0;
                var table = []
                all_wfs.forEach(function(wf) {
                    rotation_score += wf.zscore_rotation
                    table.push({
                        wfid: wf.wfId,
                        Opening: wf.rotation.Opening,
                        Closing: wf.rotation.Closing,
                        Saturday: wf.rotation.Saturday,
                        Opening_rat: wf.rotation_ratio.Opening,
                        Closing_rat: wf.rotation_ratio.Closing,
                        Saturday_rat: wf.rotation_ratio.Saturday,
                        mean_Opening: mean_rotation_ratio.Opening,
                        mean_Closing: mean_rotation_ratio.Closing,
                        mean_Saturday: mean_rotation_ratio.Saturday,
                        Opening_relat: Math.max(wf.rotation_ratio_relative_to_other_wf.Opening || 0, 0),
                        Closing_relat: Math.max(wf.rotation_ratio_relative_to_other_wf.Closing || 0, 0),
                        Saturday_relat: Math.max(wf.rotation_ratio_relative_to_other_wf.Saturday || 0, 0),
                        zscore_rotation: wf.zscore_rotation
                    })
                })
                if(opt.logTable) {
                    if(!v8js) {
                        console.table(table)
                    }
                    console.log(rotation_score)
                    // console.trace()
                }
                return rotation_score
            }

            f.is_poss_del_rota = function(from_wf,rota_name){
                let old_from_r = from_wf.rotation_ratio_relative_to_other_wf[rota_name]
                if(old_from_r <= 0){
                    // la personne est en retard ou au casi-équilibre (avec la tolérance) 
                    // donc ce n'est pas la peine de lui enlever des rotations
                    return false
                }
                let impossible = false
                // simuler le changement et vérifier qu'une personne en dessous 
                // de la moyenne ne passe pas au dessus pour les rotations
                // et inversement qu'une personne au dessus de la moyenne 
                // ne passe pas en dessous
                // un score en dessous de la moyenne = nécessité d'augmenter les rotations
                // un score au dessus de la moyenne = nécessité de diminuer des rotations
                simulate_change: {
                    from_wf.rotation[rota_name] -= 1
                    from_wf.update_rotation_ratio(rota_name)
                    const from_r = update_get_ratio_relative(from_wf,rota_name)
                    if(from_r.rota_diff < 0){
                        // si on fait cette action la personne from_wf va se retrouver en
                        // position ou elle pourra recevoir des rotations alors qu'on vient 
                        // de lui en enlever, soit on est recepteur, soit on est envoyeur
                        // mais il ne faut pas qu'une personne soit les deux sous peine de 
                        // totalement changer son planning
                        impossible = true
                    }
                }
                restore: {
                    from_wf.rotation[rota_name] += 1;
                    from_wf.update_rotation_ratio(rota_name)
                    from_wf.rotation_ratio_relative_to_other_wf[rota_name] = old_from_r
                }
                return !impossible
            }
            /**
             * Est ce que faire passer une rotation de from_wf vers to_wf est
             * une bonne idée ou non ?
             */
            f.is_poss_add_rota = function(to_wf,rota_name){
                let impossible = false
                let old_to_r = to_wf.rotation_ratio_relative_to_other_wf[rota_name]
                if(old_to_r >= 0){
                    //la personne est en avance ou au casi-équilibre (avec la tolérance) 
                    //donc ce n'est pas la peine de lui ajouter des rotations
                    return false
                }
                // simuler le changement et vérifier qu'une personne en dessous 
                // de la moyenne ne passe pas au dessus pour les rotations
                // et inversement qu'une personne au dessus de la moyenne 
                // ne passe pas en dessous
                // un score en dessous de la moyenne = nécessité d'augmenter les rotations
                // un score au dessus de la moyenne = nécessité de diminuer des rotations
                simulate_change: {
                    to_wf.rotation[rota_name] += 1
                    to_wf.update_rotation_ratio(rota_name)
                    const to_r = update_get_ratio_relative(to_wf,rota_name)
                    if(to_r.rota_diff > 0){
                        // si on accepte ce changement la personne to_wf va se retrouver 
                        // en surreffectif de rota_name
                        impossible = true
                    }
                }

                restore: {
                    to_wf.rotation[rota_name] -= 1;
                    to_wf.update_rotation_ratio(rota_name)
                    to_wf.rotation_ratio_relative_to_other_wf[rota_name] = old_to_r
                }
                return !impossible
            }

            f.get_mean = function(rota_name){
                return mean_rotation_ratio[rota_name]
            }
            return f;
        }

        get_rotation_score(opt = {}) {
            return (this.get_instance_of_get_rotation_score())(opt)
        }

        /*
         * Améliore le ratio de rotation des ouvertures / fermeture et samedi pour qu'il soit équitable
         */
         v2_improve_rotation(opt2 = {}) {
            // console.error('test');my_throw( 'test')
            const {get_rotation_score,debug_callback} = opt2
            var m = this;
            // on trie pour être sur que la somme soit toujour calculer de la même manière
            //en plus d'obtenir le score ça met en premier les personnes qui ont le pire score (= le plus grand)
            // dans m.all_wfs
            // @TODO DELETE

            // var get_rotation_score = m.get_rotation_score.bind(m)
            var old_score, score = old_score = get_rotation_score({
                logTable: opt2.log_table_before
            })
            //pour chaque personne faire des échange d'horaire de rotation avec d'autre
            m.all_wfs.some(function(wf) {
                console.iDebug18 = (console.iDebug18 || 0) + 1
                var worst_rotation = null;
                var worst_rotation_val;
                //trouver la pire rotation de la personne
                var rot_rat = wf.rotation_ratio_relative_to_other_wf
                var worst_rots = Object.keys(wf.rotation_ratio_relative_to_other_wf).sort((rot_rame1, rot_name2) => {
                    return rot_rat[rot_rame1] - rot_rat[rot_name2]
                })
                // forOf(wf.rotation_ratio_relative_to_other_wf, function(val,rot_name){
                //     if(worst_rotation === null || val > worst_rotation_val){
                //         worst_rotation = rot_name
                //         worst_rotation_val = val
                //     }
                // })
                var not_possible_saturday = null
                var nobody_can_worked_satuday_under = null
                while (worst_rotation = worst_rots.pop()) {
                    if(10416 == wf.wfId){
                        // debugger
                    }
                    if(!get_rotation_score.is_poss_del_rota(wf,worst_rotation)){
                        return; //le nombre de rotation n'est pas assez disproportionné par rapport aux autres, passer à la rotation suivante
                    }
                    // if(rot_rat[worst_rotation] <= 0 
                    //     || wf.rotation[worst_rotation] == 0
                    //     /*<= Rotation.instances[worst_rotation].tolerance*/ ) {
                    //     /*continue;*/ //le nombre de rotation n'est pas assez disproportionné par rapport aux autres, passer à la rotation suivante
                    // }
                    var score_bef = score
                    controls_enabled(()=>{
                        if(score != get_rotation_score()) {
                            console.log(score)
                            my_throw( 'not logic')
                        }
                    })

                    console.iidebug[7] = idnext('7')
                    if(console.iidebug[7] == 49064){
                        debugger
                    }
                    if(worst_rotation == 'Saturday' && nobody_can_worked_satuday_under !== null && nobody_can_worked_satuday_under >= wf.rotation[worst_rotation]){
                        //on avait pas réussi à mettre personne d'autre le samedi avec un score de rotation
                        //de  "nobody_can_worked_satuday_under" alors on ne trouvera personne avec pour 
                        //un score (qu'on veut réduire) qui est supérieur
                        continue
                    }
                    debug_callback && debug_callback()
                    var rrfw = m.rotations[worst_rotation].reduce_rotation_for_wf(wf, score, get_rotation_score,{
                        debug_callback, nobody_can_worked_satuday_under,

                    })
                    debug_callback && debug_callback()
                    score = rrfw.score
                    if(rrfw.nobody_can_worked_satuday !== undefined){
                        nobody_can_worked_satuday_under = worst_rotation
                    }
                    controls_enabled(()=>{
                        if(typeof score == 'undefined') {
                            my_throw( 'le score doit toujours être retouné par reduce_rotation')
                        }
                        if(score != get_rotation_score()) {
                            my_throw( 'not logic')
                        }
                    })
                    if(score < score_bef) {
                        break; //on a réussi à améliorer le score maintenant on peut passer à la personne suivante
                    }
                }
            })
            return {
                improvement: score < old_score,
                get_rotation_score,
                zscore_rotation: score

            } // si vrai alors on a amélioré les rotations en diminuant le score qui est en fait un antiscore
        }


        v2_schedule_planning__addIntervention(opt7) {
            if(opt7.start_minute == 10 * H && opt7.date == '2018-03-26') {
                ////debugger
            }
            var m = this,
            date = opt7.date,
            start_minute = opt7.start_minute,
            end_minute = opt7.end_minute,
            opt3 = opt7.opt3,
            ret = opt7.ret,
            wf = opt7.wf,
            nb_minutes_wanted_at_final = opt7.nb_minutes_wanted_at_final;
            if(wf.wfId == 25373 && date == '2019-02-04'){
                // debugger
            }
            // Debug.is_round(nb_minutes_wanted_at_final)
            // il faut maintenant choisir une entité
            var sector = m.blockArr["0"].sectorEntArr["0"]
            var minTime = sector.minTimeSector

            if(start_minute < 8 * H) return; //@TODO remove

            // Il se peut que la personne ne travaille pas ce jour là dans ce cas l'intervention ne sera pas affecté
            // on peut le voir dans le interhasBeenInsert
            if(opt3.get_backup_functions) {
                ret.backup_functions.push(m.v2_try_a_change({
                    date,
                    person: wf
                }))
            }
            wf.nb_min_minute_fakely_worked = (wf.nb_min_minute_fakely_worked || 0) + 1 * H //on a ajouté une heure destiné à la pause et ça fait planter le console.watch sur la règle des 35 h du coup
            controls_enabled(()=>{
                if(wf.listInfo_by_date[date].first_minu_worked){
                    my_throw( "la journée devrait être vide")
                }
            })
            var opt_inter = {
                startMinute: start_minute,
                endMinute: end_minute,
                rules: {
                    dontMoveExistingInterventionOrPause: true,
                    v2_do_not_respect_min_time_after_before: true
                },
                date: date,
                id: 'placement' + Main.getUniqId(),
                sector: sector,
                // person: wf,
                main: m,
            }
            var inter = new Intervention(opt_inter);
            if(date == '2018-03-29' && wf.wfId == 10324){
                //debugger
            }
            idnext('no_name3')
            console.iidebug[41] = idnext('41')
            // if(console.iidebug[41] == 52852) {
            //     // debugger;
            //     m.logPersonSectorBug()
            //     Debug.logDayWf(wf,date)
            // }
            var adi_inter = wf.addInterv(inter)
            var hasBeenInsert = adi_inter.has_been_insert
            if(hasBeenInsert) {
                controls_enabled(()=>{
                    if(Debug.is_forced_mode){
                        if(inter.startMinute != start_minute){
                            //cela pet arriver si il y a des protected dans la journée 
                            //dans ce cas c'est normal
                            console.warn("ce n'est pas ce qui avait été demandée si il y a des protected ou des pauses différentes dans la journée c'est normal")
                        }
                        if(wf.listInfo_by_date[date].first_minu_worked != opt7.start_minute){
                            my_throw( "la journée commence pas au bon endroit")
                        }
                    }
                })
                // })
                // var debugWf = ['10575', '10179', '10634'];
                // var debug_date = ['2018-04-01'];
                // var debug_ena = false
                // if(debugWf.has(wf.wfId) && debug_date.has(date)) {
                //     debug_ena = true
                //     console.log("l'inter est entre " + start_minute + ' à ' + end_minute +
                //         'le ' + date + 'iDebug' + console.iDebug)
                // }
                var nb_minu_worked = wf.nb_minu_worked

                var probleme = false;
                var to_inc = opt3.include_this_amplitude;
                controls_enabled(()=>{
                    var pb = hasBeenInsert && to_inc
                    if(pb) {
                        var pb3 = to_inc.start_minute < inter.first_minu_worked
                        var pb4 = to_inc.end_minute > inter.last_minu_worked
                        var pb5 = pb3 || pb4
                    }
                    if(pb5) {
                        my_throw( "mais ce n'est pas dans les amplitudes demandées")
                    }
                });
                // Debug.is_round(nb_minutes_wanted_at_final)
                var rpatmid = m.resolve_pause_and_time_in_day(inter,
                        clone(opt3,new opt_v2_schedule_planning({
                            allow_to_go_more_that_nb_minutes_wanted_at_final: opt3.allow_to_go_more_that_nb_minutes_wanted_at_final || opt3.initial_planning
                        }),
                        void 8,
                        nb_minutes_wanted_at_final
                    )
                )
                if(rpatmid.probleme) {
                    hasBeenInsert = false
                    wf.reset_date(date, {
                        even_if_fake_protected: true /*@warning: penser à sauvegarder les protected avant */
                    })
                }
            } else {
                if(opt3.get_backup_functions) {
                    ret.backup_functions.length = 0;
                }
                // zscores_of_the_day.pop()
            }
            wf.nb_min_minute_fakely_worked -= 1 * H //on avait ajouté une heure destiné à la pause
            if(hasBeenInsert) {
                ret.date_ok[date] = ret.date_ok[date] || {}
                ret.date_ok[date][wf.wfId] = true
                if(opt3.get_dates_added) {
                    ret.dates_added = ret.dates_added || []
                    ret.dates_added.push(date)
                }
                ret.last_inter = inter
                Debug.check_too_much_worked(wf)
            }
            return hasBeenInsert
        }

        generate_forced_absence_and_presence(){
            var m = this;
            m.all_wfs.forEach((wf)=>{
                wf.lis_absence = new Set()
                wf.lis_mandatory = new Set()
                const add_forced_absence = function(li){
                    wf.lis_absence.add(li)
                }
                wf.listInfo_sorted.forEach((li)=>{
                    if(!li.max || li.min_start_day == li.max_end_day){
                        add_forced_absence(li)
                    }
                })
                if(conf.consecutive_days_on_2weeks){
                    //si le précédent dimanche est pas travaillé mais le samedi l'était mettre le lundi en tant que non travaillé
                    var prev_sunday_alone_not_worked = wf.previous_sunday_not_worked && !wf.previous_week_with_consecutive_abs
                    if(prev_sunday_alone_not_worked){
                        add_forced_absence(wf.listInfo_by_date[conf.planifStartDate/*lundi*/])
                    }
                    //si le prochain lundi est non travaillé mais le mardi suivant est travaillé mettre dimanche en non travaillé
                    var next_monday_alone_not_worked = wf.next_monday_not_worked && !wf.next_week_with_consecutive_abs
                    if(next_monday_alone_not_worked){
                        add_forced_absence(wf.listInfo_by_date[conf.planifEndDate/*dimanche*/])
                    }
                }
                wf.listInfo_sorted.forEach((li)=>{
                    if(!wf.lis_absence.has(li) && li.working_mandatory){
                        wf.lis_mandatory.add(li)
                    }
                })
            })
        }

    /**
     * Des test sont disponible à test_is_possible_to_add_this_new_day_as_worked
     * cette fonction doit être appelé avant chaque nouvelle date où on l'affecte
     * pour savoir si c'est possible
     * Ne surtout pas prendre les heures en compte dans cette fonction car
     * handle_forced_ampli suppose que l'on a vérifié que la journée
     * @WARNING dates_considered_as_worked peut être vide si on veut savoir si la situation actuelle 
     * est bonne ou non
     */        
     is_possible_to_add_this_new_day_as_worked(wf,dates_considered_as_worked){
        const ret = (info)=>{
            return {
                is_possible: !impossible_to_work,
                info
            }
        }
            //les listInfo que l'on va considéré comme travaillé
            //contient à la fois ceux obligatoires et les nouvelle date qu'on 
            //veut essayé d'ajouter
            const li_considered_as_worked = new Set();
            let reason;
            let impossible_to_work = dates_considered_as_worked.some((d1)=>{
                var li = wf.listInfo_by_date[d1]
                if(!li){
                    reason = 'no_li'
                    return true/*impossible_to_work*/
                }
                if(wf.lis_absence.has(li)){
                    reason = 'is_absence';
                    return true/*impossible_to_work*/
                }
                //simuler que l'on travaille à cette date
                li_considered_as_worked.add(li)
            })
            if(impossible_to_work) return ret({error_message: reason})//la personne ne peut pas travailler ce jour-ci

                const lis_mandatory = wf.lis_mandatory;
            let nb_worked = 0;
            let nb_worked_mandatory_placed = 0;
            wf.listInfo_sorted.forEach((li)=>{
                if(li.working_mandatory){
                    li_considered_as_worked.add(li)
                }
                if(li.is_worked()){
                    if(li.working_mandatory){
                        ++nb_worked_mandatory_placed
                    }
                    ++nb_worked
                }
            })

            // si le nombre de jour restant qui sont obligatoires et pas encore travaillés
            // est égale au nombre de jour qu'il reste à placer alors seule les jours obligatoires sont 
            // possible
            if(lis_mandatory.size - nb_worked_mandatory_placed >= wf.nb_days_by_week - nb_worked){
                impossible_to_work = dates_considered_as_worked.some((date)=>{
                    var d = wf.listInfo_by_date[date]
                    if(!d || !d.working_mandatory){
                        return true
                    }
                })
                if(impossible_to_work) return ret({error_message:'working_mandatory'})//personne suivante
            }

        if(!conf.force_to_have_2_consecutive_absences){
                return ret(); //succès
            }
            var is_forced_absence = (li)=>{
                return !li || wf.lis_absence.has(li)
            }
            var get_nb_absence_to_place_not_forced = () => 7 - wf.nb_days_by_week - wf.lis_absence.size
            var nb_absence_to_place_not_forced = get_nb_absence_to_place_not_forced()

            // Si il a une ou des des absences libres de placement non forcées par 
            // la semaine précédente ou suivante
            // alors regarder si à cause de la semaine suivante ou précédente
            // il ne faudrait pas les forcer
            var ls = wf.listInfo_sorted
            var nb_abs_tota_to_place = wf.nb_days_by_week

            if(nb_absence_to_place_not_forced == 0){
                //toutes les dates d'absence sont placé et ne sont pas aux dates "dates_considered_as_worked"
                //donc on peut inséré aux dates dates_considered_as_worked
                return ret({error_message:"nb_absence_to_place_not_forced_is_0"})
            }
            // Attention à l'absence qui crée des heures worked mais ne doit
            // pas compter comme une journée worked si il n'y a pas d'affectaton dans un vrai secteur
            var is_worked = (li)=>{
                return li.working_mandatory || li.first_interv_sector || li_considered_as_worked.has(li)
            }

            //est ce que la contrainte des deux jours successif est toujours respectée
            var consecutive_days_is_respected = ()=>{
                //comme il n'y a qu'une seule absence que l'on peut librement placée
                //il faut que celle-ci soit à coté d'une absence forcé ou bien
                //que deux forcée soit déjà l'une à coté de l'autre pour en avoir deux consécutive
                let is_possible;
                if(nb_absence_to_place_not_forced == 1){
                    var forced_exist = false;
                    var abs_around_forced_exist = false
                    is_possible = ls.some(function(li,i){
                        if(is_forced_absence(li)){
                            var prev = ls[i - 1]
                            var next = ls[i + 1]
                            if(prev && !is_worked(prev) || next && !is_worked(next)){
                                return true
                            }
                        }
                    })
                    if(!is_possible){
                        reason = 'nb_absence_to_place_not_forced_is_1'
                    }
                }else{
                    var consecutive_days = 0;
                    if(conf.consecutive_days_on_2weeks){
                        consecutive_days = +!!wf.previous_sunday_not_worked;
                    }
                    is_possible = ls.some(function(li,i){
                        consecutive_days = !is_worked(li) ? consecutive_days + 1 : 0;
                        if(conf.consecutive_days_on_2weeks){
                            if(!ls[i+1] && wf.next_monday_not_worked){
                                consecutive_days += 1
                            }
                        }
                        if(consecutive_days >=2){
                            return true
                        }
                    })
                    if(!is_possible){
                        reason = 'consecutive_days_is_not_respected'
                    }
                }
                return is_possible
            }
            impossible_to_work = !consecutive_days_is_respected()
            return ret({
                error_message : reason,
                nb_absence_to_place_not_forced
            })
        }

        // change_hours_cause_opening_closing_time(wf,start_minute,nb_minu_worked_in_day3){
        //     var pause_time = m.get_pause_time(start_minute,nb_minu_worked_in_day3)
        //     //le temps rajouter aux ouverture et fermeture avant le début et la fin
        //     let estimated_end_minute = start_minute + pause_time + nb_minu_worked_in_day3;
        //     var openning_and_closing_time = wf.get_unplaced_time(start_minute,estimated_end_minute)
        // }

       /**
        * On va faire en sorte que le start_minute et end_minute soit dans les bornes
        * du paramétrage
        */
        change_hour_to_respect_rules(wf,date,start_minute_bis,nb_minute_by_day_without_pause,pause_time){
            var not_possible,m = this,d = wf.listInfo_by_date[date]
            ,{max_end_day,min_start_day} = m.get_min_start_and_end({wf,date})
            ,get_betweeen = (val, min, max) => {
                return Math.max(Math.min(val, max), min)
            },
            nb_minute_by_day = get_betweeen(nb_minute_by_day_without_pause, d.min,d.max) + pause_time
            ;
            // Debug.is_round(nb_minute_by_day)
            var start_minute1 = upper(get_betweeen(start_minute_bis, min_start_day, d.max_start_day))
            var end_minute1 = start_minute1 + nb_minute_by_day
            var end_minute1_lower = lower(end_minute1)
            var diff_lower_end = end_minute1 - end_minute1_lower
            var end_minute2_lower = lower(get_betweeen(end_minute1_lower, d.min_end_day, max_end_day));
            var end_minute2 = end_minute2_lower + diff_lower_end

            Debug.is_round(start_minute1)
            Debug.is_round(end_minute2_lower)

            let start_minute = lower(get_betweeen(end_minute2 - nb_minute_by_day,min_start_day, d.max_start_day))
            let end_minute = end_minute2_lower

            var time_done = end_minute - start_minute - pause_time
            if(time_done < d.min || time_done > d.max) {
                not_possible = true
            }
            Debug.is_round(start_minute)
            Debug.is_round(end_minute)
            return {start_minute ,end_minute, not_possible}
        }

        get_min_start_and_end({
            wf,
            date
        }){     
            //il faut vérifier la distance par rapport au jours précédent et suivant
            let libd_d = wf.listInfo_by_date[date]
            ,ls = wf.listInfo_sorted
            ,idx = wf.listInfo_sorted.indexOf(libd_d)
            ,i,j; i = idx - 1, j = idx + 1

            if(conf.min_time_between_days_ignore_absence){
                while(i >= 0 && !ls[i].is_worked()){--i}
                while(j < ls.length && !ls[j].is_worked()){++j}
            }

            //@TODO: ramener l'information de la base de données
            let  previous_day = (i == -1 ? wf.previous_day : ls[i]) || {}
            ,next_day = (j == ls.length ? wf.next_day : ls[j]) || {}
            ,last_minu_prev = first_def(previous_day.last_minu_worked, 0 * H)
            ,first_minu_tomo = first_def(next_day.first_minu_worked, 24 * H)
            ,min_start_day =  Math.max(last_minu_prev + conf.min_time_between_days - 24 * H,libd_d.min_start_day)         
            ,max_end_day = Math.min(24 * H - conf.min_time_between_days + first_minu_tomo,libd_d.max_end_day)
            
            controls_enabled(()=>{
                if(!isFinite(min_start_day) || !isFinite(max_end_day)){
                    my_throw( 'erreur sur les heures')
                }
            })
            return {max_end_day,min_start_day} 
        }

        get_pause_time(start_minute,nb_minu_worked_this_day,date,person){
            return this.pause_dispatcher.get_time_pause_not_counted_in_worktime(start_minute,nb_minu_worked_this_day,date,person)
        }

        get_openning_and_closing_time(wf,start_minute,nb_minu_worked_this_day){

        }
        /*
        * @cibip @check_if_batt_is_possible
        */
        check_if_batt_is_possible({wf,
            method2,
            opt2,
            nb_minute_by_day,
            nb_minu_worked_in_day,
            zscores_of_the_day,
            zone_info,
            I_know_day_is_possible,
            f_day_not_possible,
            pause_is_include_in_nb_minute_by_day = false
        }){ 
            console.iidebug[56] = idnext('56')
            if(92011 == console.iidebug[56]){
                debugger;
            }
            if(pause_is_include_in_nb_minute_by_day){
                my_throw( "ce paramètre n'est plus possible")
            }
            controls_enabled(()=>{
                if(!method2){
                    my_throw('avec method 2 il faut que le retour passe go_to_next_zone_info et donc on ne retourne pas que success_info')
                }
                if(nb_minute_by_day){
                    my_throw( "c'est nb_minu_worked_in_day maintenant")
                }
                if(!opt2 || !nb_minu_worked_in_day || !zscores_of_the_day || !zone_info 
                    || !zone_info.is_zone_info){
                    my_throw( 'missing arguments')
                }
                if( (!I_know_day_is_possible ^ !f_day_not_possible) !== 1){
                    my_throw( "soit le jour est déjà vérifié soit il faut fournir une fonction quand la journée n'est pas possible, les deux paramètres sont exclusifs")
                }
            })
            // if(wf.wfId == 10634){
            //     debugger
            // }
            var m = this;
            var opt3 = opt2.opt3;
            var start_amp = zone_info.amp
            var date = start_amp.date

            // if(91765 == console.iidebug[25]){
            //     debugger
            // }
            // if(Debug.deb145 && start_amp.startMinute == 660){
            //     debugger
            // }
            var go_to_next_zone_info = function(error_code,success_info){
                controls_enabled(()=>{
                    if(!error_code && error_code !== 0){
                        my_throw( 'error code missing')
                    }
                })
                return {
                    go_to_next_zone_info_has_been_called: true,
                    success_info,
                    error_code,
                    find_next_batt: error_code !== 0,
                    ori_start_minutes: start_amp.startMinute,
                    date:start_amp.date
                }
            }
            //Comme ce n'est pas possible d'ajouter la journée entière 
            //peu importe les horaire on sauvegarde les possibilité pour cette date 
            //dans ArrayByDay_not_possible_because_of_consecutive_abs
            //que l'on utilisera si on arrive pas à respecter les deux jours consécutifs
            if(18739 == console.iidebug[56]){
                debugger;
            }
            if(f_day_not_possible){
                var iptatndaw = m.is_possible_to_add_this_new_day_as_worked(wf,[date])
                if(!iptatndaw.is_possible){
                    if(wf.wfId == 10574){
                        // debugger
                    }
                    f_day_not_possible()
                    return go_to_next_zone_info(501)
                }      
            }

            var min = wf.listInfo_by_date[date].min
            var max = wf.listInfo_by_date[date].max
            nb_minu_worked_in_day_without_pause = Math.min(Math.max(nb_minu_worked_in_day, min), max)
            controls_enabled(()=>{
                if(!isFinite(nb_minu_worked_in_day_without_pause)){my_throw( 'not_finite')}
            })
            var nb_minu_wor = wf.nb_minu_worked
            var nb_minutes_wanted_at_final = Math.min(nb_minu_wor + nb_minu_worked_in_day_without_pause, wf.nbMinutesMin);
            // nb_minu_worked_in_day_without_pause = nb_minu_worked_in_day_without_pause * conf.minTime / conf.minTime
            // 
            // var to_add = pause_is_include_in_nb_minute_by_day ? 0 : m.pauses_obj.noon.time

            // if(pause_is_include_in_nb_minute_by_day){

            // }


            var nb_minu_worked_in_day_without_pause = nb_minutes_wanted_at_final - nb_minu_wor
            // nb_minu_worked_in_day_without_pause = lower(nb_minu_worked_in_day_without_pause)
            if(nb_minu_worked_in_day_without_pause < min){
                if(wf.wfId == 25386 && date == '2019-02-04'){
                    debugger
                }
                return go_to_next_zone_info(502)
            }
            // comme il est possible qu'il y ait un nombre au quart d'heure
            // et que en plus on rerajoute un quart d'heure à cause d'une ouverture
            // on arrondit à la demi-heure au dessus
            // nb_minutes_wanted_at_final = upper(nb_minutes_wanted_at_final)
            // Debug.is_round(nb_minutes_wanted_at_final)

            var iDebug7 = idnext('no_name4')
            if(opt2.forbidden_dates[date]) {
                return go_to_next_zone_info(503)
            }

            var start_minute = start_amp.startMinute, end_minute;
            let nb_minu_worked_in_day_with_pause2
            if(zone_info.forced_amp){
                ;({start_minute,end_minute} = zone_info.forced_amp)
                nb_minu_worked_in_day_with_pause2 = end_minute - start_minute
                // to_add = 0;
            }

            controls_enabled(()=>{
                if(!isFinite(start_minute)){
                    my_throw( 'not finite')
                }
            })
            
            console.iidebug[53] = idnext('53')
            if(84055 ==  console.iidebug[53] ){
                debugger
            }
            
            var pause_time = m.get_pause_time(start_minute,nb_minu_worked_in_day_without_pause,date,wf)
            let nb_minu_worked_in_day_with_pause = nb_minu_worked_in_day_without_pause + pause_time
            var opening_closing_time = wf.get_unplaced_time(start_minute, nb_minu_worked_in_day_with_pause,date)
            let get_end = () => lower(start_minute + nb_minu_worked_in_day_with_pause)

            var ori = {
               start_minute,
               end_minute : get_end()
            }

            if(ori.end_minute == 1410){
                // debugger
            }
            if(ori.endMinute - ori.start_minute - pause_time + opening_closing_time > wf.nbMinutesMin){
                return go_to_next_zone_info(502)
            }   

            nb_minutes_wanted_at_final = nb_minu_worked_in_day_without_pause + wf.nb_minu_worked
            var control_forced_not_change = (start_minute,end_minute) => {
                controls_enabled(()=>{
                    if(zone_info.forced_amp){
                        if(zone_info.forced_amp.start_minute != start_minute ||  zone_info.forced_amp.end_minute != end_minute){
                            my_throw( 'les heures ont changées')
                        }
                    }
                })
            }
            control_forced_not_change(ori.start_minute,ori.end_minute)

            var new_minu = m.change_hour_to_respect_rules(
                wf,date,start_minute, nb_minu_worked_in_day_without_pause - opening_closing_time,pause_time)

            if(new_minu.not_possible){
                return go_to_next_zone_info(508)
            }
            start_minute = new_minu.start_minute
            end_minute = new_minu.end_minute
            control_forced_not_change(start_minute,end_minute)


            if(opt3.opt_min_max){
                let error_code = wf.get_start_end_minu_error({opt_min_max: opt3.opt_min_max,start_minute,end_minute,date,whole_day: true})
                if(error_code) return go_to_next_zone_info(error_code);
            }

            Debug.is_round(end_minute)
            Debug.is_round(start_minute)

            controls_enabled(()=>{
                //on va réajuster le nb_minu_worked_in_day_without_pause à cause des min et des max
                var too_much = nb_minutes_wanted_at_final - wf.nbMinutesMin
                //à cause du temps minimal on ferait trop d'heure
                if(too_much > 0) {
                    my_throw( 'le temps minimal est trop grand ou bien on a trop affecté aux autres jours')
                }
            })
            // nb_minu_worked_in_day_without_pause += m.pauses_obj.noon.time;
            //On avait demandé à ce que ces amplitudes soit inclus dans la planification de la journée
            //sauf qu'avec ce ampli_info ce n'est pas le cas
            if(opt3.include_this_amplitude) {
                var st = opt3.include_this_amplitude.start_minute
                var end = opt3.include_this_amplitude.end_minute
                if(start_minute > st || end_minute < end) {
                    return go_to_next_zone_info(505)
                }
            }
            
            var nb_recursivity = zscores_of_the_day.nb_recursivity =  (zscores_of_the_day.nb_recursivity || 0) + 1
            controls_enabled(()=>{
                // var res2 = wf.nb_minu_worked + ori.end_minute - ori.start_minute - pause_time + opening_closing_time
                // if(nb_recursivity == 1 && upper(nb_minutes_wanted_at_final) != upper(res2)){
                //     my_throw( "il y a une erreur dans le calcul des heures")
                // }

                if(nb_recursivity - zone_info.nb_recursivity < zscores_of_the_day.length + 1){
                    my_throw( 'il devrait y avoir un écart plus grand entre 2 appels du même zone_info')
                }
                if(!isFinite(ori.end_minute) || !isFinite(ori.start_minute) ||  !isFinite(end_minute) || ! isFinite(start_minute)){
                    my_throw( 'not finite')
                }
                Debug.is_round(end_minute)
                Debug.is_round(start_minute) 
            })
            //si on a du modifier les heures alors reporter cette amplitude à la fin
            if(!zone_info.nb_recursivity && (/*ori.end_minute != end_minute ||*/ ori.start_minute != start_minute )){

                zone_info.nb_recursivity = nb_recursivity
                zone_info.zscore = 0;
                zone_info.has_been_place_at_end = true
                zscores_of_the_day.unshift(zone_info)
                return go_to_next_zone_info(506)
            }
            
            controls_enabled(()=>{
                if(lower(end_minute) != end_minute){
                    console.log(lower(end_minute),end_minute)
                    my_throw( 'le temps demandé est mal arrondi')
                }
                if(lower(start_minute) != start_minute){
                    console.log(lower(start_minute),start_minute)
                    my_throw( 'le temps demandé est mal arrondi')
                }
            })
            // Debug.is_round(nb_minutes_wanted_at_final)
            var ret = {
                // start_amp :  zone_info.amp,
                start_minute,
                end_minute,
                date,
                zscores_of_the_day,
                nb_minutes_wanted_at_final,
            }
            const ret2 = go_to_next_zone_info(0,ret)
            controls_enabled(()=>{
                if(!isFinite(ret.end_minute) || !isFinite(ret.end_minute)) {
                    console.log(ret)
                    my_throw( 'not finite' + ret)
                }
            })
            return ret2
        }
       // obtenir la meilleur amplitude suivante dans le tableau 
       // qui est ArrayOfArrayByDay
       // si ArrayByDay_not_possible_because_of_consecutive_abs est non vide
       // et wf.do_not_check_consecutive_abscence est true alors on va aussi regarder 
       // les amplitude de ce tableau
       // c'est une option permettant de désactiver le controle sur les 2 jour consécutive d'absence
       get_next_best_amp_to_test(opt2){
            var m = this
            var wf = opt2.wf
            var ArrayOfArrayByDay = opt2.ArrayOfArrayByDay
            var opt3 = opt2.opt3//paramètre de v2_schedule_planning
            var before_next_wf = opt2.before_next_wf
            var imposs_days = def(opt2,'ArrayByDay_not_possible_because_of_consecutive_abs',new Set)
            var nb_minute_by_day = opt2.nb_minute
            // var pause_is_include_in_nb_minute_by_day = opt2.pause_is_include_in_nb_minute_by_day
            if(18339 == console.iidebug[35] ){
                // debugger
            }

            //si on a rien trouvé pour la personne alors s'autoriser à 
            //ne pas respect la règles des jours consécutifs en 
            //les remettant comme possible
            if(wf.do_not_check_consecutive_abscence && imposs_days && imposs_days.size){
                ;[].push.apply(ArrayOfArrayByDay,[...imposs_days])
                imposs_days.clear()
            }

            opt2.ArrayOfArrayByDay = ArrayOfArrayByDay = m.sort_arrays(ArrayOfArrayByDay);
            controls_enabled(()=>{
                if(ArrayOfArrayByDay.length > 7){
                    my_throw( 'too much days')
                }
            })
            if(!ArrayOfArrayByDay.length) {
                before_next_wf()
                return; //passer au worksfor suivant
            }

            var ArrayOfArrayByDay = opt2.ArrayOfArrayByDay

            console.iidebug[22] = idnext('22')
            if(console.iidebug[22] == 61606) {
                //// debugger
            }
            var remaining_time = wf.nbMinutesMin - wf.nb_minu_worked;
            if(remaining_time <= 0) {
                before_next_wf()
                return; //personne suivante
            }
            controls_enabled(()=>{
                if(!isFinite(remaining_time)) {
                    my_throw( 'not finite')
                }
            })
            
            // var nb_minute_by_day = fl(remaining_time / conf.displayMinTime / wf.getNumberOfDaysRemained()) * conf.displayMinTime
            var zscores_of_the_day = last(ArrayOfArrayByDay)
            var zone_info = zscores_of_the_day.pop()
            var start_amp = zone_info.amp;
            var date = start_amp.date
            Debug.is_round(nb_minute_by_day)

            var cibip = m.check_if_batt_is_possible({
                method2: true,
                wf,
                opt2,
                nb_minu_worked_in_day: nb_minute_by_day,
                zscores_of_the_day,
                zone_info,
                f_day_not_possible: ()=>{
                    if(wf.do_not_check_consecutive_abscence){
                        return true
                    }
                    ArrayOfArrayByDay.pop()/*zscores_of_the_day*/.push(zone_info)
                    controls_enabled(()=>{
                        if(imposs_days.has(imposs_days)){
                            my_throw( 'Ce jour est supposé être déjà')
                        }
                    })
                    imposs_days.add(zscores_of_the_day)
                    controls_enabled(()=>{
                        if(imposs_days.size > 7){
                            my_throw( 'Il ne peut pas y avoir plus de 7 jours impossibles')
                        }
                    })
                },
                // pause_is_include_in_nb_minute_by_day
            })

            if(!cibip || !cibip.go_to_next_zone_info_has_been_called){
                my_throw("il faut absolument passer par go_to_next_zone_info")
            }
            if(cibip.success) return cibip.success
            return cibip
        }

        insert_best_amp({forced_ampli,batt,ArrayOfArrayByDay,opt3,before_next_try,ret,wf}){
            var m = this;
            console.iidebug[37] = idnext('37');
            Debug.checker()
            if(console.iidebug[37] == 62311) {
                debugger
            }
            controls_enabled(()=>{
                if(lower(batt.end_minute) != batt.end_minute){
                    console.log(lower(batt.end_minute),batt.end_minute)
                    my_throw( 'le temps demandé est mal arrondi')
                }
                if(lower(batt.start_minute) != batt.start_minute){
                    console.log(lower(batt.start_minute),batt.start_minute)
                    my_throw( 'le temps demandé est mal arrondi')
                }
            })
            // Debug.is_round(batt.nb_minutes_wanted_at_final)
            var hasBeenInsert = m.v2_schedule_planning__addIntervention({
                date: batt.date,
                start_minute: batt.start_minute,
                end_minute: batt.end_minute,
                nb_minutes_wanted_at_final: batt.nb_minutes_wanted_at_final,
                opt3: opt3,
                ret: ret,
                wf: wf
            })
            Debug.checker()
            var to_inc = opt3.include_this_amplitude;
            controls_enabled(()=>{
                if(hasBeenInsert){
                    var pb = hasBeenInsert && to_inc
                    if(pb) {
                        var pb3 = to_inc.start_minute < ret.last_inter.first_minu_worked
                        var pb4 = to_inc.end_minute > ret.last_inter.last_minu_worked
                        if(pb3 || pb4){
                            my_throw( "mais ce n'est pas dans les amplitudes demandées")
                        }
                    }
                    if(Debug.is_forced_mode){
                        if(wf.listInfo_by_date[batt.date].first_minu_worked != batt.start_minute){
                            console.warn("Ce n'est pas ce que l'on a demandé mais si il y a ou des pauses différentes des protected dans la journée c'est normal")
                        }
                    }
                }
            })

            if(!forced_ampli && (hasBeenInsert || batt.zscores_of_the_day.length == 0)) {
                //On a fait ce que l'on devait faire pour cette journée donc l'enlever de la liste
                --ArrayOfArrayByDay.length
            }
            before_next_try()
            Debug.checker()
        }


        handle_forced_ampli({ret,exact_amplitude_or_not_planified,forced_ampli,
            opt3,find_and_insert_batt26,wf,forced_date = []}){
            Debug.is_forced_mode = true
            var m = this;
            var impossible9 = false;
            // var dates_placed = {}
            var backups = {}
            var arraysOfDaysForSpecAmp = []
            ret.success = true
            console.iidebug[55] = idnext('55')
            if(54342 == console.iidebug[55]){
                debugger
            }
            var ArrayOfDays_to_forced_amp = new WeakMap;
            Object.values(forced_ampli).forEach(function(forced_amp){
                var i_best_zone_to_place_by_date = m.getArrayGlobalZscoreByDay(
                    forced_amp.end_minute - forced_amp.start_minute, wf, clone(opt3,{
                        start_minute: forced_amp.start_minute,
                        end_minute: forced_amp.end_minute
                    })
                    )

                var ArrayOfDaysForSpecAmp = Object.values(i_best_zone_to_place_by_date)
                controls_enabled(()=>{
                    if(ArrayOfDaysForSpecAmp[0] && ArrayOfDaysForSpecAmp[0].length > 1){
                        my_throw( 'anormal')
                    }
                })
                ArrayOfDays_to_forced_amp.set(ArrayOfDaysForSpecAmp,forced_amp)
                // m.sort_arrays(ArrayOfDaysForSpecAmp);
                if(ArrayOfDaysForSpecAmp.length){
                    let info = {
                        ArrayOfDaysForSpecAmp,
                        nb_minutes: forced_amp.nb_minutes,
                        associated_date: [],
                        disabled_date: {},
                        nb_to_place: forced_amp.nb_to_place,
                        forced_amp,
                        is_info : true
                    }

                    arraysOfDaysForSpecAmp.push(info)
                    forced_amp.info = info
                }
            })

            forOf(exact_amplitude_or_not_planified,(eaonp,date)=>{
                backups[date] = eaonp.place_like_before
                controls_enabled(()=>{
                    if(backups[date] instanceof Function == false){
                        my_throw( "ceci devrait être une fonction")
                    }
                })
            })


            var disable_date = (date9)=>{
                //l'insertion a réussi il faut enlever le jour des jours a ajouter
                //dans les différents tableaux possibles
                //la boucle doit être à l'envers à cause des splices
                for(var k = arraysOfDaysForSpecAmp.length;k--;){
                    let arrayOfDaysForSpecAmp = arraysOfDaysForSpecAmp[k].ArrayOfDaysForSpecAmp
                    for(var j = arrayOfDaysForSpecAmp.length; j--;){
                        let zscores_of_the_day = arrayOfDaysForSpecAmp[j]
                        if(zscores_of_the_day[0] && zscores_of_the_day[0].amp.date == date9){
                            arraysOfDaysForSpecAmp[k].disabled_date[date9] = arrayOfDaysForSpecAmp.splice(j,1)[0]
                        }
                    }
                }
            }
            // on avait supprimé une date d1 des possibilités,
            // car on y avait mis une amplitude A
            // mais en fait le jour d2 n'accepte que l'amplitude A
            // alors on enlève l'amplitude A du jour d1
            // pour la mettre au jour d2.
            // Il faut donc réautorisé la journée d1 
            var enable_date = function(date){
                console.log(date)
                for(var k = arraysOfDaysForSpecAmp.length;k--;){
                    var zscores_of_the_day = arraysOfDaysForSpecAmp[k].disabled_date[date]
                    if(zscores_of_the_day){
                        var arrOfDayForSpecAmp = arraysOfDaysForSpecAmp[k].ArrayOfDaysForSpecAmp
                        arrOfDayForSpecAmp.push(zscores_of_the_day)
                        delete arrOfDayForSpecAmp[date]
                    }
                }
            }


            the_while : while(1){
                var time4 = wf.nb_minu_worked
                var nb_to_place_remaining = 0;
                arraysOfDaysForSpecAmp.forEach(function(a){
                    nb_to_place_remaining += a.nb_to_place
                    time4 += a.nb_to_place * a.nb_minutes
                    m.sort_arrays(a.ArrayOfDaysForSpecAmp,wf)
                })
                controls_enabled(()=>{
                    var date = (new Date())
                    if(today_str > '20181125'){
                        if(time4 > wf.nbMinutesMin){
                          my_throw( "si on place toutes les amplitudes la personne ferait trop d'heure " + console.iidebug[52])
                      }    
                  }
              })
                console.iidebug[52] = idnext('52')
                if(console.iidebug[52] == 50980){
                    debugger
                }

            // Chaque tableau dans arraysOfDaysForSpecAmp
            // donne les meilleurs jours pour une zone horaire
            // spécifique. il faut trier pour avoir la meilleur zone horaire
            arraysOfDaysForSpecAmp.sort((a,b)=>{
                var end = false,val,undef;
                var f5 = (a,b,prop)=>{
                    if(a === void 8 || b === void 8){
                        end = true;
                        if(a === b){//si les deux sont undefined
                            return 0;
                        }
                        return (b === void 8) ? 1 : -1
                    }
                    if(a[prop] === 0 || b[prop] === 0){
                        end = true
                        val = a[prop] - b[prop];
                    }
                }
                f5(a,b,'nb_to_place');if(end) return val;
                f5(a,b,'length');if(end) return val;
                var a1p = last(a.ArrayOfDaysForSpecAmp), b2p = last(b.ArrayOfDaysForSpecAmp)
                f5(a1p,b2p,'length');if(end) return val;
                var a1 = last(a1p),b2 = last(b2p)
                return m.sort_array_by_score(a1,b2,wf)
            })
            // while(1){
            // if(arraysOfDaysForSpecAmp.length == 0){
            //     break the_while
            // }
            var daysForSpecAmp  = last(arraysOfDaysForSpecAmp)
            if(daysForSpecAmp && daysForSpecAmp.nb_to_place == 0){
                break the_while;//mince on ne peut pas ajouter
            }
            if(!daysForSpecAmp){
                break the_while
            }
            var ArrayOfDays = daysForSpecAmp.ArrayOfDaysForSpecAmp
            if(ArrayOfDays.length == 0){
                break the_while//il n'y a plus d'amplitudes à tester
            }
            controls_enabled(()=>{
                if(ArrayOfDays.length > 0 && last(ArrayOfDays).length == 0){
                    my_throw( 'not logic')
                }
            })
            var zscores_of_the_day = last(ArrayOfDays)

            if(daysForSpecAmp.nb_to_place <= 0){
                break the_while// on a placé toutes nos amplitudes
            }
            var zone_info = zscores_of_the_day.pop()
            if(zone_info.nb_recursivity){
                // il y a une récursivité quand les heures ont été changées
                // par rapport à ce que l'on voulait, ici on ne veut aucun changement
                break the_while
            }
            var date9 = zone_info.amp.date
            controls_enabled(()=>{
                if(!date9){
                    my_throw( 'la date est manquante')
                }
            })
            var nb_forced_not_placed = forced_date.filter((date)=> !wf.is_worked(date) ).length
            if(nb_to_place_remaining <= nb_forced_not_placed){
                // Comme on a un certain nombre de date à placer
                // et un certain nombre d'amplitudes à placer et qu'il faut une amplitude exactement par date
                // si le nombre d'amplitude est inférieurs ou égale au nombre de date
                // c'est que toutes dates autre que forced_date sont impossibles
                // donc date9 est impossible
                if(forced_date.indexOf(date9) < 0){
                    disable_date(date9)
                    continue the_while
                }
            }
            let lbd = wf.listInfo_by_date[date9]
            // si la journée n'est pas travaillable ou bien qu'elle est déjà travaillée
            // ou bien qu'on peut pas y travailler à cause de certaines règles (absences consécutives)
            if(!lbd || (lbd &&  lbd.is_worked())
                || !m.is_possible_to_add_this_new_day_as_worked(wf,[...forced_date,date9]).is_possible){
                disable_date(date9) //alors désactiver la date 
            continue the_while
        }
        if(zscores_of_the_day.length == 0){
            --ArrayOfDays.length
        }
        var nb_minu_worked = wf.nb_minu_worked
        var eaonp = exact_amplitude_or_not_planified[date9];
        var nb_minute = daysForSpecAmp.nb_minutes
        var success = false
        if(eaonp){
            var d = wf.listInfo_by_date[date9] 
                    // la meilleure amplitude trouvée est sur une journée
                    // ou l'on n'a pas le choix des heures (car on veut remettre comme avant)
                    // cependant si l'amplitude optimale n'a pas
                    // d'heure commune avec celle forcée
                    // ou bien que c'est sur une heure ou la personne ne peut pas
                    // travailler alors on ne force pas à remettre comme avant
                    // 
                    var zone_info_start_minute =  zone_info.amp.startMinute
                    var zone_info_end_minute =  zone_info_start_minute + nb_minute
                    if(!(zone_info_start_minute < eaonp.last_minute_worked 
                        && zone_info_end_minute > eaonp.first_minute_worked)
                        || d.max_end_day < zone_info_end_minute
                        || d.min_start_day > zone_info_start_minute
                        ){
                        continue the_while
                }

                    //est ce qu'il y a une amplitude au même horaire
                    //que l'on nous a demandé de placé, logiquement oui
                    var fa
                    someOf(forced_ampli,(forced_amp,key,stop_some_of)=>{                    
                        var diff = zone_info_end_minute - zone_info_start_minute
                        controls_enabled(()=>{
                            if(isNaN(diff)){
                                my_throw( 'NaN')
                            }
                        })                           
                        if(forced_amp.start_minute == eaonp.first_minute_worked 
                            && forced_amp.end_minute ==  eaonp.last_minute_worked 
                            ){

                            fa = forced_amp.info
                        controls_enabled(()=>{
                            if(!fa.is_info){
                                my_throw( 'not an info')
                            }
                            if(fa.nb_minute != forced_amp.worked){
                                my_throw( 'illogic')
                            }
                        })         
                        // console.table(arraysOfDaysForSpecAmp)
                        // console.log(wf.nb_minu_worked)
                        fa.nb_to_place -= 1//l'amplitude est bien placée
                        if(fa.nb_to_place < 0){
                            //mince on avait déjà placé une journée avec 
                            //ces même horaire on va donc l'annuler
                            //car on ne doit les placer qu'une fois
                            console.iidebug[64] = idnext('64')
                            if(console.iidebug[64] == 62889){
                                debugger
                            }
                            var already_place_date = fa.associated_date.pop()
                            controls_enabled(()=>{
                                if(!already_place_date){
                                    my_throw( 'illogic')
                                }
                            })                           
                            var nb_minu_worked3 = wf.nb_minu_worked
                            wf.reset_date(already_place_date,{even_if_fake_protected: true})
                            controls_enabled(()=>{
                               if(nb_minu_worked3 - fa.nb_minutes != wf.nb_minu_worked){
                                   my_throw( 'illogic')
                               }
                           })
                            enable_date(already_place_date)
                            fa.nb_to_place = 0
                        }
                        // if(!v8js){
                        //     console.table(arraysOfDaysForSpecAmp)
                        //     console.log(wf.nb_minu_worked)
                        // }
                        fa.associated_date.push(date9)
                        stop_some_of()
                    }
                })
                    controls_enabled(()=>{
                        if(!fa){
                            my_throw( "comment ce fait il que fa n'existe pas")
                        }
                    })
                    var nb_minu_worked2 = wf.nb_minu_worked
                    eaonp.place_like_before()
                    if(nb_minu_worked2 + eaonp.worked !== wf.nb_minu_worked){
                        controls_enabled(()=>{
                            if(today_str > '20181125'){
                                my_throw( "la restauration a raté")
                            }
                        })
                    }else{
                        success = true
                    }
                }else{
                    if(!m.is_possible_to_add_this_new_day_as_worked(wf,[date9]).is_possible){
                        disable_date(date9)
                        continue the_while
                    }
                    if(!backups[date9]){
                        backups[date9] = m.v2_try_a_change({
                            person:wf,
                            date: date9
                        })
                    }
                    var forced_amp2 = ArrayOfDays_to_forced_amp.get(ArrayOfDays)
                    zone_info.forced_amp = {
                        start_minute : forced_amp2.start_minute,
                        end_minute : forced_amp2.end_minute
                    }

                    console.iidebug[94] = idnext(94)
                    if(105599 == console.iidebug[94]){
                        debugger
                    }
                    find_and_insert_batt26(ArrayOfDays, nb_minute,{
                        zscores_of_the_day, zone_info
                    })
                    if(nb_minu_worked < wf.nb_minu_worked){
                    //date qui a été utilisée pour cette amplitude
                    daysForSpecAmp.associated_date.push(date9)
                    var info = last(arraysOfDaysForSpecAmp)
                    controls_enabled(()=>{
                        var forced_amp2 = ArrayOfDays_to_forced_amp.get(ArrayOfDays)
                        if(!forced_amp2){
                            my_throw( "propriété n'aurait pas due disparaitre")
                        }
                        if(forced_amp2.start_minute != zone_info.amp.startMinute){
                            my_throw( "La start_minute de forced_amp2 et zone_info devrait être la même")
                        }
                        if(!info.is_info){
                            my_throw( 'not an info')
                        }
                        var lid = wf.listInfo_by_date[zone_info.amp.date]
                        if(forced_amp2.start_minute != lid.first_minu_worked || forced_amp2.end_minute != lid.last_minu_worked){
                            debugger
                            console.warn(`console.iidebug[94] = ${console.iidebug[94]} ce n'est pas ce qui était prévue mais ça peut arrivé s'il y ou des pauses différentes a des absence protected dans la journée`)
                        }else{
                            if(nb_minu_worked + nb_minute != wf.nb_minu_worked){
                                console.warn( `le temps devrait être exactement le même vu que l'on a gardé des horaires existante pour les mettre`
                                    +`sur un autre jour et ce même pour les quart d'heure mais ça peut arrivé s'il y a des absence protected dans la journée`)
                            }       
                        }
                 })
                    info.nb_to_place -= 1
                    success = true
                }
            }
            if(success){
                disable_date(date9)
            }
            controls_enabled(()=>{
                var time5 = wf.nb_minu_worked
                arraysOfDaysForSpecAmp.forEach(function(a){
                    time5 += a.nb_to_place * a.nb_minutes
                })
                var date = (new Date())
                if(today_str > '20181125'){
                    if(time5 > wf.nbMinutesMin){
                      my_throw( "si on place toutes les amplitudes la personne ferait trop d'heure " + console.iidebug[52])
                  }    
              }
          })
        }

        if(opt3.get_backup_functions){
            [].push.apply(ret.backup_functions,Object.values(backups))
        }
        //si il reste une amplitude non placée c'est un echec
        ret.success = !arraysOfDaysForSpecAmp.some((info)=>{
            if(info.nb_to_place){
                return true
            }
        })
        Debug.is_forced_mode = false
    }

    find_and_insert_batt00(ArrayOfArrayByDay,nb_minute,{zscores_of_the_day, zone_info} = {}){
        //@warning ici this a été bind 
        console.iidebug[85] = idnext('85')
        if(58624 == console.iidebug[85]){
            debugger
        }
        const {m,wf,opt3,forced_ampli,retF} = this;
        const {ret} = retF
        controls_enabled(()=>{
            if(!m) my_throw( "find_and_insert_batt n'a pas été bind ")
            // if(lower(nb_minute) != nb_minute){
            //     console.log(lower(nb_minute),nb_minute)
            //     my_throw( 'le temps demandé est mal arrondi')
            // }
        })


        let before_next_wf,nb_minutes_bis,before_next_try, get_next_best_amp_to_test21,
        insert_best_amp, ArrayByDay_not_possible_because_of_consecutive_abs

        //pour accélérer le débuggage la déclarartion des fonction est faite dans une fonction
        ;(()=>{
            nb_minutes_bis = nb_minute;
            //un tableau ou une case est un jour, cette case est un tableau de différentes possibilités pour la startMinute et
            //la endMinute avec le score associé à chaque choix
            before_next_wf = function() {
                controls_enabled(()=>{
                    // passer à la personne suivante
                    if(wf.nb_minu_worked == 0) {
                        console.error('cette personne '+ wf.wfId +' ne travaille pas du tout, tour numéro ')
                        // my_throw( 'error')
                    }
                    if(wf.nb_minu_worked  > wf.nbMinutesMin) {
                        my_throw( 'pb')
                    }
                })
            }
            before_next_try = function() {
                Debug.check_too_much_worked(wf)
                Debug.checker()
            }

            //retourne l'amplitude qui permettra le mieux de
            //couvrir le besoin
            ArrayByDay_not_possible_because_of_consecutive_abs = new Set

            get_next_best_amp_to_test21 = function(opt){
                const retF2 = (batt) => {
                    return batt
                }
                if(zone_info){
                    controls_enabled(()=>{
                        if(!nb_minute){
                            my_throw( 'nb minute doit être un entier positif')
                        }
                    })
                    var opt_cibip = {
                        method2: true,
                        wf,
                        nb_minu_worked_in_day: nb_minute,
                        I_know_day_is_possible: true,//les 2 jours de repos consécutifs ont déjà été vérifié ainsi
                        opt2: {
                            wf,
                            opt3,
                            ArrayOfArrayByDay,
                            nb_minute,
                            before_next_wf,
                            ArrayByDay_not_possible_because_of_consecutive_abs,
                            forbidden_dates: opt3.forbidden_dates || {},
                        },
                        zscores_of_the_day,
                        zone_info,
                        // pause_is_include_in_nb_minute_by_day: true
                    }

                    let batt = m.check_if_batt_is_possible(opt_cibip)
                    ret.error_codes[batt.error_code] = 1 + (ret.error_codes[batt.error_code] || 0)
                    if(batt.success_info){
                        return retF2(batt.success_info)
                    }else{
                        return retF2()
                    }
                }else{
                    let batt;
                    var bef_while = console.iidebug[86] = idnext('86')
                    do{
                        // if(61604 == console.iidebug[86]){
                        //     debugger
                        // }
                        controls_enabled(()=>{
                           // forOf(ret.date_ok,(tr,date)=>{
                               // if(!wf.listInfo_by_date[date].first_interv_sector){
                                   // my_throw(`Comment une date ok peut ne pas avoir d'affectation  console.iidebug[58] = ${ console.iidebug[58]}`)
                               // }
                           // })
                        })
                        console.iidebug[86] = idnext('86')
                        batt = m.get_next_best_amp_to_test({
                            wf,
                            opt3,
                            ArrayOfArrayByDay,
                            nb_minute,
                            before_next_wf,
                            ArrayByDay_not_possible_because_of_consecutive_abs,
                            forbidden_dates: opt3.forbidden_dates || {}
                        })
                        controls_enabled(()=>{
                           // forOf(ret.date_ok,(tr,date)=>{
                               // if(!wf.listInfo_by_date[date].first_interv_sector){
                                   // my_throw(`Comment une date ok peut ne pas avoir d'affectation  console.iidebug[58] = ${ console.iidebug[58]}`)
                               // }
                           // })
                        })
                        if(!batt){break}
                        // if(58846 == console.iidebug[85] && batt.date == '2019-02-10' && batt.ori_start_minutes > 570){
                        //     // debugger
                        // }
                        errors_codes: {
                            ret.error_codes[batt.error_code] = 1 + (ret.error_codes[batt.error_code] || 0)
                            let t = ret.current_person_info
                            t = t[batt.date] = t[batt.date] || {}
                            inc(t, batt.error_code)
                        }
                    }while(batt.find_next_batt)
                    if(batt) delete ret.error_code
                    if(batt && batt.success_info){
                        return retF2(batt.success_info)
                    }else{
                        return retF2()
                    }
                }
            }

            insert_best_amp = function(){

                controls_enabled(()=>{
                    // forOf(ret.date_ok,(tr,date)=>{
                        // if(!wf.listInfo_by_date[date].first_interv_sector){
                            // my_throw(`Comment une date ok peut ne pas avoir d'affectation  console.iidebug[58] = ${ console.iidebug[58]}`)
                        // }
                    // })
                })

                //regarder ret.error_code pour savoir d'où vient le pb
                for (var batt; batt = get_next_best_amp_to_test21({});) {
                    controls_enabled(()=>{
                        // forOf(ret.date_ok,(tr,date)=>{
                            // if(!wf.listInfo_by_date[date].first_interv_sector){
                                // my_throw(`Comment une date ok peut ne pas avoir d'affectation  console.iidebug[58] = ${ console.iidebug[58]}`)
                            // }
                        // })
                    })
                    console.iidebug[58] = idnext('58')
                    if(console.iidebug[58] == 22925){
                        debugger
                    }
                    if(!batt.find_next_batt){
                        if(zone_info){
                            // debugger
                        }
                        // Debug.is_round(batt.nb_minutes_wanted_at_final)

                        m.insert_best_amp({
                            batt,
                            ArrayOfArrayByDay,
                            opt3,
                            before_next_try,
                            ret,
                            wf,
                            forced_ampli
                        })
                    }
                    controls_enabled(()=>{
                        // forOf(ret.date_ok,(tr,date)=>{
                            // if(!wf.listInfo_by_date[date].first_interv_sector){
                                // my_throw(`Comment une date ok peut ne pas avoir d'affectation  console.iidebug[58] = ${ console.iidebug[58]}`)
                            // }
                        // })
                    })
                    if(zone_info) return;
                }
            }
        })();
        //tant qu'il y a des jours à placer
        insert_best_amp()
        if(zone_info) return retF();
        //on a pas réussi à placer certains jours
        //on va donc désactiver la règle des deux jours consécutifs d'absence
        if(opt3.initial_planning && !wf.do_not_check_consecutive_abscence && wf.getNumberOfDaysRemained()){
            // Report.add({
            //     name: 'consecutive_days_not_respected',
            //     wf
            // })
            wf.do_not_check_consecutive_abscence = true
            insert_best_amp()
        }
    }

    v2_schedule_planning_some({wf,opt3,retF}){
        var m = this;
        const {ret} = retF
        ret.person_info[wf.wfId] = ret.current_person_info = {
            wfId: wf.wfId,
            error_code_date: {}
        }

        const {forced_ampli,forced_date,exact_amplitude_or_not_planified} = opt3

        console.iidebug[35] = idnext('35')
        if(236013 == console.iidebug[35]){
            // debugger
        }
        if(wf.nb_minu_worked  > wf.nbMinutesMin) {
            console.error('la personne travail trop');
            return;
        }
        if(wf.nb_minu_worked >= wf.nbMinutesMin) {
            return;
        }

        // on calcule la somme des scores à réduire pour toute les zones possibles
        // mais comme on est inteligent à chaque fois que l'on se décale d'une demis heure on enlève
        // juste le score de la demis_heure décalé et on ajoute celui de celle ajouté
        // le but étant de placer la personne d'un seul bloc sur la journée

        // var nb_minute_by_day2 = (opt3.minutes_to_place ||
        //     ((wf.nbMinutesMin - wf.nb_minu_worked) / wf.nb_days_by_week)) + m.pauses_obj.noon.time;


        let find_and_insert_batt21 = m.find_and_insert_batt00.bind({
            m,wf,opt3,forced_ampli,retF
        })
        controls_enabled(()=>{
            // forOf(ret.date_ok,(tr,date)=>{
                // if(!wf.listInfo_by_date[date].first_interv_sector){
                    // my_throw(`Comment une date ok peut ne pas avoir d'affectation  console.iidebug[58] = ${ console.iidebug[58]}`)
                // }
            // })
        })
        if(forced_ampli){
            console.iidebug[75] = idnext('75')
            if(68701 == console.iidebug[75]){
                // debugger
            }
            m.handle_forced_ampli({
                ret,exact_amplitude_or_not_planified,forced_ampli,
                opt3,
                find_and_insert_batt26: find_and_insert_batt21,
                wf,forced_date})
        }else{
            var remaining_time = wf.nbMinutesMin - wf.nb_minu_worked;
            var days_remained = wf.getNumberOfDaysRemained();
            if(days_remained || !opt3.do_not_planif_on_planified_days){
                var nb_minute_by_day
                if(opt3.minutes_to_place != void 8){
                    nb_minute_by_day = opt3.minutes_to_place
                }else{
                    nb_minute_by_day = fl(remaining_time / conf.displayMinTime / days_remained) * conf.displayMinTime
                }
                nb_minute_by_day = lower(nb_minute_by_day)
                controls_enabled(()=>{
                    if(nb_minute_by_day < 0) {
                        my_throw( 'un temps ne peut pas être négatif')
                    }
                })
                //@danger si nb_minute_by_day est trop grand on risque de ne rien trouver
                var i_best_zone_to_place_by_date = m.getArrayGlobalZscoreByDay(lower(nb_minute_by_day), wf, opt3)
                // if(opt3.debug_forbid_saturday_wf && opt3.debug_forbid_saturday_wf.has(+wf.wfId)) {
                //     delete i_best_zone_to_place_by_date['2018-03-31'];
                //     delete i_best_zone_to_place_by_date['2018-04-07']; 
                // }
                
                find_and_insert_batt21(Object.values(i_best_zone_to_place_by_date),nb_minute_by_day)
            }
        }
        controls_enabled(()=>{
            // forOf(ret.date_ok,(tr,date)=>{
                // if(!wf.listInfo_by_date[date].first_interv_sector){
                    // my_throw(`Comment une date ok peut ne pas avoir d'affectation  console.iidebug[58] = ${ console.iidebug[58]}`)
                // }
            // })
        })
        
        if(opt3.allow_to_move_time_between_days){
            /**
            * Si on a fait tous les jours alors vérifier
            * que la personne fasse bien assez d'heure sinon
            * le lui rajouter
            */
            console.iidebug[66] = idnext('66')
            if(console.iidebug[66] == 23936){
                debugger
            }
            
            Debug.check_nb_days_planif(wf)
            m.solve_not_enough_worked([wf],{
                put_in_main_sector: true,
                allow_to_change_pause: true
                // zscore_f: () => m.global_score_ignoring_sectors()
            })
            Debug.check_not_enough_worked(wf)
        }
        // before_next_wf()
        Debug.checker()
        m.logPersonSectorBug()
    }

    /**+
     * @v2_schedule_planning
     *   Le fonctionnement de cet algorithme est d'affecter personne par personne en affectant d'abord celle qui ont le plus
     *   de contraintes
     *
     * 
     *  forbidden_dates: date interdite
     *  do_not_planif_on_planified_days: Attention à ne pas appeler cet algo si des journées sont déjà planifié sans passer le paramètre
     *      sinon ça va fusionner les 2 planif puis corriger les heures en trop après
     *   get_dates_added: true, retourne une tableau de date dans dates_added
     *   get_backup_functions: true retourne une fonction permettant d'annuler ce qui a été fait ne fonctionne pas avec 
     *       allow_to_move_time_between_days
     *   dates: satur_date,
     *   minutes_to_place: infos_rem.time_removed il faut placer minutes_to_place dans une
     *   initial_planning: true,//est ce que c'est le premier appel de la fonction
     *   allow_to_move_time_between_days: true, autorise pour corriger les problèmes 
     *       de contraintes non respectées de bouger des heures du ou vers le jour planifié avec un autre jour
     *   debug_forbid_saturday_wf: liste de worksfor dont le samedi leur est interdit
     *   include_this_amplitude: {
     *       start_minute,le début de la journée dôit être inférieur à start_minute
     *       end_minute la fin de la journée doit être supérieur à end_minute
     *   }, 
     *   only_these_pers: [wf],ne traiter que certaine personne
     *   do_not_put_pause_on:  tableau d'objet avec un startMinute et endMinute que la pause ne doit pas croiser
     *
     */

     v2_schedule_planning(opt3_bis = {}) {
        const opt3 = new opt_v2_schedule_planning(opt3_bis)
        const retF = () => ret
        const ret = retF.ret = {
            date_ok: {},
            error_codes: {},//combien de fois chaque erreur à eu lieu
            person_info: {},//contient les erreur et info pour la personne courrante
        }
        controls_enabled(()=>{
            if(opt3.allow_to_move_time_between_days && opt3.get_backup_functions){
                'la sauvegarde ne marche pas si on peut déplacer des heures entre plusieurs jours elle doit être fait avant cette fonction sur tous les potentiels jours concernés'
            }
            if(opt3.minutes_to_place < 0) {
                my_throw( 'un temps ne peut pas être négatif')
            }
            if(opt3.forced_date && !opt3.forced_ampli){
                my_throw( "le paramètre forced date n'a pas été implémenté sans forced_ampli")
            }
        })
        // var id = 0;
        var m = this;
        var all_wfs = opt3.only_these_pers || m.all_wfs
        // var kkk = 0;
        if(opt3.get_backup_functions) ret.backup_functions = []
            if(opt3.initial_planning){
            //trier les personne de sorte à directement placer celle qui ont déjà un planning existant comme cela ce sera les nouvelles personnes
            //qui s'occuperont de corriger les problèmes
            all_wfs.sort((wf1, wf2)=>{
                wf2.nb_minu_worked - wf1.nb_minu_worked // décroissant
            })
        }
        all_wfs.some((wf,ooo)=>{
            Debug.check_rotation(wf)
            const some_ret =  m.v2_schedule_planning_some({wf,opt3,retF})
            return some_ret
        })
        Debug.checker()
        m.logPersonSectorBug()
        controls_enabled(()=>{
            var to_inc = opt3.include_this_amplitude
            if(to_inc) {
                var li = ret.last_inter
                if(li) {
                    var start_minute = to_inc.start_minute
                    var end_minute = to_inc.end_minute
                    var last_minute_worked = li.last_minu_worked
                    var first_minute_worked = li.first_minu_worked
                    if(last_minute_worked < end_minute || first_minute_worked > start_minute) {
                        my_throw( 'On avait demandé à encadrer ces amplitudes pourtant')
                    }
                }
            }
        })
        Debug.checker()
        return retF();
    }


    getMinTime(sector, wf) {
        return Math.max(sector.minTimeSector, conf.displayMinTime);
    }

    v2_todo_multiple_change_sector(opt) {
        var m = this;
        var all_amp_open = m.v2_get_all_amp_open()
                //pour chaque horaire ou au moins une entité est ouverte
                all_amp_open.some(function(amp) {
                    idnext('no_name5')
                    var wfArr = m.all_wfs
                    Debug.perfStart('v2_todo_multiple_change_sector_@_shuffle')
                    RandomGenerator.shuffle(wfArr)
                    Debug.perfEnd('v2_todo_multiple_change_sector_@_shuffle')
                //tenter de déplacer la personne dans l'entité qui en a le plus besoin
                wfArr.some(function(wf) {
                    var debug88 = false
                    // if(!opt.do_not_let_too_small_inter)
                    if(amp.date == '2018-03-26')
                        if(amp.startMinute >= 10)

                            if(wf.wfId == 10324) {
                                ////////////////debugger;
                                // console.log('pk_ça_affecte_pas')
                                // debug88 = true
                                // // Debug.stop = true
                            }

                    //choisir le secteur qui sera le mieux à cette heure-ci
                    Debug.perfStart('v2_todo_multiple_change_sector_@_sector_choice')

                    //le plus attractif à la fin
                    var sectorAmp = amp.periods.slice().sort((ampliSector1, ampliSector2) =>
                        ampliSector1.v2_attractivity() - ampliSector2.v2_attractivity()
                        );
                    Debug.perfEnd('v2_todo_multiple_change_sector_@_sector_choice')
                    Debug.perfStart('v2_change_sector')
                    while (sectorAmp.length) {
                        //on commence par le secteur qui en a le plus besoin avec le score le plus haut
                        var the_amp = sectorAmp.pop();
                        // var sector =< the_amp.v2_sector
                        console.iDebug5 = console.iDebug
                        if(m.v2_change_sector(the_amp, wf, opt)) {
                            break;
                        }
                    }
                    // if(debug88 && sector.id == 2522){
                    //     Debug.amp = the_amp;
                    // }
                    Debug.perfEnd('v2_change_sector')

                    return Debug.stop
                })
                return Debug.stop
            })
            }
        /*
         * nous allons essayer des changement de secteur tant que cela améliore le résultat global
         * on va affecter d'abords vers les entité qui sont en sous effectif
         * si opt.do_not_let_too_small_inter alors on ne pas pas affecter si cela laisse des interventions trop petites sur les bords
         * si l'intervention sur le bord est vraiment petite alors on va l'intégrer < 30 minutes
         * si il n'y a pas opt.do_not_let_too_small_inter alors on va affecter les bords sur les cotés trop petit en même temps
         * le problème c'est que cela à tendance à créer des blocs très gros
         */
         v2_change_sector(amp, wf, opt) {
            /* @TODO: REMOVE*/
            var checkMinTime = function() {
                controls_enabled(()=>{
                    wf.intervsRacines.forEach(function(inter) {
                        while (inter.next) {
                            inter = inter.next
                            if(!inter.v2_respectMinTime()) {
                                Debug.logDayWf(wf.wfId, inter.date)
                                my_throw( 'min_time not respected')
                            }
                        }
                    })
                })
            }
            checkMinTime()
            var start_minute = amp.startMinute
            var end_minute = amp.endMinute
            var date = amp.date
            var sector = amp.v2_sector
            // var total_score = amp.totalPeriod.v2_sum_zscore_on_period()
            var m = this;
            console.iDebug11 = idnext('iDebug11')
            var iDebug6
            controls_enabled(()=>{
                if(!(sector instanceof Sector)) {
                    my_throw( 'error sector is not of type Sector')
                }
            })
            // if(console.iDebug11 == 15234){
            ////////////// ////debugger;
            // }

            var intervs = wf.v2_get_contiguous_interv_between(date, date, start_minute, end_minute, true /*only_with_sector*/ )
            var interv = intervs[0];
            if(!interv) {
                // console.warn('personne');
                return
            }
            if(start_minute < interv.startMinute || last(intervs).endMinute < end_minute) return; // si l'interention ne couvre pas de start_minute à end_minute alors quitter
            if(!interv.get_real_sector()) return;
            if(interv.sector == sector) return;

            var old_score_total_period = amp.totalPeriod.v2_sum_zscore_on_period()
            var old_score = m.v2_get_zscore_global() //le but est de réduire le score
            // var objInter = interv.v2_serialize()
            var objInter = {
                date,
                sector
            }
            objInter.startMinute = start_minute
            objInter.person = wf
            objInter.endMinute = start_minute + m.getMinTime(sector, wf)
            objInter.dontAffectNow = true
            objInter.rules = {
                // v2_break_if_min_time_not_ok_around_pause: true, //c'est le mintime avant et après la pause que l'on vérifie
                v2_do_not_respect_min_time_after_before: true, // pour le précédent et le suivant on va vérifier manuellement
                v2_do_not_extend_workstime: true // ne pas étendre le temps de travail sur la journée ou même toucher au pause
            }
            //try a change permet d'avoir une fonction d'annulation pour remettre comme avant
            // si le score a empiré
            var hasBeenInsert
            var p = {
                hasBeenInsert: false,
                arguments,
                objInter,
                intervs
            }

            var interv2;
            var new_score, new_score_total_period
            var is_score_better = function() {
                new_score_total_period = amp.totalPeriod.v2_sum_zscore_on_period()
                new_score = m.v2_get_zscore_global()
                var is_score_better = new_score < old_score && new_score_total_period < old_score_total_period
                return is_score_better
            }

            // var undoFuncs = [UndoFunc];//tableau de callback pour annuler les actions
            interv2 = new Intervention(objInter)
            idnext('no_name6')
            iDebug6 = console.iDebug
            console.iDebug61 = iDebug6
            if(11045 == iDebug6) {
                ////////////////debugger
            }
            // if(iDebug6 == 22252){
            ////////////// ////debugger
            // }
            interv2.end_minute_too_late = function(interv2, last_minu_worked) {
                interv2.startMinute = last_minu_worked - m.getMinTime(sector, wf)
                interv2.endMinute = last_minu_worked
            }
            var undoFunc_4;
            //backup de la journée
            interv2.before_add = () => {
                undoFunc_4 = m.v2_try_a_change({
                    date,
                    person: wf,
                    // startMinute: -48 * H,// 0 n'est pas suffisemment petit car la journée pourrait commencé à -2h pour 22h de la veille
                    // endMinute: 48 * H, //24 n'est pas suffisant car une journée peut débordé sur le lendemain
                })
            }
            wf.addInterv(interv2, m)
            p.hasBeenInsert = interv2.hasBeenInsert;
            var undo = false;
            if(p.hasBeenInsert) {
                controls_enabled(()=>{
                    if(interv2 && !interv2.v2_respectMinTime()) {
                        my_throw( 'error minTime')
                    }
                })
                if(is_score_better()) {;
                    ['next', 'prev'].forEach((dir) => {
                        var prev = interv2[dir]
                        if(!prev) return;
                        // si on avait déjà la personne placé en dir et que cela ne
                        // respecte pas le temps minimum alors le changer pour
                        // l'entité actuelle.
                        //var missingTime = prev.v2_missing_for_min_time()
                        var m = this
                        var start_end = prev.getStartAndEndSameSector()
                        var missingTime = prev.getMinTime() - start_end.time
                        if(prev && prev.get_real_sector() && missingTime > 0) {
                            // si do_not_let_too_small_inter alors ne pas faire l'ajout si cela laisse des interventions sur le coté trop petite
                            // cependant si l'intervention laissée est vraiment petite <= tolerance_min_time alors l'intégrer dans le nouveau secteur
                            // le but est de ne pas laissé des intervention de 1.5 heure quand le min time est de 2 et si on l'intègre on va généré une grande
                            // intervention alors que l'on veut privilégier les petites
                            if(!opt.do_not_let_too_small_inter || missingTime <= conf.tolerance_min_time) {
                                if(!prev.type.is_pause()) {
                                    // var objPrev = prev.v2_serialize()
                                    // m.v2_try_a_change(objPrev,(UndoFuncPrev) => {
                                    // undoFuncs.push(UndoFuncPrev)
                                    var objPrev2 = prev.v2_serialize()
                                    objPrev2.sector = interv2.sector
                                    objPrev2.dontAffectNow = true
                                    objPrev2.rules = {
                                        v2_break_if_min_time_not_ok: true
                                        // v2_break_if_min_time_not_ok_around_pause: true //c'est le mintime de cette intervention que l'on va vérifier mais on doit vérifier manuellement celle avant et après
                                    }
                                    var inter9 = new Intervention(objPrev2)
                                    wf.addInterv(inter9, m)
                                    if(!inter9.hasBeenInsert) {
                                        undo = true
                                        iDebug6;
                                        p;
                                    } else {
                                        // if(Debug.wf_id.has(+wf.wfId) && inter9.startMinute < Debug.between_end && inter9.endMinute > Debug.between_start){
                                        // // if(wf.wfId == 10356 && date == '2018-03-26' && inter9.start_minute == 15.5){
                                        //     // var sector = inter9.sector
                                        //     console.log('why it is so long')
                                        // }
                                    }
                                    // })
                                } else {
                                    undo = true
                                }
                            } else {
                                undo = true
                            }
                        }
                    })
                    if(!undo) {
                        //si le score a baissé on annule
                        undo = !is_score_better()
                    }
                } else {
                    //le score a baissé on annule
                    undo = true
                }
                //si on ne respecte pas la condition de min time ou que le score est pire alors annuler
                if(undo) {
                    // undoFuncs.reverse().forEach((callback) => {
                    //     callback()
                    // })
                    undoFunc_4() // remettre la journée telle que elle était
                    p.hasBeenInsert = false
                }
            }
            //TODO: Débug only
            checkMinTime()
            return p.hasBeenInsert
        }


        v2_try_a_change(info_to_backup, changementFunc) {
            var intervs = info_to_backup.person.v2_get_contiguous_interv_between(
                info_to_backup.date,
                info_to_backup.date,
                info_to_backup.startMinute,
                info_to_backup.endMinute,
                false /*false => take invervention even if they haven't sector*/
                )
            var backups = []
            var last_update_debug_id = 0;
            var lid = info_to_backup.person.listInfo_by_date
            var li,first_interv_sector,last_interv_sector
            if(lid){
                li = lid[info_to_backup.date]
                first_interv_sector = li.first_interv_sector
                last_interv_sector = li.last_interv_sector
            }
            const backup = (inter) => {
                if(!inter) return
                    controls_enabled(()=>{
                        if(!inter[isProxy] || !inter.person[isProxy]){
                            my_throw( 'devrait être un proxy')
                        }
                    })
                last_update_debug_id = Math.max(inter.last_update_debug_id,last_update_debug_id)
                var seria8 = inter.v2_serialize()
                if(inter.protected) {
                    seria8.protected = true
                    seria8.fakeProtected = inter.fakeProtected
                    seria8.protected_reason = inter.protected_reason
                    seria8.true_protected_information = inter.true_protected_information
                }
                seria8.dontAffectNow = true
                var backup = new Intervention(seria8)
                backup.old_inter = inter
                backup.ori_addIntervDebug = inter.ori_addIntervDebug || inter.addIntervDebug
                backup.debug_origin = inter.id
                // backup.iDebug_create = inter._debug_iagp && inter._debug_iagp.iDebug, //le idebug lors de la création de cette intervention
                backup.iDebug = console.iDebug
                backups.push(backup)
            }
            // il faut mieux placer la dernière et la première directement car sinon les quart d'heure
            // ouverture et fermeture risque d'être recalculer à chaque fois.
            backup(first_interv_sector)//;backup.first_interv_sector = first_interv_sector
            backup(last_interv_sector)//;backup.last_interv_sector = last_interv_sector
            intervs.forEach(function(inter){
                if(inter === first_interv_sector || inter === last_interv_sector){
                    return
                }
                backup(inter)
            })


            if(info_to_backup.get_array_of_backup) {
                return backups
            }
            var UndoFunc = function() {
                Debug.undo_until_ludi = last_update_debug_id
                Debug.undo = true
                var debugs = intervs
                if(!backups[0]) return;
                var person = backups[0] && backups[0].person
                person.nb_min_minute_fakely_worked = (person.nb_min_minute_fakely_worked || 0) + 10 * H //debug only       
                backups.forEach(function(backup) {
                    backups;
                    var iDebug = console.iDebug;
                    if(iDebug == 102365) {
                       // debugger;
                   }
                   if(backup.old_inter.hasBeenInsert &&
                    backup.startMinute == backup.old_inter.startMinute &&
                    backup.endMinute == backup.old_inter.endMinute &&
                    backup.sector == backup.old_inter.sector &&
                    backup.fakeProtected == backup.old_inter.fakeProtected &&
                    backup.protected == backup.old_inter.protected
                    ) {
                        //apparemment on a rien changé donc rien à restorer
                    return;
                }
                backup.rules = {
                        megaForcedInsert: true, // l'insertion ne peut que réussir, car c'est une sauvegarde d'un état stable
                    }

                    var inter = new Intervention(backup)
                    controls_enabled(()=>{
                        if(!inter) {
                            my_throw( 'inter is null')
                        }
                    })
                    inter.updateDebugId = backup.old_inter.updateDebugId || ''
                    inter.before_restore = true;
                    backup.person.addInterv(inter)
                    controls_enabled(()=>{
                        if(!inter.hasBeenInsert) {
                            my_throw( 'comment une restoration peut raté')
                        }
                    })
                    inter.before_restore = false;
                    if(Debug.put_historic_in__historic_inter) {
                        Debug.historic_inter = Debug.get_inter_history(backup)
                    }
                })
                Debug.undo = false
                person.nb_min_minute_fakely_worked -= 10 * H;
            }
            changementFunc && changementFunc(UndoFunc)
            return UndoFunc;
        }

        replace_the_pause(person, date, sector) {

        }

        /*pause du midi */
    // addAPause(pause_instance, person,date,start_minu,end_minu,sector,debug_ena,opt6={}){
        addAPause(opt6 = {}) {
            var m = this;
            console.iidebug[38] = idnext('38')
            const {date,person,min_start_pause,max_start_pause,pause_instance} = opt6
            var debug_ena = opt6.debug_ena;
            if(console.iidebug[38] == 77133) {
                //// debugger;
                var ddug = true
                Debug.logDayWf(person, date)
            }
            var undoF = m.v2_try_a_change({
                person,
                date
            });
            const hc = pause_instance.get_hours_config(date,person)
            var pauseTime = hc.time
            if(m.is_test) {
                var zscores_of_the_day = [{
                    amp: {
                        startMinute: min_start_pause,
                        endMinute: min_start_pause + pauseTime
                    }
                }, {
                    amp: {
                        startMinute: min_start_pause + pauseTime,
                        endMinute: min_start_pause + 2 * pauseTime
                    }
                }, {
                    amp: {
                        startMinute: min_start_pause + 2 * pauseTime,
                        endMinute: min_start_pause + 3 * pauseTime
                    }
                }, {
                    amp: {
                        startMinute: min_start_pause + 3 * pauseTime,
                        endMinute: min_start_pause + 4 * pauseTime
                    }
                }]
            } else {
                //il faut enlever totalement la personne pour savoir le meilleur endroit ou 
                //ou on doit la mettre
                person.reset_date(date,{
                    even_if_protected: true
                })
                var zscores_of_the_day = m.getArrayGlobalZscoreByDay(pauseTime, person, {
                    dates: [date],
                })[date] //.reverse()
                undoF()//restaurer la journée
            }
            var iDebug1 = console.iDebug
            var checkDoublePause = function(wf, date) {
                controls_enabled(()=>{
                    var inter = wf.get_first_interv_at_date(date)
                    inter.listInfo.check_logic()
                    var nb_pause = 0;
                    while (inter) {
                        if(inter.is_pause()) {
                            ++nb_pause
                        }
                        inter = inter.next
                    }
                    zscores_of_the_day
                    if(nb_pause > 1) {
                        my_throw( 'error too much pause')
                    }
                })
            }
            for (var i = 0; i < zscores_of_the_day.length; ++i) {
                checkDoublePause(person, date)
                var zone_info = zscores_of_the_day[i]
                var start_amp = zone_info.amp
                var start_minute = start_amp.startMinute
                var end_minute = start_minute + pauseTime
                if(start_minute < min_start_pause || start_minute > max_start_pause) {
                    continue;
                }
                if(opt6.do_not_put_pause_on) {
                    //si la pause va croiser des interventions protégées alors ne pas la mettre
                    var not_possible_because_of_protected = false
                    opt6.do_not_put_pause_on.some(function(protected_inte) {
                        if(date == protected_inte.date && protected_inte.startMinute < end_minute && protected_inte.endMinute > start_minute) {
                            not_possible_because_of_protected = true
                            return true
                        }
                    })
                    if(not_possible_because_of_protected) {
                        continue
                    }
                }
                if(debug_ena) {
                    console.log("pause entre " + start_minute + ' et ' + end_minute + 'le ' + start_amp.date + 'iDebug ' + console.iDebug)
                }
                var pause = new Intervention({
                    startMinute: start_minute,
                    endMinute: end_minute,
                    rules: {
                        //grace à v2_break_if_min_time_all_sector_not_ok_around_pause
                        //on sait que l'on pourra changer le secteur des intervention suivant
                        //ou précédente pour que le temps minimum soit respecté
                        // on ne gère pas le cas où cela n'est pas possible à cause du temps maximum
                        v2_break_if_min_time_all_sector_not_ok_around_pause: true,
                        //On se fiche pour le moment de savoir si cela laisse sur les
                        //cotés des intervention trop petite car on les changera de secteur plus tard
                        //avec solve_next_prev_too_small_for_pause
                        safe_insertion: conf.do_not_try_to_change_sector_around_pause,
                        v2_do_not_respect_min_time_after_before: !conf.do_not_try_to_change_sector_around_pause
                    },
                    date: date,
                    main: m,
                    id: 'pause' + Main.getUniqId(),
                    // type:'PAUSE',
                    sector: hc.fake_sector
                }, m);
                console.iidebug[63] = idnext('63')
                if(console.iidebug[63] == 26819){
                    // debugger
                }
                person.get_first_interv_at_date(pause.date).listInfo.check_logic()
                person.addInterv(pause, m)
                controls_enabled(()=>{
                    if(!isFinite(pause.startMinute)) {
                        my_throw( 'minutes have to be finite')
                    }
                })
                checkDoublePause(person, date)
                if(pause.hasBeenInsert) {
                    controls_enabled(()=>{
                        if(pause.next.hasBeenInsert == false) {
                            my_throw( 'comment le suivant peut ne pas être inséré !!!')
                        }
                        if(!pause.next.is_counted_in_worktime() /*|| pause.next.sector.id =='fake'*/ ) {
                            my_throw( 'comment le suivant peut ne pas avoir une vrai secteur !!!')
                        }
                    })
                    if(pause.endMinute - pause.startMinute > pauseTime) {
                        ////////debugger;
                        // my_throw( 'error')
                    }
                    pause.protected = pause.fakeProtected = true // il ne faut pas pour le moment toucher au pause donc c'est protégé
                    if(!conf.do_not_try_to_change_sector_around_pause){
                        var info_solve = pause.solve_next_prev_too_small_for_pause(opt6)
                        checkDoublePause(person, date)
                        if(info_solve.echec) {
                            undoF()
                            continue //il faut tenter ailleur
                        }
                    }

                    if(!pause.sector.pause_instance.is_pause_ok(pause)) {
                        undoF()
                        continue
                    }
                    return pause;
                }
            }
            return {
                hasBeenInsert: false
            }
        }
        reduce_time_worked_to_respect_max_time(wfArr){
            var m = this;
            return m.v2_move_multiple_extremity({
                wfArr,
                delete_while_too_much_job_time: true,
                from: 'reduce_time_worked_to_respect_max_time'
            })
        }
        /*
        * le but est de bouger les extrémité des journées pour améliorer
        * le résultat global et éviter le surrefectif 
        * Une extremité n'est bougé qu'une seule fois il faut donc relancé l'algorithm
        * tant que celui-ci améliore le résultat
        *
        * si opt.delete_while_too_much_job_time alors supprimer le temps fait en trop et ne pas le replacer
        */
        v2_move_multiple_extremity(opt) {
            var m = this,
            failled_and_next_extremity = () => {},
            success_and_next_extremity = () => {},
            wfArr = opt.wfArr || RandomGenerator.shuffle(m.all_wfs.slice())
            ;

            controls_enabled(()=>{
                if(opt.from != 'reduce_time_worked_to_respect_max_time'){
                    my_throw( 'cette fonction a été remplacé par improve_day_extremities')
                }
            })
            if(!opt.delete_while_too_much_job_time){
                var zscore_f = opt.zscore_func || (() => m.v2_get_zscore_global());
                if(opt.dont_make_rotation_worst) {
                    opt.get_rotation_score = m.get_rotation_score.bind(m)
                }
            }
            var checkTime = (wf)=>{
                controls_enabled(()=>{
                    Debug.check_not_enough_worked(wf)
                    Debug.check_too_much_worked(wf)
                })
            }
            // m.all_wfs.forEach(function(wf) {
            //     console.watch(wf, 'nb_minu_worked', function() {
            //       checkTime(wf)
            //     })
            // })
            wfArr.forEach((wf,i) => {
                if(opt.from != 'reduce_time_worked_to_respect_max_time'){
                    checkTime(wf)
                }
                wf.intervsRacines.forEach(function(inter) {
                    [
                    ['get_first_inter_with_sector_same_day', 'first'],
                    ['get_last_inter_with_sector_same_day', 'last']
                    ].forEach(function(infos) {
                        console.iidebug[19] = idnext('19');
                        if(console.iidebug[19] == 112922) {
                            debugger
                        }
                        var get_first_inter_with_sector_same_day = infos[0]
                        var f_l = infos[1];
                        var first = inter[get_first_inter_with_sector_same_day]()
                        if(!first) return failled_and_next_extremity()
                            var date = inter.date
                        var interTime = first.time
                        // ne décaller que si ça ne casse pas le temps minimal de l'intervention
                        // Si notre intervention dans le même secteur laisse un petit bloc inutilisable
                        // alors bouger l'intervention entière
                        // erreur il faut enlever moins du coup
                        // on enlève pas trop sinon on passe en dessous du min par jour
                        var d = inter.get_listInfo_by_date()
                        var time = d.worked - d.min//le temps maximal que l'on peut enlever à la journée
                        if(opt.delete_while_too_much_job_time){
                            //il faut enlever le temps que l'on fait en trop de sorte 
                            //à respecter le minimum par jour
                            time = Math.min(time,wf.nb_minu_worked - wf.nbMinutesMin)
                        }
                        time = lower(time)
                        for (; time > 0; time -= conf.displayMinTime) {
                            console.iidebug[21] = idnext('21')
                            if(97401 == console.iidebug[21]) {
                                Debug.logDayWf(wf, inter.date)
                                //// debugger;
                            }

                            first = inter[get_first_inter_with_sector_same_day]()
                            if(!first) return failled_and_next_extremity()
                                var restore_wf = m.v2_try_a_change({
                                    person: wf,
                                    date
                                })
                            if(!opt.delete_while_too_much_job_time){
                                var zscore_global1 = zscore_f()
                                var wf_minute_worked = wf.nb_minu_worked;
                                var zscore_global2 = zscore_f()
                                controls_enabled(()=>{
                                    if(zscore_global2 != zscore_global1) {
                                        my_throw( 'On devrait être au moins égale')
                                    }
                                })
                            }
                            var d = wf.listInfo_by_date[date]

                            //il faut choisir les horaire à déplanifier de sorte à ce que l'on reste dans les bornes pour le début de la journée
                            //et la fin de la journée
                            if(f_l == 'first') {
                                var unplanif = {
                                    start_minute: first.startMinute,
                                    end_minute: Math.min(d.max_end_day, first.startMinute + time),
                                }
                                //on ne veut pas replanier à cette endroit
                                var forbid_amp = {
                                    date: d.date,
                                    start_minute: d.first_minu_worked,
                                    end_minute: d.first_interv_sector.endMinute
                                }
                            } else {
                                var unplanif = {
                                    date: d.date,
                                    end_minute: first.endMinute,
                                    start_minute: Math.max(d.min_end_day, first.endMinute - time)
                                }
                                //on ne veut pas replanier à cette endroit
                                var forbid_amp = {
                                    start_minute: d.last_interv_sector.startMinute,
                                    end_minute: d.last_minu_worked
                                }
                            }
                            if(unplanif.start_minute >= unplanif.end_minute) {
                                return failled_and_next_extremity()
                            }

                            //on désaffecte pour réaffecter ailleur
                            var desa = new Intervention({
                                endMinute: unplanif.end_minute,
                                startMinute: unplanif.start_minute,
                                sector: null, //on désaffecte grace au secteur null
                                date,
                                dontAffectNow: true,
                                rules: {
                                    v2_do_not_respect_min_time_after_before: true
                                }
                            })

                            var adi_desa = wf.addInterv(desa) //libérer des heures pour les mettre ailleurs
                            if(!adi_desa.has_been_insert) {
                                //rien à annuler comme la désaffectation a raté
                                // return failled_and_next_extremity();
                                continue//tester avec un temps plus petit
                            }
                            var dir = f_l == 'first' ? 'next' : 'prev'
                            var is_min_solved = desa[dir] && desa[dir].solve_min_time_not_respected()
                            if(is_min_solved === false) {
                                restore_wf()
                                continue;
                            }

                            console.iidebug[18] = ++console.iDebug
                            if(console.iidebug[18] == 114695) {
                                // Debug.logDayWf(wf, inter.date)
                                debugger;
                            }

                            if(!opt.delete_while_too_much_job_time){
                                var zscore_global3 = zscore_f()
                                controls_enabled(()=>{
                                    if(!m.is_penalise_overstaffing && zscore_global3 < zscore_global1) {
                                        my_throw( "on vient d'enlever un personne donc le score devrait être plus haut")
                                    }
                                })
                                var time_to_place = wf_minute_worked - wf.nb_minu_worked
                                var info_extrem = m.add_inter_on_extremity({
                                    zscoreG: zscore_global1,
                                    time: time_to_place,
                                    wf,
                                    forbid_amp,
                                    put_in_main_sector: true,
                                    inter_of_the_day: opt.rules.has('same_day') ? inter : null
                                })
                                var is_score_better2 = info_extrem.zscoreG < zscore_global1
                                controls_enabled(()=>{  
                                    if(!m.is_penalise_overstaffing && is_score_better2 && !info_extrem.has_added_minute) {
                                        my_throw( "comment le score peut s'améliorer si l'ajout à raté")
                                    }
                                })
                                var is_worst =  !is_score_better2
                                if(info_extrem.has_added_minute && !is_worst) {
                                    if(opt.rules.has('dont_make_rotation_worst')) {
                                        var zscore_rotation = opt.get_rotation_score()
                                        is_worst = zscore_rotation > opt.zscore_rotation
                                    }
                                    var d = wf.listInfo_by_date[desa.date]
                                    if(d.worked < d.min){
                                        is_worst = true
                                    }
                                    if(!is_worst) {
                                        console.green('Le score est passée de ' + zscore_global1 + ' à ' + info_extrem.zscoreG)
                                        zscore_global1 = info_extrem.zscoreG
                                        return success_and_next_extremity();
                                    }
                                }
                                //on annule là ou on avait placer les heures enlevées
                                info_extrem.undo_func && info_extrem.undo_func()
                                controls_enabled(()=>{
                                    if(zscore_f() > zscore_global3) {
                                        my_throw( 'On devrait être au moins égale')
                                    }
                                })
                                restore_wf() //on remet les heures là où elles étaient car ça n'améliore pas le score
                                controls_enabled(()=>{
                                    if(!equal_fl(zscore_f(), zscore_global1)) {
                                        Debug.logDayWf(wf, inter.date)
                                        my_throw( 'On devrait être au moins égale')
                                    }
                                })
                                m.zscoreGlobal = zscore_global1 // à cause des arrondis je preffère remettre comme c'était
                                controls_enabled(()=>{
                                    if(wf.nb_minu_worked != wf_minute_worked) {
                                        my_throw( 'mais où passent les heures')
                                    }
                                })
                            }
                        }
                    })
                })
                // console.green("une personne supplémentaire a eu ses horaires d'extrémité de journée optimisé "+ wf.wfId + ' ' + (new Date()).getTime() )
            })
        }

        /**
         * Ajouter un temps time en début ou fin de journée si ça n'empire pas le score
         * Attention on ne choisi pas forcément la meilleure solution
         */
         add_inter_on_extremity(opt) {
            const {step,do_not_check_pause_and_time,avoid_closing_opening_problem,put_in_main_sector,forbid_amp,wf} = opt
            var  m = this
                ,opt_min_max = opt.opt_min_max//les heures max et min
                ,zscore_global1 = opt.zscoreG//peut être null si on se moque de l'augmenter
                ,score_has_increase = false
                ,restore_day//fonction d'annulation de l'action
                ,returnf = () => {
                    return {
                        zscoreG: zscore_global1,
                        undo_func: opt.dont_care_about_score || score_has_increase ? restore_day : null,
                        has_added_minute: hasBeenInsert4
                    }
                }
                ,get_zscore = opt.get_zscore || (() => m.v2_get_zscore_global())
                // Pour chaque journée travaillé par la personne essayé d'ajouté au début de la journée puis
                // à la fin en gardant le même secteur que le voisin où l'on va se coller.
                ,wf_minute_wanted = wf.nb_minu_worked + time
                ,init_score = get_zscore()
                ;

                var time = lower(opt.time,step)
                // controls_enabled(()=>{
                //     if(time <= 0){
                //         my_throw( 'erreur')
                //     }
                // })
                if(time <= 0){
                    return returnf();
                }
                console.iidebug[17] = idnext('17')
                if(console.iidebug[17] == 26973) {
                    var deb = true
                    console.log('have ' + wf.nb_minu_worked)
                    console.log('wanted ' + wf_minute_wanted)
                    debugger;
                }
                Debug.check_too_much_worked(wf)
                var days_first_interv = opt.inter_of_the_day ? [opt.inter_of_the_day] : RandomGenerator.shuffle(wf.intervsRacines.slice())
                var hasBeenInsert4 = false
                var time_has_been_added = false
            //pour chaque jour de la semaine (préalablement mélangé) tenter d'ajouter au début ou à la fin
            days_first_interv.some((inter) => {
                var dayI = wf.listInfo_by_date[inter.date]
                if(dayI.worked + time > dayI.max) {
                    return; //cette journée à inter.date est déjà bien trop remplie pour y ajouter des interventions
                }
                console.iidebug[16] = idnext('16')
                if(console.iidebug[16] == 26974) {
                    console.log(wf.nb_minu_worked)
                    debugger;
                }
                var restore_day2 = m.v2_try_a_change({
                    date: inter.date,
                    person: inter.person
                });
                restore_day = function() {
                    hasBeenInsert4 = false
                    restore_day2()
                }

                if(deb) {
                    console.log(wf.nb_minu_worked)
                }
                Debug.check_too_much_worked(wf)
                var check_minutes = function() {
                    controls_enabled(()=>{
                        if(wf.nb_minu_worked < wf_minute_wanted) {
                            console.log('console.iidebug[16]='+console.iidebug[16])
                            console.log('console.iidebug[15]='+console.iidebug[15])
                            console.log('console.iidebug[21]='+console.iidebug[21])
                            my_throw( 'pk le nombre d\heure de la personne n a pas atteint ' + wf_minute_wanted)
                        }
                    })
                }
                var nb_minu_worked_before_some = wf.nb_minu_worked;
                var some_ret = function(retn) {
                    if(deb) {
                        console.log(wf.nb_minu_worked, retn)
                    }
                    controls_enabled(()=>{
                        if(wf.nb_minu_worked  > wf.nbMinutesMin) {
                            my_throw( '> 35')
                        }
                    })
                };
                ['first', 'last'].some((f_l) => {
                    controls_enabled(()=>{
                        if(wf.nb_minu_worked < nb_minu_worked_before_some) {
                            my_throw( 'what is happen')
                        }
                    })
                    console.iidebug[15] = idnext('15')

                    //on récupère la valeur actualisé de la première ou dernière inter de la journée
                    var first_interv = inter['get_' + f_l + '_inter_with_sector_same_day']();
                    if(!first_interv) return some_ret(1);
                    var inter11 = first_interv.v2_clone()
                    var date = inter11.date
                    var {max_end_day,min_start_day} = m.get_min_start_and_end({wf,date})
                    if(f_l == 'first') {
                        inter11.endMinute = inter11.startMinute
                        inter11.startMinute = Math.max(inter11.endMinute - time, min_start_day)
                    } else {
                        inter11.startMinute = inter11.endMinute
                        inter11.endMinute = Math.min(inter11.startMinute + time, max_end_day)
                    }
                    if(inter11.startMinute >= inter11.endMinute){
                        return some_ret(5)
                    }
                    if(avoid_closing_opening_problem){
                        const open_info = wf.opening_info({
                            date:inter11.date,
                            start_minute: inter11.startMinute
                        })
                        const close_info = wf.closing_info({
                            date:inter11.date,
                            end_minute: inter11.endMinute
                        })
                        //si ça nous met sur une heure ou il faut ajouter du temps ne pas le faire
                        if(open_info.is_supplement_time() || close_info.is_supplement_time()){
                            return some_ret(6)
                        }
                    }
                    if(forbid_amp && forbid_amp.date == date && forbid_amp.start_minute == inter11.startMinute){
                        //ça ne sert à rien de tenter de placer au même endroit qu'avant
                        return some_ret(6)
                    }
                    inter11.rules = {
                        dontMoveExistingInterventionOrPause: true, // comme on veut ajouter avant ou après, pas de raison de bouger
                        v2_do_not_respect_min_time_after_before: true, // ne pas vérifier le temps restant après ou avant l'intervention vu que ça change pas
                        v2_break_if_min_time_not_ok: true //c'est le mintime de cette intervention que l'on va vérifier et si il est pas bon on n'ajoutera pas
                    }
                    // console.log(wf.rotation.Saturday)
                    controls_enabled(()=>{
                        if(wf.nb_minu_worked < nb_minu_worked_before_some) {
                            my_throw( 'what is happpen')
                        }
                    })
                    var debug_i = inter11.v2_clone()
                    var start_minute = inter11.startMinute
                    var end_minute = inter11.endMinute
                    if(opt_min_max){
                        let error_code = wf.get_start_end_minu_error({opt_min_max,start_minute,end_minute,date,whole_day:false})
                        if(error_code){
                            return some_ret(error_code)
                        }
                    }
                    if(put_in_main_sector){
                        inter11.sector = m.blockArr[0].sectorEntArr[0]
                    }

                    if(26985 == console.iidebug[15]) {
                        debugger;
                    }
                    var adi_inter11 = wf.addInterv(inter11)
                    controls_enabled(()=>{
                        if(wf.nb_minu_worked < nb_minu_worked_before_some) {
                            my_throw( 'what is happen')
                        }
                    })
                    if(!adi_inter11.has_been_insert) {
                        return some_ret(2); //rien à annuler on a rien fait
                    }

                    console.iidebug[20] = idnext('20')
                    if(console.iidebug[20] == 34873) {
                        debugger;
                    }
                    hasBeenInsert4 = true
                    check_minutes()
                    wf.nb_min_minute_fakely_worked = (wf.nb_min_minute_fakely_worked || 0) + 10 * H //debug only

                    // résoudre les problèmes de pauses tout en gardant le même nombre d'heure
                    // et sans remettre en cause l'intervention nouvellement ajoutée
                    var rpatmid = {}
                    if(wf.nb_minu_worked <= wf.nbMinutesMin){
                        if(!do_not_check_pause_and_time){
                            var no_pause = inter11.clone()
                            no_pause.startMinute = start_minute;
                            no_pause.endMinute = end_minute;
                            var rpatmid =  m.resolve_pause_and_time_in_day(inter11,new opt_v2_schedule_planning({
                                include_this_amplitude: {
                                    start_minute: start_minute,
                                    end_minute: end_minute
                                },
                                do_not_put_pause_on: [no_pause]
                            }),void 8,{
                                nb_minutes_wanted_at_final: Math.min(wf.nb_minu_worked,wf.nbMinutesMin),
                            })
                        }
                        // si la pause a raté ou que le score n'a pas diminué (c'est mieux quand il diminue)
                        if(!rpatmid.probleme && wf.nb_minu_worked <= wf.nbMinutesMin) {
                            if(!opt.dont_care_about_score) {
                                var zscore_global2 = get_zscore()
                                score_has_increase = zscore_global2 < zscore_global1
                            }
                            var d = wf.listInfo_by_date[date]
                            if(d.max < d.worked || d.min_end_day > inter11.last_minu_worked){
                                score_has_increase = false
                            }
                            if(opt.dont_care_about_score || score_has_increase) {
                                some_ret(3);
                                time_has_been_added = true
                                wf.nb_min_minute_fakely_worked -= 10 * H //debug only
                                return true
                            }
                            //@TODO faire un changement de secteur aussi
                        }
                    }
                    //TODO détécter à l'avance que ça va dépasser au niveau du temps à cause des fermeture ou ouverture 
                    //qui compte pour un quart d'heure de plus
                    // alors annuler
                    restore_day()
                    wf.nb_min_minute_fakely_worked -= 10 * H //debug only
                    controls_enabled(()=>{
                        if(wf.nb_minu_worked < nb_minu_worked_before_some) {
                            my_throw( 'what is happen')
                        }
                    })
                    some_ret(4)
                })
                some_ret();
                return time_has_been_added //score_has_increase
            })
            controls_enabled(()=>{
                if(wf.nb_minu_worked + time < wf_minute_wanted) {
                    my_throw( 'problème')
                }
            })
            return returnf()
    }  

    main_resolve_pause_unrespected(opt) {
        var m = this;
        var is_pause_ok = true
        console.iidebug[45] = idnext('45');
        if(console.iidebug[45]==77099){
                    //// debugger;
            }
        // m.pause_dispatcher.some(function(pause_instance){
            console.iDebug16 = console.iDebug;
            if(opt.person) {
                var first_interv = person.get_first_interv_at_date(date)
            } else {
                var first_interv = opt.intervention.first_interv
            }
            controls_enabled(()=>{
                if(!first_interv){
                    my_throw( 'error first interv')
                }
            })
            var pauseInfo = m.pause_dispatcher.resolve_pause_unrespected(first_interv, {
                do_not_put_pause_on: opt.do_not_put_pause_on
            })
            if(!pauseInfo.pause_is_ok) {
                is_pause_ok = false
                // console.log('i_debug16', console.iDebug16)
                // console.warn('undo because of pause lets try with the prev person')
            }
            return pauseInfo
        }

        affectMinTimeBlock() {
            var m = this
            m.blockArr.forEach((block) => {
                var minTime = block.sectorEntArr[0].minTimeSector
                block.sectorEntArr.forEach((sector) => {
                    if(sector.minTimeSector < minTime) {
                        minTime = sector.minTime
                    }
                })
                block.minTimeSectors = minTime
            })
        }
        affectProtectedIntervs(protectedIntervArr) {
            var m = this;

            m.minTimeAllSectorIs0 = true;
            protectedIntervArr.forEach(function(int) {
                var r = int.person.wfId + ''
            })

            var cmp = function(a, b) {
                if(a > b) {
                    return 1
                }
                return a < b ? -1 : 0
            }
            protectedIntervArr.sort(function(a, b) {
                return cmp(a.person.wfId, b.person.wfId) ||
                cmp(a.listInfo.date, b.listInfo.date) ||
                cmp(a.startMinute, b.startMinute)
            })
            protectedIntervArr.forEach((interv,i) => {
                interv.protected = true;
                // L'id de l'intervention en base correspond à une intervention
                // cependant comme on a potentiellement découpé cette intervention à la demis cette id n'est plus unique
                // il faut donc le compléter pour le rendre unique
                // une intervention peut être sans secteur si dontPlanif
                interv.id = interv.id + '_internalId' + Main.getUniqId();
                let wf = interv.person
                let inter_info = interv.true_protected_information
                let d = wf.listInfo_by_date[interv.date]
                /**
                *   Comme les protected sont sur les heures d'ouverture et fermeture on ne 
                *   pourra pas ajouter le temps nécéssaire
                */
                const close_info = wf.closing_info({
                 start_minute: inter_info.start_minute,
                 end_minute: inter_info.end_minute,
                 date: interv.date
             })
                // if(inter_info.start_minute <= conf.closing_minute + wf.time_opening
                //     && inter_info.end_minute >= conf.closing_minute){
                    if(close_info.block_closing('supplement_time')){
                    // debugger
                    d.disabled_time_closing = true
                }
                const open_info = wf.opening_info({
                    start_minute: inter_info.start_minute,
                    end_minute: inter_info.end_minute,
                    date: interv.date
                })
                 // && inter_info.end_minute >= conf.opening_minute - wf.time_opening
                 if(open_info.block_opening('supplement_time')){
                    d.disabled_time_opening = true
                }

                if(d){
                    if(inter_info.end_minute > d.max_end_day ){
                        d.max_end_day = lower(inter_info.end_minute)
                        d.last_interv.endMinute = d.max_end_day
                    }
                    if(inter_info.start_minute < d.min_start_day ){
                        d.min_start_day = upper(inter_info.start_minute)
                        d.first_interv.startMinute = d.min_start_day
                    }
                }
                interv.rules = {
                    megaForcedInsert: true,
                    canFail: false //il se peut que l'on ait ramené des jours où la personnes ne travaille normalement pas mais où il y a une intervention
                }
                var pers = interv.person
                pers.addInterv(interv, m)
                m.logPersonSectorBug(false,pers)
                // console.log(console.iDebug)
                // if(interv.id == 92405){
                //  window.ii = interv
                // }
            })

            // si la personne dépasse déjà au niveau du temps de travail
            // à cause des protected (inter déjà en bdd)
            // alors augmenter ce maximum
            m.all_wfs.forEach((wf)=>{
                if(wf.nb_minu_worked > wf.nbMinutesMin){
                    wf.real_nbMinutesMin = wf.nb_minu_worked;
                    wf.nbMinutesMin = wf.nb_minu_worked;
                }
                m.logPersonSectorBug(false,wf)
            })

            if(conf.fillMinProtectedFirst) {
                var iDebug2
                //trier par worksfor puis date puis startMinute
                protectedIntervArr.sort(function(i1, i2) {
                    return i1.person.wfId - i2.person.wfId || (
                        i1.date > i2.date ? 1 :
                        i1.date < i2.date ? -1 :
                        i1.startMinute - i2.startMinute
                        )
                })
                protectedIntervArr.forEach((interv) => {
                    // si un clone a été fait de la protected on possède une référence morte
                    // donc récupérer la dernière
                    interv = interv.getLastProtectedCloneOrOriginal()
                    var opds = interv.getOpenAndCloseMinute(main) //les heures du secteur
                    var minTime = interv.sector.minTimeSector
                    // var interv5 = interv.clone()

                    idnext('no_name7')
                    var lInfo = interv.listInfo // les heure de la personne
                    if(console.iDebug == 755) {
                        console.error('hey');
                    }
                    var timeInfo = interv.getStartAndEndSameSector() //le temps au même secteur
                    if(timeInfo.time < minTime /* || la place restante est trop petite*/ ) {
                        var next = interv
                        while (next.next) {
                            if(next.sector) {
                                break
                            }
                        }
                        var prev = interv
                        while (prev.prev) {
                            if(prev.sector) {
                                break
                            }
                        }
                        var start1 = Math.max(timeInfo.endMinute - minTime,
                            interv.getMinOrMaxWithoutSector('prev').startMinute, //heure de début qui écrase aucune autre intervention et qui respecte l'emploie du temps de la personne
                            opds.minstartMinute,
                            prev.endMinute //heure de fin de la dernière précédente intervention de cette personn
                            )
                        var end1 = Math.min(start1 + minTime,
                            interv.getMinOrMaxWithoutSector('next').endMinute,
                            next.startMinute, //heure de la prochaine intervention de cette personne
                            opds.maxendMinute //heure de fin du secteur,
                            )

                        // on met une interv avant et après pour répondre au condition de mintime du secteur
                        var inter1 = {
                            startMinute: start1,
                            endMinute: timeInfo.startMinute,
                        }
                        var inter2 = {
                            startMinute: timeInfo.endMinute,
                            endMinute: end1
                        }

                        ;
                        [inter1, inter2].forEach((inter6) => { //attention au point virgule sinon ça fait crocher sur l'objet précedent
                            var uid = Main.getUniqId();
                            if(uid == 'uniq281') {
                                console.log('errr');
                                console.log(console.iDebug);
                            }
                            if(inter6.startMinute >= inter6.endMinute || !interv.sector) {
                                return;
                            }
                            var interv5 = new Intervention({
                                date: lInfo.date,
                                startMinute: inter6.startMinute,
                                endMinute: inter6.endMinute,
                                id: 'fakeProtec' + uid + '_' + console.iDebug,
                                sector: interv.sector
                            })
                            interv5.protected = true;
                            interv5.fakeProtected = true

                            while (interv.prev) {
                                interv = interv.prev;
                                if(interv.startMinute >= interv.endMinute) {
                                    console.log(iDebug2);
                                    console.error('not possible');
                                }
                            }
                            iDebug2 = console.iDebug;
                            var pers = interv.person
                            pers.addInterv(interv5, m)

                        })
                    }
                })
}
m.minTimeAllSectorIs0 = false;
}




getMaxRelativeDelta(interv, sector, main) {
            var delta = 0; // le delta max de ce secteur parmis les périodes horaire concernées par l'intervention
            // pour chaque periode concerné par l'intervention caluler le delta et retourner le max
            interv.forEachiPeriod(sector.planning, (iPeriod) => {
                var period = sector.planning.periodSortedArr[iPeriod]
                delta = Math.max((period.need - period.interventionMap.length) / period.need, delta)
            })
            return delta
        }
        affectUnaffectedPerson() {
            var id = 0;
            var m = this;
            m.blockArr.forEach((block) => { // pour chaque entité devant affecter à des secteurs
                block.wfArr.forEach((wf) => { // pour chaque personne de cette entité
                    forOf(wf.intervNotAffectedMap, (inter2, interId) => { // pour chaque intervention non affecté
                        if(!inter2) {
                            addToStackTrace('erreur étrange')
                            return;
                        }
                        if(inter2.hasBeenInsert) {
                            addToStackTrace("cette intervention a déjà été inséré ce n'est pas normale")
                            return;
                        }
                        for (var j = (inter2.endMinute - inter2.startMinute) / conf.minTime; j-- > 0;) { //pour chaque temps minimum de l'intervention
                            // on va organiser les secteur en fonction de ceux qui ont le plus grand besoins
                            // non comblé relativement au besoins
                            var sectorToDeltaMaxOnPeriod = []
                            block.sectorEntArr.forEach((sector) => {
                                var sectorId = sector.id;
                                sectorToDeltaMaxOnPeriod.push({
                                    sector: sector,
                                    relativeDelta: m.getMaxRelativeDelta(inter2, sector, m)
                                })
                            })
                            //trier en mettant en dernier l'entity qui a le plus grand écart relatif entre son besoin et son effectif
                            sectorToDeltaMaxOnPeriod.sort((a, b) => {
                                return +a.relativeDelta - b.relativeDelta // ordre croissant
                            })

                            // checher la mediane
                            // si on a été affecté à ce secteur plus que la mediane alors
                            // prendre une autre entité
                            // @TODO mettre le bon tableau par un générale à tous les blocs
                            var personSectorMaps = inter2.listInfo.person.personSectorMap

                            /*@toOptimize : recrée le tableau à chaque fois est une perte de temps */
                            // tableau des personSector trié en en mettant ce qui on le plus d'écart entre ce qui ont pas fait assez
                            // d'heure dans le secteur en dernier
                            var arr = []
                            forOf(personSectorMaps, (personSector) => {
                                arr.push(personSector)
                            })
                            arr.sort((a, b) => {
                                return -a.nb_minu_worked / a.nbMinuteToDoTotal + b.nb_minu_worked / b.nbMinuteToDoTotal // celui qui a pas assez travaillé en dernier
                            })
                            for (var k = sectorToDeltaMaxOnPeriod.length; k--;) { //tant que l'intervention n'a pu affecter passer au secteur suivant
                                // on vient de trouver le secteur qui avait le plus besoins de nous
                                // c'est celui qui avait le plus petit delta
                                var sctr = last(sectorToDeltaMaxOnPeriod).sector

                                // si l'écart relatif des besoins est plus important que l'écart de répartition
                                // des personnes sur les différents secteurs alors réduire l'écart en besoin et personne dispo
                                // sinon  prendre le secteur qui réduira l'écart de temps passé par la personne sur chaque secteur
                                var helpNeed = conf.fillNeedWeight * sctr.relativeDelta;
                                var persoSecto = personSectorMaps[sctr.id]
                                var HelpPersSctr = conf.personSectorMinuteWeight * (persoSecto.nbMinuteWanted - persoSecto.nb_minu_worked);

                                if(helpNeed < HelpPersSctr) {
                                    sctr = last(arr).ent
                                }
                                if(!sctr.hasOwnProperty('id')) {
                                    console.error(sctr.id)
                                }
                                var inter = new Intervention({
                                    startMinute: inter2.startMinute,
                                    endMinute: inter2.startMinute + conf.displayMinTime,
                                    date: inter2.listInfo.date,
                                    id: 'placement' + Main.getUniqId(),
                                    sector: sctr,
                                    person: wf,
                                    main: m
                                }, m);
                                if(inter.hasBeenInsert) {
                                    break;
                                }
                                if(helpNeed < HelpPersSctr) {
                                    sctr = arr.pop().ent
                                } else {
                                    sectorToDeltaMaxOnPeriod.pop()
                                }
                            }

                        }
                    })
})
})
}

forAllSectorOfAllBlock(callback) {
    var m = this;
    m.blockArr.forEach((block) => {
        block.sectorEntArr.forEach((sector) => {
            callback(sector)
        })
    })
}
forAllWfOfAllBlock(callback) {
    var m = this;
    m.blockArr.forEach((block) => {
        block.wfArr.forEach((wf) => {
            callback(wf)
        })
    })
}
addPersonSectorToSectorAndPerson() {
    var m = this;
            // m.forAllSectorOfAllBlock((sector) => {
            //  // var parent = m.blockMap[sector.parentEntityId]
            //  var parent = sector.parentEntity;
            //  parent.wfArr.forEach(function(wf){
            //      var wfId = wf.wfId;
            //      wf.personSectorMap[sector.id] = sector.personSectorMap[wfId] = m.personSectorMap[sector.id+'_'+wfId];
            //  })
            // })
            m.blockArr.forEach((block) => {
                block.wfArr.forEach(function(wf) {
                    var wfId = wf.wfId;
                    var lg = block.sectorEntArr.length;
                    forOf(block.sectorEntArr, (sector) => {
                        // var sectorId = sector.id
                        // wf.personSectorMap[sectorId] = sector.personSectorMap[wfId] = m.personSectorMap[sectorId+'_'+wfId];
                        sector.personSectorMap[wfId].nbMinuteWanted = wf.nbMinuteToDoTotal / lg
                    })
                })
            })
        }

        fillPossibleNeed() {
            var m = this;
            m.blockArr.forEach((block) => {
                var allAmplitudes = block.planning.periodSortedArr;
                allAmplitudes.forEach((amp, iAmp) => {
                    var totalNeed = 0;
                    var personAvailable = 0;
                    var wfArr = block.wfArr;

                    //calculer le total des personnes souhaitées à ce moment là
                    forOf(block.sectorEntArr, (sector) => {
                        var period = sector.planning.periodSortedArr[iAmp]
                        totalNeed += period.need
                    })

                    //pour toutes les personnes compter celles disponibles sur cette période
                    for (var i = wfArr.length; i--;) {
                        var wf = wfArr[i];
                        if(wf.planning.periodSortedArr[iAmp].openingType == "OUVERTURE") {
                            ++personAvailable
                        } else {
                            // console.log('magasin fermé')
                        }
                    }

                    if(!totalNeed) return //continue
                        var ratio = (personAvailable / totalNeed)

                    // on va maintenant affecter un besoin raisonnable
                    // au prorata des besoins qui a été choisit
                    forOf(block.sectorEntArr, (sector) => {
                        var period = sector.planning.periodSortedArr[iAmp]
                        period.need *= ratio
                    })
                })
            })
        }
        // sortedArrFunc.lowerBound(planning.periodSortedArr,)
        // on place des intervention sans affectation, juste pour dire qu'il faut les affecter
        createInterventionFromPlanning() {
            var m = this;
            var lastPerson = null;
            m.forAllWfOfAllBlock((person) => {
                controls_enabled(()=>{
                    if(person == lastPerson){
                        my_throw( 'cette personne est plusieurs fois présente !!')
                    }
                })
                lastPerson = person
                var planning = person.planning
                person.initAllInterv(planning.periodSortedArr,person)
            })
        }

        //ancienne version
        schedulePlanning() { //@@
            var m = this;
            var id = 0;
            m.blockArr.forEach((block) => { // pour chaque entité non secteur
                //prendre un planning au hazard parmis nos secteurs, vu qu'il ont les mêmes amplitudes;
                var amplitudeArr = block.planning.periodSortedArr;
                for (var i = 0; i < amplitudeArr.length; ++i) { //pour chaque demis-heure
                    var wfArr = block.wfArr.slice(); //clone
                    RandomGenerator.shuffle(wfArr);
                    RandomGenerator.shuffle(block.sectorEntArr);
                    sectorLoop: for (var k = block.sectorEntArr.length; k--;) { //pour chaque secteur de cette entité
                        var sctr = block.sectorEntArr[k];
                        var periodSector = sctr.planning.periodSortedArr[i];
                        if(!periodSector || "FERMETURE" == periodSector.openingType) continue;

                        // on trie de façon à avoir ceux qui n'ont pas été beaucoup affecté à ce secteur à la fin
                        wfArr.sort((a, b) => {
                            var nbMinuteA = a.personSectorMap[sctr.id].nb_minu_worked
                            var nbMinuteB = b.personSectorMap[sctr.id].nb_minu_worked
                            if(nbMinuteA == nbMinuteB) {
                                return a.wfId - b.wfId;
                            }
                            return nbMinuteB - nbMinuteA // décroisant
                        })


                        'debug';
                        for (var iLastWf = wfArr.length; iLastWf--;) { //pour chaque personne pas encore affecté durant cette périore
                            if(periodSector.interventionMap.length >= Math.floor(periodSector.need)) {
                                break; //ce secteur est plein pour cette périod on passe au suivant
                            }
                            var wf = wfArr.pop(); //prendre celui qui a été le moins affecté à ce secteur
                            var inter = new Intervention({
                                startMinute: periodSector.startMinute,
                                endMinute: periodSector.startMinute + sctr.recommendedTime,
                                date: periodSector.date,
                                id: 'affectation' + Main.getUniqId(),
                                sector: sctr,
                                rules: {
                                    dontMoveExistingInterventionOrPause: true
                                },
                                person: wf,
                                main: m
                            }, m);
                            // inter.setPerson(wf,m)
                            try {
                                m.logPersonSectorBug();
                            } catch (e) {
                                my_throw( 'debug')
                            }
                        }
                    }
                }
            })
        }


        getAllIntervToFlush() {
            var m = this
            var d = (new Date).getMinutes()
            var f = () => {
                var intervs = [];
                m.forAllWfOfAllBlock((wf) => {
                    wf.intervsRacines.forEach(function(racine) {
                        for (var inter = racine; inter; inter = inter.next) {
                            intervs.push({
                                id: 'pos: ' + intervs.length + ', minute:' + d + ',interId:' + inter.id + /*', idebug:' + inter._debug_iagp.iDebug */ ', addIntervDebug:' + inter.addIntervDebug + ', updateDebugId:' + inter.updateDebugId,
                                startDate: inter.listInfo.date,
                                endHour: Main.formatMinute(inter.endMinute),
                                startHour: Main.formatMinute(inter.startMinute),
                                wfId: inter.person.wfId,
                                sectorId: inter.sector ? inter.sector.id : 0 ,
                                //on doit retourner même les intervention qu'il ne faut pas flush pour la mise à jour des amplitudes
                                to_flush: inter.get_real_sector() && (!inter.protected || inter.fakeProtected) && !(inter.is_pause()) ? true : false
                            });
                        }
                    })
                })
                return JSON.stringify(intervs);
            }
            // pour v8js si on catch pas les erreurs parfois elle s'affiche pas
            if(v8js) {
                try {
                    return f()
                } catch (e) {}
            } else {
                return f()
            }
        }



        reportProblem() {
            var m = this;
            m.forAllSectorOfAllBlock((sector) => {
                sector.parentEntity.planning.periodSortedArr.forEach((amp, iAmp) => {
                    var period = sector.planning.periodSortedArr[iAmp]
                    if(!period) return; //continue
                    // on affiche les periode où le besoin n'est pas remplit
                    if(Math.floor(period.need) > period.interventionMap.length) {
                        // console.log(sector)
                        // console.log(period)
                        // console.log(period.need)
                        // console.log(period.interventionMap.length)
                        // console.log('')
                    }
                })
            })

            //on affiche le total de la personne, de plusieur manière différente:
            // nombre d'heure à faire dans la semaine
            // somme des interventions de la personne
            // somme des interventions de chaque personne secteur

            m.blockArr.forEach((block) => { // pour chaque entité devant affecter à des secteurs
                //on prend un planning d'un secteur au hazard car ils ont les mêmes découpage (périodes)
                block.wfArr.forEach((wf) => { // pour chaque personne de cette entité
                    // console.log('nbMinuteToDoTotal' + wf.nbMinuteToDoTotal)

                    var nbMinuteMegaMinute = 0;
                    var nbMinute2 = 0;
                    forOf(wf.intervsRacines, (interv) => {
                        nbMinuteMegaMinute += interv.listInfo.max_end_day - interv.listInfo.min_start_day
                        for (var next = interv; next; next = next.next) {
                            nbMinute2 += next.endMinute - next.startMinute + next.unplaced_time()
                        }
                    })
                    if(nbMinuteMegaMinute != nbMinute2){
                        console.error("nbMinuteTotal intervention megaMinute " + nbMinuteMegaMinute)
                        console.error("nbMinuteTotal intervention " + nbMinute2)
                    }

                    var nbMinute3 = 0;
                    forOf(wf.personSectorMap, (pSector) => {
                        nbMinute3 += pSector.nb_minu_worked
                    })
                    if(nbMinute3 != wf.nb_minu_worked){
                        console.error('minutes worked')
                    }
                    // console.log("nbMinute personSector " + nbMinute3)
                })
            })
        }

        logPersonSectorBug(bool, wfId, date) {
            controls_enabled(()=>{
                var m = this;
                if(wfId instanceof Array){
                    var all_wfs = wfId
                }else if(wfId instanceof Person){
                    var all_wfs = [wfId]
                }else{
                    var all_wfs = wfId ? [m.wfMap[wfId]] : m.all_wfs
                }
                var all_wfs = array_create({from: all_wfs})
                all_wfs.forEach((wf)=>{
                // m.blockArr.forEach((block) => { // pour chaque entité devant affecter à des secteurs
                //     block.wfArr.forEach((wf) => { // pour chaque personne de cette entité
                        // console.iidebug[33] = idnext('33')
                        var str = ''
                    // var deb = idnext('deb' )
                    var nbMinute2 = 0;
                    var intervId = array_create({})
                    forOf(wf.intervsRacines, (interv) => {
                        if(date && interv.date != date) return
                            var minu_in_day = 0;
                        for (var next = interv; next; next = next.next) {
                            // if(next.sector) {
                                //  console.log(next);
                                if(next.is_counted_in_worktime()) {
                                    str += '| ' + interv.id + ' = ' + next.startMinute + ' - ' + next.endMinute
                                    if(next.unplaced_time()){
                                        // debugger
                                    }
                                    var inter_time = next.endMinute - next.startMinute + next.unplaced_time()
                                    if(inter_time != next.finalTime.total_time){
                                        my_throw( "Le temps final enregistré pour cette intervention n'est pas le même que celui calculé")
                                    }
                                    minu_in_day += inter_time
                                    intervId.push(next.id)
                                }
                            // }
                        }
                        nbMinute2 += minu_in_day
                        controls_enabled(()=>{
                            if(minu_in_day != wf.listInfo_by_date[interv.date].worked){
                                Debug.logDayWf(wf,date)
                                my_throw( "il y a une incohérence dans les heures pour la personne " + wf.wfId + " le " + interv.date)
                            }
                        })
                    })
                    if(!date){
                        var nbMinute3 = 0;
                        var intervId2 = array_create({})
                        forOf(wf.personSectorMap, (pSector) => {
                            forOf(pSector.interventionMap, (inter) => {
                                if(inter.is_counted_in_worktime()) {
                                    intervId2.push(inter.id)
                                }
                            })
                            nbMinute3 += pSector.nb_minu_worked
                        })
                    }

                    var nb_minute4 = 0;
                    wf.listInfo_sorted.forEach(function(li){
                        if(date && date != li.date) return;
                        nb_minute4 += li.worked || 0
                    })

                    var sortF = (a, b) => {
                        a += ''
                        b += ''
                        return a.length > b.length ? 1 :
                        a.length < b.length ? -1 :
                        a > b ? 1 :
                        -1
                    }
                    if(bool) {
                        if(!wfId || wfId == wf.wfId) {
                            console.log(intervId.sort(sortF))
                            console.log(intervId2.sort(sortF))
                            console.log(nbMinute3)
                            console.log(str)
                        }
                    }
                    var s1 = JSON.stringify(intervId.sort(sortF))

                    var set_minu = new Set([nbMinute2,nb_minute4])
                    if(!date){
                        var s2 = JSON.stringify(intervId2.sort(sortF))
                        set_minu.add(wf.nb_minu_worked).add(nbMinute3)
                    }

                    controls_enabled(()=>{
                        if(set_minu.size > 1 || !date && s1 != s2) {
                            console.log(intervId.sort(sortF))
                            !date && console.log(intervId2.sort(sortF))
                            console.error("difference d'heure" + nbMinute3 + ' ' + nbMinute2 + ' ' + wf.nb_minu_worked + nb_minute4 + ' ' + 'personne ' + wf.wfId)
                            if(!wfId) {
                                Debug.logDayWf(wf)
                                my_throw( '')
                            }
                        } else {
                            //console.log("no difference d'heure" + nbMinute3 + ' ' + nbMinute2 + 'personne' + wf.wfId)
                        }
                    })
                })
                m.logError_total_info_amp()
            }
        )
    }

    logError_total_info_amp() {
        var m = this;
        m.total_info_amp.forEach(function(amp) {
            var have = 0;
            amp.periods.forEach(function(per) {
                have += per.have
            })
            controls_enabled(()=>{ 
                if(amp.have != have) {
                    console.error(amp, have)
                    my_throw( 'error on period have')
                }
            })
        })
    }
    getPosArrInArray(date, startMinute, endMinute) {
        console.iidebug[23] = idnext('23')
        // if(console.iidebug[23] == 92){
        ////////////// ////debugger;
        // }
        var diffDayInMinute = (Date.parse(date) - Date.parse(conf.planifStartDate)) / fl(60 * 1000);
        controls_enabled(()=>{
            if(diffDayInMinute < 0) {
                my_throw( "diffDayInMinute doit être positif")
            }
        })
        var startPos = Math.ceil((startMinute + diffDayInMinute) / conf.minTime)
        var endPos = Math.floor((endMinute + diffDayInMinute) / conf.minTime)
        var posArr = []
        controls_enabled(()=>{
            if(startPos != ~~startPos) {
                my_throw( 'not an integer')
            }
        })
        var diffTime = endMinute - startMinute;
        for (var i = startPos; i < endPos; ++i) {
            posArr.push(i)
        }
        return posArr;
    }

    getDataFromDataBase(params) {
            var templateWfPeriods = []
            var m = this;
            var ret = {};
            ret.idEntArr = [];
            ret.entArr = [];


            ret.wfMap = {};
            ret.wfArr = []
            ret.sectorMap = {};
            ret.blockMap = {}
            //console.error(params.entityArr)
            forOf(params.entityArr, (entity) => {
                ret.blockMap[entity.ent_id] = new Block({
                    id: entity.ent_id,
                    type: 'ENTITY',
                    wfArr: [],
                    sectorEntArr: []
                })
            })

            // var block = ret.blockMap[403];
            // var ids = [1175,1176,1177]
            // var minTimeSector = [1.5,1.5,1.5,1.5]
            // var recommendedTime = [1.5,1.5,1.5,1.5]
            forOf(params.sectorArr, (entity) => {
                ret.sectorMap[entity.ent_id] = new Sector({
                    is_real_sector: true,
                    id: entity.ent_id,
                    name: 'sector ' + entity.ent_id,
                    minTimeSector: entity.minTimeSector,
                    maxTimeSector: entity.maxTimeSector,
                    recommendedTime: entity.recommendedTime,
                    parentEntity: ret.blockMap[entity.parent_ent_id],
                    main: m
                })
                ret.blockMap[entity.parent_ent_id].sectorEntArr.push(ret.sectorMap[entity.ent_id])
                ret.entArr.push(ret.sectorMap[entity.ent_id]);
            })

            //permet de remplir un planning par demi-heure pour que les magasin
            //et les personne ait les même demi-heure à la même position
            var fillPlanning = (arr, idName, PeriodClass, totalTime, callback) => {
                var periodArrById = {}
                var is_planning_not_empty = false
                for (var i = 0; i < arr.length; ++i) { //7 personnes
                    var obj = arr[i];
                    var id = obj[idName]
                    // créer un planning sans trou par paquet de mintime
                    // pour chaque jour
                    periodArrById[id] = periodArrById[id] || []
                    if(!obj.startHour) continue;
                    var startMinute = Main.stringToMinute(obj.startHour)
                    var endMinute = Main.stringToMinute(obj.endHour)
                    // maxendMinute = Math.max(endMinute,maxendMinute)
                    // minstartMinute = Math.min(startMinute,minstartMinute)
                    var date = obj.startDate || obj.dateDayFormated
                    var poses = m.getPosArrInArray(date, startMinute, endMinute)
                    totalTime[id] = fl(totalTime[id]) + (endMinute - startMinute)
                    poses.forEach((pos, k) => {
                        controls_enabled(()=>{
                            if(pos != ~~pos) {
                                var deb = console.iidebug[23];
                                my_throw( 'not integer')
                            }
                        })
                        // on décompose en paquet de demis heure et on ajoute à la bonne position
                        var stMinute = startMinute + k * conf.minTime
                        var d = startMinute + (k + 1) * conf.minTime
                        periodArrById[id][pos] = new PeriodClass({
                            dateKey: Main.getDateKey(date, stMinute, d),
                            date: date,
                            startMinute: stMinute,
                            endMinute: d,
                            openingType: 'OUVERTURE', //only for sector
                            need: obj.staffSize, //only for sector
                            max_need: obj.max_need, //only for sector
                            min_need: obj.min_need, //only for sector ignore for person
                            main: m
                        })
                    })
                    is_planning_not_empty = true
                }
                forOf(periodArrById, callback)
                if(!is_planning_not_empty) {
                    console.warn('le planning est vide')
                }
            }
            var totalTimePers = {}
            var patchAntiTrou = false;

            function antiTrou(periodArrById) {
                if(patchAntiTrou) {
                    var lg = periodArrById.length
                    var arr2 = [];
                    var keys = Object.keys(periodArrById);
                    keys.forEach(function(key) {
                        arr2.push(periodArrById[key]);
                        delete periodArrById[key];
                    })
                    arr2.sort(function(a, b) {
                        return a.dateKey > b.dateKey ? 1 : -1;
                    })
                    periodArrById.length = 0;
                    [].push.apply(periodArrById, arr2)
                }
            }
            var wf_pos = 0;
            fillPlanning(params.periodsPerson, 'wfId', PeriodPerson, totalTimePers, (periodArrById, wfId) => {
                antiTrou(periodArrById);
                var wf = params.wfs[i];
                ret.wfMap[wfId] = new Person({
                    wfId: wfId,
                    name: 'person' + wfId,
                    nbMinuteToDoTotal: totalTimePers[wfId],
                    planning: new PlanningPerson({
                        periodSortedArr: periodArrById
                    }, m),
                    main: m
                })

                ret.wfArr.push(ret.wfMap[wfId])
            })
            // m.person_by_rotation_zscore = new SortedDoubleCircularLinkedList(ret.wfArr,(wf1,wf2) => {
            //     return wf2.rotation_zscore - wf1.rotation_zscore; // le plus grand au début, le plus grand = le pire le but est de réduire
            // })
            // m.person_by_rotation_zscore.someCell((cell)=>{
            //     cell.elm.cell_in_rotation_linked_list = cell
            // })
            //supprimer les worksfor sans possibilité d'affectation et recopier les attributs des worksfors
            for (var i = params.wfs.length; i--;) {
                var wf = params.wfs[i];
                var wf2 = ret.wfMap[wf.id]
                if(!wf2) { //car pas de planning
                    delete ret.wfMap[wf.id]
                    params.wfs.slice(i, 1)
                    continue;
                }
                wf2.nbMinutesMin = wf.minutes_to_place
                wf2.next_monday_not_worked =  wf.next_monday_not_worked,
                wf2.previous_sunday_not_worked =  wf.previous_sunday_not_worked,
                wf2.next_week_with_consecutive_abs =  wf.next_week_with_consecutive_abs,
                wf2.previous_week_with_consecutive_abs =  wf.previous_week_with_consecutive_abs,
                wf2.nb_days_by_week = wf.nb_days_by_week
                ret.blockMap[wf.entity_id].wfArr.push(wf2)
            }


            //récupérer la hierarchies des entreprise
            var totalTimeSect = {}
            fillPlanning(params.needsByAmpl, 'idEnt', PeriodNeed, totalTimeSect, (periodSortedArr, idSector) => {
                antiTrou(periodSortedArr);
                ret.sectorMap[idSector].planning = new PlanningNeedForEntity({
                    periodSortedArr: periodSortedArr,
                    totalMinuteOpened: totalTimeSect[idSector]
                }, m)
            })

            //on enlève les trous des planning


            // maintenant on va faire en sorte que le planning entreprise corresponde avec le planning person
            // en remplissant avec des besoins de 0 personne de 0 à 24h pour chaque jour
            var nothingTodo = false;
            var fillBlankCase = (map, PeriodClass) => {
                forOf(map, (obj) => {
                    var startDate = new Date(conf.planifStartDate);
                    var endDate = new Date(conf.planifEndDate);
                    var k = -1;
                    for (var d = startDate; d <= endDate; d.setDate(d.getDate() + 1)) {
                        for (var start_minute = 0; start_minute < 24 * H; start_minute += conf.displayMinTime) {
                            ++k;
                            if(!obj.planning) {
                                nothingTodo = true
                            }
                            if(!obj.planning || !obj.planning.periodSortedArr || obj.planning.periodSortedArr[k]) {
                                continue; //la case est déjà remplit on y touche pas
                            }
                            var date = Main.format(d);
                            var d2 = start_minute + conf.displayMinTime
                            var dateKey = Main.getDateKey(date, start_minute, d2)
                            obj.planning.periodSortedArr[k] = new PeriodClass({
                                dateKey: dateKey,
                                date: date,
                                startMinute: start_minute,
                                endMinute: d2,
                                need: 0,
                                openingType: 'FERMETURE',
                                main: m
                            })
                        }
                    }
                })
            }
            fillBlankCase(ret.sectorMap, PeriodNeed)
            fillBlankCase(ret.wfMap, PeriodPerson)
            if(nothingTodo) {
                return {
                    nothingTodo: true
                }
            }



            //var periodForSector =  {}
            // en fonction des besoins par secteur remplir les besoins
            // params.needsByAmpl.forEach(function(missAmp){
            //  var date = missAmp.dateDayFormated
            //  var startMinute = Main.stringToMinute(missAmp.startMinute)
            //  var endMinute = Main.stringToMinute(missAmp.endMinute)
            //  periodForSector[missAmp.idEnt] = periodForSector[missAmp.idEnt] || []
            //  m.getPosArrInArray(date,startMinute,endMinute).forEach((pos,k)=>{
            //      var  stMinute = startMinute + k * conf.displayMinTime
            //      var d = startMinute +  (k+1) * conf.displayMinTime
            //      periodForSector[missAmp.idEnt][pos] = new PeriodNeed({
            //          dateKey: date + ' ' + Main.formatPeriod(stMinute,d),
            //          date: date,
            //          startMinute:startMinute,
            //          endMinute: endMinute
            //      })
            //  })
            // })
            // forOf(periodForSector,(periodSortedArr,idSector) => {
            //  ret.sectorMap[idSector].planning = new PlanningNeedForEntity({
            //      periodSortedArr: periodSortedArr
            //  },m)
            //  // var_dump(JSON.stringify(periodSortedArr.length));
            //})

            console.log(params.blockMap)
            ret.blockArr = []
            forOf(ret.blockMap, (block) => {
                ret.blockArr.push(block)
            })
            forOf(ret.blockMap, (block => {
                block.planning = Object.create(GeneralPlanning.prototype)
                // on considère que le planning de l'équipe qui affecte aux autres
                // est le même aux besoins près de ceux de ces secteurs qui seront ignoré
                var first;
                forOf(ret.sectorMap, (sec) => {
                    first = sec
                })
                if(!block.planning.periodSortedArr) {
                    block.planning.periodSortedArr = ret.sectorMap[first.id].planning.periodSortedArr
                }
            }))

            // ret.personSectorMap = {}
            // forOf(ret.sectorMap, (sector,idEnt) => {
            //     for(var j = sector.parentEntity.wfArr.length;j--;){
            //         var idWf = sector.parentEntity.wfArr[j].wfId;
            //         ret.wfMap[idWf].personSectorMap[idEnt] = ret.sectorMap[idEnt].personSectorMap[idWf] = ret.personSectorMap[idEnt + '_' + idWf] = new PersonSector({
            //             idEnt: +idEnt,
            //             wfId: idWf,
            //             wf: ret.wfMap[idWf],
            //             ent: ret.sectorMap[idEnt]
            //         })
            //     }
            // })
            //
            // person_sectors : {
            //     let psec_done = new Set;
            //     forOf(ret.sectorMap, (sector, idEnt) => {
            //         controls_enabled(()=>{
            //             if(psec_done.has(sector.personSectorMap)){
            //                 my_throw(`ce person secteur a déjà été traité`)
            //             }
            //         })
            //         psec_done.add(sector.personSectorMap)
            //         for (let j = sector.parentEntity.wfArr.length; j--;) {
            //             m.create_personSector(sector, sector.parentEntity.wfArr[j])
            //         }
            //     })
            // }


            // Récupérer les interventions déjà saisies
            ret.protectedIntervArr = []

            //À parcourir dans l'ordre car les abscences sont à la fin et gagne sur les interventions
            let absence_sector = new Sector({
                name: "absence",
                is_not_counted_in_worktime: false,//et oui l'absence compte dans le temps de travail
                id: 'fake_for_abs',
                name: 'fake_for_abs',
                parentEntity: null,
                main: m
            })

            ret.true_protected_informations = []
            params.minInterInfos.sort(function(a,b){
                //trier par personne puis jour puis heure
                if(a.wfId != b.wfId) return a.wfId - b.wfId
                    if(a.startDate > b.startDate){return 1}
                        if(b.startDate > a.startDate){return -1}
                            if(a.real_start_hour > b.real_end_hour){return 1}
                                if(a.real_start_hour < b.real_end_hour){return -1}
                                    return 0
                            })
            //on va fusionner les intervention qui se suivent
            const min_inter_infos = []
            let prev2;
            params.minInterInfos.forEach((inter,i)=>{
                if(!prev2 || prev2.person !== inter.person ||  inter.date !== prev2.date 
                    || prev2.real_end_hour !== inter.real_start_hour || inter.sectorId != prev2.sectorId){
                    prev2 = clone(inter,{})
                min_inter_infos.push(prev2)
            }else{
                prev2.real_end_hour = inter.real_end_hour
                prev2.endHour = inter.endHour
            }
        })
            min_inter_infos.forEach(function(minInter) {
                // if(!ret.sectorMap[minInter.sectorId]) {
                //     console.error(
                //         'secteur introuvable ' + minInter.sectorId +
                //         ', si on peut planifier ce secteur manuellement il devrait être accessible via les departments avec un manualOnly à false' +
                //         ' sinon ne pas ramener cette partie du planning et cette intervention')
                // }
                if(!ret.wfMap[minInter.wfId]) return;
                let sector = minInter.sectorId && ret.sectorMap[minInter.sectorId]
                if(minInter.descript == "is_absence"){
                    sector = absence_sector
                    var ijk = ret.protectedIntervArr.length
                    console.log(ret.protectedIntervArr.length)
                }

                let start_minute = Main.stringToMinute(minInter.startHour)
                ,end_minute = Main.stringToMinute(minInter.endHour)
                ,protected_reason = sector ? 'manager_forced' : 'dont_planif'

                let id_tpi = idnext('getDataFromDataBase_tpi')
                if(id_tpi == 7276){
                    // debugger
                }
                const true_protected_information = Object.create(Object.prototype, {
                    // foo is a regular 'value property'
                    start_minute: {
                        writable: false,configurable: false,enumerable:true,
                        value: start_minute
                    },
                    end_minute: {
                        writable: false,configurable: false,enumerable:true,
                        value: end_minute
                    },
                    date: {
                        writable: false,configurable: false,enumerable:true,
                        value: minInter.startDate,
                    },
                    time: {
                        writable: false,configurable: false,enumerable:true,
                        value: end_minute - start_minute
                    },
                    person: {
                        writable: false,configurable: false,enumerable:true,
                        value: ret.wfMap[minInter.wfId]
                    },
                    protected_reason: {
                        writable: false,configurable: false,enumerable:true,
                        value: protected_reason
                    },
                    id: {
                        writable: false,configurable: false,enumerable:true,
                        value: id_tpi
                    },
                    from: {
                        writable: false,configurable: false,enumerable:true,
                        value: 'getDataFromDataBase_tpi'
                    },
                    sector: {
                        writable: false,configurable: false,enumerable:true,
                        value: sector
                    }
                })
                // let true_protected_information = Object.create({
                //     start_minute,
                //     end_minute,
                //     date: minInter.startDate,
                //     time: end_minute - start_minute,
                //     sector,
                //     person: ret.wfMap[minInter.wfId],
                //     protected_reason,
                //     id: id_tpi,
                //     from: 
                // }

                ret.true_protected_informations.push(true_protected_information)
                ret.protectedIntervArr.push(new Intervention({
                    startMinute: /*lower(*/start_minute,/*),//upper(start_minute),*/
                    endMinute: /*upper(*/end_minute,/*),//lower(end_minute),*/
                    true_protected_information,
                    date: minInter.startDate,
                    id: minInter.id,
                    sector,
                    person: ret.wfMap[minInter.wfId],
                    dontAffectNow: true,
                    protected_reason
                }));
                if(minInter.descript == "is_absence"){
                    if(ret.protectedIntervArr[ijk].sector != absence_sector){
                        my_throw( 'erreur')
                    }else{
                        true_protected_information.debug_moi = true
                    }
                }
            })


            // ret.v2_allNeedsInfo = new v2_allNeedsInfo();
            var all_needs_info = []
            ret.blockArr.forEach(function(block) {
                block.sectorEntArr.forEach(function(sectorBlock) {;
                    [].push.apply(all_needs_info, sectorBlock.planning.periodSortedArr)
                    // ret.v2_allNeedsInfo.v2_addNeedsInfo(sectorBlock.planning.periodSortedArr)
                    var prev = null;
                    var periods = sectorBlock.planning.periodSortedArr
                    periods.forEach((period_sector, i) => {
                        period_sector.planning = sectorBlock.planning
                        period_sector.v2_sector = sectorBlock
                        period_sector.next_amp = periods[i+1] || null
                        period_sector.pos_in_periodSortedArr = i
                        prev = period_sector;
                    })
                    if(prev){
                        prev.next = null
                        prev.pos_in_periodSortedArr = sectorBlock.planning.periodSortedArr.length - 1
                    }
                })
            })

            ret.total_info_amp = []
            var lastAmp;
            all_needs_info.sort(function(amp1, amp2) {
                return amp1.dateKey > amp2.dateKey ? 1 : amp1.dateKey < amp2.dateKey ? -1 : 0
            }).forEach(function(amp1) {
                // on met en place un tableau total_info_amp qui contient des informations sur certaines plages horaires
                // quand une intervention est ajouter à une plage horaire cela finit par remonter juqu'à notre lastAmp
                // on connait ainsi le nombre de personne placées au total à une heure donnée ainsi que le besoin total
                // à une heure donnée dans need et have
                if(!lastAmp || amp1.dateKey !== lastAmp.dateKey) {
                    lastAmp = new v2_total_need(amp1)
                    ret.total_info_amp.push(lastAmp)
                }
                lastAmp.addAmp(amp1)
                // lastAmp = amp1
            })


            ret.all_wfs = [];
            ret.blockArr.forEach((block) => { // pour chaque entité devant affecter à des secteurs
                [].push.apply(ret.all_wfs, block.wfArr) // pour chaque personne de cette entité
            })

            if(!params.cpt_rotation_possible){
                console.error(`cpt_rotation_possible missing`)
                params.cpt_rotation_possible = {}
            }

            if(!params.cpt_rotation_done){
                console.error(`cpt_rotation_done missing`)
                params.cpt_rotation_done = {}
            }
            //on récupère les rotation possible
            forOf(params.cpt_rotation_possible,function(rotation_poss,wf_id){
                if(ret.wfMap[wf_id]) ret.wfMap[wf_id].rotation_possible = rotation_poss
            })

            //on récupère les rotations réelles
            forOf(params.cpt_rotation_done||{},function(rotation_done,wf_id){
                var wf = ret.wfMap[wf_id]
                if(wf){
                    if(wf.wfId == 10108){
                        // debugger
                    }
                    clone(rotation_done,wf.rotation)
                    clone(rotation_done,wf.rotation_initial)
                } 
            })
            ret.all_wfs.forEach((wf,iwf)=>{
                forOf(m.rotations,(rot,name)=>{
                    var rot =  wf.rotation
                    rot[name] = rot[name] || 0
                    wf.update_rotation_ratio(name)
                })
            })

            //on récupère les contraintes par jour pour chaque personne
            forOf(params.minutes_by_date,function(listInfo_by_date,wf_id){
                if(!ret.wfMap[wf_id]) return;
                var libd = ret.wfMap[wf_id].listInfo_by_date = {}
                forOf(listInfo_by_date,(obj,date)=>{
                    obj.from = 'getDataFromDataBase'
                    libd[date] = new ListInfo(obj)
                    libd[date].date = date
                })

            })
            return ret
        }
    }
    return Main
}