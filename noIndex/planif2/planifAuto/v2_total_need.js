/**
 * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
 */
'' + function(){
    /**
     * contient pour une heure donnée des periodes qui elle correspondent à une heure et une entité
     *
     */
     class v2_total_need extends PeriodNeed {
        constructor(o) {
            o.from_v2_total_need = true
            super(o)
            var m = this
            m.dateKey = o.dateKey
            m.date = o.date
            m.periods = [] // les periodes que l'on aggrège pour le need et le have sont sur la même heure et même journée
            m.periods_not_update = new MyMap({
                onAddRemove: function() {
                    if(!m.main) {
                        console.error('pas de main')
                    }
                    if("uniq1352" == this.id) {
                        //////debugger
                    }
                    var dd = console.iDebug
                    m.main.v2_total_needs_to_updtd.add(m)
                    m.is_score_updtd = 0
                    m.is_sum_score_updtd = 0
                    m.score_update_debug = console.iDebug
                    if(320210 == m.score_update_debug) {
                        //// ////debugger;
                    }
                }
            })
            m.need = 0, //ne surtout pas récupérer celui de o
                m.have = 0; //ne surtout pas récupérer celui de o
            // m.id = Main.getUniqId();
            m.startMinute = o.startMinute;
            m.endMinute = o.endMinute;
            m.min_need = 0;
            m.is_sum_score_updtd = false//prend le score secteur par secteur
            m.is_score_updtd = false;//fait un score qui ignore la répartition en plusieurs secteurs tout est sommé
            m._last_zscore = 0
        }

        /*
         * On ne peut pas juste recalculer la différence car les règles ont changé
         * il faut donc réinitialiser le tableau de changement avec toutes les amplitude
         *
         */
         set_zscore_as_false() {
            //toutes les périodes sont à recalculer quand on aura besoin du score
            var m = this;
            //m.periods_not_update.add_multiple(m.periods)
            m._v2_sum_zscore_on_period = 0
            m.is_score_updtd = false
            m.old_score = {}
            m.periods.forEach(function(per) {
                //pour chaque amplitude de secteur ouvert à cette heure ci réinitiliser le score
                //PeriodNeed@onAddRemove
                per.onAddRemove(0) //on n'ajoute pas et on supprime pas donc 0 mais ça va quand même forcer le recalcul du score.
                per.old_score = false // le score n'est plus à jour
            })
            controls_enabled(()=>{
              if(m.periods.length != m.periods_not_update.length){
                  my_throw( 'les tableaux devraient être de même taille')
              }  
          })
        }

        get have() {
            return this._have
        }
        set have(val) {
            controls_enabled(()=>{
                if(val < 0) my_throw( 'have_negatif')
            })
            this.have_debug = idnext('have_debug')
            return this._have = val
        }
        incHave(inc, amp) {
            var m = this;
            m.have += inc
            controls_enabled(()=>{
                if(m.have == 12){
                        // debugger
                    // console.warn("mais pk tous le monde est là")
                }
            })
            m.is_sum_score_updtd = false
            console.iidebug[99] = console.iDebug
            if(console.iidebug[99] == 55403) {
            ////debugger
        }
        m.periods_not_update.add(amp) // qui appelle onAddRemove
        controls_enabled(()=>{
            if(!m.main.v2_total_needs_to_updtd.has(amp.totalPeriod)) {
                my_throw( "pourquoi la totalNeed n'est pas dans les periodes à mettre à jour")
            }
        })
    }
    addAmp( /*PeriodNeed*/ period) {
        var m = this;
        m.periods.push(period)
        m.periods_not_update.add(period) // qui appelle onAddRemove
        controls_enabled(()=>{
            if(!m.main.v2_total_needs_to_updtd.has(this)) {
                my_throw( "pourquoi la totalNeed n'est pas dans les periodes à mettre à jour")
            }
        })
        period.totalPeriod = m
        m.need += period.need
        m.have += period.have
        m.min_need += period.min_need
    }


        /*
         * C'est le score pour une demi-heure donnée
         * Pour des raison de performances on ne recalcule que les changement
         * sur les amplitude pour un secteur qui ont changé, elle sont
         * dans periods_not_update
         */
         v2_sum_zscore_on_period(brut_force) {
            var m = this;
            console.iidebug[39] = idnext('39')
            if(3736648 == console.iidebug[39]) {
                //////debugger;
            }
            return m.v2_sum_zscore_on_period__and__attractivity(brut_force).zscore_period
        }

        /*
         * Plus attractivity est grand et plus il faut affecter de personne
         * On ne peut pas se servir de v2_sum_zscore_on_period car il peut être grand pour pénaliser
         * le surreffectif comme il peut être grand car il n'y a pas assez de monde
         *
         * On a donc v2_attractivity pour attirer du monde et v2_sum_zscore_on_period qui dit si on a améliorer (en réduisant)
         * le score
         */
         v2_sum_attractivity_on_period(brut_force) {
            var m = this;
            return m.v2_sum_zscore_on_period__and__attractivity(brut_force).attractivity
        }

        v2_sum_zscore_on_period__and__attractivity(brut_force) {
            var m = this
            controls_enabled(()=>{
                // if(today_str !== '20181019'){
                    if(m.periods_not_update.length == 0 && !m.is_sum_score_updtd){
                        console.error("c'est étrange si le score est pas à jour il devrait y avoir une length")
                        my_throw( 'here')
                    }
                // }
            })
            if(brut_force) {
                m.periods_not_update.add_multiple(m.periods)
                m._v2_sum_zscore_on_period = null
            }

            //regarder que la length génère des erreur parfois
            controls_enabled(()=>{
                if(m.is_sum_score_updtd){
                    if(m.have != this._v2_sum_zscore_on_period.have){
                        my_throw( 'not logic')
                    }
                    return m._v2_sum_zscore_on_period
                }   
            })
            def(m, 'old_score', {})
            controls_enabled(()=>{
                if(!m._v2_sum_zscore_on_period && !m.periods_not_update.length){
                    my_throw( 'si on a pas cet objet on devrait avoir toutes les amplitudes à mettre à jour')
                }
            })
            var sum = m._v2_sum_zscore_on_period || {
                attractivity: 0,
                zscore_period: 0,
                have: m.have
            }
            // var sum_zscore = m._v2_sum_zscore_on_period |0;
            m.periods_not_update.some(function(per2) {
                if(console.iDebug == 18232) {
                    ////////////// ////debugger;
                }
                var score = per2.v2_zscore_and_attractivity()

                var old_score = m.old_score[per2.id] || {
                    attractivity: 0,
                    zscore_period: 0,
                }
                sum.zscore_period += Math.round(score.zscore_period - old_score.zscore_period)
                sum.attractivity += Math.round(score.attractivity - old_score.attractivity)
                m.old_score[per2.id] = score
                controls_enabled(()=>{
                    if(Number.isNaN(sum.zscore_period) || Number.isNaN(sum.attractivity)) {
                        console.error('NaN');
                        my_throw("NAN_v2_sum_zscore_on_period__and__attractivity")
                    }
                })
            })
            m.is_sum_score_updtd = true
            m.periods_not_update.reset()
            sum.have = m.have
            return m._v2_sum_zscore_on_period = sum
        }

        set _v2_sum_zscore_on_period(val) {
            var m = this;
            if(!m.is_sum_score_updtd){
                m.is_sum_score_updtd = false
            }
            controls_enabled(()=>{
                if(val == null && !m.null_autorised) {
                    my_throw( 'forbidden val')
                }
            })
            m.__v2_sum_zscore_on_period = val
        }
        get _v2_sum_zscore_on_period() {
            return this.__v2_sum_zscore_on_period
        }
        set is_sum_score_updtd(val){
            this._is_sum_score_updtd = val
            if(val == 60583){
                //debugger;
            }
            this._is_sum_score_updtd_debug = idnext('no_name15');
            if(this._is_sum_score_updtd_debug == 60583){
                //debugger;
            }
        }
        get is_sum_score_updtd(){
            return this._is_sum_score_updtd
        }
    }
    return v2_total_need
}