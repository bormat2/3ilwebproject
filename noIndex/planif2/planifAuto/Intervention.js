'' + function(){
	/**
	 * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
	 */
	var allow_keys_intervention = new Set([
	    'startMinute',
	    'racine',
	    'before_restore',
	    'protected',
	    'dontAffectNow',
	    'addIntervDebug',
	    'date',
	    'endMinute',
	    'id',
	    'next',
	    'canFail',
	    'prev',
	    '_next',
	    '_prev',
	    "listInfo",
	    "hasBeenRemoved",
	    "sector",
	    "protected_reason",
	    "before_add",
	    "finalTime",
	    "remove_during_iiDebugInter",
	    "need_update",
	    "updateDebugId",
	    "type",
	    "old_inter",
	    "debug_origin",
	    // "_debug_iagp",
	    "iDebug_create",
	    "iDebug",
	    "rules",
	    "end_minute_too_late",
	    "error_code",
	    "_debug_ai",
	    "protectedClone",
	    "_debug_aiRem",
	    "main", 
	    "startMinute",
	    "endMinute",
	    "_listInfo",
	    "_id",
	    "hasBeenInsert",
	    "end_hour",
	    "megaForcedInsert",
	    "fakeProtected",
	    "debugInfo",
	    "ori_addIntervDebug",
	    "last_update_debug_id",
	    "true_protected_information"
	    ]
	    );
	// window.keys4 = {};
	/* @interventionClass */
	class Intervention {
	    constructor(o) {
	        this.true_protected_information = false
	        var m = this
		    controls_enabled(()=>{
	            m = proxy_creator(this,allow_keys_intervention,{
	                setter_check:(prop,val)=>{
	                    switch(prop){
	                        case "startMinute": case "endMinute" :
	                        	if(!Debug.dont_check_start_end_logic && this.startMinute >= this.endMinute){
	                        		my_throw('startMinute doit être inférieur à endMinute')
	                        	}
	                        	if(this.startMinute > 24 * 60){
	                        		my_throw('Trop grand startMinute')
	                        	}
	                        	return true
	                        break;case 'protected_reason':
	                        if(val == 'manager_forced' && !this.true_protected_information){
	                            my_throw( 'les deux propriétés doivent être présente ou bien aucune')
	                        }
	                        break;case 'hasBeenInsert':
	                        if(val === false && m.addIntervDebug == 175874){
	                            debugger
	                        }
	                    }
	                    return true
	                },
	                deleteProperty(target, prop) {
	                    if(prop == 'true_protected_information'){
	                        if(target.protected_reason == 'manager_forced'){
	                            my_throw( "il faut d'abord enlever protected_reason")
	                        }
	                    }
	                    delete target[prop];
	                    return true
	                }
	            })
		    })
	        if(o) {
	            if( !(o instanceof Intervention) && o.from_clone){
	                return m
	            }
	            if(o.racine) {
	                m.listInfo = {
	                    min_start_day: o.startMinute,
	                    max_end_day: o.endMinute,
	                    date: o.date || '2000-01-01',
	                    from: 'Intervention constructor'
	                }
	            }
	            delete o.racine

	            var keys = Object.keys(o);
	            var lg = keys.length;
	            for (var i = 0; i < lg; i++) {
	                var key = keys[i];
	                if(key == 'protected_reason') continue;
	                m[key] = o[key];
	            }
	            if(o.protected_reason){
	                m.protected_reason = o.protected_reason
	            }
	            if(o.person && !o.dontAffectNow) {
	                o.person.addInterv(m, o.main)
	            }
	            controls_enabled(()=>{
	                if(o.rules && (!o.startMinute || !o.endMinute)) {
	                    my_throw( 'Une intervention sans heure !!! vraiment ?')
	                }
	            })
	            m.addIntervDebug = console.addIntervDebug = idnext('addIntervDebug')
	            if([75473].has(m.addIntervDebug)){
	                // debugger
	            }
	            if(m.id == 'CLONEuniq2776'){
	                //// debugger
	            }
	            if(!m.addIntervDebug) {
	                console.log('fd')
	            };
	        }
	        return m
	    }
	    //pour débugger c'est pratique
	    info(){
	        var wf = this;
	        return {
	            date: wf.date,
	            startMinute: wf.startMinute,
	            endMinute: wf.endMinute,
	        }
	    }
	    get_hours_pause_config(pause_instance){
	        const {person,date} = this;
	        return (pause_instance || this.sector.pause_instance).get_hours_config(date,person)
	    }
	    //cette information concerne la journée et non l'intervention
	    is_day_worked(){
	        return this.listInfo && this.listInfo.is_worked() ? true : false
	    }
	    /*
	    * parfois le temps d'une intervention compte plus que l'heure à laquel elle est vraiment placée
	    * c'est le cas pour les ouverture et fermeture
	    * 
	    */
	    unplaced_time(){
	        var m = this,p = this.person,t;


	        if(m.is_counted_in_worktime()){
	        	// une pause de 40 minute ne comptant pas dans le temps de travail
	        	// est en fait une pause de 1 heure comptant dans le temps de travail
	        	// avec "-40" minutes de unplaced_time quand on est au pas de 30 minutes
		        if(m.is_pause()){
		        	if(!m.sector.pause_instance.is_not_counted_in_worktime){
		        		return 0
		        	}
		        	let hc = this.get_hours_pause_config()
		        	if(hc.time != hc.real_time){
		        		//on retoune donc -40 (qui est négatif) pour une pause de 40 minutes qui est d'abord placé à une heure
		        		return - hc.real_time
		        	}
		        }
	            if(m.true_protected_information){
	                //une intervention protected peut avoir des quart d'heure donc on stoque sont 
	                //vrai temps dans time
	                let time = this.true_protected_information.time - this.time
	                return time
	            }
	            if(p && (t = p.main) && !t.is_test && !t.unplaced_time_disabled){
	                let time = 0;
	                if(m.listInfo.first_interv_sector === m && !m.listInfo.disabled_time_opening){
	                    if(p.opening_info({start_minute: m.startMinute,date: m.date}).is_supplement_time()){
	                        time += p.time_opening;
	                    }
	                }
	                if(m.listInfo.last_interv_sector === m && !m.listInfo.disabled_time_closing){
	                    if(p.closing_info({end_minute: m.endMinute,date: m.date}).is_supplement_time()){
	                        time += p.time_opening;
	                    }
	                }
	                return time
	            }
	        }
	        return 0
	    }

	    put_in_main_sector(opt1 = {}){
	        var m = this
	        var new_inter = m.v2_clone()
	        //supprimer pour ne plus dépasser le max_need
	        var opt = {}
	        if(opt1.even_if_protected_and_force_insertion){
	        	opt.even_if_protected = true
	        }else{
	        	opt.even_if_fake_protected = true
	        }
	        m.reset_inter(opt)
	        new_inter.sector = m.person.main.blockArr[0].sectorEntArr[0]
	        if(opt1.even_if_protected_and_force_insertion){
	        	new_inter.rules = {
	        	    megaForcedInsert: true
	        	}
	        }else{
		        new_inter.rules = {
		            safe_insertion: true
		        }
	        }
	        m.person.addInterv(new_inter)
	    }
	    get_firstORlast_interv(mega, f_l, dir) {
	        var m = this,
	        lInfo = m.listInfo
	        if(lInfo && lInfo[mega]) {
	            return lInfo[f_l] || m
	        }
	        //quand on fait les test listInfo n'est pas forcément rempli
	        // il n'est pas rempli non plus si on l'a pas encore inséré
	        var prevORnext = m.hasBeenInsert ? m : m.person.get_first_interv_at_date(m.date)
	        if(prevORnext){
	            while (prevORnext[dir]) {
	                prevORnext = prevORnext[dir]
	            }
	        }
	        return prevORnext
	    }

	    get first_interv() {
	        return this.get_firstORlast_interv('min_start_day', 'first_interv', 'prev')
	    }
	    get last_interv() {
	        return this.get_firstORlast_interv('max_end_day', 'last_interv', 'next')
	    }

	    get id() {
	        return this._id || (this._id = Main.getUniqId())
	    }
	    set id(id) {
	        this._id = id
	    }

	    set listInfo(o){
	        controls_enabled(()=>{
	            if(!(o instanceof ListInfo) && !o.from){
	                my_throw( 'o.from est obligatoire')
	            }
	        })

	        this._listInfo = (o instanceof ListInfo) ? o : new ListInfo(o)
	        controls_enabled(()=>{
	            if(!this.listInfo.id){
	               my_throw( 'how ?')
	           }
	       })
	    }

	    get listInfo(){
	        return this._listInfo
	    }

	    get first_minu_worked(){
	        return this.first_interv_sector && this.first_interv_sector.startMinute
	    }

	    get last_minu_worked(){
	        return this.last_interv_sector && this.last_interv_sector.endMinute
	    }



	    get_listInfo_by_date(){
	        if(this.person.main.is_test){
	            return {
	                min: 0 * H,
	                max: 24 * H,
	                worked: 7 * H
	            }
	        }
	        return this.person.listInfo_by_date[this.date]
	    }
	    // get_v2_firstORlast_interv_with_sector(prop_name) {
	    //     var m = this;
	    //     var ii = m.listInfo[prop_name]
	    //     if(m.listInfo.first_interv === undefined){//comme il n'a jamais été inséré ou tenté d'être inséré son listeInfo est faux
	    //         var ii = m.person.get_first_interv_at_date(m.date)
	    //         ii = ii.listInfo[prop_name]
	    //     }
	    //     if(ii || !ii && ii !== 0) {
	    //         if(ii && !(ii.hasBeenInsert && ii.get_real_sector())) {
	    //             my_throw( "si l'intervention n'est plus un secteur ou bien a été supprimé" +)
	    //             "la valeur 0 aurait du être mise dans first_interv_sector"
	    //         }
	    //         return ii
	    //     } else {
	    //         //si l'intervention a été supprimé ou n'a plus de secteur
	    //         //alors il faut retrouver le dernier secteur
	    //         if(prop_name == '_last_interv_sector') {
	    //             var ii = m.last_interv,
	    //             dir = 'prev'
	    //         } else {
	    //             var ii = m.first_interv,
	    //             dir = 'next'
	    //         }
	    //         while (ii) {
	    //             if(ii.get_real_sector()) {
	    //                 break
	    //             }
	    //             ii = ii[dir]
	    //         }
	    //         return m[prop_name] = ii //donc undefined si il y en a pas
	    //     }
	    // }

	    get_inserted_listInfo(){
	        var m = this
	        if(!m.listInfo.first_interv === undefined){
	            return m.person.get_first_interv_at_date(m.date).listInfo
	        }else{
	            return m.listInfo
	        }
	    }
	    get last_interv_sector() {
	        return this.get_inserted_listInfo().last_interv_sector
	    }

	    get first_interv_sector() {
	        return this.get_inserted_listInfo().first_interv_sector
	    }

	    set first_interv_sector(val) {
	        this.listInfo.first_interv_sector = val
	        // if(this.listInfo.id == "uniq1404") {
	        //     if(console.iDebug == 11417) {
	                //////////debugger;
	        //     }
	        //     this.listInfo.updates = this.listInfo.updates || []
	        //     this.listInfo.updates.push(console.iDebug)
	        // }
	        // if(val && !val.hasBeenInsert) {
	        //     my_throw( "cette inter n'est pas inséré")
	        // }
	        // this.listInfo._first_interv_sector = val
	    }
	    set last_interv_sector(val) {
	        this.listInfo.last_interv_sector = val
	        // if(this.listInfo.id == "uniq1404") {
	        //     if(console.iDebug == 11417) {
	                //////////debugger;
	        //     }
	        //     this.listInfo.updates2 = this.listInfo.updates2 || []
	        //     this.listInfo.updates2.push(console.iDebug)
	        // }
	        // if(val && !val.hasBeenInsert) {
	        //     my_throw( "cette inter n'est pas inséré")
	        // }
	        // this.listInfo._last_interv_sector = val
	    }

	    get next() {
	        return this._next
	    }
	    set next(val) {
	        controls_enabled(()=>{
	            if(this == val) {
	                my_throw( 'next can not be itself')
	            }
	        })
	        this._next = val
	    }

	    // si le secteur n'est pas une pause, une absence, ou un dont planif,
	    // c'est un vrai secteur
	    // participant au besoin quand la personne est dedans
	    get_real_sector() {
	        return this.sector && this.sector.is_real_sector && this.sector
	    }

	    //une pause fake protected peut être changé car elle n'est pas vraiment protégé vu
	    //que ce n'est pas l'utilisateur qui l'a mise
	    is_pause_fake_protected() {
	        return this.fakeProtected && this.is_pause()
	    }

	    is_pause() {
	        return !!(this.sector && this.sector.pause_instance)
	    }

	    is_day_ok(opt) {
	        var m = this;
	        if(!m.first_interv_sector) return true
	            var d = m.get_listInfo_by_date()
	        var is_start_and_end_day_ok = (is_between(m.first_minu_worked, d.min_start_day, d.max_end_day)) &&
	        (is_between(m.last_minu_worked, d.min_end_day, d.max_end_day))

	        return is_start_and_end_day_ok && (opt.ignore_working_time || d.min <= d.worked && d.max >= d.worked)
	    }

	    logDayWf(wf) {
	        this.person = wf
	        var first_interv = this.first_interv;
	        Debug.logDayWf(first_interv)
	        ////////debugger
	    }

	    //une pause ne participe pas au besoin car la personne ne travaille pas
	    participate_to_need() {
	        return !!this.get_real_sector()
	    }
	    reset_person() {
	        var lInfo = this.listInfo
	        this.listInfo = {
	            min_start_day: lInfo.startMinute,
	            max_end_day: lInfo.endMinute,
	            date: lInfo.date,
	            // id: Main.getUniqId()
	        }
	    }
	    // get_firstORlast_minute_where_need_is_absolute(f_l){
	    //     var inter = this,
	    //         person = inter.person,
	    //         main = person.main,
	    //         date = this.date,
	    //         first_minute_where_need_is_absolute
	    //     ;
	    //     if(!inter.sector) return;
	    //     main.getPosArrInArray(date,inter.startMinute,inter.endMinute)
	    //     var periods = inter.sector.planning.periodSortedArr
	    //     periods[f_l == 'first' ? 'some': 'reverse_some'](function(period){
	    //         if(period.min_need == period.max_need){
	    //             first_minute_where_need_is_absolute = period[f_l == 'first' ? 'startMinute' : 'endMinute']
	    //             return true
	    //         }
	    //     })
	    //     if(first_minute_where_need_is_absolute || first_minute_where_need_is_absolute === 0){
	    //         return first_minute_where_need_is_absolute;
	    //     }
	    //     return null
	    // }

	    //Ce qui est bien c'est que même sur une intervention supprimée ça va marcher car le liste info
	    //reste le même objet

	    free_the_pause() {
	        if(this.fakeProtected) this.protected = this.fakeProtected = false
	    }
		no_more_a_pause() {
		    var m = this;
		    m.free_the_pause()
		    m.type = null;
		}
	    //@depreciated
	    get_last_inter_with_sector_same_day() {
	        return this.last_interv_sector
	    }

	    //est ce que l'on a mis une pause de 1 heure mais qu'il faudra réduire plus tard
	    is_pause_that_need_to_be_reduced() {
	        if(this.is_pause() ){ //&& this.sector.pause_instance.does_pause_need_to_be_reduced())
	        	let hc = this.get_hours_pause_config()
	        	return hc.time != hc.real_time
	        }
	        return false
	    }

	    //@depreciated
	    get_first_inter_with_sector_same_day() {
	        return this.first_interv_sector
	    }

	    is_counted_in_worktime() {
	    	if(this.sector){
	    		if(this.sector && !this.sector.is_not_counted_in_worktime) return true

	    		// une pause de 40 minute ne comptant pas dans le temps de travail
	    		// est en fait une pause de 1 heure comptant dans le temps de travail
	    		// moins 40 minutes de unplaced_time quand on est au pas de 30 minutes
		    	if(this.is_pause()){
		    		let hc = this.get_hours_pause_config()
		    		return hc.time != hc.real_time
		    	}
	    	}
	        
	    }

	    get_personSector() {
	        var p, s, ps;
	        return !!((p = this.person) && (s = this.sector) && (ps = p.personSectorMap) && ps[s.id])
	    }


	    //is_min_time_respected
	    v2_respectMinTime() {
	        var m = this
	        if(m.is_pause() || !m.sector) return true
	            var start_end = m.getStartAndEndSameSector()
	        return (m.getMinTime() <= start_end.time)
	    }

	    time_possible_to_add() {
	        return this.sector.maxTimeSector - this.time
	    }


	    v3_serialize() {
	        var seria9 = this.v2_serialize()
	        seria9.dontAffectNow = true;
	        return seria9
	    }
	    v2_clone() {
	        var seria9 = this.v3_serialize()
	        var inter9 = new Intervention(seria9)
	        return inter9
	    }

	    /* déprotégé l'intervention courrant et ajouter une copie dans protectedIntervArr pour réinsertion */
	    unprotectIfProtected(protectedIntervArr) {
	        if(this.protected) {
	            var clone_to_reinsert = this.clone()
	            clone_to_reinsert.hasBeenInsert = false
	            clone_to_reinsert.id = 'protect' + Main.getUniqId();
	            protectedIntervArr.push(clone_to_reinsert)
	            this.protected = false
	            this.protectedClone = clone_to_reinsert
	            delete this.protected_reason
	            delete this.true_protected_information
	        }
	        return this
	    }

	    //la pause prend le secteur du précédent et s'il n'y en a pas du suivant
	    unPause(main) {
	        var m = this;
	        m.type = ''
	        // m.pauseClass = null
	        m.free_the_pause()
	        var adi_inter = this.try_other_sectors()
	        return adi_inter && adi_inter.has_been_insert
	    }

	    try_other_sectors(opt = {}) {
	        var m = this;
	        var wf = m.person
	        var main = wf.main
	        var wf = m.person
	        var seria = m.v2_serialize();
	        seria.dontAffectNow = true
	        seria.rules = opt.rules || {
	            safe_insertion: true
	        }
	        //tester tout les secteurs en commençant par les voisins de la pause
	        var sectors = [m.prev && m.prev.get_real_sector(), m.next && m.next.get_real_sector()]
	        main.blockArr[0].sectorEntArr.forEach((sector) => {
	            if(sector != m.sector && sector != sectors[0] && sector != sectors[1]) {
	                sectors.push(sector)
	            }
	        })
	        var inter,adi_inter
	        sectors.some((sector) => {
	            if(sector) {
	                seria.sector = sector
	                inter = new Intervention(seria)
	                adi_inter = wf.addInterv(inter)
	                return adi_inter.has_been_insert //stop "some" if true
	            }
	        })
	        return adi_inter || {}
	    }


	    remove_all_interv_of_day() {
	        var m = this.listInfo.first_interv || this.person.get_first_interv_at_date(this.date)
	        var time = 0;
	        var success = true
	        while (m.next) {
	            if(m.sector) {
	                time += m.time
	            }
	            if(!m.reset_inter()) {
	                success = false;
	                break;
	            }
	            m = m.next
	        }
	        return {
	            time_removed: time,
	            success_removed: success
	        }
	    }

	    getLastProtectedCloneOrOriginal() {
	        var inte = this
	        while (inte.protectedClone) {
	            inte = inte.protectedClone
	        }
	        return inte
	    }

	    get date() {
	        return this.listInfo && this.listInfo.date;
	    }

	    set date(s) {
	        controls_enabled(()=>{
	            if(!s) {
	                my_throw( 'not a date')
	            }
	        })
	        if(!this.listInfo) {
	            this.listInfo = {
	                from: 'set_date'
	            }
	        }
	        this.listInfo.date = s
	    }

	    get person() {
	        return this.listInfo && this.listInfo.person;
	    }

	    set person(s) {
	        controls_enabled(()=>{
	            if(s && !s[isProxy]){
	                my_throw( 'devrait être un proxy')
	            }
	        })
	        if(!this.listInfo) {
	            this.listInfo = {
	                from: 'setPerson'
	            }
	        }
	        this.listInfo.person = s
	    }

	    get time() {
	        return this.endMinute - this.startMinute
	    }

	    /* dir = 'next' or 'prev'*/
	    getMinOrMaxWithoutSector(dir) {
	        var inte = this[dir]
	        while (inte && !inte.sector) {
	            if(!inte[dir]) {
	                break
	            }
	            inte = inte[dir]
	        }
	        return inte || this
	    }

	    // dans le cas d'une pause qui a le next ou le prev qui sont plus petit que
	    // le temps minimal par secteur alors résoudre le problème en changeant le secteur
	    // pour le suivant ou le précédent
	    // ATTENTION si il n'arrive pas à résoudre le problème il enlève les interventions autours de la
	    // pause, cela est surrement du à un paramétrage incohérent du temps minimal.
	    solve_next_prev_too_small_for_pause(opt99 = {}) {
	    	if(conf.do_not_try_to_change_sector_around_pause){
	    		return {
	    			echec: true
	    		}
	    	}
	        var pause = this;
	        var echec = false
	        // var to_inc = opt99.include_this_amplitude;
	        ;
	        ['next', 'prev'].some((dir) => {
	            if(!pause[dir].v2_respectMinTime()) {
	                // var seria = pause[dir].v3_serialize()
	                // seria.sector = seria[dir] && seria[dir][dir].sector
	                var adi_prev = pause[dir].try_other_sectors()
	                if(!adi_prev.has_been_insert) {
	                    return echec = true
	                }
	            }
	        })
	        return {
	            echec
	        }
	    }
	    //if all problems are solved or if haven't problems return true else return false
	    solve_min_time_not_respected() {
	        var m = this;
	        if(m.v2_respectMinTime()) {
	            return true
	        }
	        var wf = this.person
	        controls_enabled(()=>{
	            if(!wf) {
	                my_throw( 'person missing')
	            }
	            if(m.startMinute == m.endMinute) {
	                my_throw( 'comment cet intervention existe elle sans temps')
	            }
	        })
	        if(!m.sector) {
	            return true; //le min est ok comme ce n'est pas un secteur
	            // my_throw( 'cette fonction est pour les interventions avec secteur')
	        }

	        if(conf.do_not_try_to_change_sector_for_mintime){
	        	return false
	        }
	        
	        var adi_inter = m.try_other_sectors({
	            rules: {
	                safe_insertion: true,
	                v2_break_if_min_time_not_ok: true //c'est le mintime de cette intervention que l'on va vérifier mais on doit vérifier manuellement celle avant et après
	            }
	        });
	        if(!adi_inter.has_been_insert) {
	            return false
	        }
	        var m = adi_inter.inter
	        var start_end = m.getStartAndEndSameSector()
	        var missingTime = m.getMinTime() - start_end.time
	        if(missingTime <= 0) return true;
	        controls_enabled(()=> {
	            my_throw( "elle n'aurait pas du être insérée comme elle n'a pas réglée le problème on a donc v2_break_if_min_time_not_ok qui ne marche pas")
	        })
	    }
	    clone() {
	        var m = this
	        var int = clone(m, new Intervention({from_clone:true}) /*Object.create(Intervention.prototype*/)
	        int.listInfo = {
	            date: m.listInfo.date,
	            min_start_day: m.listInfo.min_start_day,
	            max_end_day: m.listInfo.max_end_day,
	            from: 'clone_func'
	        }
	        // int.hasBeenInsert = false//le clone n'est pas inséré
	        int.date = m.listInfo.date
	        int.person = m.person
	        return int
	    }

	    v2_serialize() {
	        var interv = this;
	        var ret = {
	            date: interv.listInfo.date,
	            startMinute: interv.startMinute,
	            endMinute: interv.endMinute,
	            sector: interv.sector,
	            person: interv.listInfo.person,
	            type: interv.type
	        }
	        return ret;
	    }

	    cloneWithNewIdToInsert(intervToUpdate) {
	        var intervReplaceCloned = clone(this, new Intervention);
	        intervReplaceCloned.id = "CLONE" + Main.getUniqId()
	        // if('CLONEuniq2776' == )
	        intervReplaceCloned.addIntervDebug = idnext('addIntervDebug')
	        intervReplaceCloned.hasBeenInsert = false
	        intervReplaceCloned.hasBeenRemoved = false
	        return intervReplaceCloned
	    }
	    setCloneBetween(prev, next, intervToUpdate) {
	        return this.cloneWithNewIdToInsert().setBetween(prev, next)
	    }

	    setNext(intervNext) {
	        this.next = intervNext
	        intervNext.prev = this;
	    }

	    setBetween(prev, next) {
	        if(prev) {
	            prev.next = this
	        }
	        if(next) {
	            next.prev = this
	        }
	        this.prev = prev
	        this.next = next;
	        return this
	    }

	    //ne gère pas le décalage entre la startMinute du suivant et la endMinute du précédent
	    // va parcontre lier le précédent et le suivant ensemble
	    // @removeInter
	    remove(opt) {
	        controls_enabled(()=>{
	            if(!opt.i_will_preserve_the_integrity_of_last_inter_of_listInfo) {
	                my_throw( 'quand une intervention est supprimée il faut que le list info contienne' +
	                'toujours la première intervention et la dernière' +
	                'de même pour la première avec secteur et la dernière avec secteur' +
	                'vous devez donc promettre que vous aller le faire')
	            }
	        })
	        var m = this;
	        if(m.addIntervDebug == 10417) {
	            //////// ////debugger;
	        }
	        if(m.addIntervDebug)
	            m.hasBeenRemoved = true;
	        m.hasBeenInsert = false;
	        // il faut supprimer avec les heures de début et de fin qui on servit lors de l'insertion
	        var startMinute = m.startMinute;
	        var endMinute = m.endMinute;
	        if(m.finalTime) {
	            m.startMinute = m.finalTime.startMinute
	            m.endMinute = m.finalTime.endMinute
	            // if(m.listInfo && m.listInfo.person && m.listInfo.person.minutes_to_place){
	            //  m.listInfo.person.minutes_to_place -= m.endMinute - m.startMinute
	            // }
	        }

	        var next = m.next
	        var prev = m.prev
	        if(prev) prev.next = next
	            if(next) next.prev = prev
	                var main = m.listInfo.person && m.listInfo.person.main
	            if(main && !main.is_test) {
	                if(m.listInfo && m.listInfo.person) {
	                    delete m.listInfo.person.intervNotAffectedMap[m.id];
	                    m.forEachPeriod((period) => {
	                        period.interventionMap.remove(m);
	                    })
	                }
	            }
	            var person = m.listInfo && m.listInfo.person
	            if(m.get_personSector()) {
	                var personSector = m.sector.personSectorMap[person.wfId];
	            //main.personSectorMap[m.sector.id + '_' + m.listInfo.person.wfId];
	            controls_enabled(()=>{
	                if(!personSector) {
	                    my_throw( 'personSector secteur non existant')
	                    return;
	                }
	            })
	            personSector.removeInterv(m, person.main);
	        }

	        //on restore au cas où on souhaiterait la réinsérer
	        if(m.finalTime) {
	            m.endMinute = endMinute
	            m.startMinute = startMinute
	        }
	        // if(m == m.listInfo.last_interv){
	        //     m.listInfo.last_interv = 0 //0 = inconnue
	        // }
	        // if(m == m.listInfo.first_interv){
	        //     m.listInfo.first_interv = 0 //0 = inconnue
	        // }
	        this.remove_during_iiDebugInter = console.iiDebugInter
	        if(this.remove_during_iiDebugInter == 14880 && this.id == "CLONEuniq1642") {
	            ////////debugger
	        }
	        return this;
	    }

	    reset_inter(opt = {}) {
	        const m = this
	        const main = m.person.main
	        if(m.addIntervDebug == 175874){
	            debugger
	        }
	        var is_pause = m.is_pause()
	        let _continue = !m.protected || opt.even_if_protected
	        ||m.fakeProtected && (opt.even_if_fake_protected || is_pause && opt.even_if_pause_fake_protected)
	        if(!_continue){return false}
	            if(m.protected) {
	                m.protected = false
	                m.fakeProtected = false
	                m.protected_reason = false
	            }

	            if(is_pause) {
	                m.no_more_a_pause()
	            }
	            var next = m.next,
	            prev = m.prev
	        // main.dont_watch = true
	        m.next = m.prev = null; //comme cela il n'y aura pas d'impact sur les intervention qui entoure lors du remove
	        // main.dont_watch = false

	        var hasBeenInsert = m.hasBeenInsert
	        //on va bien préserver le last_inter et first_inter car on la remet à sa place après
	        //suppression
	        //par contre il faut dire que ce n'est pas le cas pour les secteurs
	        m.remove({
	            i_will_preserve_the_integrity_of_last_inter_of_listInfo: true
	        })
	        controls_enabled(()=>{
	            var date = m.date,wf = m.person
	            if(main && main.is_test == false && wf.rotationMaps[date].length && !wf.is_worked(date)){
	                my_throw( 'pourquoi il y a des éléments si la personne ne travaille pas ce samedi')
	            }
	        })
	        //pour cela il suffit de mettre la valeur 0
	        m.first_interv_sector = 0
	        m.last_interv_sector = 0
	        m.hasBeenInsert = hasBeenInsert
	        m.sector = null

	        m.next = next
	        m.prev = prev;
	        controls_enabled(()=>{
	            if(m.protected) {
	                my_throw( 'still protected')
	            }
	        })

	        const lInfo = m.listInfo
	        // if(lInfo.first_minu_worked){
	        //     let is_opening = m.person.opening_info({start_minute: lInfo.first_minu_worked,date: lInfo.date}).is()
	        //     if(is_opening){
	        //         if(lInfo.exact_opening !=  lInfo.first_interv_sector){
	        //             lInfo.exact_opening = lInfo.first_interv_sector
	        //             //à cause du quart d'heure qui est là que si c'est la première inter on doit recalculer le temps
	        //             lInfo.first_interv_sector.prepareToBeReinserted(main, {
	        //                 i_will_preserve_the_integrity_of_last_inter_of_listInfo: true
	        //             })
	        //             lInfo.first_interv_sector.insertAtGoodPlace(main)
	        //             controls_enabled(()=>{
	        //                 if(!lInfo.first_interv_sector.hasBeenInsert) my_throw( "la propriété n'est pas vrai")
	        //             })
	        //         }
	        //     }
	        //     let is_closing = m.person.closing_info({end_minute: lInfo.last_minu_worked,date: lInfo.date}).is()
	        //     if(is_closing){
	        //         if(lInfo.exact_closing != lInfo.last_interv_sector){
	        //             lInfo.exact_closing = lInfo.last_interv_sector
	        //             //à cause du quart d'heure qui est là que si c'est la première inter on doit recalculer le temps
	        //             lInfo.last_interv_sector.prepareToBeReinserted(main, {
	        //                 i_will_preserve_the_integrity_of_last_inter_of_listInfo: true
	        //             })
	        //             lInfo.last_interv_sector.insertAtGoodPlace(main)
	        //             controls_enabled(()=>{
	        //                 if(!lInfo.last_interv_sector.hasBeenInsert) my_throw( "la propriété n'est pas vrai")
	        //             })
	        //         }
	        //     }
	        // }else{
	        //     lInfo.exact_closing = lInfo.exact_opening = null
	        // }
	        const add_to_update = (inter)=>{
	            inter.prepareToBeReinserted(main, {
	                i_will_preserve_the_integrity_of_last_inter_of_listInfo: true
	            })
	            inter.hasBeenInsert = true
	            inter.insertAtGoodPlace(main)
	        }
	        lInfo.update_opening_closing_time({add_to_update})
	        // main.dont_watch = false
	        return true
	    }

	    /*
	     * réinitialise les compteurs d'heure et supprimer l'intervention là ou elle était avant mais la laisse
	     * à sa position acutelle entre son prev et son next
	     * cette fonction sert quand on a modifié les heures d'une intervention et que l'on souhaite mettre à jours les informations
	     * liéess
	     */
	     prepareToBeReinserted(main, opt) {
	        var prev = this.prev
	        var next = this.next;
	        this.remove(opt)
	        this.setBetween(prev, next)
	        this.hasBeenRemoved = false;
	        return this;
	    }

	    // removeAllPrev(main){
	    //  //je fais la suppression en deux temps pour être sur qu'il n'y a pas de liaison circulaire
	    //  var toRemove = []
	    //  var prev = this.prev;
	    //  while(prev){
	    //      toRemove.push(prev)
	    //      prev = prev.prev
	    //      if(prev){
	    //          prev.next.prev = prev.next = null
	    //      }
	    //  }
	    //  toRemove.forEach(function(interv){
	    //      interv.remove(main);
	    //  })
	    // }

	    affecter(startMinute, endMinute) {

	    }
	    // setPerson(person, main) {
	    //     controls_enabled(()=>{
	    //         if(!main.is_test){
	    //             my_throw( "l'utilisation de setPerson est déprécié, utiliser directement addInterv avec un seul paramètre")
	    //         }
	    //     })
	    //     var m = this;
	    //     return person.addInterv(m, main)
	    // }

	    forEachPeriod(callback) {
	        var interv = this
	        controls_enabled(()=>{
	            if(interv.startMinute != ~~interv.startMinute) {
	                my_throw( 'les minutes doivent être entières')
	            }
	        })
	        if(!interv.get_real_sector()) return;
	        var planning = interv.sector.planning;
	        var is_empty = true;
	        this.forEachiPeriod(planning, (iPeriod) => {
	            callback(planning.periodSortedArr[iPeriod]);
	            is_empty = false
	        })
	        controls_enabled(()=>{
	            if(is_empty) {
	                my_throw( 'empty planning')
	            }
	        })
	    }

	    forEachiPeriod(planning, callback) {
	        var max_in_day = 1500 //01500 = 25h00
	        var interv = this;
	        var formatMinute = (floatMinute) => {
	            // var hour = ('00'+fl(floatMinute/60)).slice(-0);// le slice c'est pour avoir 08 au lieu de 008 et 13 au lieu de 013
	            // var unit = '0'+ Math.round((floatMinute - hour * H) * H)
	            // return hour + 'h' + unit.slice(-2)
	            return ('00000' + floatMinute).slice(0, -5)
	        }

	        // var getDateKey = (date,float_start_minute) => {
	        //     return date + ' ' + formatMinute(float_start_minute)
	        // }
	        var getPropToCompare, iFirstPeriod, periodSortedArr = planning.periodSortedArr
	        // On prend la date et l'heure de début de la linkedList pour comparer avec l'intervention que l'on veut ajouter
	        getPropToCompare = (p) => p.dateKey;
	        // on cherche la première intervention racine de la journée gràce à un algorithme performant
	        // pour les tableau triés qui est binarySearsh
	        // ça nous retourne la position ou il faudrait inserer notre intervention
	        iFirstPeriod = sortedArrFunc.upperBound(periodSortedArr, {
	            dateKey: Main.getDateKey(interv.listInfo.date, interv.startMinute, max_in_day)
	        }, getPropToCompare) - 1;

	        //ajouter à toutes les zone horaires
	        for (var iPeriod = iFirstPeriod;; ++iPeriod) {
	            var period = periodSortedArr[iPeriod];
	            if(!period || period.date > interv.listInfo.date || period.startMinute >= interv.endMinute) {
	                break;
	            }
	            callback(iPeriod);
	        }
	    }

	    /*
	    s'occupe de l'insertion dans les planning des secteurs
	    et dans les personSectors et met jour le temps de la personne
	    */
	    insertAtGoodPlace(main) {
	        var intervToAdd = this;
	        if(intervToAdd.hasBeenRemoved) return;
	        Debug.perfStart('insertAtGoodPlace')
	        if(intervToAdd.get_personSector()) {
	            intervToAdd.forEachPeriod((period) => {
	                period.interventionMap.add(intervToAdd);
	            })

	            /* <debug */
	            // intervToAdd.person._debug_nb_minu_worked = fl(intervToAdd.person._debug_nb_minu_worked) + intervToAdd.endMinute - intervToAdd.startMinute
	            // intervToAdd._debug_iagp = {
	            //     startMinute: intervToAdd.startMinute,
	            //     endMinute: intervToAdd.endMinute,
	            //     nb_minu_worked: intervToAdd.person._debug_nb_minu_worked,
	            //     iDebug: console.iDebug
	            // }
	            /* debug>*/
	            // var personSector = main.personSectorMap[intervToAdd.sector.id + '_' + intervToAdd.listInfo.person.wfId];
	            var personSector = intervToAdd.sector.personSectorMap[intervToAdd.listInfo.person.wfId];
	            var tttttt = intervToAdd.first_interv_sector
	            personSector.addInterv(intervToAdd, main);

	            if(intervToAdd.listInfo.person) {
	                //la personne est dans un secteur donc l'intervention n'est plus à affecter
	                delete intervToAdd.listInfo.person.intervNotAffectedMap[intervToAdd.id];
	                // intervToAdd.listInfo.person.minutes_to_place -= intervToAdd.endMinute - intervToAdd.startMinute
	            }
	        } else {
	            // si il n'y a pas de secteur pour la person alors l'intervention est non affectée
	            if(intervToAdd.listInfo.person) {
	                intervToAdd.listInfo.person.intervNotAffectedMap[intervToAdd.id] = intervToAdd
	            }
	        }
	        console.iidebug[78] = idnext('finaltime')
	        if(console.iidebug[78] == 175897){
	            debugger;
	        }
	        intervToAdd.finalTime = immutable({
	            startMinute: intervToAdd.startMinute,
	            endMinute: intervToAdd.endMinute,
	            total_time: intervToAdd.time + intervToAdd.unplaced_time(),
	            final_time_id: console.iidebug[78]
	        })
	        Debug.perfEnd('insertAtGoodPlace')
	    }

	    /*
	     * retourne le temps de toutes les intervention liées qui sont du même secteur ou qui n'ont pas de secteurs
	     */
	     getTimeLinkedIntervSameSector() {
	        var o = this.getStartAndEndSameSector()
	        return o.time
	    }

	    getStartAndEndSameSector() {
	        var extremityIntervArr = [];
	        ['prev', 'next'].forEach((dir) => {
	            var interv = this;
	            var anti_infinity_loop = 24 * H
	            while (interv[dir] && (interv.sector == interv[dir].sector)) {
	                if(!--anti_infinity_loop) {
	                    controls_enabled(()=>{
	                        my_throw( 'le chainage est une boucle infinie')
	                    })
	                }
	                interv = interv[dir]
	            }
	            extremityIntervArr.push(interv)
	        })
	        return {
	            startInterv: extremityIntervArr[0],
	            endInterv: extremityIntervArr[1],
	            startMinute: extremityIntervArr[0].startMinute,
	            endMinute: extremityIntervArr[1].endMinute,
	            time: extremityIntervArr[1].endMinute - extremityIntervArr[0].startMinute
	        }
	    }
	    /*
	     * Cette fonction doit avoir le this qui est l'intervention
	     * renvoit l'heure de début où il y a un secteur,
	     * on part de l'intervention courrante et on s'arrête avant de tomber sur une pause
	     */
	     getStartAndEndWithSectorAroundMe(intervToAdd) {
	        var endMinute, startMinute;
	        ['prev', 'next'].forEach((dir) => {
	            var interv = this;
	            var minute = null;
	            if(!interv.sector) {
	                if(dir == 'prev' && intervToAdd.startMinute > interv.startMinute && intervToAdd.startMinute < interv.endMinute) {
	                    ////////////debugger
	                    minute = startMinute = intervToAdd.endMinute
	                }
	                if(dir == 'next' && intervToAdd.endMinute < interv.endMinute) {
	                    minute = endMinute = intervToAdd.startMinute
	                }
	            }
	            if(minute == null) {
	                while (interv[dir] && interv[dir].sector) {
	                    interv = interv[dir]
	                }
	                if(dir == 'next') {
	                    endMinute = interv.endMinute
	                } else { //dir == prev
	                    startMinute = interv.startMinute
	                }
	            }
	        })
	        return {
	            startMinute,
	            endMinute
	        }
	    }

	    set_minu(st,end){
	    	controls_enabled(()=>{
		    	if(st >= end){
		    		my_throw(`illogic minutes order`)
		    	}
		    	Debug.dont_check_start_end_logic = true
	    	})
	    	this.startMinute = st
	    	this.endMinute = end
	    	Debug.dont_check_start_end_logic = false
	    }

	    getMinTime() {
	        return this.sector ? this.sector.minTimeSector : conf.minTime
	    }
	    getMaxTime() {
	        return this.sector ? this.sector.maxTimeSector : 24*60
	    }

	    /*
	     * retourne pour cette intervention son minimum et son maximum possible
	     * à cause de l'emploie du temps d'ouverture du magasin
	     */
	     getOpenAndCloseMinute(main) {
	        var intervToAdd = this;
	        if(this.startMinute == this.endMinute) {
	            return {
	                minstartMinute: this.startMinute,
	                maxendMinute: this.startMinute
	            }
	        }
	        var minstartMinute, maxendMinute;
	        var lInfo = intervToAdd.listInfo
	        //++window.iDebug;
	        controls_enabled(()=>{
	            if(+intervToAdd.startMinute !== intervToAdd.startMinute || +intervToAdd.endMinute !== intervToAdd.endMinute) {
	                my_throw( "heure mauvaise")
	            }
	        })
	        var poses = main.getPosArrInArray(lInfo.date, intervToAdd.startMinute, intervToAdd.endMinute)
	        var openPeriods = [];
	        var wasOpened = false;
	        var endPos;
	        var ret;
	        var returnFalse = function() { //retourner des chiffres abhérents
	            return {
	                minstartMinute: 48 * H,
	                maxendMinute: -48 * H
	            }
	        }
	        // D'abord trouver le minstartMinute
	        if(!intervToAdd.get_real_sector()) {
	            //si il y a pas de secteur comme quand on ajoute une pause alors il n'y a pas de limite d'heure
	            return {
	                minstartMinute: lInfo.min_start_day,
	                maxendMinute: lInfo.max_end_day
	            }
	        }
	        // var period = intervToAdd.sector.planning.periodSortedArr[pos]
	        // endPos = pos
	        var periods = intervToAdd.sector.planning.periodSortedArr
	        var iFirstOpenedP;
	        for (var pos = poses[0], per = periods[pos]; per; per = periods[++pos]) {
	            //ouverture ça veut juste dire que le magasin est ouvert mais ça peut
	            //autant être un début qu'une fin de journée ou un milieu
	            if(per.openingType != 'OUVERTURE' || lInfo.max_end_day <= per.startMinute) {
	                if(iFirstOpenedP) {
	                    maxendMinute = Math.min(per.startMinute, lInfo.max_end_day)
	                    break; //refuser l'ajout au heures qui suivent + la courante car le magasin n'est pas ouvert
	                }
	            } else if(!iFirstOpenedP) {
	                iFirstOpenedP = pos
	            }
	        }
	        if(!iFirstOpenedP) {
	            var ret = returnFalse()
	            return ret
	        }
	        for (var pos = iFirstOpenedP, per = periods[pos]; per; per = periods[--pos]) {
	            if(per.openingType != 'OUVERTURE' && lInfo.min_start_day >= per.startMinute) {
	                if(wasOpened) {
	                    minstartMinute = Math.max(per.endMinute, lInfo.min_start_day)
	                    break; //refuser l'ajout au heures qui suivent + la courante car le magasin n'est pas ouvert
	                }
	            } else if(!wasOpened) {
	                wasOpened = true
	            }
	        }

	        return {
	            minstartMinute,
	            maxendMinute
	        }
	    }

	    // get start_minute() {
	    //     my_throw( "c'est en camelcase 'startMinute'")
	    // }
	    // set start_minute(val) {
	    //     my_throw( "c'est en camelcase 'startMinute'")
	    // }

	    // get start_date() {
	    //     my_throw( "c'est 'date' et pas start_date")
	    // }
	    // set start_date(val) {
	    //     my_throw( "c'est 'date' et pas start_date")
	    // }
	}

	return Intervention
}