/**
 * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
 */
'' + function(){

	const date_pause_keys = ['id','min_start_day','min_end_day','max_end_day','max_start_day','min_start_pause','max_start_pause','real_time','time','fake_time','min_time_in_day','fake_sector','pause_instance']
	/** information de pause pour une date donnée **/
	class Date_pause{
		constructor(info_by_key, date, pause_instance, get_fake_sector){
        	let ibk = this
        	controls_enabled(()=>{
        		ibk = proxy_creator(ibk,date_pause_keys,{})
        		if(!pause_instance){
        			throw my_throw('missing pause_instance')
        		}
        	})
        	ibk.id = idnext('date_pause_id')
        	// if(1195 === ibk.id){
        	// 	debugger
        	// }
            ibk.pause_instance = pause_instance
            Object.assign(ibk, info_by_key.default, info_by_key[date])
            controls_enabled(()=>{
            	if(ibk.time === void 8){
            		my_throw(`object.assign n'a pas fait son travail `)
            	}
            })
            ibk.fake_sector = get_fake_sector(date,pause_instance)

            return ibk
		}


		get time(){
			if(this.pause_instance.is_real_time_mode){
				return this.real_time
			}
			return this.fake_time
		}

		set time(x){
			throw "ceci n'est accessible qu'en getter"
		}
	}


	const allow_keys_pause = new Set([
	    'main','_info_by_key','nb_max_by_day','min_time_in_day','is_real_time_mode','is_not_counted_in_worktime'
	])







	/*@Pause*/
	class Pause {
	    constructor(o,info_by_key,{is_not_counted_in_worktime,uniq_name}) {
	        this.main = o.main
            var m = this
        	controls_enabled(()=>{
	            m = proxy_creator(this,allow_keys_pause,{
	                // setter_check: (prop,val,target)=>{
	                //     return true
	                // },
	                // is_allowed: (property)=>{
	                //     return true
	                // }
	            })
	            assert_exists(is_not_counted_in_worktime, uniq_name)
        	})
	        let ibk1 = m._info_by_key = {}
	        clone(o,m)
	        const get_fake_sector = (key,p)=>{
	            // une intervention de pause est une intervention ayant un secteur
	            // qui contient pause_instance
	            // Mettre un secteur permet d'utiliser le code déjà existant pour compter les heures
	            // de travail d'une personne qui ne marche pas sans secteur
	            // var info = this.info_by_key[key]
	            return new Sector({
	            	// en fait même si ça doit pas compter dans le temps de travail
	            	// on dit que ça doit compter dedans mais on fera le complément avec unplaced_time
	            	// c'est à cause des pause de 40 minute que l'on met d'abord à une heure que 
	            	// l'on doit faire ça
	                is_not_counted_in_worktime: false,
	                id: uniq_name,
	                sector_group_name: uniq_name,//cela va permettre d'avoir le même person_sector peut importe la clef
	                name: m.constructor.name,
	                // minTimeSector : 0,//p.fake_time,
	                // maxTimeSector : 0,//p.fake_time,
	                // recommendedTime: info.fake_time,
	                parentEntity: null,
	                pause_instance: p,
	                main:o.main
	            })
	        }
	        Object.keys(info_by_key).forEach((date)=>{
				ibk1[date] = new Date_pause(info_by_key, date, m, get_fake_sector)
	        })
	        this.is_not_counted_in_worktime = is_not_counted_in_worktime
	        return m
	    }

	    get_hours_config(date,person){
	        controls_enabled(()=>{
	            if(!date || !person){
	                my_throw('missing parameters')
	            }
	        })
	        return this._info_by_key[date] ||  this._info_by_key.default
	    }


	    unpause_illogic_pause(first_interv,opt_uip){
	        var p = this
	        first_interv = first_interv.first_interv
	        console.iidebug[27] = idnext('27')
	        if(console.iidebug[27] == 507982) {
	            //// debugger
	            Debug.logDayWf(first_interv)
	        }
	        var pause_intervs2 = p.get_all_pauses_day(first_interv);
	        var need_pause = opt_uip.force_to_remove ? false : p.is_pause_needed(first_interv,pause_intervs2)
	        var pause_intervs = p.filter_pauses(pause_intervs2) //chaque classe ne supprime que son type de pause
	        controls_enabled(()=>{
	            if(pause_intervs.length > 1) {
	                Debug.logDayWf(first_interv)
	                my_throw( 'il y a deux pause dans cette journée !!!')
	            }
	        })
	        var pause_interv = pause_intervs[0]
	        var info_remove = p.remove_pause_if_necessary(pause_interv, need_pause)
	        return {
	            need_pause,
	            pause_is_ok: info_remove.success,
	            pause_interv
	        }
	    }

	    // //est ce que l'on a mis une pause de 1 heure mais qu'il faudra réduire plus tard
	    // does_pause_need_to_be_reduced() {
	    //     return this.real_time !== this.fake_time
	    // }
	    add_a_new_pause(first_interv, need_pause, pause_interv, opt99) {
	        const {person, date} = first_interv
	        const p = this.get_hours_config(date, person)
	        const pause_added = this.main.addAPause({
	            pause_instance: this,
	            person,
	            date,
	            min_start_pause: Math.max(p.min_start_pause, first_interv.startMinute),
	            max_start_pause: p.max_start_pause,
	            pauseTime: p.time,
	            include_this_amplitude: opt99.include_this_amplitude,
	            do_not_put_pause_on: opt99.do_not_put_pause_on
	        });
	        // }
	        return {
	            pause_is_ok: pause_added.hasBeenInsert,
	            pause: pause_added
	        }
	    }


	    filter_pauses(pauses) {
	        var pauses2 = pauses.filter((inter_pause) => {
	            return inter_pause.sector && inter_pause.sector.pause_instance === this
	        })
	        controls_enabled(()=>{
	            if(pauses2.length > this.nb_max_by_day) {
	                my_throw( 'Cette pause est en trop d\'exemplaire dans cette journée')
	            }
	        })
	        return pauses2
	    }

	    get is_real_time_mode(){
	    	this.main && this.main.is_real_time_mode
	    }

	    go_real_time_mode() {
	       // this.is_real_time_mode = true
	        // this.time = this.real_time
	        // forOf(this._info_by_key,(obj)=>{
	        //     obj.time = obj.real_time
	        // })
	    }

	    get_all_pauses_day(first_interv) {
	        controls_enabled(()=>{
	            if(!first_interv) {
	                my_throw( 'paramètre obligatoire')
	            }
	        })
	        var pauses = [];
	        //si la personne travaille entre 11.5 et 15 il lui faut une pause entre 11.5 et 15 de une heure
	        for (var interv = first_interv; interv; interv = interv.next) {
	            if(interv.is_pause()) {
	                pauses.push(interv)
	            }
	        }
	        return pauses
	    }

	    is_pause_ok(pause_interv) {
	        var p = pause_interv.prev,ps = p.get_real_sector()
	        var n = pause_interv.next,ns = n.get_real_sector()
	        var pi = pause_interv.sector.pause_instance
	        var hc = pause_interv.get_hours_pause_config()
	        return pi == this
	            && ps && ns && p.v2_respectMinTime() && n.v2_respectMinTime()
	            //il ne faut pas être entouré par des interventions dont le secteur n'est pas souple
	            //si il faudra ajouter 20 minutes à un des deux coté comme la pause est de 40 minute et que
	            //dans un premier temps on place une heure
	            && (
	                hc.real_time == hc.time//la pause n'a pas besoin d'être réduite
	                || ps.minTimeSector !== ps.maxTimeSector//ou bien il y a le coté gauche que l'on pourra réduire
	                || ns.minTimeSector !== ns.maxTimeSector//ou bien il y a le coté droit que l'on pourra réduire
	        )
	    }


	    day_hours(first_interv){
	        const p = this;
	        const hc = first_interv.get_hours_pause_config(p)
	        var parent = first_interv.first_interv_sector.sector.parentEntity
	        if(!parent){
	            var offset1 = 1 * H
	            var offset2 = 2 * H
	        }else{
	            var offset1 = parent.minTimeSectors /*+ this.fake_time*/ 
	            //offset2 est le temps de la pause + le temps minimum après la pause
	            var offset2 = parent.minTimeSectors + hc.fake_time
	        }
	        const min_end_day = hc.min_start_pause + offset2, max_start_day = hc.max_start_pause - offset1
	        assert_finite(min_end_day);assert_finite(max_start_day)
	        return {max_start_day,min_end_day,}
	    }

	    //si la personne travaille entre 11.5 et 15 il lui faut une pause entre 11.5 et 15 de une heure
	    //pause_validated est une pause 
	    is_pause_needed(first_interv) {
	        const p = this;
	        const hc = first_interv.get_hours_pause_config(p)
	        var start = hc.min_start_pause;
	        var end = hc.max_start_pause;
	        var d = first_interv.get_listInfo_by_date()
	        if(d.worked < hc.min_time_in_day){
	            return false
	        }
	        const dh = p.day_hours(first_interv)
	        const fmw = d.first_minu_worked, lmw = d.last_minu_worked
	        //la négation gère les undefined car chaque option est facultative
	        return !(fmw > dh.max_start_day) && !(fmw < dh.min_start_day)
	            && !(lmw < dh.min_end_day) && !(lmw > dh.max_end_day)
	    }

	    // si la pause était présente et qu'elle est mauvaise alors
	    // tenter de l'enlever, si on a pas réussis le mettre false dans inter_that_replace_pause_is_ok
	    // sinon true
	    // par défaut null
	    remove_pause_if_necessary(pause_interv, need_pause) {
	        var success = true;
	        //si la pause est présente mais mal placé alors l'enlever pour la remettre
	        if(pause_interv && (!need_pause || !this.is_pause_ok(pause_interv) ) ){
	            //on enlève la pause pour mieux la remettre et on met un secteur à la place de cette pause
	            if(!pause_interv.unPause(this.main)) {
	                console.error('la pause n\'a pas pu être enlevé pour être replacé console.iidebug[27] ' + console.iidebug[27])
	                var success = false
	            }
	        }
	        return {
	            success,
	            pause_interv
	        }
	    }


	    resolve_pause_unrespected(first_interv, opt5) {
	        var p = this
	        console.iidebug[48] = idnext('48')
	        if(console.iidebug[48] == 77131) {
	            //// debugger
	            Debug.logDayWf(first_interv)
	        }
	        first_interv.listInfo.check_logic()
	        var uip = this.unpause_illogic_pause(first_interv,opt5.opt_uip || {})
	        var pause_is_ok = uip.pause_is_ok
	        var need_pause = uip.need_pause
	        var pause_interv = uip.pause_interv
	        if(pause_is_ok) {
	            var add_a_new_pause = need_pause && (!pause_interv || pause_interv.hasBeenInsert == false)
	            if(add_a_new_pause) {
	                first_interv.listInfo.check_logic()
	                var info_add = p.add_a_new_pause(first_interv, need_pause, pause_interv, opt5)
	                first_interv.listInfo.check_logic()
	                pause_is_ok = info_add.pause_is_ok
	                if(pause_is_ok) {
	                    controls_enabled(()=>{
	                        if(!p.is_pause_ok(info_add.pause)) {
	                            ////////debugger;
	                            p.is_pause_ok(info_add.pause)
	                            my_throw( "On a ajouté une pause qui ne respecte pas les conditions, shame")
	                        }
	                    })
	                }
	            } else {
	                pause_is_ok = true
	            }
	        }
	        if(!add_a_new_pause && !pause_interv) {
	            // var person = first_interv.person, date = first_interv.date
	            // console.error('cette personne n\'avait pas besoin de pause',person.wfId ,date)
	        }
	        if(!add_a_new_pause && need_pause) {
	            // console.warn("la pause était déjà bonne")
	        }

	        var all_pauses
	        if(pause_is_ok && need_pause) {
	            all_pauses = p.get_all_pauses_day(first_interv.first_interv)
	            controls_enabled(()=>{
	                if(!all_pauses[0] || !all_pauses[0].hasBeenInsert) {
	                    ////////debugger
	                    first_interv.listInfo.check_logic()
	                    all_pauses = p.get_all_pauses_day(first_interv.first_interv)
	                    my_throw( 'Si la pause est insérée où est elle')
	                }
	            })
	        }
	        return {
	            pause_is_ok,
	            pause_is_placed: need_pause && pause_is_ok,
	            all_pauses
	        }
	    }


	    //@@@
	    //On va tester de quel coté c'est le mieux d'ajouter un secteur pour réduire la pause
	    reduce_time_pause(pause_interv) {
	        const {real_time,fake_time} = pause_interv.get_hours_pause_config()
	        if(pause_interv.time <= real_time){
	            return;// rien à faire la pause est déjà de bonne taille
	        }
	        console.iidebug[40] = idnext('40')
	        if(1388192 == console.iidebug[40]) {
	            //// debugger;
	        }
	        controls_enabled(()=>{
	            if(!pause_interv.hasBeenInsert || !pause_interv.date) {
	                my_throw( 'il manque des données')
	            }
	            if(pause_interv.startMinute == pause_interv.endMinute) {
	                my_throw( 'not logic hours')
	            }
	        })
	        var too_much_time = pause_interv.time - fake_time
	        controls_enabled(()=>{
	            if(too_much_time < 0) {
	                my_throw( 'négatif')
	            }
	        })
	        if(pause_interv.date == '2018-03-30' && pause_interv.person.wfId == 10416) {
	            //// ////debugger
	        }

	        pause_interv.free_the_pause()
	        // si une intervention sur le bord est à son maximum on ne pourra pas l'étendre donc le
	        // choix du temps à rajouter n'est pas un choix

	        var add_sector_time_left = pause_interv.prev.time_possible_to_add()
	        var add_sector_time_right = pause_interv.next.time_possible_to_add()
	        controls_enabled(()=>{
	            if(!add_sector_time_left && !add_sector_time_right) {
	                my_throw( "l'intervention ne peut pas être agrandie")
	            }
	        })
	        var seria, interv
	        var initial_start_minute = pause_interv.startMinute
	        var initial_end_minute = pause_interv.endMinute
	        var initial_sector = pause_interv.sector
	        var rules = {
	            safe_insertion: true
	        }
	        //il faut le faire avant que pause_interv ne soit supprimé
	        var next_sector = pause_interv.next.sector
	        var prev_sector = pause_interv.prev.sector
	        var mode,adi;
	        var extend_right = function() {
	            seria = seria || pause_interv.v3_serialize()
	            seria.endMinute = initial_end_minute
	            seria.startMinute = initial_start_minute + real_time
	            seria.sector = next_sector
	            interv = new Intervention(seria)
	            interv.rules = rules
	            adi = pause_interv.person.addInterv(interv)
	            mode = 'right'
	            return adi.has_been_insert
	        }
	        var extend_left = function() {
	            seria = seria || pause_interv.v3_serialize()
	            seria.endMinute = initial_end_minute - real_time
	            seria.startMinute = initial_start_minute
	            seria.sector = prev_sector
	            interv = new Intervention(seria)
	            interv.rules = rules
	            adi = pause_interv.person.addInterv(interv)
	            mode = 'left'
	            return adi.has_been_insert
	        }
	        var undo = function() {
	            //on annule en remettant la pause à sa taille trop grande
	            seria.startMinute = initial_start_minute
	            seria.endMinute = initial_end_minute
	            seria.sector = initial_sector
	            interv = new Intervention(seria)
	            interv.rules = {megaForcedInsert:true}
	            pause_interv.person.addInterv(interv)
	        }
	        pause_interv.free_the_pause()
	        var z_score_global_left = null,
	        zscore_global_right = null;
	        if(add_sector_time_left !== 0) {
	            if(extend_left()) {
	                z_score_global_left = main.v2_get_zscore_global()
	            }
	        }
	        if(add_sector_time_right !== 0) {
	            if(z_score_global_left !== null) {
	                undo()
	                if(extend_right()) {
	                    zscore_global_right = main.v2_get_zscore_global()
	                }
	                //c'était mieux avec extend left donc y revenir
	                if(z_score_global_left !== null && (zscore_global_right === null || z_score_global_left < zscore_global_right)) {
	                    undo()
	                    extend_left()
	                }
	            } else {
	                extend_right()
	            }
	        }
	        var n = interv.next,
	        pr = interv.prev;
	        controls_enabled(()=>{
	            if(!adi.has_been_insert) {
	                my_throw( 'error pause has not been reduced')
	            }
	            if(pause_interv.time > fake_time){
	                my_throw( 'error pause has not been reduced')
	            }
	            if(mode == 'right') {
	                if(!(pr && pr.is_pause() && pr.sector.pause_instance.is_pause_ok(pr))) {
	                    my_throw( 'error placement pause')
	                }
	            } else if(mode == 'left') {
	                if(!(n && n.is_pause() && n.sector.pause_instance.is_pause_ok(n))) {
	                    my_throw( 'error placement pause')
	                }
	            }
	        })
	    }
	}
	return Pause
}