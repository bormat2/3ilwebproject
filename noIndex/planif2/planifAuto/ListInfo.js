/**
 * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
 */
'' + function(){
    var allow_keys_listInfo = new Set([
        '_first_interv_sector',
        '_last_interv_sector',
        'exact_opening',
        'exact_closing',
        'absence',
        'date',
        'first_interv',
        'from',
        'id',
        'iRacine',
        'last_interv',
        'max_start_day',
        'min_start_day',
        'max',
        'min',
        'max_end_day',
        'min_end_day',
        'person',
        'worked',
        'working_mandatory',//la journée est obligatoirement travaillé si true
        'disabled_time_opening',//ne pas ajouter le quart d'heure ou 15 minute d'ouverture car une protected l'empêche
        'disabled_time_closing',//ne pas ajouter le quart d'heure ou 15 minute de fermeture car une protected l'empêche
    ]);
    // window.keys5 = {}

    /**
     * Toutes les interventions d'une même journée pour une personne donnée 
     * possède le même listInfo qui contient la date et la personne
     * ainsi que la première et dernière inter de la journée
     * et la première et dernière intervention de la journée avec secteur
     */
    class ListInfo{//@ListInfo
        constructor(o) {
            var m = this
            controls_enabled(()=>{
                m = proxy_creator(this,allow_keys_listInfo,{
                    setter_check:(prop,val,target)=>{
                        switch(prop){
                            case "worked":
                                // if(11340 == target.id && val == 555 ){
                                //     debugger
                                // }
                                break
                            }
                            return true
                        }
                    })
            })
            clone(o,m)
            controls_enabled(()=>{
                if(o.from == 'getDataFromDataBase'){
                    if(!('working_mandatory' in m)){
                        my_throw( 'working_mandatory missing')
                    }
                }
            })
            controls_enabled(()=>{
                if(m.id){
                    my_throw( 'error')
                }
            })
            m.id = idnext('no_name11')
            return m
        }

        get_all_pauses_day(){
            const li = this
            const pauses = []
            for(var inte = li.first_interv_sector;inte;inte = inte.next){
                if(inte.is_pause()){
                    pauses.push(inte)
                }
            }
            return pauses
        }

        /**
         * Quelles sont les heures limite pour que la pause reste la même
         * si on change les heures
         */
        get_hours_rules_to_keep_same_pauses(){
            const li = this
            var min_start_day = -48 * H, max_start_day = 48 * H
            var min_end_day = -48 * H, max_end_day = 48 * H
            this.get_all_pauses_day().forEach((pause_inter)=>{
                var hc = pause_inter.get_hours_pause_config()
                if(hc.min_start_day > min_start_day) ;({min_start_day} = hc);
                if(hc.min_end_day > min_end_day) ;({min_end_day} = hc);
                if(hc.max_start_day < max_start_day) ;({max_start_day} = hc);
                if(hc.max_end_day < max_end_day) ;({max_end_day} = hc);
            })

            controls_enabled(()=>{
                var is_ok = !(max_start_day < min_start_day) && !(min_end_day > max_end_day)
                    && !(max_start_day > max_end_day) && !(min_start_day > max_end_day)
                if(!is_ok){
                    my_throw("Certaines inagalité ne sont pas logique")
                }
                const fmw = li.first_minu_worked, lmw = li.last_minu_worked
                if(fmw > max_start_day || lmw < min_end_day || fmw < min_start_day || lmw > max_end_day){
                    my_throw("Mais la pause est déjà mauvaise en faite")
                }
            })


            return {//min_start_day, max_start_day, min_end_day, max_end_day,
                amplitudes_to_include: {
                    start_minute: max_start_day,
                    end_minute: min_end_day
                },
                opt_min_max: {
                    start_not_before: min_start_day,
                    end_not_after: max_end_day
                }
            }
        }
        /*
        * si il y a une journée avec que une absence elle 
        * n'est pas worked mais .worked n'est pas égale à 0
        **/
        is_worked(){
            var li = this;
            return li && li.first_interv_sector
        }
        get_v2_firstORlast_interv_with_sector(prop_name) {
            var m = this;
            var ii = m[prop_name]
            if(ii || !ii && ii !== 0) {
                controls_enabled(()=>{
                    if(ii && !(ii.hasBeenInsert && ii.get_real_sector())) {
                        my_throw( "si l'intervention n'est plus un secteur ou bien a été supprimé" +
                        "la valeur 0 aurait du être mise dans first_interv_sector")
                    }
                })
            } else {
                //si l'intervention a été supprimé ou n'a plus de secteur
                //alors il faut retrouver le dernier secteur
                if(prop_name == '_last_interv_sector') {
                    var ii = m.last_interv,
                    dir = 'prev'
                } else {
                    var ii = m.first_interv,
                    dir = 'next'
                }
                while (ii) {
                    if(ii.get_real_sector()) {
                        break
                    }
                    ii = ii[dir]
                }
                m[prop_name] = ii //donc undefined si il y en a pas
            }
            if(m.id == 78070 && m._last_interv_sector && m._last_interv_sector.addIntervDebug == 1387041){
                Debug.logDayWf(m.first_interv)
                //debugger;
            }
            return ii
        }
        set first_interv_sector(val) {
            var m = this
            controls_enabled(()=>{
                if(val && !val.hasBeenInsert) {
                    my_throw( "cette inter n'est pas insérée")
                }
                if(val.startMinute < m.startMinute){
                    my_throw( "ça ne devrait pas arriver")
                }
            })
            m._first_interv_sector = val
        }
        set last_interv_sector(val) {
            var m = this
            controls_enabled(()=>{
                if(val && !val.hasBeenInsert) {
                    my_throw( "cette inter n'est pas insérée")
                }
                if(val.endMinute > m.endMinute){
                    my_throw( "ça ne devrait pas arriver")
                }
            })
            m._last_interv_sector = val
        }

        get last_interv_sector() {
          return this.get_v2_firstORlast_interv_with_sector('_last_interv_sector')
        }

        get first_interv_sector() {
        return this.get_v2_firstORlast_interv_with_sector('_first_interv_sector')
        }

        get first_minu_worked(){
            return this.first_interv_sector && this.first_interv_sector.startMinute
        }

        get last_minu_worked(){
            return this.last_interv_sector && this.last_interv_sector.endMinute
        }

        get_inter_with_startMinute(start_minute){
            var next = this.first_interv
            while(next){
                if(next.startMinute <= start_minute && next.endMinute > start_minute){
                    return next
                }
                next = next.next
            }
            return null;
        }
        /**
         * Sur quel horaire doit on planifier la journée à cause des interventions protected
         * et des pauses
         * @return {[type]} [description]
         */
        amplitudes_to_include(){
            let start_minute,end_minute,inter;
            const main = this.person && this.person.main
            if(main){
                for(inter = this.first_interv;inter;inter = inter.next){
                    if(inter.protected){
                        if(inter.is_pause()){
                            //il faut de la place autour de la pause
                            start_minute = inter.startMinute - main.blockArr[0].sectorEntArr[0].minTimeSector
                        }else if(inter.get_real_sector()){//donc pas une pas une absence mais une vrai affectation
                            start_minute = inter.startMinute
                        }
                        break;
                    }
                }
                if(start_minute !== void 8){
                    const inter2 = inter
                    for(inter = this.last_interv;inter;inter = inter.prev){
                        if(inter.protected){
                            if(inter.is_pause()){
                                //il faut de la place autour de la pause
                                end_minute = inter.endMinute + main.blockArr[0].sectorEntArr[0].minTimeSector
                            }else if(inter.get_real_sector()){//donc pas une pas une absence mais une vrai affectation
                                endMinute = inter.endMinute
                            }
                            break
                        }
                    }
                }

            }
            return start_minute !== void 8 && {start_minute,end_minute} 
        }


        check_logic(){
            controls_enabled(()=>{
                var m = this;
                if(m.first_interv && m.first_interv.prev){
                    my_throw( 'la première ne peut pas avoir de précédent')
                }
                if(m.last_interv && m.last_interv.next){
                    my_throw( 'la première ne peut pas avoir de suivant')
                }
                var first_interv_sec = m._first_interv_sector
                    if(first_interv_sec){//si il y a au moins une fois où l'on est affecté à un secteur
                        var vliwsn = m._last_interv_sector.next
                    if(vliwsn && vliwsn.get_real_sector()){
                        window.vliwsn = vliwsn
                        my_throw( 'la dernière avec secteur ne peut pas pas avoir de suivante avec secteur')
                    }
                    var vfiwsp = first_interv_sec.prev
                    if(vfiwsp && vfiwsp.get_real_sector()){
                        window.vfiwsp = vfiwsp
                        my_throw( 'la dernière avec secteur ne peut pas pas avoir de précédente avec secteur')
                    }
                }
                // m.get_hours_rules_to_keep_same_pauses()
            })
        }


        update_opening_closing_time({add_to_update}){
            const lInfo = this
            if(lInfo.exact_opening && !lInfo.exact_opening.hasBeenInsert){
                lInfo.exact_opening = null
            }
            if(lInfo.exact_closing && !lInfo.exact_closing.hasBeenInsert){
                lInfo.exact_closing = null
            }

            if(lInfo.first_minu_worked){
                //c'était la première intervention à une ouverture mais maintenant il y en a d'autre avant 
                //alors la mettre à jour
                if(lInfo.exact_opening){
                    if(lInfo.first_interv_sector !== lInfo.exact_opening){
                        add_to_update(lInfo.exact_opening)
                        lInfo.exact_opening = lInfo.exact_opening = null
                    }
                }
                if(lInfo.exact_closing){
                    if(lInfo.last_interv_sector !== lInfo.exact_closing){
                        add_to_update(lInfo.exact_closing)
                        lInfo.exact_closing = null
                    }
                }
                const wf = lInfo.person
                if(wf){
                    const is_opening = wf.opening_info({start_minute: lInfo.first_minu_worked,date: lInfo.date}).is_supplement_time()
                    if(is_opening && lInfo.exact_opening != lInfo.first_interv_sector){
                        lInfo.exact_opening = lInfo.first_interv_sector
                        add_to_update(lInfo.exact_opening)
                    }
                    const is_closing = wf.closing_info({end_minute: lInfo.last_minu_worked,date: lInfo.date}).is_supplement_time()
                    if(is_closing && lInfo.exact_closing != lInfo.last_interv_sector){
                        lInfo.exact_closing = lInfo.last_interv_sector
                        add_to_update(lInfo.exact_closing)
                    }
                }
            }else{
                lInfo.exact_closing = lInfo.exact_opening = null
            }
        }
    }

    return ListInfo
}