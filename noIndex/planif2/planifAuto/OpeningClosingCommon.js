/**
 * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
 */
'' + function(){
    class OpeningClosingCommon extends Rotation {
        constructor(main) {
            controls_enabled(()=>{
                if(!main){
                    my_throw( 'missing main')
                }
            })
            super(main)
            controls_enabled(()=>{
                if(!OpeningClosingCommon.child_class){
                    OpeningClosingCommon.child_class = new WeakSet()
                }else{
                    OpeningClosingCommon.child_class.add(this.__proto__)
                    ;['get_limit_minute','get_names'].forEach((name) => {
                        if(void 8 == this[name]){
                            my_throw( `${name} does not exist in class ${this.constructor.name}`)
                        }
                    })
                }
            })
            this.tolerance = 1
            this.init()
        }

        // is_wf_date_rotation(wf,date,n){
        //     const first_minu_worked = wf[n.get_first_minu_worked]()
        //     if(!first_minu_worked) return false
        //     const is_rotation = wf[n.opening_info]({
        //         [n.start_minute]: first_minu_worked,
        //         date: date
        //     })[n.before_or_equal]('rotation')
        //     return is_rotation
        // }

        /*
         * si on est en ouverture ou veut l'intervention de la fin de la journée
         * alors qu'en fermeture ou veut la première de la journée
         */
        get_the_far_interv_of_same_day() {
            abstractError()
        }

        /*@depreciated*/
        reduce_rotation_for_wf_without_replanification(){
            lastORfirst_inter = rota.get_the_far_interv_of_same_day(theInter)
            // last_minute_worked = lastORfirst_inter.endMinute
            lastORfirst_inter_info = {
                start_minute: lastORfirst_inter.startMinute,
                end_minute: lastORfirst_inter.endMinute,
            }
            last_sector = lastORfirst_inter.sector

            //faire en sorte que la première intervention ne soit plus une ouverture
            if(replanif_the_day){
                
            }else{
                seria = theInter.v2_serialize()
                old_sector = theInter.sector
                old_time_opening = theInter.time
                beforeDelete = {
                    startMinute: theInter.startMinute,
                    endMinute: theInter.endMinute
                }
                if(!theInter.reset_inter()) {
                    echec = 'A2: une intervention protected bloque l\algo';
                    //pas besoin d'annuler on a rien fait
                    return;
                }
                old_startORend = rota.get_old_startORend(seria)
                nb_minute_to_remove = rota.update_seria_to_delete_the_rotation_on_first_person(seria)
                seria.rules = {
                    dontMoveExistingInterventionOrPause: true, // comme on a supprimé le secteur avant il y a normalement la place
                    v2_do_not_respect_min_time_after_before: true, // ne pas vérifier le temps restant après ou avant l'intervention
                    v2_break_if_min_time_not_ok: true //c'est le mintime de cette intervention que l'on va vérifier et si il est pas bon on n'ajoutera pas
                }
                seria.dontAffectNow = true

                //on remet l'intervention que l'on a enlevée mais avec des heures réduites pour ne plus être une ouverture
                inter4 = new Intervention(seria)
                adi4 = seria.person.addInterv(inter4) //si ça rate tampi on ajoutera plus le soir
                if(!theInter.is_day_ok({
                    ignore_working_time: true
                })) {
                    //si le début et la fin de la journée ne sont pas bon alors annuler
                    undoFunc_1()
                    console.error('à cause des début et fin de journée ou doit annuler le changement de rotation')
                    echec = true
                    return
                }
            }

            //mettre ce que l'on a enlevé le matin le soir
            ;
            (function() {
                //combien d'heure à t'on vraiment enlevé ?
                var nb_minute_to_really_remove_opening = adi4.has_been_insert ? nb_minute_to_remove : old_time_opening
                //ajouter ce que l'on a enlevé au début de la journée en fin de journée
                rota.update_seria_to_add_what_we_delete_at_the_other_side_of_the_same_day(seria,
                    lastORfirst_inter_info,
                    nb_minute_to_really_remove_opening
                    )
                // seria.startMinute = last_minute_worked
                // seria.endMinute = seria.startMinute + nb_minute_to_really_remove_opening
                seria.sector = last_sector
                var inte = new Intervention(seria)
                seria.person.addInterv(inte)
                if(!inte.hasBeenInsert) {
                    echec = 'ajout le soir (matin si closing) raté'
                }
                if(!theInter.is_day_ok({
                    ignore_working_time: true
                })) {
                    //si le début et la fin de la journée ne sont pas bon alors annuler
                    echec = 'à cause des début et fin de journée ou doit annuler le changement de rotation'
                    console.error(echec)
                    return
                }
            })();
            if(echec) {
                console.error('echec de inte ' + echec)
                undo_what_we_do()
                return;
            }
            var rotation_ratio_wf_aft = wf.rotation_ratio[rota_name] || 0

            console.log(
                "person " + wf.wfId +
                " déplacement de " + nb_minute_to_remove +
                " de " + date +
                " réduction de " + beforeDelete.startMinute + ' à ' + beforeDelete.endMinute + " vers " +
                adi.startMinute + ' à ' + adi.endMinute +
                ' ajout de ' + seria.startMinute + ' à ' + seria.endMinute +
                ' le ratio de rotation est ainsi passé de ' + rotation_ratio_wf_bef +
                ' à ' + rotation_ratio_wf_aft
                // + lastORfirst_inter_info.end_minute
                );

            //var is_pause_ok = main.main_resolve_pause_unrespected({
                //intervention: wf.get_first_interv_at_date(date)
            //}).pause_is_ok

            var rpatmid =  m.resolve_pause_and_time_in_day(inte,new opt_v2_schedule_planning({
                include_this_amplitude: {
                    start_minute: inter11.start_minute,
                    end_minute: inter11.end_minute
                }
            }))

            if(rpatmid.probleme) {
                console.error('echec de pause ')
                undo_what_we_do()
                return;
            }
        }

        reduce_rotation_for_wf_with_replanification({wf,date,get_rotation_score,debug_callback}){
            const old_rota_score = get_rotation_score()
            const rota = this, m = rota.main, n = rota.names, lid = wf.listInfo_by_date[date], old_nb_minutes = lid.worked
            const rot_op_nam = rota.opposite_rota.names
            const {is_rotation_opening: is_rotation_closing,force_opening_rota: force_closing_rota,not_opening_rota: not_closing_rota} = rot_op_nam
            const was_closing = wf[is_rotation_closing](date);//la personne était en fermeture
            let rota_before_not_enough_worked;
            var undoFunc_1bis = m.v2_try_a_change({person: wf,date})
            var undo_solve_not_enough_worked
            var undoFunc_1 = function(){
                undoFunc_1 = null;
                if(rota_before_not_enough_worked != void 8){
                    undo_solve_not_enough_worked()
                    if(rota_before_not_enough_worked != get_rotation_score()){
                        my_throw("L'annulation n'a pas fonctionné")
                    }
                }
                undoFunc_1bis && undoFunc_1bis()
            }
            console.iidebug[79] = idnext('79')
            if(console.iidebug[79] == 98842){
                debugger;
            }
            debug_callback()
            wf.reset_date(date,{even_if_pause_fake_protected: true})

            const first_minu_worked = lid[n.first_minu_worked]
            if(first_minu_worked){
                let is_still_rotation = wf[n.is_rotation_opening](date)
                if(is_still_rotation){
                    //la journée est encore une ouverture donc annuler
                    undoFunc_1 && undoFunc_1();
                    return {error_code: 1101}
                }
            }
            const get_opt_min_max = (was_closing)=>{
                //empêcher de remettre en ouverture
                let opt_min_max = {[n.not_opening_rota]: true}
                // si la personne etait en ouverture et qu'on la sort de l'ouverture
                // il y a un risque qu'elle aille en fermeture, il faut voir si on a le droit
                if(was_closing){
                    // il y a déjà une fermeture, ce qui veut dire que la personne travail
                    // beaucoup aujourd'hui pour être en ouverture et en fermeture
                    // cependant là on s'occupe des ouverture et pas des fermetures donc 
                    // on remet comme c'était
                    // sinon il va aussi falloir trouver un remplaçant pour la fermeture
                    opt_min_max[force_closing_rota] = true 
                }else{
                    if(!get_rotation_score.is_poss_add_rota(wf,n.closing_rot_name)){
                        // empêcher de mettre en fermeture seulement si la personne passerait au
                        // dessus de la moyenne de rotation_ratio_relative
                        // sinon on autorise si cela est mieux pour le score
                        opt_min_max[not_closing_rota] = true
                    }
                }
                return opt_min_max
            }
            const opt7 = {dates: [date],only_these_pers: [wf],get_dates_added: false,get_backup_functions: false,opt_min_max: get_opt_min_max(was_closing)}
            let nb_minutes = lid.min
            const amp_to_inc = lid.amplitudes_to_include()
            if(amp_to_inc){//à cause d'une protected ou d'une pause forcée
                opt7.include_this_amplitude = amp_to_inc
                debugger;//@TODO_fast
                const diff = opt7.include_this_amplitude.end_minute - opt7.include_this_amplitude.start_minute
                if(diff > nb_minutes){
                    nb_minutes = diff
                }
            }
            opt7.minutes_to_place = nb_minutes
            var ret_schel = m.v2_schedule_planning(opt7);
            controls_enabled(()=>{
                if(wf.getNumberOfDaysRemained()){
                    my_throw(1103)
                }
            })
            controls_enabled(()=>{
                if(old_nb_minutes != nb_minutes && wf.listInfo_by_date[date].worked == old_nb_minutes){
                    my_throw( "pk il n'y a pas de temps dispo alors qu'on a mis moins que avant")
                }
                if(get_rotation_score() >= old_rota_score){
                    my_throw( "mais pk les rotations n'ont pas été améliorées")
                }
            })
            debug_callback()
            if(console.iidebug[79] == 77603){
                debugger;
            }
            //pour empêcher de mettre en rotation ouverture
            //on doit recalculer opt_min_max mais avec was_closing = false cette fois
            rota_before_not_enough_worked = get_rotation_score()
            var snew = m.solve_not_enough_worked([wf],{
                need_undo_func: true,
                opt_min_max: {
                    do_not_make_rotation_worst: true,
                    get_rotation_score,
                }
            })
            debug_callback()
            undo_solve_not_enough_worked = snew.undo_func;

            controls_enabled(()=>{
                if(wf[n.is_rotation_opening](date)){
                    my_throw( `la personne est encore en ouverture ce n'est pas normal opt_min_max ne fait pas son job`)
                }
            })
            if(upper(wf.nb_minu_worked) < wf.nbMinutesMin){
                console.warn('Annulation car on a pas pu placer le temps restant')
                undoFunc_1 && undoFunc_1();
                return {error_code: 1100}
            }
            console.green('succes1100')
            return {success: true,undoFunc_1}
        }
        /*
         * si la classe courrante est Closing alors pour lire les commentaire de code suivant il faut inverser closing et opening
         * Réduire le nombre d'ouverture faite par le worksfor et mettre cette ouverture à quelqu'un d'autre
         */
        reduce_rotation_for_wf(wf, score, get_rotation_score,{debug_callback}) {
            const replanif_the_day = true,rota = this,rota_name = rota.name,m = rota.main;
            var echec = false,opennings_period = []
            console.iDebug14 = 1 + (console.iDebug14 || 0)
            forOf(wf.rotationMaps[rota.name], function(map) {
                if(map.length) {
                    //on récupère une intervention d'ouverture quelconque dans la journée
                    opennings_period.push(map.get_one())
                }
            })
            // maintenant que l'on a trouvé une période opening
            // il faut l'enlever comme il en a trop de ce type
            
            const iidebug8 = []
            const check_rotation = () =>  controls_enabled(()=>{
                if(score != get_rotation_score()){
                    my_throw("Le score de rotation n'a pas bien été réstoré")
                }
            })
            const next_some1 = function(){
                check_rotation()
            }
            some1: opennings_period.some((opening_period) => {
                let undoFunc_2,undoFunc_1,error_code
                //// debug_callback && debug_callback()
                var zscore_start_some = m.v2_get_zscore_global()
                var backup_week = wf.backup_week()
                echec = false
                console.iidebug[8] = idnext('8')
                iidebug8.push(console.iidebug[8])
                if(182970 == console.iidebug[8]){
                    controls_enabled(()=>{if(score != get_rotation_score()) {my_throw( 'some1_rotation_erreur') }})
                    debugger
                }

                var date = opening_period.date
                var info_someone_else,undoFunc2
                var mega_undo = function() {
                    undoFunc2 && undoFunc2()
                    undoFunc_1 && undoFunc_1()
                    controls_enabled(()=>{if(score != get_rotation_score()) {my_throw( 'some1_rotation_erreur2') }})
                };
                //pour ce cas-ci on sauvegarde la journée entière
                var adj4,inter4
                console.iidebug[14] = idnext('14')
                if(182971 ===  console.iidebug[14]){
                    debugger
                }
                check_rotation()

                // undo_what_we_do = undoFunc_1
                var last_minute_worked, last_sector;
                var rotation_ratio_wf_bef = wf.rotation_ratio[rota_name] || 0
                var lastORfirst_inter, lastORfirst_inter_info, last_sector, seria, old_sector, old_time_opening, beforeDelete, old_startORend, nb_minute_to_remove, inter4;
                // enlever le matin
                if(replanif_the_day){
                    //enlever l'ouverture à la personne
                    //debug_callback && debug_callback()
                    const rrfwwr = rota.reduce_rotation_for_wf_with_replanification({wf,date,get_rotation_score,debug_callback})
                    ;({error_code,undoFunc_1} = rrfwwr)
                    //debug_callback && debug_callback()
                    if(error_code){
                        return next_some1();
                    };
                }

                var replace_by_a_new_person = true;
                if(replace_by_a_new_person) {
                    var new_score = get_rotation_score()
                    var success
                    if(new_score > score) {
                        echec = 'le score a empiré'
                        undoFunc2 = null;
                    } else {
                        if(replanif_the_day){
                            //debug_callback && debug_callback()
                            info_someone_else = rota.v2_place_someone_else_at_this_opening({rota_name,old_wf: wf,date,replanif_the_day,get_rotation_score,debug_callback})
                            //debug_callback && debug_callback()
                            if(!info_someone_else.hasBeenInsert) {
                                echec = 'nobody else found to replace'
                            }
                            controls_enabled(()=>{
                                if(!info_someone_else.undoFunc && get_rotation_score() != new_score){
                                    my_throw("Si le score a changé il faut une fonction d'annulation pour remettre dans l'état initial si besoin")
                                }
                            })
                            undoFunc2 = info_someone_else.undoFunc
                        }
                    }
                    if(echec) {
                        console.iidebug[9] = idnext('9')
                        //on annule à la fois la personne remplaçante et la personne courante.
                        mega_undo()
                        console.error("we_have_to_undo let's try with the next person")
                    }
                }

                if(!echec) {
                    //debug_callback && debug_callback()
                    var new_score = get_rotation_score({log_table_before:true})
                    if(new_score < score) {
                        score = new_score
                        check_rotation()
                        console.iidebug[11] = idnext('11')
                        //debug_callback && debug_callback()
                        return true // break some1
                    }
                    backup_week.log_diff()
                    console.error(`C'est étrange que echec soit à false et que le score de rotation ne soit pas meilleur les personnes concernées sont ${info_someone_else.wf.wfId} et ${wf.wfId}`)
                    mega_undo()
                    const zscore_end_some = m.v2_get_zscore_global()
                    controls_enabled(()=>{
                        if(zscore_end_some != zscore_start_some){
                            console.error(`on a pas tout annulé avec mega undo si le score global n'est pas le même`)
                        }
                    })
                    console.iidebug[10] = idnext('10')
                }
                console.iidebug[12] = idnext('12')
                next_some1()
            })
            controls_enabled(()=>{
                if(score != get_rotation_score()) {
                    my_throw( 'erreur')
                }
            })
            ++console.iidebug[5];
            return {
                score
            } //le score a été réduit ou bien est resté pareil donc il faut le retourner
        }


        v2_place_someone_else_at_this_opening_old(){
            // on parcours toute les premières interventions des personnes pour voir s'il y a de la place avant
            // pour mettre l'ouverture
            // on parcourt les interventions dans l'ordre chonologique car c'est supposé réduire les dégats
            // lors de l'ajout avant et la suppression à la fin de la journée de ce que l'on a mis au début
            var hasBeenInsert = false
            var undoFunc_2;

            firstORlast_interv_and_wfId.some((firstORlast_interv_and_pers) => {
                console.iDebug15 = 1 + (console.iDebug15 || 0)
                var firstORlast_interv = firstORlast_interv_and_pers.firstORlast_interv
                //si la précédente est une pause alors prendre la pause comme intervention
                var firstORlast_interv = rota.patchPause(firstORlast_interv)
                var person = firstORlast_interv.person
                person.nb_min_minute_fakely_worked = (person.nb_min_minute_fakely_worked || 0) + 60 //debug only
                var rotation_ratio_wf_bef = person.rotation_ratio[worst_rotation] || 0
                // soit c'est le même secteur soit on vérifie qu'il restera de la place pour respecter
                // le temps minimun si on ajoute l'intervention avant
                // car on n'écrase pas ici on ajoute avant la première intervention de la personne dans la journée
                // puis on enlève à la fin ce que l'on a mis au début
                var planif1 = rota.get_start_and_end_minute_to_planif_for_substitute(firstORlast_interv, old_startORend)
                // var place_is_existing_to_place_inter = firstORlast_interv.sector == old_sector
                //     || (firstORlast_interv.endMinute - firstORlast_interv.sector.minTimeSector >= old_startORend + old_sector.minTimeSector )
                //on veut placer l'intervention de l'opening juqu'au début de inter3
                var seria_inter3 = {
                    date,
                    sector: old_sector,
                    startMinute: planif1.start_minute,
                    endMinute: planif1.end_minute,
                    dontAffectNow: true,
                    rules: {
                        dontMoveExistingInterventionOrPause: true, // on ajoute avant il n'y a aucune raison de bouger une autre intervention
                        v2_do_not_respect_min_time_after_before: true, // on le gère déjà
                        v2_break_if_min_time_not_ok: true // on ne veut pas des ouverture de 30 minutes ça doit respecter le temps minimal dans le même secteur
                    }
                }
                var new_open_inter = new Intervention(seria_inter3)
                //si l'intervention a des chances d'être ajoutée alors faire d'abord une sauvegarde de la journée
                undoFunc_2 = null; //il ne faudrait pas annuler une chose déjà annulé
                new_open_inter.before_add = () => {
                    undoFunc_2 = m.v2_try_a_change({
                        date,
                        person,
                        // startMinute: -48 * H,// 0 n'est pas suffisemment petit car la journée pourrait commencé à -2h pour 22h de la veille
                        // endMinute: 48 * H, //24 n'est pas suffisant car une journée peut débordé sur le lendemain
                    })
                }
                var adi_new_open_inter = person.addInterv(new_open_inter)
                var new_open_inter_startMinute = new_open_inter.startMinute
                var new_open_inter_endMinute = new_open_inter.endMinute
                hasBeenInsert = adi_new_open_inter.has_been_insert
                var rotation_ratio_wf_aft = person.rotation_ratio[worst_rotation] || 0
                var end_msg = ' ' + (hasBeenInsert ? ' success' : ' failed')
                var msg = 'Affectation de la personne remplaçante ' + person.wfId + ' à ' + worst_rotation +
                ' de ' + new_open_inter.startMinute + ' à ' + new_open_inter.endMinute +
                ' le ratio de rotation est ainsi passé de ' + rotation_ratio_wf_bef +
                ' à ' + rotation_ratio_wf_aft +
                end_msg
                console.log(msg, new_open_inter)
                if(hasBeenInsert) {
                    // il faut supprimer autant que l'on a ajouté dans cette journée,
                    // on désaffecte en fin de la journée si Opening et Début si Closing
                    var lastORFirst_inter = rota.get_the_far_interv_of_same_day(new_open_inter)

                    /* @TODO: DELETE */
                    if(!lastORFirst_inter) {
                        var lastORFirst_inter = rota.get_the_far_interv_of_same_day(new_open_inter)
                    }

                    var nb_minutes = (seria_inter3.endMinute - seria_inter3.startMinute)
                    var unplanif = rota.get_start_and_end_minute_to_unplanif_for_substitute(lastORFirst_inter, nb_minutes)
                    //c'est une intervention de désafectation
                    var desa = new Intervention({
                        // endMinute: unplanif.endMinute,
                        // startMinute: unplanif.endMinute  - (seria_inter3.endMinute - seria_inter3.startMinute),
                        endMinute: unplanif.end_minute,
                        startMinute: unplanif.start_minute,
                        sector: null, //on désaffecte grace au secteur null
                        date,
                        // type: 'UNAFFECT',
                        dontAffectNow: true,
                        rules: {
                            // on le gèrera après que ce qu'il reste ne soit plus assez grand
                            // on le gèrera en même temps que la pause
                            v2_do_not_respect_min_time_after_before: true
                        }
                    })
                    // person.nb_min_minute_fakely_worked -= 10//debug only

                    var adi_desa = person.addInterv(desa)
                    console.log('UNAFFECT de la personne remplaçante ' + person.wfId + ' ' +
                        ' de ' + unplanif.start_minute + ' à ' + unplanif.end_minute +
                        (adi_desa.has_been_insert ? ' success' : ' failed'), desa)
                    hasBeenInsert = adi_desa.has_been_insert;
                    if(hasBeenInsert) {
                        var prev = rota.prev()
                        var it_prev = desa[prev];
                        var prev_sector = it_prev[prev] && it_prev[prev].sector
                        if(prev_sector && !it_prev.v2_respectMinTime()) {
                            // dans le cas ou la première intervention serait trop petite et que cela ne peut pas
                            // être réglé en décalant la pause car il n'y a pas de pause après
                            // alors changer le secteur de l'intervention
                            var inter9 = it_prev.v2_clone()
                            inter9.sector = prev_sector
                            inter9.rules = {
                                v2_do_not_extend_workstime: true // ne pas changer les horaires travaillés
                            }
                            var adi_inter9 = person.addInterv(inter9)
                            hasBeenInsert = adi_inter9.has_been_insert
                        }

                        if(hasBeenInsert) {
                            var rpatmid =  m.resolve_pause_and_time_in_day(desa,new opt_v2_schedule_planning({
                                include_this_amplitude: {
                                    start_minute: new_open_inter_startMinute,
                                    end_minute: new_open_inter_endMinute
                                }
                            }))

                            if(rpatmid.probleme) {
                                hasBeenInsert = false
                                console.log('i_debug16', console.iDebug16)
                                console.warn('undo because of pause lets try with the next person')
                                // return //STOP_LOOP
                            }else if(!desa.is_day_ok({
                                ignore_working_time: true
                            })){
                                hasBeenInsert = false
                                //si le début et la fin de la journée ne sont pas bon alors annuler
                                console.warn('à cause des début et fin de journée ou doit annuler le changement de rotation')
                            }
                        }
                    }
                    if(!hasBeenInsert) {
                        undoFunc_2()
                        console.log('ANNULATION')
                    }
                }
                person.nb_min_minute_fakely_worked -= 10 * H //debug only
                return hasBeenInsert; //stop the some function
            }) //end_some
            
        }

        /*
         * Placer quelqu'un à l'opening pour remplacer le "old_wf" qui a enlevé son intervention à "date"
         * et "old_start" du secteur "old_start"
         *
         * old_wf: la personne qui était à l'intervention que l'on a enlevée, on a besoin de le savoir pour ne pas mettre
         * date: // date de l'intervention que l'on a enlevé, c'est aussi celle on l'on va ajouter
         * replanif_the_day: c'est le nouveau mode qui replanifie la journée de ce que l'on modifie
         */
        v2_place_someone_else_at_this_opening({
            old_wf, 
            date, 
            get_rotation_score,
            replanif_the_day,
            debug_callback 
        }) {
            const init_score_rot = get_rotation_score();
            const refF = () => {
                info_someone_else = {
                    hasBeenInsert:error_code === 0,
                    error_code,
                    wf: wf_to_return,
                    undoFunc: error_code === 0 ? mega_undo : null //si on a déjà annulé à quoi bon transférer la fonction d'annulation
                } //si c'est faux c'est que l'on a parcouru toute les personne de la journée et qu'aucune ne pouvait déplacer la fin au début de la journée
                controls_enabled(()=>{
                    if(!info_someone_else.undoFunc && get_rotation_score() != init_score_rot){
                        my_throw("Si le score a changé il faut une fonction d'annulation pour remettre dans l'état initial si besoin")
                    }
                })
                return info_someone_else
            }
            var mega_undo = function(){
                undo_solve_not_enough_worked2 && undo_solve_not_enough_worked2()
                if(!undoFunc_2){
                    controls_enabled(()=>{
                        my_throw('undoFunc_2 devrait existé')
                    })
                    console.log(info_someone_else)
                }else{
                    undoFunc_2()
                }
                controls_enabled(()=>{
                    if(get_rotation_score() != init_score_rot){
                        my_throw('La restauration a raté pour undoFunc_2')
                    }
                })
            }
            console.iidebug[82] = idnext('82')
            const rota = this, m = rota.main, n = rota.names;
            var undoFunc_2,error_code,undo_solve_not_enough_worked2, info_someone_else
            console.iDebug_v2_place_someone_else_at_this_opening = idnext('seato');
            let success_replacement = false; //est ce que l'on a pu trouver une personne à mettre à cette ouverture
            // Il faut maintenant trouver une autre personne à décaller
            // pour prendre la place de la personne que l'on a enlever
            // on va chercher la personne la plus proche dans le secteur courrant
            var person_that_cant_be_at_opening = new Set()
            // il ne faut pas que la personne que l'on prend soit au dessus de la moyenne pour les rotations
            // ou au dessus de nous
            m.all_wfs.forEach(function(wf2) {
                if(180212 == console.iidebug[82] && wf2.wfId == 10417){
                    debugger
                }
                //peut on ajouter une ouverture à la personne
                if(wf2[n.is_rotation_opening](date) || !get_rotation_score.is_poss_add_rota(wf2,rota.name)){
                    person_that_cant_be_at_opening.add(wf2)
                }
                //si la personne est en fermeture et qu'on la met en ouverture
                //on va lui enlever une fermeture donc il faut aussi regarder si
                //on a le droit de faire ça
                const is_rotation_closing = rota.opposite_rota.names.is_rotation_opening
                if(wf2[is_rotation_closing](date)){
                    if(!get_rotation_score.is_poss_del_rota(wf2,n.closing_rot_name)){
                        person_that_cant_be_at_opening.add(wf2)
                    }
                }
            })

            // on boucle sur les intervention de la periode de ce secteur
            // et on regarde si les personnes dans ces secteurs peuvent être avancer dans la journée
            // pour faire l'ouverture
            const firstORlast_interv_and_wfId = []
            m.all_wfs.forEach(function(wf3) {
                var wfId = wf3.wfId
                if(!person_that_cant_be_at_opening.has(wf3)) {
                    var inter = rota.get_firstORlast_interv_with_sector_at_date(wf3, date)
                    if(inter) {
                        firstORlast_interv_and_wfId.push({
                            firstORlast_interv: inter,
                            wf_id: wfId
                        })
                    }
                }
            })
            //Dans le cas de openning on met dans l'ordre des heures sinon l'inverse
            rota.sort_and_put_nearest_interv_first(firstORlast_interv_and_wfId) 

            let wf_to_return;
            if(!replanif_the_day){
                rota.v2_place_someone_else_at_this_opening_old()
            }else{
                debug_callback && debug_callback()
                var itera = (wf_inter)=>{
                    console.iidebug[89] = idnext('89')
                    if(202314 == console.iidebug[89]){
                        debugger
                    }
                    const wf = wf_inter.firstORlast_interv.person
                    const lid = wf.listInfo_by_date[date]
                    undoFunc_2 = m.v2_try_a_change({person:wf,date})
                    wf.reset_date(date,{
                        even_if_pause_fake_protected: true
                    })
                    const opt_min_max = {
                        [n.force_opening_rota]: true
                    }
                    var opt15 = {
                        dates: [date],
                        only_these_pers: [wf],
                        get_dates_added: false,
                        get_backup_functions: false,
                        opt_min_max
                    }
                    let nb_minutes = lid.min
                    const amp_to_inc = lid.amplitudes_to_include()
                    if(amp_to_inc){//à cause d'une protected                       debugger;//@TODO_fast
                        const diff = opt7.include_this_amplitude.end_minute - opt7.include_this_amplitude.start_minute
                        if(diff > nb_minutes){
                            nb_minutes = diff
                        }
                    }
                    opt15.minutes_to_place = nb_minutes;
                    const sp = m.v2_schedule_planning(opt15)
                    if(!sp.date_ok[date]){
                        undoFunc_2()
                        error_code = 1301
                    }else{
                        error_code = 0
                        const snew = m.solve_not_enough_worked([wf],{
                            opt_min_max: {
                                do_not_make_rotation_worst: true,
                                get_rotation_score
                            },
                            need_undo_func: true
                        })
                        undo_solve_not_enough_worked2 = snew.undo_func
                        if(upper(wf.nb_minu_worked) != wf.nbMinutesMin){
                            console.error('solve not enougth worked ne fonctionnne pas')
                            mega_undo()
                            error_code = 1302
                        }
                        if(!error_code){
                            wf_to_return = wf
                        }
                    }
                    return error_code === 0//on a fait un changement c'est suffisant pour le moment
                }

                special_some(firstORlast_interv_and_wfId,itera, (hasBeenInsert_0)=>{
                    const info_someone_else2 = refF()
                    debug_callback && debug_callback()
                    if(get_rotation_score() != init_score_rot && error_code){
                        my_throw('Le score a changé sans raison iidebug[82]')
                    }
                })
            }
            debug_callback && debug_callback()
            if(error_code){
                console.error(error_code)  
            } 
            info_someone_else = refF()
            return info_someone_else
        }
    }
    return OpeningClosingCommon
}