'' + function(){
    /**
     * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
     */
    let Debug = {
        // utile pour comparer l'historique avec le mode débug et simulate_serveur
        // il faut que ce soit le même
        histo_function: false,
        iidebug_history: [],

        //nécessaire pour connaitre l'historique des interventions
        slow_debug: v8js ? false : false,//ne changer que après les : pas avant sinon ralentit le serveur
        // is_enabled: true,
        wf_id: [10606],
        date: '2018-03-26',
        between_start: 20 * H,
        between_end: 24 * H,
        interventions: {},
        timers: {

        },
        number_of_call: [],
        current_function: [],
        // contient l'id du moment ou on est passé d'un autre secteur à celui-ci
        // intervention commençant à une heure donnée pour un secteur donnée
        // ex: 10575_600: [{sec: 2522,aid:10}] pour 10h (10*60)
        // on ignore les restaurations d'intervention
        // car elle ne crée pas de situation elle ne font que les restaurer 
        // et ne peuvent pas être la cause d'un problème 
        change_to_sector :{},
        // dbg(i){
        //     return console.iidebug[i] = idnext('i')
        // },
        checker(obj) {
            // var inter = Debug.getWf(10416).get_first_interv_at_date('2018-03-29')
            // while (inter) {
            //     if(inter.is_pause() && inter.next && !inter.next.sector) {
            //         //////debugger
            //     }
            //     inter = inter.next
            // }
        },
        check_rotation: function(wf){
            controls_enabled(()=>{
                if(!wf){
                    main.all_wfs.forEach(function(wf){
                        if(!wf){
                            my_throw('error_check_rotation')
                            Debug.check_rotation(wf)
                        }
                    })
                    return;
                }
                forOf(wf.rotationMaps,(obj,rota_name)=>{
                    var nb_rota = 0;
                    forOf(obj,(map,key/*date*/)=>{
                        nb_rota += map.length > 0
                    })
                    if(nb_rota +  (wf.rotation_initial[rota_name]||0) != wf.rotation[rota_name]){
                        my_throw( 'le compteur de rotation ne fonctionne pas')
                    }
                })
            })
        },
        is_round(hour){
            controls_enabled(()=>{
                if(lower(hour) != hour){
                    console.log(lower(hour),hour)
                    my_throw( 'le temps demandé est mal arrondi')
                }
            })
            return true
        },
        getWf(wf_id) {
            var wf;
            main.all_wfs.some((wf1) => {
                if(wf1.wfId == wf_id) {
                    wf = wf1
                }
            })
            return wf
        },
        checkTime() {
            // if(Debug.getWf(10575).nb_minu_worked == 33.5) {
            //     console.log('là')
            // }
        },
        //@logDayWf
        logDayWf(inter_or_wf, date,recursive_call) {
            var endF = function(){
                if(!recursive_call){
                    console.log(`total dans la semaine ${wf.nb_minu_worked}`)
                    console.warn(console.iDebug)
                }
            }
            var d = this
            if(v8js) return;
            var table = []

            if(inter_or_wf instanceof Intervention) {
                ir = inter_or_wf.first_interv
                wf = ir.person
            } else {
                var wf = inter_or_wf
                if(isFinite(wf) || typeof wf == 'string') {
                    wf = Debug.getWf(+wf)
                }
                if(!date) {
                    var str = ''
                    wf.intervsRacines.forEach((inter4) => {
                        str += '[' + inter4.listInfo.first_minu_worked + '_' + inter4.listInfo.last_minu_worked + '] \n'
                        d.logDayWf(inter4,null,true)
                    })
                    endF()
                    console.log(str)
                    return;
                }
                var ir;
                wf.intervsRacines.forEach(function(ir1) {
                    if(ir1.date == date) {
                        ir = ir1
                    }
                })
            }

            let sum_unplaced_time = 0, sum_time = 0;
            if(ir) {
                console.log(`Debug.logDayWf('${ir.person.wfId}','${ir.date}')//day_minu_worked = ${wf.listInfo_by_date[ir.date].worked}`)
                controls_enabled(()=>{
                    if(ir.next) {
                        if(ir.next.prev != ir) {
                            my_throw( 'we have a probleme')
                        }
                    }
                    if(ir.prev) {
                        if(ir.prev && ir.prev.next != ir) {
                            my_throw( 'we have a probleme')
                        }
                    }
                })
                // console.log(date)
                var i = 0;
                while (ir) {
                    var minOk = ir.v2_respectMinTime()
                    if(!minOk) {
                        console.error('not min ok')
                        ////////////debugger
                    }
                    let unplaced_time = ir.unplaced_time()
                    table.push({
                        sect: ir.is_pause() ? 'PAUSE' : ir.sector ? ir.sector.id : ' None',
                        start: ir.startMinute,
                        end: ir.endMinute,
                        // date: ir.date,
                        addIntervDebug: ir.addIntervDebug,
                        interv: ir,
                        protected: ir.protected,
                        minTOk: minOk,
                        ori_addIntervDebug: ir.ori_addIntervDebug,
                        updateDebugId: ir.updateDebugId,
                        time: ir.time,
                        unplaced_time,
                        tot_time: unplaced_time + ir.time,
                        hasBeenInsert: ir.hasBeenInsert
                    })
                    // console.warn('sect: ' + (ir.sector ? ir.sector.id : ' None')
                    //     + ' st ' +ir.startMinute + ' end '+ ir.endMinute
                    //     )
                    if(ir.is_counted_in_worktime()){
                        sum_unplaced_time += unplaced_time
                        sum_time += ir.time
                    }
                    ir = ir.next
                }
            }
            table.push({
                sect: 'TOTAL',
                start: 0,
                end: 24,
                // date: ir.date,
                addIntervDebug: 0,
                interv: 0,
                protected: 0,
                minTOk:0,
                ori_addIntervDebug: 0,
                updateDebugId: 0,
                time: sum_time,
                unplaced_time: sum_unplaced_time,
                tot_time: sum_time + sum_unplaced_time
            })
            console.table(table)
            endF()
            if(console.iDebug == 61116) {
                ////////////////debugger
            }
        },
        perfStart: function(name) {
            controls_enabled(()=>{
                if(!name){
                    my_throw('Le name est obligatoire')
                }
                this.number_of_call[name] = fl(this.number_of_call[name]) + 1
                this.current_function.push(name)
                Debug.timers[name] = fl(Debug.timers[name]) - (new Date()).getTime()
            })
        },
        perfEnd: function(name, debugInfo) {
            controls_enabled(()=>{
                var current_function = this.current_function.pop(name)
                if(current_function != name) {
                    console.error(debugInfo);
                    console.error(`you can't close this func because, it is not the last open ${current_function}`)
                    my_throw('')
                }
                Debug.timers[name] += (new Date()).getTime()
            })
        },
        get_inter_history(inter) {
            var intervs = []
            var ids_done = {}
            for (var old = inter.old_inter; old && ids_done[old.addIntervDebug] !== old; old = old.old_inter) {
                ids_done[old.addIntervDebug] = old;
                intervs.push(old)
            }
            return {
                intervs,
                inter_addIntervDebug: Object.keys(ids_done).map((v) => +v)
            }
        },
        get_interv_to_flush(wf, date) {
            var jsIntervArr = JSON.parse(main.getAllIntervToFlush())
            var filter2 = jsIntervArr.filter((i) => {
                return (!date || i.startDate == date) && (!wf || i.wfId == wf)
            })
            return filter2
        },
        inc_error_code(code) {
            if(Debug.is_not_test) {
                Debug.error_code[code] = (Debug.error_code[code] || 0) + 1
                if(code == 'success') {
                    var debug = 2;
                }
            }
        },
        rapport_error_code() {
            if(important_errors.size){
                console.error("Des erreurs importantes ont eu lieu désactiver do_not_my_throw_error pour générer les throws", important_errors)
            }
            console.log('erreurs les plus fréquente lors de l ajout d une intervention', Debug.error_code)
            console.log('erreurs les plus fréquente lors de l ajout du temps manquant', Debug.error_codes_place_missing_minutes_wf)
        },
        check_not_enough_worked(wf,strict){
            controls_enabled(()=>{
                if((strict ? wf.nb_minu_worked : upper(wf.nb_minu_worked)) < wf.nbMinutesMin){
                    console.error('la personne ne travaille pas assez')
                    // my_throw( 'la personne ne travaille pas assez')
                }
            })
        },
        check_too_much_worked(wf,iidebug){
            controls_enabled(()=>{
                var msg = ''
                if(iidebug){
                    msg += `console.iidebug[${iidebug}] ==  ${console.iidebug[iidebug]}`
                }
                if(wf.nb_minu_worked > wf.nbMinutesMin){
                    my_throw( 'la personne travaille trop' + msg)
                }
            })
        },
        check_nb_days_planif(wf,iidebug){
            controls_enabled(()=>{
                if(wf.getNumberOfDaysRemained()){
                    var msg = ''
                    if(iidebug){
                        msg += `console.iidebug[${iidebug}] ==  ${console.iidebug[iidebug]}`
                    }
                    my_throw( "des jours ne sont pas placés" + msg)
                }
            })
        },
        check_not_enough_worked_all_wfs(m,strict){
            controls_enabled(()=>{
                m.all_wfs.forEach((wf)=>{
                    if(wf.getNumberOfDaysRemained()){
                        my_throw( (wf.wfId + " a des jours qui ne sont pas placés"))
                    }
                    Debug.check_not_enough_worked(wf,strict)
                })
            })
        },
        check_too_much_worked_all_wfs(m){
            controls_enabled(()=>{
                m.all_wfs.forEach((wf)=>{
                    Debug.check_too_much_worked(wf)
                })
            })
        },
        check_intervention(m){
            m.all_wfs.forEach((wf)=>{
                wf.intervsRacines.forEach((inter)=>{
                    while(inter){
                        if(inter.endMinute <= inter.startMinute){
                            my_throw( "Ce n'est pas normal")
                        }
                        inter = inter.next
                    }
                })
            })
        },
        check_protected_still_present(){
            // if(!Debug.addInter_running){
            //     var wf = main.wfMap[10324]
            //     var inter = wf.listInfo_by_date['2018-03-26'].get_inter_with_startMinute(840)
            //     if(!inter.true_protected_information){
            //         my_throw( 'ça devrait être une absence')
            //     }else{
            //         if(inter.startMinute != inter.true_protected_information.start_minute
            //             || inter.true_protected_information.end_minute != inter.endMinute
            //         ){
            //             debugger;
            //         }
            //     }
            // }
        }
    }
    Debug.log_period_info = function(date,start_minute,end_minute){
        console.log(`Debug.log_period_info('${date}',${start_minute},${end_minute})`)
        var table = []
        var poses = main.getPosArrInArray(date, start_minute, end_minute)
        var sum_inf = {}
        var f4 = (amp2,obj,is_sum,id_ent)=>{
            if(id_ent){
                var id_sector = id_ent
            }else{
                var id_sector = amp2.v2_sector && (typeof amp2.v2_sector == 'string') ? amp2.v2_sector.id : 'global'
            }
            let zsc = ('zscor' in amp2) ? amp2.zscor : amp2.v2_zscore_period()
            let attr = ('attra' in amp2) ? amp2.attra : amp2.v2_attractivity()
            if(!is_sum){
                sum_inf[id_sector] = sum_inf[id_sector] || {zscor: 0, attra:0,need:0,have:0} 
                sum_inf[id_sector].zscor += zsc
                sum_inf[id_sector].attra += attr
                sum_inf[id_sector].need +=  amp2.need
                sum_inf[id_sector].have += amp2.have
            }
            obj[`${id_sector}_need`] = amp2.need
            obj[`${id_sector}_have`] = amp2.have
            obj[`${id_sector}_attra`] = attr
            obj[`${id_sector}_zscor`] = zsc
        }
        poses.forEach((pos)=>{
            let amp = main.total_info_amp[pos]
            var zsc = amp.v2_zscore_period()
            var attr = amp.v2_attractivity()
            obj = {
                dateKey: amp.dateKey,
                pos: pos
            }
            f4(amp,obj,false,'global')
            table.push(obj)
            amp.periods.forEach(function(amp2){
                f4(amp2,obj,false,amp2.v2_sector.id)
            })
        })
        var obj = {
            dateKey: 'TOTAL'
        }
        forOf(sum_inf,(fake_amp, id_ent)=>{
            fake_amp.v2_sector = id_ent
            f4(fake_amp,obj,true,id_ent)
        })
        table.push(obj)
        console.table(table)
    }
    Debug.error_code = {}
    Debug.reload = function() {
        try {
            console.iDebug = -1; //générer une erreur
        } catch (e) {}
        window.location.reload()
    }
    // Debug.showNeedHave = function(sector, start_date_key, end_date_key) {
    //     var by_sector = {
    //         2520: [],
    //         2521: [],
    //         2522: []
    //     }
    //     planifObj.all_amp_open.forEach(function(amp) {
    //         amp.periods.forEach(function(per) {
    //             if((!start_date_key || per.dateKey >= start_date_key) && (!end_date_key || per.dateKey <= end_date_key)) {
    //                 by_sector[per.v2_sector.id].push({
    //                     dateKey: per.dateKey,
    //                     need: per.need,
    //                     have: per.have,
    //                     zscoreAllSector: amp.v2_sum_zscore_on_period(),
    //                     zscoreSector: per.v2_zscore_period()
    //                 })
    //             }
    //         })
    //     })
    //     console.table(by_sector[sector])
    // }

    Debug.check_all_wfs_pause = (main)=>{
        controls_enabled(()=>{
            main.all_wfs.forEach(function(wf){
                wf.listInfo_sorted.forEach((li)=>{
                    let fis = li.first_interv_sector
                    if(fis){
                        let pause_is_present = false
                        for(;fis;fis = fis.next){
                            if(fis.is_pause()){
                                pause_is_present = true
                                break;
                            }
                        }
                        if(!pause_is_present){
                            // Debug.logDayWf(wf,li.date)
                            console.error(`il manque une pause pour la personne ${wf.wfId} le ${li.date} `)
                        }
                    }
                })
            })
        })
       
    }
    return Debug
}