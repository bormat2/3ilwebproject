/**
 * Pour que la fonction ait accès aux variables exterieurs on utilisera eval()
 */
'' + function(){
    // @Saturday
    class Saturday extends Rotation {

        constructor(main) {
            super(main)
            this.tolerance = 1
        }

        inc_r(wf,o,plusMoins1){
            var ret = super.inc_r.apply(this,arguments)
            controls_enabled(()=>{
                var date = o.period.date
                if(wf.main && wf.main.is_test == false && wf.rotationMaps[date].length && !wf.is_worked(date)){
                    my_throw( 'pourquoi il y a des éléments si la personne ne travaille pas ce samedi')
                }
            })
            return ret
        }
        get_key(o) {
            return o.period.date
        }
        is_rotation(o) {
            return (new Date(o.period.date)).getDay() == 6
        }

        /* @todo factoriser avec main.get_args_for_schdl_plan*/
        get_args_for_schdl_plan(wf,dates_to_remove,except_eaonp = []){
            console.iidebug[72] = idnext('gafsp')
            if(55634 == console.iidebug[72]){
                // debugger;
            }
            var t;
            var days_worked = {},exact_amplitude_or_not_planified = {},forced_ampli = {}
            ;dates_to_remove.forEach(function(date){
                console.iidebug[73] = idnext('gafsp'+wf.wfId+'__'+date)
                t = wf.listInfo_by_date[date]
                if(t && t.is_worked()){
                    days_worked[date] = {
                        worked : t && t.worked,
                        first_minute_worked: t && t.first_minu_worked,
                        last_minute_worked: t && t.last_minu_worked
                    };
                    var key = t.first_minu_worked + '_'+t.last_minu_worked
                    forced_ampli[key] = forced_ampli[key] || {
                        start_minute : t.first_minu_worked,
                        end_minute : t.last_minu_worked,
                        nb_to_place: 0,
                        nb_minutes: t.worked
                    }
                    ++forced_ampli[key].nb_to_place
                    //le samedi est interdit donc ça ne peut pas être une date
                    //sur laquel il faudrait restaurer les heures
                    if(!except_eaonp.has(date)){
                        var eaonp = exact_amplitude_or_not_planified[date] = clone(days_worked[date] ,{})
                        eaonp.date = date 
                    }
                }
            })

            forOf(exact_amplitude_or_not_planified,(eaonp,date) =>{
                var [undof, success_remove] = wf.reset_date_undofunc(date)
                if(success_remove){
                    eaonp.place_like_before = undof
                    controls_enabled(()=>{
                        if(eaonp.place_like_before instanceof Function == false){
                            my_throw( 'not normal')
                        }
                    })
                }else{
                    delete exact_amplitude_or_not_planified[date]
                }
            })

            var undo_modif = (ret_schel)=>{
                if(ret_schel){
                    ret_schel.backup_functions.forEach((undo_func)=>{
                        undo_func()
                    })
                }else{
                    forOf(exact_amplitude_or_not_planified,(eaonp,date) =>{
                        eaonp.place_like_before()
                    })
                }
            }
            return {undo_modif,days_worked,exact_amplitude_or_not_planified,forced_ampli}
        }

        /**
         *  On va déplanifier du vendredi au dimanche interdir de planifier
         *  le samedi et replanifier la semaine de sorte qu'il y ait deux jours 
         *  consécutif de repos
         */
         unplanif_week_end_and_replanif_week(wf,satur_date,get_rotation_score,score){
            var rota = this;var score_begin_func;
            console.iidebug[51] = idnext('51'+'_'+wf.wfId)
            if( console.iidebug[51]  == 48178){
                Debug.logDayWf(wf)
                debugger
            }
            const rotations_scores = [get_rotation_score()]
            controls_enabled(()=>{
                score_begin_func = get_rotation_score()
                if(score_begin_func != score){
                    my_throw( `le score n'est pas à jour`)
                }
            })
            var ret_schel,undo_all,date_added_wf1,t,m = wf.main,saturday_removed = null,
            ret = () => {
                controls_enabled(()=>{
                    if(get_rotation_score() != score){
                        my_throw( 'comment ça il est pas à jour !!!')
                    }
                })
                return {
                    saturday_removed,
                    undo_all,
                    date_added_wf1,
                    score
                }                
            }
            ;
            //récupération de la première intervention du samedi travaillé
            var saturday_inter = wf.get_first_interv_at_date(satur_date)
            //on ne veut pas planifier le jour que l'on déplanifie
            var forbidden_dates = {[satur_date]:true};
            //enregistrer le score de rotation le but est de réduire sa valeur
            console.iidebug[2] = idnext('2')
            var days_worked = {}

            //vérifions que les jours autorisés permettent de déplacer le samedi
            var nb_days_possible_to_work = 0
            var nb_days_worked = 0;
            wf.listInfo_sorted.forEach(function(li){
                nb_days_possible_to_work += li.first_interv ? 1 : 0
                nb_days_worked += li.is_worked() ? 1 : 0
            })
            if(nb_days_possible_to_work - nb_days_worked <= 0){
                return ret()
            }
            rotations_scores.push(get_rotation_score())
            rotations_scores.push(get_rotation_score())
            //différentes amplitudes qui sont autorisés indépendemment des jours, 
            //les autres sont interdites, pour chacune dire combien de fois exactement
            //elle doit être placée
            // var forced_ampli = {}
            var date = new Date(satur_date)
            date.setDate(date.getDate() - 1)
            var friday_date = Main.format(date)
            //+2 et pas plus 1 car on était passé au vendredi
            date.setDate(date.getDate() + 2)
            var sunday_date = Main.format(date)
            
            var nb_minu_worked = wf.nb_minu_worked
            var  {undo_modif,days_worked,forced_ampli,exact_amplitude_or_not_planified} = rota.get_args_for_schdl_plan(wf,[friday_date, satur_date,sunday_date],[satur_date])
            
            var [undo_wf_saturday, saturday_removed] = wf.reset_date_undofunc(satur_date)
            if(!saturday_removed){
                return ret()
            };

            if(wf.is_worked(friday_date) && wf.is_worked(sunday_date) ){
                undo_modif()
                undo_wf_saturday()
                saturday_removed = false
                return ret()
            }
            rotations_scores.push(get_rotation_score())

            // Maintenant planifions cette personne sur les journées qui 
            // en ont le plus besoin en interdisant le samedi.
            // Le vendredi ou le dimanche peuvent être replanifier mais pas les deux sauf 
            // si il y a déjà deux jours consécutifs dans la semaine d'absence
            ret_schel = m.v2_schedule_planning({
                forbidden_dates,
                //Si la journée que l'on veut planifié est dans exact_amplitude 
                //alors remettre la journée tel qu'on la connaissait
                exact_amplitude_or_not_planified, 
                //différentes amplitudes qui sont autorisés indépendemment des jours, 
                //les autres sont interdites, pour chacune dire combien de fois exactement
                //elle doit être placé
                forced_ampli, 
                only_these_pers: [wf],
                get_dates_added: true,
                get_backup_functions: true,
                do_not_planif_on_planified_days: true
            });
            rotations_scores.push(get_rotation_score())

            var undo_all = ()=>{
                rotations_scores.push(get_rotation_score())

                undo_modif(ret_schel)
                rotations_scores.push(get_rotation_score())
                undo_wf_saturday()
                saturday_removed = false
                rotations_scores.push(get_rotation_score())

                controls_enabled(()=>{
                    if(score_begin_func != get_rotation_score()){
                        Debug.logDayWf(wf)
                        msg = `console.iidebug[51] ==  ${console.iidebug[51]}`
                        my_throw( 'error score ' + msg)
                    }
                })
            }
            if(!ret_schel.success){
                undo_all()
                return ret()
            }
            // debugger;

            controls_enabled(()=>{
                if(wf.nb_minu_worked != nb_minu_worked){
                    console.warn("le nombres d'heure à changé si cela est du a une absense protégé c'est possible que ce soit normal")
                }
            })

            //Il faut savoir si on a planifié le Samedi ou bien le Dimanche
            ;[friday_date,sunday_date].some(function(date){
                //si avant journée non travaillée et maintenant jour travaillé
                if(!days_worked[date] && (t = wf.listInfo_by_date[date]) && t.is_worked()){
                    date_added_wf1 = date
                    return STOP_LOOP
                }
            })
            //comment on a réussi le score de rotation doit être mis à jour
            score = get_rotation_score()
            return ret()
        }

        add_this_wf_saturday(wf2,satur_date,date_added_wf1,score,get_rotation_score){
            var m = wf2.main,rota = this;
            console.iidebug[0] = idnext('0_'+wf2.wfId)
            Debug.check_too_much_worked(wf2)
            if(48178 == console.iidebug[0]){
                debugger
                Debug.logDayWf(wf2)
            }
            var other_person_for_saturday_found = false, undo_wf2,
            ret = function(){
                    // debugger
                    controls_enabled(()=>{
                        if(score != get_rotation_score()){
                            my_throw( 'anormal')
                        }
                    })
                    return {
                        other_person_for_saturday_found,
                        undo_wf2,
                        score
                    }
                }
                ;
            // Est ce que la personne wf2 pourrait travailler le samedi à la place de wf1,
            // bien sur il faut qu'elle ne soit pas déjà présente ce jour là
            var d = wf2.listInfo_by_date[satur_date]
            if(!d || d.is_worked()) {
                // debugger
                return ret()
            }

            //Comme on va la mettre présente le samedi cela risque de cassez les deux jours
            //successif de repos on va donc d'abord déplanifier les jours qui sont voisins d'un
            //jour de repos comme cela on est sûr d'avoir deux jours successif
            var undo_funcs_days_removed = []
            var li_s = wf2.listInfo_sorted
            var days_to_remove = []
            // li_s.forEach((li,i)=>{
            //     if(li_s[i-1] && !li_s[i-1].worked) days_to_remove.add(li.date)
            //     if(li_s[i+1] && !li_s[i+1].worked) days_to_remove.add(li.date)
            // })
            
            //on déplanifie la semaine entière de sorte à ce que les 
            //2 jours d'absences consécutif puisse être respecté
            li_s.forEach((li,i)=>{
                if(li.first_interv){
                    days_to_remove.push(li.date)
                }
            })
            var nb_minu_worked = wf2.nb_minu_worked
            //reset les journées days_to_remove
            var gafsp = rota.get_args_for_schdl_plan(wf2,days_to_remove)
            var {undo_modif,days_worked,forced_ampli,exact_amplitude_or_not_planified} = gafsp
            var undo_all = ()=>{
                debugger;
                console.error('mince on annule')
                undo_modif()
            }

            //La personne ne va pas forcément travaillé le samedi
            //Mais seulement y travailler si c'est nécessaire
            var ret_schel = m.v2_schedule_planning({
                forced_date: [satur_date],//forcer à placer ce jour-ci
                // forbidden_dates,
                //Si la journée que l'on veut planifié est dans exact_amplitude 
                //alors remettre la journée tel qu'on la connaissait
                exact_amplitude_or_not_planified, 
                //différentes amplitudes qui sont autorisés indépendemment des jours, 
                //les autres sont interdites, pour chacune dire combien de fois exactement
                //elle doit être placé
                forced_ampli, 
                only_these_pers: [wf2],
                get_dates_added: true,
                get_backup_functions: true,
                do_not_planif_on_planified_days: true
            });
            // alert(console.iidebug[55] )
            var undo_all = ()=>{
                undo_modif(ret_schel)
                // ret_schel.backup_functions.forEach((undo_func)=>{
                //     undo_func()
                // })
                // undo_wf_saturday()
                // if(score != get_rotation_score()){
                //     my_throw( 'error score')
                // }
            }
            if(!ret_schel.success){
                undo_all()
                return ret()
            }
            // //Si le nombre d'heure travaillées n'a pas évolué c'est que l'on a pas pu ajouter
            // if(wf2.nb_minu_worked == nb_minu_worked) {
            //     undo_wf2(2);return ret()
            // }
            // debugger;
            other_person_for_saturday_found = true
            score = get_rotation_score()
            console.iidebug[71] = idnext('0'+wf2.wfId+score)
            return ret();
        }

        reduce_rotation_for_wf(wf, score, get_rotation_score,others) {
            Debug.check_too_much_worked_all_wfs(wf.main)
            var rota = this,undo_all,date_added_wf1,undo_first_wf,saturday_removed
            ,rota_name = rota.name
            ,m = wf.main
            ,echec = false
            ,opening_period
            ,person = wf
            ,initial_time_worked= wf.nb_minu_worked
            ,other_person_for_saturday_found = false
            ,nobody_can_worked_satuday_under = others.nobody_can_worked_satuday_under
            ;
            const initial_score = score
            controls_enabled(()=>{
                if(score != get_rotation_score()) {
                    my_throw( 'error score')
                }
            })
            var control1 = ()=>{
                if(initial_time_worked == wf.nb_minu_worked) {
                    console.log('success')
                } else {
                    //@ TODO gérer le cas où les heures max ne sont pas les mêmes
                    console.warn(`nombre d\heure pas ok  si cela est du a une absense protégé c'est possible que ce soit normal`)
                }
            }
            console.iDebug14 = 1 + (console.iDebug14 || 0)
            if(console.iDebug14 == 2){
                // debugger
            }

            var next_saturday = function(){
                // cela permet de vérifer qu'en cas de raté
                // on a bien tout restaurer de manière à revenir à l'ancien
                // score
                controls_enabled(()=>{
                    if(score != get_rotation_score()) {
                        Debug.logDayWf(wf)
                        my_throw( 'error score')
                    }
                })
            }
            // parcours de tous les samedis où wf est présent
            // le but est d'en enlever un seul et de le mettre à quelqu'un d'autre
            someOf(wf.rotationMaps[rota.name], function(map, satur_date, stop_some_of) {
                if(!map.length) return next_saturday()
                    controls_enabled(()=>{
                        if(score != get_rotation_score()) {
                            Debug.logDayWf(wf)
                            my_throw( 'error score')
                        }
                        if(!wf.listInfo_by_date[satur_date].is_worked()){
                            console.log(map)
                            my_throw(  'Pourquoi la map à cette date contient des éléments si la personne ne travaille par ce samedi')
                        }
                    })
                var old_score_rotation_ratio_saturday_wf = wf.rotation_ratio.Saturday || 0;
                // debugger
                console.iidebug[54] = idnext('54')
                if(console.iidebug[54] == 60132){
                    debugger
                }
                ;({undo_all: undo_first_wf,date_added_wf1,score,saturday_removed} = rota.unplanif_week_end_and_replanif_week(wf,satur_date,get_rotation_score,score) )//Destructuring_assignment
                if(!saturday_removed){
                    return next_saturday()
                }
                control1()

                // il faut maintenant enlever une journée à quelqu'un qui est en retard sur les samedi
                m.all_wfs.slice().sort((wf1, wf2) => {
                    return wf1.rotation_ratio_relative_to_other_wf.Saturday -
                    wf2.rotation_ratio_relative_to_other_wf.Saturday
                })

                var undo_wf2
                var score2;
                controls_enabled(()=> {
                    score2 = get_rotation_score()
                    if( score != score2) {
                        my_throw( 'error score')
                    }
                })

                //il faut trouver un remplaçant pour travailler le samedi
                //ça ne sert à rien de chercher pour un score de rotation
                //supérieur à nobody_can_worked_satuday_under car on a déjà essayé
                //@TODO uncomment
                // if(nobody_can_worked_satuday_under === undefined 
                //     || nobody_can_worked_satuday_under <= wf.rotation_ratio[rota.name]){
                    m.all_wfs.some((wf2,i) => {
                        if(wf2.rotation_possible[rota.name] <= wf2.rotation[rota.name]){
                            return //la personne ne peut pas faire plus de samedi
                        }
                        if(wf == wf2 || (wf2.rotation_ratio.Saturday || 0) >= old_score_rotation_ratio_saturday_wf) {
                            return //next wf
                        }
                        var atws = rota.add_this_wf_saturday(wf2,satur_date,date_added_wf1,score,get_rotation_score)
                        ;({other_person_for_saturday_found,undo_wf2,score} = atws)
                        controls_enabled(()=>{
                            if( score != get_rotation_score()) {
                                my_throw( 'error score')
                            }
                        });
                        return other_person_for_saturday_found
                    }) //fin de boucle sur les worksfor avec qui échanger!

                // }
                controls_enabled(()=> {
                    var new_score = get_rotation_score()
                    if(other_person_for_saturday_found){
                        if(new_score != score){
                            my_throw( "le score de rotation n'a pas été mis à jour")
                        }
                    }else if(new_score != score2){
                        my_throw( 'certaine chose ont été mal annulé')
                    }
                })

                var failed = ()=>{
                    undo_first_wf();
                    undo_wf2 && undo_wf2()
                    score = initial_score;
                    controls_enabled(()=>{
                        if( score != get_rotation_score()) {
                            my_throw( 'error score')
                        }
                    });
                    return next_saturday();
                }
                //even_if_no_replacement_found = si personne ne nous remplace 
                //samedi alors quand même faire le déplacement du samedi vers un autre jour
                if(!conf.even_if_no_replacement_found && !other_person_for_saturday_found) {
                    //on annule tout c'est un échec la personne va rester le samedi
                    return failed()
                }
                var new_score = get_rotation_score()
                if(new_score > score) {
                    return failed()
                }
                score = new_score
                controls_enabled(()=>{
                    if(score != get_rotation_score()) {
                        my_throw( 'error score')
                    }
                })              
            })
            controls_enabled(()=>{
                if( score != get_rotation_score({logTable: false}) ){
                    get_rotation_score({logTable: true})
                    my_throw( 'error score')
                }
            })         
            return {
                score,
                nobody_can_worked_satuday: !other_person_for_saturday_found
            }
        }
    }
    return Saturday
}