function(){
	//ce sot des paramètres qui normalement devrait être calculé ou codé autrement
	var Fake = {
	    // rotationPossible: function(wf) {
	    //     return 1
	    // },
	    // rotationPossibleTotal: function(main) {
	    //     return main.all_wfs.length
	    // }
	}

	class Cell {
	    constructor(elm) {
	        // this.list = list
	        this.elm = elm
	    }
	    setPrevNextBefore(cell) {
	        controls_enabled(()=>{
	            if(cell == this) {
	                my_throw( 'illogic')
	            }
	        })
	        cell.remove()
	    }
	    setNext(cell) {
	        this.setPrevNextBefore(cell)
	        this.next.prev = cell
	        cell.next = this.next
	        this.next = cell
	        cell.prev = this
	        controls_enabled(()=>{
	            if(cell.next == cell) {
	                my_throw( "error")
	            }
	        })
	    }
	    setPrev(cell) {
	        this.setPrevNextBefore(cell)
	        this.prev.next = cell
	        cell.prev = this.prev
	        this.prev = cell
	        cell.next = this
	        controls_enabled(()=>{
	            if(cell.next == cell) {
	                my_throw( "error")
	            }
	        })
	    }
	    remove() {
	        if(this.prev) {
	            this.next.prev = this.prev.next
	            this.prev.next = this.next.prev
	            this.next = this.prev = null
	        }
	    }
	    // update_position(){
	    //     if(this.list.sort_func(this.next,this) < 0 || this.list.sort_func(this,this.prev) < 0){
	    //         this.remove()
	    //         this.list.add(this)
	    //     }
	    // }
	}
	class SortedDoubleCircularLinkedList {
	    constructor(items, sort_func) {
	        items.sort(sort_func)
	        var first = this.first = new Cell()
	        first.isFirst = true
	        var prev = first.prev = first.next = first
	        items.forEach((elm) => {
	            var last = new Cell(elm)
	            prev.setNext(last)
	            prev = last
	        })
	        this.sort_func = sort_func
	    }
	    sort_cell(cell) {
	        cell.remove()
	        // //C'est pas bien on retrie un tableau déjà presque trié alors que seul un element a changé
	        // Debug.perfStart('sort_elmt_at_pos')
	        // this.array.sort(this.sort_func)
	        // Debug.perfEnd('sort_elmt_at_pos')
	    }
	    add_cell(cell_to_add) {
	        var m = this
	        var found = false
	        m.someCell((cell) => {
	            if(m.sort_func(cell_to_add.elm, cell.elm) < 0) {
	                //cell_to_add < cell
	                cell.setPrev(cell_to_add)
	                found = true
	                return true
	            }
	        })
	        if(!found) {
	            this.first.setPrev(cell_to_add)
	        }
	    }
	    add(val) {
	        var cell = new Cell(val)
	        this.add_cell(cell)
	        return cell
	    }
	    someCell(callback) {
	        for (var next = this.first.next; !next.isFirst; next = next.next) {
	            if(callback(next)) {
	                return;
	            }
	        }
	    }
	    some(callback) {
	        return this.someCell((cell) => callback(cell.elm))
	    }
	}

	return {Fake,Cell,SortedDoubleCircularLinkedList}
}