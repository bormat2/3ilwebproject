<?php 
    $o = $_GET;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Template mailing Alsacreations</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">
        /* Fonts and Content */
        #bodyMail, td { font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; font-weight: 
       700;font-size:13px;color:#9a2f26; }
        #bodyMail { background-color: black; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;
                background:white;margin:0px; padding:0px; -webkit-text-size-adjust:none;
         }
        #bodyMail h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#0E7693; font-size:22px; }
    </style>
</head>
<body id="bodyMail" style="">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#dbd9bb" >
        <tbody>
            <tr>
                <td align="center" >
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tbody>                            
                            <tr class="top">
                                <td class="w640" width="100%" height="10"></td>
                            </tr>

                            <tr class="top">
                                <td align="center" class="w640" width="100%" height="20"> <a style="color:#36631c; font-size:12px;" href="{{url}}"><span style="color:#36631c; font-size:12px;">Voir le contenu de ce mail en ligne</span></a> </td>
                            </tr>
                            <tr class="top">
                                <td class="w640" width="100%" height="10"></td>
                            </tr>

                            <tr class="pagetoplogo top " style="background-color:#f2e9d5" height="13px">
                                <td></td>
                            </tr>
                            <!-- entete -->
                            <tr class="pagetoplogo top">
                                <td class="w640" width="100%">
                                    <table  class="w640" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#f2e9d5">
                                        <tbody>
                                            <tr >
                                                <td class="w30"  width="30"></td>
                                                <td  class="w580"  width="120" valign="middle" align="left">
                                                    <div class="pagetoplogo-content">
                                               <!--          <img class="w580" style="text-decoration: none; display: block; color:#476688; font-size:30px" src="" alt="Mon Logo" width="108" height="108"/> -->
                                               <img class="w580" style="text-decoration: none; display: block; color:#476688; font-size:30px;" src="http://motelavigna.co/img/logoMedium2.png" alt="Mon Logo" width="108" height="108"/>


                                                    </div>
                                                </td>
                                                <td style="color:#9a2f26;text-align: center" width="300">
                                                <table style="    margin: auto;">
                                                        <tr><td>
                                                    MOTEL A VIGNA<br>
                                                    Chemin de Saint-Pancrace 20250 - CORTE<br>
                                                    <a style="color:#9a2f26; font-size:12px;text-decoration: none" href="#"><span style="color:#9a2f26; font-size:12px;">04.95.46.02.19 / 06.77.87.68.74</span>
                                                    </a><br>
                                                    tcampana@wanadoo.fr</td></tr>
                                                </table>
                                                    </td>
                                                <td  class="w580"  width="108" valign="middle" align="left">
                                                    <div class="pagetoplogo-content">
                                                        <img class="w580" style="text-decoration: none; display: block; color:#476688; font-size:30px;float:right" src="http://motelavigna.co/img/logoMedium2.png" alt="Mon Logo" width="108" height="108"/>
                                                    </div>
                                                </td> 
                                                <td class="w30"  width="30"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr class="pagetoplogo top" style="background-color:#f3e7d6" height="13px">
                                <td></td>
                            </tr>

                            <!-- contenu -->
                            <tr class="content">
                                <td class="w640" class="w640" width="100%" bgcolor="#ffffff">
                                    <table style="margin:auto;" class="w640" width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td  class="w30"  width="30"></td>
                                                <td  class="w580"  width="580">
                                                    <!-- une zone de contenu -->
                                                    <table style="margin:auto;" class="w580"  width="580" cellpadding="0" cellspacing="0" border="0">
                                                        <tbody>                                                            
                                                            <tr>
                                                                <td class="w580"  width="580">
                                                                    <h2 style="color:#9a2f26; font-size:22px; padding-top:12px;">
                                                                        Prise en compte de votre réservation
                                                                        </h2>
                                                                    <div align="left" class="article-content">
                                                                        <p>
                                                                        Dès que vous recevrez un mail de confirmation, vous devrez envoyer un virement de 30% du montant de la réservation soit {{pricePart1}}€ sous un délai de 1 semaine
                                                                        </p>
                                                                        <p>
                                                                            Votre réservation pour la période du  :
                                                                            <p style="text-decoration:underline">
                                                                            Du  {{checkin}} au {{checkout}}  
                                                                            soit {{interval}}  nuits
                                                                            </p>
                                                                            <p>
                                                                            sera analysé, vous recevrez une réponse dans les plus brefs délais :
                                                                            <br>
                                                                            </p>
                                                                            <p style="text-align: center;text-decoration:underline">
                                                                            - {{nbStudio}} studio(s) 
                                                                            <br>- {{nbPers}} personne(s).
                                                                            <br>Sur la somme totale de {{totalPrice}} € TTC <br>
                                                                            Reste à payer {{diffPrice}} € à l’arrivée<br>
                                                                            </p>
                                                                            <br><br>
                                                                        </p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <!-- fin zone -->                                                   

                                                    <!-- une autre zone de contenu -->
                                                    <table style="margin:auto;" width="580" cellspacing="0" cellpadding="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td colspan="3">
                                                                En cas d’imprévu, veuillez nous signaler votre retard ou votre annulation. Une somme de <br> 10 % sera retenue à titre de frais dans ce cas.<br><br>

                                                                L’heure de votre arrivée est libre après 14 h 00. Si celle-ci est très tardive 
                                                                (après 18 H 00) veuillez nous téléphoner dans l’après-midi.<br><br>

                                                                Dans l’attente de vous recevoir et avec nos remerciements, recevez nos salutations distinguées.<br><br>

                                                                </td>
                                                            </tr>
                                                            
                                                        </tbody>
                                                    </table>
                                                    <table style="margin:auto;" class="w580"  width="580" cellpadding="0" cellspacing="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td class="w180"  width="600" valign="top"></td>
                                                                <td class="w180"  width="180" valign="top">
                                                                    <div align="left" class="article-content">
                                                                        <p>La direction</p>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="w30" class="w30"  width="30"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>


                            <!-- pied de page -->
                            <tr class="pagebottom bot" >
                                <td class="w640" width="100%">
                                    <table style="margin:auto;" class="w640" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#f3e7d6">
                                        <tbody>
                                            <tr>
                                                <td colspan="5" height="20"></td>
                                            </tr>
                                            <tr>
                                                <td class="w30"  width="30"></td>
                                                </td>

                                                <td class="w30"  width="30"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" height="20"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr class="bot">
                                <td class="w640" width="100%" height="60"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>