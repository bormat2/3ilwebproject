<?php include('phpFunc.php');?>
<!DOCTYPE html>
<html lang="<?=$lang?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="<?= $_GET['path'].'/'.$lang ?>/" >
 	<link rel="stylesheet" type="text/css" href="../css/index.css">
 	<link rel="stylesheet" type="text/css" href="../css/index_uncompiled.css">
  <link rel="stylesheet" type="text/css" href="../fonts/fonts.css">
	<link rel="icon" type="image/png" href="../img/favicon.png" />
	<script>var $_GET = <?= json_encode($_GET) ?> </script>
	<title><?= $pageCode ?></title>
	<script type="text/javascript">
		// si il y a une erreur php il faudra 2 secondes avant de l'afficher
		setTimeout(function(){
			var $body = $0('body.opacity0')
			if($body){
				$body.classList.remove('opacity0')
				console.error("une erreur a empêché de changer l'opacité" )
			}
		},2000)
	</script>
	<style>
	.block{
		width:100%;
		height: auto;
		display: block;
	}
	</style>
</head>
<body  id="body" class=""  data-current-page="<?= $pageCode ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HJWLX4"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<script src="../js/ie.js"></script>
	<script src="../js/index.js"></script>
	<script src="../noIndex/dynamicJS.php"></script>
	<script src="../js/utils.js"></script>
	<script src="../js/navigationUrlHistory.js"></script>
	<script src="../js/domManipulation.js"></script>
	<script src="../js/domEvent.js"></script>
	<script src="../js/library/moment.min.js"></script>
	<script src="../js/carousel.js"></script>
	<script src="../js/library/scrollTo.js"></script>
	<script src="../js/library/progressbar.js"></script>
	<div id="fakeBody" class="fakeBody">
		<div id="fakeBody2">
			<script>document.body.classList.add('opacity0')</script>
			<!--div auto-height  id="header-and-content" class="f-container column"-->
			<div class="headerRow"><?php include('pageHeader.php') ?></div>
			<div class="bodyRow">
				<div id="bodyContent" class="justify-start f-container fill" >
					<div  id="ajaxContent" class="f-container">
						<div  class="page active loadByPHP f-container fill column" id="<?= $pageCode ?>">
							<?php
							//$user->setPasswordConnection('tt');
							if(file_exists('public/'.$page)){
								require('public/'.$page);
							}else{
								require('public/'.'404.php');
							}?>
						</div>
					</div>
					<? include('public/advisors.php'); ?>
					<div id="scriptExecutor">
			<!-- 			<script src="https://use.fontawesome.com/698aa4e2c2.js"></script>
			 -->	</div>

					<script>
						$.initScript()
					</script>
				</div>
			<!--/div-->
			</div>
			<style>
				/* Pour firefox */
				.footerRow{
					height: 144px;
				}
				.footerRow footer{
					padding: 0;
					width:100%;
					height: 1px;
					vertical-align: middle;
					padding-top: 20px;
					padding-bottom: 20px;
					display: table-cell;
				}
				#fakeBody2{
					height: 1px;
				}
			</style>
			<div class="footerRow">
				<footer auto-height class="flex-start f-container column">
					<div>
						<div>
							<div class="footerDiv f-container wrap ul containerPaddingV2">
								<div class="li">
									<address>
										<a
											target="_blank"
											class="noLink"
											href="https://www.google.fr/maps/dir//Motel+a+Vigna,+20250+Corte/@42.3173289,9.0788402,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x12d0ad097a63f531:0xe11168c028a55cb4!2m2!1d9.1488804!2d42.3172233"
										>
											<span auto-height class="f-container wrap maxWidthContentV2">
												<span><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;Motel A Vigna,&nbsp;</span>
												<span>Chemin de Saint-Pancrace,&nbsp;</span>
												<span>20250 CORTE</span>
											</span>
										</a>
									</address>
								</div>
								<div class="li">
									<a target="_blank" href="tel:+33495460219" class="noLink">
										<i class="fa fa-phone" aria-hidden="true"></i>&nbsp;
										 +33 (0)4 95 46 02 19
									</a>
								</div>
								<div class="li">
									<a target="_blank" href="mailto:tcampana@wanadoo.fr" class="noLink">
										<i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
										 tcampana@wanadoo.fr
									 </a>
								 </div>
					 			<div class="li f-container wrap maxWidthContent">
									<i class="fa fa-copyright" aria-hidden="true"></i>
									&nbsp;Designed By
									<a target="_blank" href="mailto:rafael.horvat@icloud.com" class="noLink">
										&nbsp;Horvat Rafael
									</a>
									<a target="_blank" href="mailto:bormat2@gmail.com" class="noLink">
										&nbsp;&amp;&nbsp;Bortolaso Mathieu
									</a>
								</div>
							<div>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>
	<script>
		$.endPage()
	</script>
	<div id="resizeRunnning">
		<div>
			<div>we are waiting the end ot the resize</div>
		</div>
	</div>
	<div id="progressbar"></div>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-97212138-1', 'auto');
		ga('send', 'pageview');
	</script>

	<!-- how improve user experience -->
	<script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '15e53185f8349c9b6924eb38c51eb9f8e3a5bef9');
</script>

</body>
</html>
