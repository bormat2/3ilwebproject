-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Client :  db669536218.db.1and1.com
-- Généré le :  Sam 03 Février 2018 à 22:49
-- Version du serveur :  5.5.58-0+deb7u1-log
-- Version de PHP :  5.4.45-0+deb7u12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `db669536218`
--

-- --------------------------------------------------------

--
-- Structure de la table `Reservation`
--

CREATE TABLE IF NOT EXISTS `Reservation` (
  `client_name` char(50) COLLATE latin1_general_ci DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `d_create` char(11) COLLATE latin1_general_ci DEFAULT NULL,
  `d_checkin` char(11) COLLATE latin1_general_ci DEFAULT NULL,
  `d_checkout` char(11) COLLATE latin1_general_ci DEFAULT NULL,
  `paid` tinyint(1) DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci,
  `mail` char(50) COLLATE latin1_general_ci DEFAULT NULL,
  `phone` char(50) COLLATE latin1_general_ci DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT NULL,
  `color_code` char(7) COLLATE latin1_general_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `nbPers` int(11) DEFAULT NULL,
  `alreadyPaid` char(11) COLLATE latin1_general_ci DEFAULT NULL,
  `message` varchar(10000) COLLATE latin1_general_ci DEFAULT NULL,
  `note` varchar(10000) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=80 ;

--
-- Contenu de la table `Reservation`
--

INSERT INTO `Reservation` (`client_name`, `id`, `d_create`, `d_checkin`, `d_checkout`, `paid`, `comment`, `mail`, `phone`, `confirmed`, `color_code`, `price`, `nbPers`, `alreadyPaid`, `message`, `note`) VALUES
('voisin campana', 49, '2017-04-10', '2017-06-02', '2017-06-11', 1, NULL, 'spyrlord@hotmail.fr', '0684440190', 1, '#808000', 420, 2, NULL, '', '    		    		    		\n    	\n    	\n    	'),
('Guerin', 50, '2017-04-11', '2017-04-14', '2017-04-17', 1, NULL, 'tcampana@wanadoo.fr', '0677876874', 0, '#800ddd', 120, 2, NULL, 'Test', '    		    		    		\n    	\n    	\n    	'),
('Gc', 51, '2017-04-11', '2017-05-16', '2017-05-31', 0, NULL, 'tcampana@wanadoo.fr', '0677876874', 0, '#800ddd', 990, 2, NULL, 'Test de 0rix', '    		    		\n    	\n    	'),
('perrot l prioul', 42, '2017-04-10', '2017-08-23', '2017-08-26', 1, NULL, 'psylord2b@hotmail.fr', '0495610652', 1, '#80ff80', 195, 2, NULL, '', '    		    		\n    	\n    	'),
('CYRIELLE ANTOINE MEL LIAM MARTIN', 43, '2017-04-10', '2017-08-13', '2017-08-16', 1, NULL, '?psylord2b@hotmail.fr', '0495610408', 1, '#ffff00', 0, 5, NULL, '', '    		    		    		\n    	nièce et enfants\n    	\n    	'),
('Théophile', 52, '2017-04-11', '2017-07-26', '2017-08-05', 1, NULL, 'tcampana@wanadoo.fr', '0677876874', 0, '#800ddd', 10172, 2, NULL, 'Test', '    		    		    		\n    	\n    	\n    	'),
('Clara Marcelli', 54, '2017-04-13', '2017-04-13', '2017-04-17', 1, NULL, 'clara.marcelli@gmail.com', '0782671444', 0, '#800ddd', 180, 1, NULL, 'Voilà tatie je fais un essai ça a l''air de bien marcher je trouve', '    		\n    	'),
('houdiarne jagu patricia', 48, '2017-04-10', '2017-09-02', '2017-09-03', 1, NULL, 'PSYLORD2B@HOTMAIL.FR', '0475262913', 1, '#80ff80', 65, 2, NULL, '', '    		    		\n    	\n    	'),
('MATHIEU', 46, '2017-04-10', '2017-08-03', '2017-08-27', 1, NULL, 'bormat2@gmail.com', '0607690235', 1, '#80ffff', 0, 1, NULL, '', '    		\n    	petit fils'),
('volle', 47, '2017-04-10', '2017-08-22', '2017-08-29', 1, NULL, '?psylord2b@hotmail.fr', '0495614563', 1, '#00ff00', 400, 2, NULL, '', '    		\n    	'),
('LUCAS COCO ET TONTON', 44, '2017-04-10', '2017-08-15', '2017-08-18', 1, NULL, '?psylord2b@hotmail.fr', '0495610879', 1, '#ffff00', 195, 4, NULL, '', '    		    		\n    	neveu et oncle\n    	'),
('BLAISE NEVEU', 45, '2017-04-10', '2017-08-07', '2017-08-22', 1, NULL, '?psylord2b@hotmail.fr', '049560856', 1, '#ffff00', 800, 3, NULL, '', '    		    		    		    		\n    	neveu\n    	\n    	\n    	'),
('gola et telliez', 40, '2017-04-09', '2017-08-20', '2017-08-22', 1, NULL, '?psylord2b@hotmail.fr', '0495610102', 1, '#80ff80', 130, 2, NULL, '', '    		    		    		\n    	\n    	\n    	'),
('treppo harlaux', 41, '2017-04-09', '2017-08-21', '2017-08-28', 1, NULL, '?psylord2b@hotmail.fr', '0495610506', 1, '#80ff80', 1110, 6, NULL, '', '    		    		\n    	\n    	'),
('mamyblue', 53, '2017-04-11', '2017-07-01', '2017-08-01', 1, NULL, 'PSYLORD2B@HOTMAIL.FR', '0495825003', 0, '#800ddd', 1872, 2, NULL, 'test', '    		\n    	'),
('bozzi', 39, '2017-04-09', '2017-08-11', '2017-08-13', 1, NULL, 'psylord2b@hotmail.fr', '0683489610', 1, '#80ff80', 260, 5, NULL, '', '    		    		    		\n    	\n    	\n    	'),
('marie villard pour foire', 38, '2017-04-09', '2017-07-29', '2017-07-30', 1, NULL, 'spsylord2b@hotmail.fr', '0680614074', 1, '#00ff80', 130, 2, NULL, '', '    		    		\n    	\n    	'),
('berriau', 37, '2017-04-09', '2017-08-05', '2017-08-07', 1, NULL, 'spsylord2b@hotmail.fr', '0625187376', 1, '#80ff80', 430, 10, NULL, '', '    		\n    	'),
('huguenin leila', 36, '2017-04-09', '2017-07-29', '2017-08-08', 1, NULL, '?psylord2b@hotmail.fr', '0495610101', 1, '#00ff80', 650, 3, NULL, '', '    		    		\n    	\n    	'),
('luberda ambel', 35, '2017-04-09', '2017-08-05', '2017-08-06', 1, NULL, '?psylord2b@hotmail.fr', '0645805332', 1, '#00ff80', 65, 2, NULL, '', '    		    		\n    	\n    	'),
('luberda', 34, '2017-04-09', '2017-07-21', '2017-07-22', 1, NULL, '?psylord2b@hotmail.fr', '0645805332', 1, '#00ff80', 65, 2, NULL, '', '    		    		\n    	\n    	'),
('pitout', 33, '2017-04-09', '2017-07-20', '2017-07-22', 1, NULL, 'psylord2b@hotmail.fr', '0631121969', 1, '#00ff80', 430, 9, NULL, '', '    		    		    		\n    	\n    	\n    	'),
('dindinaud', 32, '2017-04-09', '2017-06-24', '2017-06-26', 1, NULL, '?psylord2b@hotmail.fr', '0607419436', 1, '#80ff80', 130, 2, NULL, '', '    		\n    	'),
('MARANINCHI PETRA', 31, '2017-04-09', '2017-04-09', '2017-06-01', 1, NULL, 'PSYLORD2B@HOTMAIL.FR', '0782126389', 1, '#c0c0c0', 520, 1, NULL, 'etudiante', '    		    		\n    	\n    	'),
('HARTIGUES', 30, '2017-04-09', '2017-04-09', '2017-04-14', 1, NULL, 'PSYLORD2B@HOTMAIL.FR', '41762780818', 1, '#00ff80', 280, 3, NULL, '', '    		\n    	'),
('Dominique MOSER', 29, '2017-04-09', '2017-06-10', '2017-06-18', 1, NULL, 'moser@sfr.fr', '0603408931', 1, '#00ff80', 400, 1, NULL, '', '    		    		\n    	\n    	'),
('barbetti', 28, '2017-04-09', '2017-07-11', '2017-07-14', 1, NULL, 'barbetti.laurent@wanadoo.fr', '0633306522', 1, '#80ff00', 180, 2, NULL, '', '    		\n    	'),
('raffini', 27, '2017-04-09', '2017-04-09', '2017-06-01', 1, NULL, 'lauriane-x2b@hotmail.fr', '0603216854', 1, '#c0c0c0', 0, 1, NULL, 'étudiante', '    		    		    		\n    	mise à jour planning \n    	\n    	'),
('martelli', 26, '2017-04-09', '2017-04-09', '2017-06-01', 1, NULL, 'mcambrosi@hotmail.fr', '0627283901', 1, '#c0c0c0', 0, 1, NULL, 'etudiante', '    		    		    		\n    	pour mise à jour planning\n    	\n    	'),
('cipriani', 25, '2017-04-09', '2017-04-09', '2017-06-01', 1, NULL, 'bormat2@gmail.com', '0679675900', 1, '#c0c0c0', 0, 1, NULL, 'étudiant', '    		\n    	pour mise a jour planning étudiant'),
('riche benoit', 24, '2017-04-09', '2017-04-09', '2017-07-01', 1, NULL, 'bormat2@gmail.com', '0467895043', 1, '#c0c0c0', 0, 1, NULL, 'etudiant', '    		    		\n    	\n    	'),
('castelli', 23, '2017-04-09', '2017-04-09', '2017-07-01', 1, NULL, 'bormat2@gmail.com', '0620584839', 1, '#808080', 0, 1, NULL, 'étudiant', '    		    		\n    	pour mise à jour planning\n    	'),
('COUPEAUD', 15, '2017-04-08', '2017-07-24', '2017-08-11', 1, NULL, 'charrescoupeaudc@orange.fr', '04 75 41 56 87', 1, '#00ff80', 1150, 3, NULL, 'ça marche ? !', '    		    		deja enregistré\n    	\n    	'),
(' Mickael TEST', 14, '2017-04-07', '2017-04-20', '2017-04-22', 1, NULL, 'psylord2b@hotmail.fr', '0670998714', 0, '#800ddd', 60, 1, NULL, 'RESERVATION DE TEST ', '    		    		    		    		    		\n    	test\n    	\n    	\n    	\n    	'),
('GERMOND YVES', 16, '2017-04-08', '2017-05-27', '2017-05-31', 1, NULL, 'zalain.imbert@numericable.fr', '0618993856', 1, '#00ff80', 520, 4, NULL, 'fait terry', '    		    		    		\n    	déjà enregistré\n    	\n    	'),
('Morroni helena', 13, '2017-04-04', '2017-05-13', '2017-05-21', 1, NULL, 'lunartemistiger@gmail.com', '0629388799', 0, '#800ddd', 1200, 5, NULL, 'essaie', '    		    		    		    		    		    		    		\n    	\n    	essai helena\n    	\n    	\n    	\n    	\n    	'),
('Vanthuyne Kimberly belgique', 17, '2017-04-08', '2017-06-02', '2017-06-03', 1, NULL, 'imberly_vanthuyne@hotmail.com', '0000000000', 1, '#00ff80', 75, 3, NULL, 'dejà enregistré', '    		    		    		    		    		\n    	\n    	deja enregistré\n    	\n    	\n    	'),
('chossonnery', 18, '2017-04-08', '2017-06-02', '2017-06-04', 1, NULL, 'andree.chossenery@sfr.fr', '0000000000', 1, '#80ff80', 600, 5, NULL, 'dejà enregistre', '    		    		    		    		    		\n    	déja enregistré\n    	\n    	\n    	\n    	'),
('Francony', 10, '2017-04-02', '2017-06-13', '2017-06-16', 1, NULL, 's.francony@gmail.com', '0686445394', 1, '#00ff80', 180, 2, NULL, '', '    		    		    		    		\n    	deja enregistré\n    	\n    	\n    	'),
('gavalda', 19, '2017-04-08', '2017-06-17', '2017-06-18', 1, NULL, 'frederic.gavalda@orange.fr', '0000000000', 1, '#80ff80', 65, 2, NULL, 'deja enregistré', '    		    		    		\n    	fait terry\n    	\n    	'),
('fauchard', 22, '2017-04-09', '2017-04-17', '2017-04-28', 1, NULL, 'psylord2b@hotmail.fr', '0770006809', 1, '#80ff80', 0, 1, NULL, 'déja enregistré pour validation', '    		\n    	'),
('bergogni m.h', 21, '2017-04-09', '2017-04-17', '2017-04-28', 1, NULL, 'psylord2b@hotmail.fr', '0770006809', 1, '#80ff80', 0, 1, NULL, 'déja enregistré pour validation', '    		    		fait terry\n    	\n    	'),
('ROBERT Pierre-Yves', 3, '2017-03-29', '2017-06-17', '2017-06-19', 1, NULL, 'p.yves-nath@orange.fr', '0666741342', 0, '#800ddd', 70, 3, NULL, 'Bonjour, nous souhaiterions réserver un studio pour la nuit du 17 au 18 juin 2017. Nous sommes 3 personnes. Merci pour votre réponse. Cordialement', '    		    		    		\n    	\n    	\n    	'),
('djelah', 20, '2017-04-09', '2017-04-17', '2017-04-26', 1, NULL, 'psylord2b@hotmail.fr', '0616634010', 1, '#80ff80', 460, 1, NULL, 'deja enregistre pour vzlidation', '    		fait terry\n    	'),
('GIRAULT', 55, '2017-04-27', '2017-07-15', '2017-07-16', 0, NULL, 'annemuriel.girault@gmail.com', '0685540067', 1, '#00ff80', 140, 5, NULL, 'Bonjour. Je vous ai appelé tout à l''heure. Nous souhaitons deux studios, l''un pour les parents avec une enfant de 5 ans, l''autre pour deux ados de 16 et 17 ans. Merci. AM GIRAULT 53 RUE PAUL BERT 79000 NIORT; PS : devons nous envoyer un acompte? Cordialement', '    		    		\n    	\n    	'),
('Nicolo', 56, '2017-06-06', '2017-08-13', '2017-08-15', 1, NULL, 'nico.panz@gmail.com', '00393282910620', 0, '#800ddd', 136, 2, NULL, 'Bonjour, je cherche une chambre pour 2 personnes. Check in 13/08 - check out 15/08. Vous avez disponibilitè? merci', '    		    		    		\n    	annulation\n    	\n    	'),
('Marco Capsoni', 57, '2017-06-14', '2017-07-07', '2017-07-08', 1, NULL, 'marco@capsonistudio.it', '3771153354', 1, '#00ff00', 65, 2, NULL, 'Non fumatori - letto matrimoniale e bagno privatoe ', '    		    		\n    	\n    	'),
('Stefania Profeti', 58, '2017-06-25', '2017-08-28', '2017-08-29', 0, NULL, 'stefania.profeti@unibo.it', '0039 328 5346870', 1, '#00ff00', 68, 2, NULL, '', '    		\n    	'),
('Giorgia Vallinotti', 59, '2017-07-05', '2017-07-30', '2017-07-31', 1, NULL, 'giorgia.vallinotti.94@gmail.com', '3466990450', 1, '#80ff80', 68, 2, NULL, 'Bonsoir, je voudrais réserver une chambre pour deux personnes du 30 juillet au 31 juillet.', '    		    		\n    	\n    	'),
('cantelaube', 60, '2017-07-09', '2017-08-07', '2017-08-09', 1, NULL, 'marroumarrou79@gmail.com', '0553810992', 0, '#800ddd', 272, 3, NULL, 'je souhaite savoir si vous avez des disponibilités du 7 au 9 aout matin pour 3 adultes une chambre pour 2personnes et une chambre pour 1 personne', '    		\n    	'),
('', 61, '2017-07-12', '', '2018-06-30', 1, NULL, '', '0603991144', 0, '#ffffff', 0, 0, NULL, 'Bonjour,', '    		    		\n    	\n    	'),
('Tom Clarius', 62, '2017-08-06', '2017-09-10', '2017-09-14', 1, NULL, 'Tom.clarius@saint-gobain.com', '0033 6 24 96 37 99', 1, '#80ff80', 260, 2, NULL, '', '    		    		    		\n    	\n    	\n    	'),
('Lianne veldman', 63, '2017-08-07', '2017-09-12', '2017-09-13', 1, NULL, 'Lianneveldman@gmail.com', '0031615687757', 1, '#80ff00', 65, 1, NULL, '', '    		    		\n    	\n    	'),
('Kerzel, Corinna', 64, '2017-08-14', '2017-09-05', '2017-09-09', 0, NULL, 'corinna-kerzel@gmx.de', '0049-17638236130', 0, '#800ddd', 260, 2, NULL, '', '    		\n    	'),
('LUCIA DE VINCENTI', 65, '2017-08-14', '2017-09-03', '2017-09-05', 0, NULL, 'arch.luciadevincenti@gmail.com', '00393491350940', 0, '#800ddd', 260, 2, NULL, '', '    		\n    	'),
('LUCIA DE VINCENTI', 66, '2017-08-14', '2017-09-03', '2017-09-05', 0, NULL, 'luciadevincenti@hotmail.it', '0039 3491350940', 0, '#800ddd', 130, 2, NULL, '', '    		\n    	'),
('CHAMPEAU CATHERINE', 67, '2017-08-18', '2017-09-01', '2017-09-04', 0, NULL, 'ktychampo@orange.fr', '0676098129', 0, '#800ddd', 195, 2, NULL, '3 nuits pour un couple', '    		\n    	'),
('Céline Savineau', 68, '2017-08-27', '2017-09-06', '2017-09-08', 1, NULL, 'celsav@free.fr', '0688848588', 1, '#80ff80', 130, 2, NULL, 'Bonjour, nous souhaiterions séjourner dans un de vos studios du 6 au 8 septembre. Merci d''avance pour votre réponse', '    		    		\n    	\n    	'),
('GREGOIRE', 69, '2017-09-12', '2017-09-13', '2017-09-15', 0, NULL, 'Matt-pt@hotmail.fr', '0677492309', 0, '#800ddd', 130, 2, NULL, 'Bonjour, je souhaiterais connaître vos disponibilités pour une chambre de 2 personnes, pour 2 nuits. Merci.', '    		\n    	'),
('WATIER-FERACCI', 70, '2017-09-13', '2017-09-13', '2018-04-30', 1, NULL, 'claudinewf95@gmail.com', '0637500472', 1, '#800ddd', 1290, 1, NULL, 'etudiante', NULL),
('PAPI Chjara', 71, '2017-09-13', '2017-09-13', '2018-05-31', 1, NULL, 'papichjara@gmail.com', '0777365133', 1, '#800ddd', 1290, 1, NULL, 'etudiante', NULL),
('RONCHANT MANON', 72, '2017-09-14', '2017-09-14', '2018-05-31', 1, NULL, 'manonronchant@hotmail.fr', '06.23.50.58.08', 1, '#800ddd', 1290, 1, NULL, 'etudiante', '    		\n    	'),
('HESSE Joffrey', 73, '2017-09-14', '2017-09-14', '2018-05-31', 1, NULL, 'joffreyhesse@gmail.com', '06.4602.40.65', 1, '#800ddd', 1290, 1, NULL, 'etudiant', '    		    		\n    	\n    	'),
('CORNELI THOMAS', 74, '2017-09-14', '2017-09-14', '2018-05-31', 1, NULL, 'thomas.jf.corneli@gmail.com', '06.20.77.76.08', 1, '#800ddd', 1290, 1, NULL, 'étudiant', '    		    		\n    	\n    	'),
('ETTAOUSSI Noëlla', 75, '2017-09-14', '2017-09-14', '2018-05-31', 1, NULL, 'noellaettaoussi2a@gmail.com', '06.21.06.66.61', 1, '#800ddd', 1290, 1, NULL, 'étudiante', '    		    		\n    	\n    	'),
('CASANOVA Michel', 76, '2017-09-14', '2017-09-17', '2018-05-31', 1, NULL, 'jm.casanova@wanadoo.fr', '06.85.01.45.98', 1, '#800ddd', 860, 1, NULL, 'étudiant', '    		    		    		\n    	\n    	\n    	'),
('Étudiant', 77, '2017-09-16', '2017-09-16', '2018-06-17', 1, NULL, '@', '0607690235', 1, '#800ddd', 0, 1, NULL, '', '    		    		    		\n    	étudiant\n    	\n    	'),
('Étudiant 2', 78, '2017-09-16', '2017-09-16', '2018-05-17', 1, NULL, '@', '0', 1, '#800ddd', 65, 1, NULL, 'étudiant provisoire', '    		\n    	'),
('Teste', 79, '2018-01-31', '2018-07-23', '2018-07-25', 0, NULL, 'agnespousse@gmail.com', '0679430946', 1, '#00ff80', 176, 4, NULL, 'Bonjour nous sommes une famille de 4 avec 2 enfants de 11 et 15 ans du 23 au 25/07 2018 ', '    		\n    	');

-- --------------------------------------------------------

--
-- Structure de la table `Room`
--

CREATE TABLE IF NOT EXISTS `Room` (
  `num` int(11) NOT NULL DEFAULT '0',
  `surface` int(11) DEFAULT NULL,
  `nb_pers` int(11) DEFAULT NULL,
  PRIMARY KEY (`num`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `Room`
--

INSERT INTO `Room` (`num`, `surface`, `nb_pers`) VALUES
(16, 0, 4),
(5, 0, 3),
(6, 0, 3),
(7, 0, 3),
(8, 0, 3),
(13, 0, 3),
(14, 0, 2),
(15, 0, 2),
(18, 0, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Room_Reservation`
--

CREATE TABLE IF NOT EXISTS `Room_Reservation` (
  `reservation_id` int(11) NOT NULL DEFAULT '0',
  `room_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reservation_id`,`room_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `Room_Reservation`
--

INSERT INTO `Room_Reservation` (`reservation_id`, `room_id`) VALUES
(3, 0),
(10, 13),
(13, 0),
(14, 0),
(15, 5),
(16, 15),
(16, 16),
(17, 8),
(18, 15),
(18, 16),
(19, 15),
(20, 13),
(21, 15),
(22, 16),
(23, 7),
(24, 6),
(25, 8),
(26, 5),
(27, 14),
(28, 14),
(29, 8),
(30, 16),
(31, 18),
(32, 5),
(33, 13),
(33, 16),
(33, 18),
(34, 8),
(35, 8),
(36, 6),
(37, 13),
(37, 16),
(37, 18),
(38, 15),
(38, 18),
(39, 6),
(39, 7),
(40, 14),
(41, 5),
(41, 6),
(41, 18),
(42, 14),
(43, 16),
(44, 5),
(44, 18),
(45, 13),
(46, 15),
(47, 7),
(48, 13),
(49, 5),
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 15),
(55, 13),
(55, 14),
(56, 14),
(57, 15),
(58, 15),
(59, 14),
(60, 14),
(60, 18),
(61, 0),
(62, 15),
(63, 14),
(64, 14),
(65, 5),
(65, 16),
(66, 6),
(67, 8),
(68, 15),
(69, 14),
(70, 7),
(71, 8),
(72, 6),
(73, 13),
(74, 16),
(75, 5),
(76, 15),
(77, 14),
(78, 18),
(79, 16);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
