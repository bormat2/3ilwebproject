<?php
	error_reporting(~0);
	header('Content-type: text/html; charset=utf-8');
	date_default_timezone_set('UTC');
	include('dataBase.php');
	global $user;
	global $baseDirPath;
	global $trad;
	global $post; // contient les les valeurs décodé du post
	global $config;
	global $db; //database manipulation
	global $trad;
	global $page;
	global $pageCode;
	ini_set('display_errors', 1);
	// Si il y a ".php" dans l'url alors l'enlever pour avoir le pageCode
	// pour avoir la page correspondant au pageCode on met ".php" à a fin
	$pageCode = preg_match('/.*.php/', $_GET['page'])
		?  substr($_GET['page'],0,-4)
		:  $_GET['page'];
	if($pageCode == '')	$pageCode = 'home';
	$page = "$pageCode.php";
	$baseDirPath = __DIR__;
	$db = new MainModel();
	function encodeToJson($val){
		return json_encode($val,JSON_UNESCAPED_UNICODE);
	}
	function decodeFromJson($val){
		return json_decode($val,true);
	}

	//contient le password
	$config = json_decode(file_get_contents('private/config.json'), true);

	//page par défaut et langue par défaut
	if(!$_GET['page']) $_GET['page'] = 'home.php';
	if(!$_GET['lang']) $_GET['lang'] = 'fr';

	//base du site web
	$_GET['path'] = 'http://'.$_SERVER['SERVER_NAME'].dirname($_SERVER['SCRIPT_NAME']);
	if(substr($_GET['path'], -1) == '/'){
		$_GET['path'] = substr($_GET['path'], 0,-1);
	}
	$trads = decodeFromJson(file_get_contents('noIndex/Translation.json'));
	$lang = $_GET['lang'];

	//fonction de traduction
	$tradOrEmptyString = function ($codeTrad) use ($trads,$lang){
		return $trads[$lang][$codeTrad] ?? $trads['fr'][$codeTrad] ?? $trads['en'][$codeTrad] ?? "";
	};
	$trad = function ($codeTrad) use ($tradOrEmptyString){
		$theTrad = $tradOrEmptyString($codeTrad);
		if($theTrad == ""){
			$theTrad = $codeTrad;
		}
		return "<span data-trad=\"$codeTrad\">$theTrad</span>";
	};


	if(isset($_POST['data'])){
		$post = decodeFromJson($_POST['data']);
	}

	function formSuccess($mess){
		return '<span class="success">'.$GLOBALS['trad']($mess).'</span>';
	}

	function formError($mess){
		return '<span class="error bigBlockError">'.$GLOBALS['trad']($mess).'</span>';
	}

	class UserConnection {
		private $hashDataBase;
		function crypt($password){
			return  utf8_encode(md5(md5($password,PASSWORD_DEFAULT)));
		}
		function connect($password){
			$password = $this->crypt($password);
			echo "<script type='text/whenDocumentReady'>$.setCookie('passwordEncript','$password')</script>";
			$_COOKIE['passwordEncript'] = $password;
			return $this->isConnect();
		}
		function isConnect(){
			if(!isset($_COOKIE['passwordEncript'])) return false;
			$isConnect = $GLOBALS['config']['passwordEncript'] == $_COOKIE['passwordEncript'] ;
			return $isConnect;
		}
		function setPasswordConnection($pwd){
			$GLOBALS['config']['passwordEncript'] = $this->crypt($pwd);
			//write json data into data.json file
			$json = encodeToJson($GLOBALS['config']);
		  	if(file_put_contents('private/config.json',$json) === false) {
				echo '<script type="text/whenDocumentReady">alert("error password not changed")</script>';
			}
		}
	}

	function reloadMenu(){?>
		<div>
			<script type="text/whenDocumentReady" >
			// on déplace le contenu de newMenuTemplate à la place
			// de l'ancien menu pour mettre à jour login en logout
			var newMenu = $id('newMenuTemplate');
			$0('header').outerHTML = newMenu.innerHTML
			newMenu.remove();
			$.setLang($.getLang())
			</script>
			<div id="newMenuTemplate" class="hide">
				<?php include('pageHeader.php'); ?>
			</div>
		</div><?php
	}

	$user = new UserConnection();
	$user->isConnect();
	/*$o = [
	* 	to : ['emailHere','nameHere'] 
	* 	replyTo : ['emailHere','nameHere'] faculative
	* 	htmlMessage : htmlMessageHere
	* 	title: titleHere
	*]
	*/
	
	// sendMail([
	// 	"to" => 'bormat2@gmail.com',
	// 	"replyTo" => 'bormat2@gmail.com',
	// 	"htmlMessage" => "html mess",
	// 	"title" => "titleHere"
	// ]);

	function sendMail($o){
		$sujet = $o['title'];

		if(isset($o['templateUrl'])){
			$message = preg_replace_callback('/{{(.*?)}}/', function($matches)use ($o){
				return $o['vars'][$matches[1]];
			}, file_get_contents($_GET['path'].$o['templateUrl']));
		}else{
			$message = $o['htmlMessage'];
		}

		if(!isset($o['to'])){
			// $o['to'] = 'tcampana@wanadoo.fr';
			$o['to'] = 'bormat2@gmail.com';
		}
		$destinataire = trim($o['to']);
		// $contentType = $o['hmtlMode'] ??0? 'html' : 'plain';
		// $headers = join("\r\n",[
		// 	'From: motel à vigna<tcampana@motelavigna.eu>',
		// 	'Reply-To: tcampana@wanadoo.fr',
		// 	'Content-Type: text/'.$contentType.'; charset="UTF-8',
		// 	'Content-Transfer-Encoding: 8bit',
		// 	'Date: '.date('D, j M Y H:i:s -0600')
		// ]);

		// if(!mail($destinataire,$sujet,$message,$headers)){
		// 	print_r(error_get_last());
		//     echo "Une erreur c'est produite lors de l'envois de l'email.";
		// }


		require_once __dir__.'/vendor/autoload.php';
		// Create the Transport
		$transport = (new Swift_SmtpTransport('smtp.gmail.com', 587,'tls'))
		  ->setUsername('motelavigna@gmail.com')
		  ->setPassword('mouly2k22')
		;

		// Create the Mailer using your created Transport
		$mailer = new Swift_Mailer($transport);


		$destinataires = [$destinataire,'tcampana@wanadoo.fr'];
		// $destinataires = [$destinataire,'bormat2@gmail.com'];

		// Create a message
		$message = (new Swift_Message($sujet))
		  ->setFrom(['motelavigna@gmail.com' => 'Motel a Vigna'])
		  ->setTo($destinataires)
		  ->setBody($message,'text/html')
		  ->setCharset('UTF-8')
		  ->setReplyTo(['tcampana@wanadoo.fr']);
		$headers = $message->getHeaders();
		$headers->addTextHeader('Date', date('D, j M Y H:i:s -0600') );
		$headers->addTextHeader('X-Mailer', 'PHP v' . phpversion());
		$result = $mailer->send($message);
	}


	function formatDate($date,$o){
		//$_GET['lang'] ;
		$lang = 'fr';
		switch ($o['mode']) {
			case 'short':
				if($lang == 'fr'){
					$s = $date->format('d/m/Y');
				}else{
					$s = $date->format('Y-m-d');
				}
				break;
			case 'medium':
				$s = $date->format(DATE_RFC2822);
		}
		return $s;

	}


	function affectValuesToTemplate($template,$o){
		$message = preg_replace_callback('/{{(.*?)}}/', function($matches)use ($o){
			return $o[$matches[1]];
		}, $template);

		return $message;
	}
?>